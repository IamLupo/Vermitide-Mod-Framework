/*
	Detail:
		This is a dll that injects itself automaticly in Warhammer End Time - Vermitide.
		It will Proxy the dinput8.dll and inject "vermintideconsole.dll" in the game.
	
	Compile:
		g++ main.cpp -shared -static -O2 -o dinput8.dll
*/

#include <windows.h>

//Declaration
extern "C" {
	BOOL WINAPI DllMain(HMODULE hDLL, DWORD Reason, LPVOID Reserved);
	DWORD WINAPI MyThread(LPVOID v);
	
	HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID *ppv);
	HRESULT __stdcall DllRegisterServer();
	HRESULT __stdcall DllUnregisterServer();
	HRESULT __stdcall DllCanUnloadNow();
	HRESULT DirectInput8Create(HINSTANCE hinst, DWORD dwVersion, REFIID riidltf, LPVOID * ppvOut, LPUNKNOWN punkOuter);
}

//Function structures
typedef HRESULT (__stdcall *DLLGETCLASSOBJECT)(REFCLSID, REFIID, LPVOID*);
typedef HRESULT (__stdcall *DLLREGISTERSERVER)();
typedef HRESULT (__stdcall *DLLUNREGISTERSERVER)();
typedef HRESULT (__stdcall *DLLCANUNLOADNOW)();
typedef HRESULT (*DIRECTINPUT8CREATE)(HINSTANCE, DWORD, REFIID, LPVOID*, LPUNKNOWN);

//Global var
HMODULE g_hModule;

void DisplayLastError() { 
	void* lpBuffer;

    FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		::GetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) &lpBuffer,
		0,
		NULL);

    MessageBox( NULL, (LPCTSTR)lpBuffer, TEXT("LastRrror"), MB_OK );
    LocalFree( lpBuffer );
}

/*
	Generate path to the windows dinput8.dll
	
	After research noticed a friend has installed Windows not in C:\ but on D:\
*/
void GetDinput8Path(char* path, int length) {
	//Init
	path[0] = 0;
	
	//Get Windows Path
	GetWindowsDirectory(path, length);
	
	//Add dinput8.dll
	strcat(path, "\\SysWOW64\\dinput8.dll");
}

BOOL WINAPI DllMain(HMODULE hDLL, DWORD Reason, LPVOID Reserved) {
    switch(Reason) {
		case DLL_PROCESS_ATTACH:
			TCHAR path[MAX_PATH * 2];
			
			//Disables the DLL_THREAD_ATTACH and DLL_THREAD_DETACH notifications
			DisableThreadLibraryCalls(hDLL);
			
			//Debug
			//MessageBox(0, "DllMain", "State", MB_OK);
			
			GetDinput8Path(path, sizeof(path));
			
			//Debug
			//MessageBox(0, path, "State", MB_OK);
			
			//Load second dinput8.dll
			g_hModule = LoadLibrary(path);
			
			//Debug
			//if(g_hModule != NULL)
			//	MessageBox(0, "LoadLibrary worked", "State", MB_OK);
			
			if(g_hModule == NULL)
				DisplayLastError();
			
			//Create Thread
			CreateThread(NULL, NULL, &MyThread, NULL, NULL, NULL);
		break;
		case DLL_THREAD_ATTACH:
		case DLL_PROCESS_DETACH:
		case DLL_THREAD_DETACH:
			break;
    }
	
    return TRUE;
}

DWORD WINAPI MyThread(LPVOID v) {
	HMODULE mod;
	
	//Debug
	//MessageBox(0, "MyThread", "State", MB_OK);
	
	//Wait till requered dll is loaded
	while(!GetModuleHandle("lua51.dll") or !GetModuleHandle("steam_api.dll")) {
		Sleep(10);
	}
	
	//Debug
	//MessageBox(0, "lua51.dll and steam_api.dll loaded", "State", MB_OK);
	
	//Sleep 7 seconds
	Sleep(7000);
	
	//Load mod
	mod = LoadLibrary("vermintideconsole.dll");
	
	//Debug
	//if(mod != NULL)
	//	MessageBox(0, "vermintideconsole.dll loaded", "State", MB_OK);
	
	if(mod == NULL)
		DisplayLastError();
	
    return 0;
}

HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID *ppv) {
	DLLGETCLASSOBJECT func;
	
	//Debug
	//MessageBox(0, "DllGetClassObject", "State", MB_OK);
	
	func = (DLLGETCLASSOBJECT)GetProcAddress(g_hModule, "DllGetClassObject");
	
	if(func != NULL) {
		//Debug
		//MessageBox(0, "DllGetClassObject found", "State", MB_OK);
	
		return func(rclsid, riid, ppv);
	}
}

HRESULT __stdcall DllRegisterServer() {
	DLLREGISTERSERVER func;
	
	//Debug
	//MessageBox(0, "DllRegisterServer", "State", MB_OK);
	
	func = (DLLREGISTERSERVER)GetProcAddress(g_hModule, "DllRegisterServer");
	
	if(func != NULL) {
		//Debug
		//MessageBox(0, "DllRegisterServer found", "State", MB_OK);
	
		return func();
	}
}

HRESULT __stdcall DllUnregisterServer() {
	DLLUNREGISTERSERVER func;
	
	//Debug
	//MessageBox(0, "DllUnregisterServer", "State", MB_OK);
	
	func = (DLLUNREGISTERSERVER)GetProcAddress(g_hModule, "DllUnregisterServer");
	
	if(func != NULL) {
		//Debug
		//MessageBox(0, "DllUnregisterServer found", "State", MB_OK);
	
		return func();
	}
}

HRESULT __stdcall DllCanUnloadNow() {
	DLLCANUNLOADNOW func;
	
	//Debug
	//MessageBox(0, "DllCanUnloadNow", "State", MB_OK);
	
	func = (DLLCANUNLOADNOW)GetProcAddress(g_hModule, "DllCanUnloadNow");
	
	if(func != NULL) {
		//Debug
		//MessageBox(0, "DllCanUnloadNow found", "State", MB_OK);
	
		return func();
	}
}

HRESULT DirectInput8Create(HINSTANCE hinst, DWORD dwVersion, REFIID riidltf, LPVOID * ppvOut, LPUNKNOWN punkOuter) {
	DIRECTINPUT8CREATE func;
	
	//Debug
	//MessageBox(0, "DirectInput8Create", "State", MB_OK);
	
	func = (DIRECTINPUT8CREATE)GetProcAddress(g_hModule, "DirectInput8Create");
	
	if(func != NULL) {
		//Debug
		//MessageBox(0, "DirectInput8Create found", "State", MB_OK);
	
		return func(hinst, dwVersion, riidltf, ppvOut, punkOuter);
	}
}