EchoConsole = function(message)
	Managers.chat:add_local_system_message(1, message, true)
end

local status, err = pcall(function ()
	local f = io.open("mod_loader/mod_loader.lua", "r")
	local data = f:read("*all")
	local func = loadstring(data)
	func()
	f:close()
end)

if err ~= nil then
	EchoConsole("Error on file 'mod_loader/mod_loader.lua'")
	EchoConsole(err)
end