Installation:
	Place the files in the following directory:
	"Steam\steamapps\common\Warhammer End Times Vermintide\binaries"

Installing additional mods:
	Place .mod files in the following directory:
	"Steam\steamapps\common\Warhammer End Times Vermintide\binaries\mods"
	
Start:
	When the game starts and you reach the inn,
	a message will appear in chat: "Vermitide Mod Framework Started"

Documentation:
	Type "/help" in the chat or open "doc/index.html" in your web browser
	
