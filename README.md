# Vermintide Mod Framework  
Vermintide Mod Framework provides an unofficial modding framework for Warhammer: End Times - Vermintide. It contains:  

1. Lua code injector that allows for custom code to be executed at runtime  
2. A set of tools to create, install and run mods  
3. A basic API that lets mods interact with the game loop  
4. A bunch of mods  

[ModDB page](http://www.moddb.com/mods/vermintide-mod-framework)

## Building mods  

We're using node.js, npm and gulp to zip up mods into .mod files and create copy of the whole mod for distribution.  

Node.js must be installed  
Run npm to install dependencies:

	npm install gulp-cli -g
	npm i

To build all or specified mods from mod_loader/mods directory to mods directory:   
	
	gulp build [-f <mod1>.<mod2>...]  

To automatically rebuild all or specified mod files when you edit them:  

	gulp autobuild [-f <mod1>.<mod2>...]  

To pack all or specified mods' .mod files into zip files with readme.txt for uploading to ModDB:  

	gulp moddb [-f <mod1>.<mod2>...]  

To create a copy of the mod in dist folder for distribution 

	gulp dist

## Developing mods  

To create a new mod, follow these steps:  

1. Copy ModTemplate folder from *mod_loader/mods_examples* to *mod_loader/mods*  
2. Rename the copied folder to match your mod's name  
3. Add .lua scripts to your mod or edit the examples. Suggested placement is as follows:    
	* Scripts executed when the mod is loaded -> *patch* folder   
	* Scripts executed via chat commands -> *chat* folder  
	* Scripts executed by other scripts -> *action* folder  
	* Scripts used by other scripts -> *module* folder  
4. Edit config.json with data relevant to your mod. ModTemplate has examples.   
	* *mod_name* will be the name of your mod (it's better to have it be the same as the folder name)  
	* *version* is the current version of the mod that will also be in the name of the .mod file
	* *author* is the author of the mod, this will be listed in the readme.txt in the final .mod file  
	* Lua scripts (.lua) listed under *patch* will be executed when your mod is loaded  
	* Players will be able to execute commands listed under *chat* via the chat window  
5. Edit *readme.txt* with general information about your mod  
6. Edit *index.html* with information about your mod that will be displayed when the user executes the */help* command (not implemented yet)  
6. If this is the first version of your mod, remove *change.log* file, otherwise list changes in it, they will show up in readme.txt once you build the mod  
7. Build your mod using gulp commands  

To edit a mod:  

1. Make changes to an already existing mod in *mod_loader/mods* folder  
2. Use gulp commands to rebuild the mod  
3. Use the */reload* chat command in game to reload the mod loader  

Take note that Mod Settings will not update until you re-open them.