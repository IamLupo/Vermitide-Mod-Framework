Mods = {
	
	saved_reqs = {}, 

	init = function()
		Mods.add_plugins()
		
		-- Load settings
		Mods.exec("", "Settings")
		
		-- Load Functions
		Mods.exec("function", "Global")
		Mods.exec("function", "Table")
		Mods.exec("function", "Mods.chat")
		Mods.exec("function", "Mods.hook")
		Mods.exec("function", "Mods.debug")
		Mods.exec("function", "Mods.gui")
		Mods.exec("function", "Mods.ui")
		Mods.exec("function", "Mods.network")
		Mods.exec("function", "Mods.keyboard")
		Mods.exec("function", "Mods.ffi")
		Mods.exec("function", "Mods.manager")
		Mods.exec("function", "Mods.option_menu")
		Mods.exec("function", "Mods.mutators")
		
		-- Needed moddifications to let the Mods run
		Mods.exec("patch/core", "Keyboard")
		Mods.exec("patch/core", "Chat")
		Mods.exec("patch/core", "OptionsInjector")

		--Option menu groups
		Mods.option_menu:add_group("mod_settings", "Mod Settings")
		Mods.option_menu:add_group("system", "System")
		Mods.option_menu:add_group("system_chat", "System Chat")
		Mods.option_menu:add_group("mutators", "Mutators")
		Mods.option_menu:add_group("cheats", "Cheats")
		Mods.option_menu:add_group("items", "Items")
		Mods.option_menu:add_group("spawning", "Spawning")
		Mods.option_menu:add_group("movement", "Movement")
		Mods.option_menu:add_group("hud", "HUD")

		--System chat
		Mods.exec("patch/core", "SystemChat")

		--Add keyblock to options menu
		Mods.keyboard.init_settings()
		
		-- Fixes
		Mods.exec("patch/fix", "Backend")
		Mods.exec("patch/fix", "Fire Grenade")
		Mods.exec("patch/fix", "13_02_2017_0001")
		Mods.exec("patch/fix", "Fireflame")
		Mods.exec("patch/fix", "Breed")
		Mods.exec("patch/fix", "Books")
		Mods.exec("patch/fix", "Animation")
		Mods.exec("patch/fix", "Grey_Seer")
		
		-- Extra Patches and functionality
		Mods.exec("patch", "ModSettings")
		Mods.exec("patch", "Fatshark")
		
		-- Execute mods
		Mods.manager:run()

		-- Populate mutators menu
		Mods.mutators:run()
		
		-- Draw options menu
		Mods.option_menu:draw()
		
		--Unstable patches
		--Mods.exec("patch/unstable", "SwitchCharacter")
		--Mods.exec("patch/unstable", "Menu")
		
		EchoConsole("Vermitide Mod Framework Started")
	end,
	
	reload = function()
		if Mods.mutators then Mods.mutators:disable() end
		Mods.exec("", "mod_loader")
	end,

	--[[
		Assignes a namespace for a new mod and returns it alongside mod name and OptionsInjector
	]]--
	new_mod = function(mod_name)
		local oi = OptionsInjector
		if Mods[mod_name] then
			EchoConsole([[Mods["]].. mod_name ..[["] already exists]])
			return nil
		end
		Mods[mod_name] = {
			get = Application.user_setting,
			set = Application.set_user_setting,
			save = Application.save_user_settings
		}
		return Mods[mod_name], mod_name, oi
	end,

	--[[
		Returns mod's table alongside mod name and OptionsInjector
	]]--
	get_mod = function(mod_name)
		local oi = OptionsInjector
		return Mods[mod_name], mod_name, oi
	end,
	
	--[[
		Execute a lua file/mod file
	]]--
	exec = function(group, file_name, args)
		-- file path
		local file_path = "mod_loader/" .. file_name .. ".lua"
		if group and group ~= "" then
			file_path = "mod_loader/" .. group .. "/" .. file_name .. ".lua"
		end
		
		-- Open file
		local file = io.open(file_path, "r")

		-- if file path not exist, check if mod file exist
		if not file and Mods.manager then
			local config = Mods.manager:get(group)
			if config then
				file_path = "mods/" .. config.mod_file .. ".mod"

				-- Open mod file
				local zfile, zfile_err = zip.open(file_path)
				if zfile then
					file_path = file_name .. ".lua"
					file = zfile:open(file_path)
				end
			end
		end

		-- If file exist then execute file
		if file then
			local status, value = Mods.exec_file(file, args)
			if not status then
				EchoConsole("Error on file '" .. file_path .. "'")
				EchoConsole(value)
			end
			
			-- Close file
			file:close()
			
			return status, value
		end

		EchoConsole("File not found '" .. file_path .. "'")
		
		return false
	end,

	--[[
		Loads lua file and caches return value to avoid loading the same thing multiple times
	]]--
	require = function(group, file_name)
		if Mods.saved_reqs[group] and Mods.saved_reqs[group][file_name] then
			return Mods.saved_reqs[group][file_name]
		end
		if not Mods.saved_reqs[group] then
			Mods.saved_reqs[group] = {}
		end
		local status, value = Mods.exec(group, file_name)
		Mods.saved_reqs[group][file_name] = status and value
		return status and value
	end,
	

	--[[
		Execute a lua file
	]]--
	exec_file = function(file, args)
		return pcall(function()
			-- Read lua code
			local data = file:read("*all")
			
			-- Convert string to a function
			local func = loadstring(data)
			
			-- Execute function
			local value = func(unpack(args or {}))
			
			return value
		end)
	end,
	
	--[[
		Add Mods/plugins directory to package
	]]--
	add_plugins = function()
		-- ZIP
		local f = assert(package.loadlib([[.\mod_loader\plugins\zip.dll]], "luaopen_zip"))
		f()  -- actually open the library
		require("zip")
		
		-- LFS
		local f = assert(package.loadlib([[.\mod_loader\plugins\lfs.dll]], "luaopen_lfs"))
		f()  -- actually open the library
		require("lfs")
		
		-- JSON
		Mods.exec("plugins", "json")
		require("json")
	end,
}

-- Initialize Mods
Mods.init()