--[[
	Json encode/decode
		
	Reference:
		http://json.luaforge.net/
		http://luaforge.net/projects/json/
]]--

safe_pcall(function()
	local object = {1,2,'fred',{first='mars',second='venus',third = "earth"} }
	local json_str = json.encode(object)
	
	EchoConsole(json_str)
	mdod(json.decode(json_str), "json", 4)
end)