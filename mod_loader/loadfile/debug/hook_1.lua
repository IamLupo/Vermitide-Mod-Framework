local mod_name = "example"

-- Example Function
test = function()
	EchoConsole("Hello World! :D")
	
	return
end

-- Hook a function
Mods.hook.set(mod_name, "test", function(func)
	EchoConsole("Begin Hook 1")
	
	func()
	
	EchoConsole("End Hook 1")
	
	return
end)

-- Disable hooking
--Mods.hook.enable(false, mod_name, "test")

-- Execute
test()