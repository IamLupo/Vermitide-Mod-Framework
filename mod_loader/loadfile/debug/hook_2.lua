-- Example function
hello = {}
hello.world = function()
	EchoConsole("Hello World! :D")
end

-- Create hook structure
Mods.hook.set("example_1", "hello.world", function(func, ...)
	EchoConsole("Begin Hook 1!")
	
	func(...)
	
	EchoConsole("End Hook 1!")
end)

Mods.hook.set("example_1", "hello.world", function(func, ...)
	EchoConsole("Begin Patched Hook 1!")
	
	func(...)
	
	EchoConsole("End Patched Hook 1!")
end)

Mods.hook.set("example_2", "hello.world", function(func, ...)
	EchoConsole("Begin Hook 2!")
	
	func(...)
	
	EchoConsole("End Hook 2!")
end)

Mods.hook.set("example_3", "hello.world", function(func, ...)
	EchoConsole("Begin Hook 3!")
	
	func(...)
	
	EchoConsole("End Hook 3!")
end)

-- Enable/Disable options
--Mods.hook.enable(false, "example_1")
--Mods.hook.enable(false, "example_1", "hello.world")
--Mods.hook.enable(false, "example_2")
--Mods.hook.enable(false, "example_2", "hello.world")

-- Remove hook
--Mods.hook.remove("hello.world")
--Mods.hook.remove("hello.world", "example_1")

-- Set hooked Function to front to execute at first
--Mods.hook.front("example_2", "hello.world")

--Execute
hello.world()

-- Debug results
mdod(MODS_HOOKS, "MODS_HOOKS", 7)
