--[[
	Type in the chat: loadfile debug/para
	
	In this script you can figure out parameters of function calls
--]]

-- Create some example functions --
-- Only the function name is known for the user --
foo = function(a, b, c, d)
	EchoConsole("yaay")
	return
end

test_obj = {}
test_obj.foo = function(a, b)
	return a + b
end

-- Hook calls --
Mods.debug.parameter("foo")
Mods.debug.parameter("test_obj.foo")

-- Call functions --
-- The user dont know when this happens --
foo(1, "Hello World", test_obj, true)
EchoConsole("value = " .. tostring(test_obj.foo(5, 6)))

-- Hook must be removed now
EchoConsole("value = " .. tostring(test_obj.foo(6, 8)))