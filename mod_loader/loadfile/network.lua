--[[
	Type in the chat: /loadfile network
	
	Network Example:
		Unfinished module to communicate custome rpc calls.
		
		Client to server: Works
		Server to client: Works
--]]

-- Register rpc call
Mods.network.register("rpc_hello_world", function(sender, message, message2)
	pcall(function()
		EchoConsole("Message: " .. message)
		EchoConsole("Message2: " .. message2)
	end)
end)

-- call rpc call
pcall(function()
	if Managers.player.is_server then
		Mods.network.send_rpc_clients("rpc_hello_world", "Trolololo :D", "Test 2")
	else
		Mods.network.send_rpc_server("rpc_hello_world", "Trolololo :D", "Test 2")
	end
end)