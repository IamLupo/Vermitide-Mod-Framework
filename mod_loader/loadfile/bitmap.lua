--[[
	Type in the chat: /loadfile bitmap
	
	This shows how to draw bitmap on the screen
	
	Reference:
		UIAtlasHelper.get_atlas_settings_by_texture_name
		UIRenderer.script_draw_bitmap
--]]

local mod_name = "Bitmap"

Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, ...)
	Gui.bitmap_uv(
		Mods.gui.gui,					-- Gui
		"gui_hud_atlas",				-- Material
		Vector2(0.222656, 0.584961),	-- uv00
		Vector2(0.25293, 0.615234),		-- uv11
		Vector3(300, 300, 200),			-- Position
		Vector2(100, 100),				-- Size
		0)								-- Color			
end)

-- Disable hook
--Mods.hook.enable(false, mod_name, "MatchmakingManager.update")