--[[
	Type in the chat: /loadfile dir
	
	Reference:
		https://keplerproject.github.io/luafilesystem/examples.html
		http://luaforge.net/projects/luafilesystem/
--]]

-- Example 1: Read out directory
for file in lfs.dir("./mods") do
	if string.find(file, "%.mod$") then
		EchoConsole(file)
	end
end

--Example 2: Create directory with C calls
-- local dir_name = [[.\derp]]
-- if Mods.C.CreateDirectory(dir_name) then
-- 	EchoConsole("Created directory: " .. dir_name)
-- end