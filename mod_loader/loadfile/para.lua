-- Example data
local example_1 = {
	name = "\"Test",
	list = {
		number = 1,
		["1"] = "derp",
	},
	rly = true,
}

-- Execute
local gen_str = table.serialization(example_1)
local get_lst = table.deserialization(gen_str)

-- Debug
EchoConsole(gen_str)
mdod(get_lst, "get_lst", 5)