--[[
	Read zip files:
		This allows me in lua envirement to read zip file
	
	Reference:
		http://math2.org/luasearch/zip.html
		http://luaforge.net/projects/luazip/
]]--

safe_pcall(function()
	local zfile_name = 'mod_loader/loadfile/zip/test.zip'
	
	-- Open zip file
	local zfile, zfile_err = zip.open(zfile_name)
	if zfile then
		-- Debug
		EchoConsole("Opened file '" .. zfile_name .. "'")
		
		-- Find files in zip file
		for found_file in zfile:files() do
			-- Debug
			EchoConsole("Found file: " .. found_file.filename)
			
			-- Open found file
			local file, file_err = zfile:open(found_file.filename)
			if file then
				local data = file:read("*a")
				EchoConsole("data: " .. data)

				file:close()
			else
				EchoConsole(file_err)
			end
		end
		zfile:close()
	else
		EchoConsole(zfile_err)
	end
end)
