local mod_name = "VideoPlayer"

local video_player_remove = function(obj)
	if obj then
		if obj.videoplayer then
			-- Remove video player
			World.destroy_video_player(obj.world, obj.videoplayer)
		end
		if obj.gui then
			-- Remove Gui
			World.destroy_gui(obj.world, obj.gui)
		end
	end
end

video_player_remove(CustomVideoPlayer)
CustomVideoPlayer = {}

local local_player_unit = Managers.player:player_from_peer_id(Network.peer_id()).player_unit
local pos = Unit.local_position(local_player_unit, 0)
local rot = Unit.local_rotation(local_player_unit, 0)

CustomVideoPlayer.position = {
	x = pos.x,
	y = pos.y,
	z = pos.z
}
CustomVideoPlayer.size = {
	x = 1920,
	y = 1080,
}

CustomVideoPlayer.material = "video/trailer"
CustomVideoPlayer.material_name = "trailer"

--CustomVideoPlayer.world = Application.main_world()
CustomVideoPlayer.world = Managers.state.conflict._world

CustomVideoPlayer.gui = World.create_world_gui(
	CustomVideoPlayer.world,
	Matrix4x4.identity(), 1, 1,
	"immediate",
	"material", "materials/ui/ui_1080p_title_screen",
	"material", "materials/ui/ui_1080p_start_screen",
	"material", "materials/fonts/gw_fonts",
	"material", "materials/ui/ui_1080p_ingame_common",
	"material", CustomVideoPlayer.material
)

--[[
CustomVideoPlayer.gui = World.create_screen_gui(
	CustomVideoPlayer.world,
	"immediate",
	"material", "materials/ui/ui_1080p_title_screen",
	"material", "materials/ui/ui_1080p_start_screen",
	"material", "materials/fonts/gw_fonts",
	"material", "materials/ui/ui_1080p_ingame_common",
	"material", CustomVideoPlayer.material
)
]]--

CustomVideoPlayer.videoplayer = World.create_video_player(CustomVideoPlayer.world, CustomVideoPlayer.material, true)

VideoPlayer.set_loop(CustomVideoPlayer.videoplayer, false)

Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, ...)
	func(...)
	
	safe_pcall(function()
		if CustomVideoPlayer.videoplayer then
			local x = VideoPlayer.current_frame(CustomVideoPlayer.videoplayer)
			local y = VideoPlayer.number_of_frames(CustomVideoPlayer.videoplayer)
			
			-- Debug
			--EchoConsole("number_of_frames: " .. tostring(x) .. "/" .. tostring(y))
			
			if x < y then
				local local_player_unit = Managers.player:player_from_peer_id(Network.peer_id()).player_unit
				local pos = Unit.local_position(local_player_unit, 0)
				local rot = Unit.local_rotation(local_player_unit, 0)
				
				local camera_rotation = Managers.state.camera:camera_rotation("player_1")
				local tm = Matrix4x4.from_quaternion_position(camera_rotation, pos)
				
				--[[
				EchoConsole(tostring(Matrix4x4.identity()))
				Gui.video_3d(
					CustomVideoPlayer.gui,
					CustomVideoPlayer.material_name,
					CustomVideoPlayer.videoplayer,
					tm,
					Vector3(CustomVideoPlayer.position.x, CustomVideoPlayer.position.z, CustomVideoPlayer.position.y),
					1,
					Vector2(1920, 1080),
					Color(150, 255, 255, 255)
				)
				]]--
				
				Gui.video(
					CustomVideoPlayer.gui,
					CustomVideoPlayer.material_name,
					CustomVideoPlayer.videoplayer,
					--Vector3(CustomVideoPlayer.position.x, CustomVideoPlayer.position.y, CustomVideoPlayer.position.z),
					Vector3(0, 0, 0),
					Vector2(10, 10),
					Colors.get_table("white")
				)
				
			else
				video_player_remove(CustomVideoPlayer)
				CustomVideoPlayer = {}
			end
		else
			-- Remove hook
			Mods.hook.remove("MatchmakingManager.update", mod_name)
		end
	end)
end)
--Mods.hook.enable(false, mod_name, "MatchmakingManager.update")