Mods.C.CreateDirectory( get_directory() .. "output")
local file1 = io.open("output\\item_descriptions.txt", "w")
local file2 = io.open("output\\dialogues.txt", "w")
local file3 = io.open("output\\map_briefs.txt", "w")
local descs = {}

for k,v in pairs(ItemMasterList) do
	local str = ""
	if v.description and not table.has_item(descs, v.description) then
		local description = Localize(v.description)
		if string.len(description) > 0 then
			table.insert(descs, v.description)
			str = str .. "##" .. Localize(v.display_name) .. "\n" .. description .. "\n"
			file1:write(str)
		end
	end
end
local player = Managers.player:local_player(1)
-- for i,v in pairs(player.network_manager.entity_system.entity_manager._systems.hud_system.ingame_ui.ingame_hud.subtitle_gui.localizers) do
-- 	file2:write(i .. " " .. tostring(v) .. "\n")
-- end
local localizers = player.network_manager.entity_system.entity_manager._systems.hud_system.ingame_ui.ingame_hud.subtitle_gui.localizers
local dialogue_system = player.network_manager.entity_system.entity_manager._systems.dialogue_system
local dialogues = dialogue_system.dialogues

-- for k,v in pairs(NetworkLookup.sound_events) do
-- 	file2:write(k .. " " .. tostring(v) .. "\n")
-- end

local lines = {}

for local_name,localizer in pairs(localizers) do
	lines[local_name] = {}
	for _,dialogue in pairs(dialogues) do
		if local_name == dialogue.database and #dialogue.localization_strings then

			-- for k,v in pairs(dialogue) do
			-- 	if k == "sound_events" then
			-- 		for kk,vv in pairs(v) do
			-- 			local looked_up = NetworkLookup.sound_events[vv]
			-- 			if looked_up then
			-- 				file2:write(kk .. " " .. tostring(looked_up) .. "\n")
			-- 			end
			-- 		end
			-- 	end
			-- end

			table.insert(lines[local_name], {dialogue.localization_strings[1], {}})
			for k,string in pairs(dialogue.localization_strings) do
				if string then
					local text = Localizer.lookup(localizer, string)
					-- local speaker_name = dialogue.speaker_name and Localize("subtitle_name_" .. dialogue.speaker_name) or string
					-- if speaker_name then
					-- 	file2:write(speaker_name .. " ")
					-- end
					if text then
						table.insert(lines[local_name][#lines[local_name]][2], Localize(text))
					end
				end
			end
		end
	end
end

function compare(a, b)
	return a[1] < b[1]
end

for k,v in pairs(lines) do
	table.sort(v, compare)
end

for k,v in pairs(lines) do
	file2:write(k .. "\n")
	for kk,vv in pairs(v) do
		file2:write(vv[1] .. "\n")
		for kkk,vvv in pairs(vv[2]) do
			file2:write(vvv .. "\n")
		end
		file2:write("\n")
	end
	file2:write("----------\n\n\n")
end

for name,settings in pairs(LevelSettings) do
	file3:write(name .. "\n")
	if settings.map_screen_wwise_events and #settings.map_screen_wwise_events then
		file3:write("	Map screen\n")
		for k,v in pairs(settings.map_screen_wwise_events) do
			file3:write("		" .. Localize(v) .. "\n")
		end
	end
	if settings.map_settings and settings.map_settings.wwise_events and #settings.map_settings.wwise_events then
		file3:write("	Briefing\n")
		for k,v in pairs(settings.map_settings.wwise_events) do
			file3:write("		" .. Localize(v) .. "\n")
		end
	end
	if settings.loading_screen_wwise_events and #settings.loading_screen_wwise_events then
		file3:write("	Loading screen\n")
		for k,v in pairs(settings.loading_screen_wwise_events) do
			file3:write("		" .. Localize(v) .. "\n")
		end
	end
	file3:write("\n")
end
EchoConsole("Success")

file1:close()
file2:close()
file3:close()