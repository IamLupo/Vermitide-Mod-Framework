ModsKeyMap = {
	win32 = {
		["left ctrl"]	= {"keyboard", "left ctrl", "held"},
		["left shift"]	= {"keyboard", "left shift", "held"},
		["left alt"]	= {"keyboard", "left alt", "held"},
	},
}
ModsKeyMap.xb1 = ModsKeyMap.win32

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

Mods.keyboard = {
	update = {},

	SETTINGS = {
		["save"] = "cb_mod_settings_keyblock",
		["widget_type"] = "stepper",
		["text"] = "Keyboard Shortcuts",
		["tooltip"] = "Toggle all keyboard shortcuts",
		["value_type"] = "boolean",
		["options"] = {
			{text = "Off", value = false},
			{text = "On", value = true},
		},
		["default"] = 2,
	},
	
	init = function()
		Managers.input.create_input_service(Managers.input, "Mods", "ModsKeyMap")
		Managers.input.map_device_to_service(Managers.input, "Mods", "keyboard")
		Managers.input.map_device_to_service(Managers.input, "Mods", "mouse")
		Managers.input.map_device_to_service(Managers.input, "Mods", "gamepad")
	end,
	
	pressed = function(keys)
		local blocks = { "left ctrl", "left shift", "left alt" }
		local input_service = Managers.input:get_service("Mods")
		local shortcuts_enabled = get(Mods.keyboard.SETTINGS.save)

		if input_service and shortcuts_enabled then
			-- Check first if all keys are pressed
			for _, key in ipairs(keys) do
				if input_service.get(input_service, key) == false then --or not table.has_item(ModsKeyMap.win32, key) then
					return false
				end
			end
			
			-- Check Blocks
			for _, block_key in ipairs(blocks) do
				if table.has_item(keys, block_key) == false and input_service.get(input_service, block_key) then
					return false
				end
			end
			
			return true
		else
			return false
		end
	end,
	
	get = function(folder, script)
		for _, command in pairs(Mods.keyboard.update) do
			if command[2] == folder and command[3] == script then
				return command
			end
		end
		return nil
	end,
	
	add = function(keys, folder, script)
		local action = Mods.keyboard.get(folder, script)
		
		if action then
			action[1] = keys
			action[2] = folder
			action[3] = script
		else
			local command = {table.clone(keys), folder, script}
			Mods.keyboard.update[#Mods.keyboard.update+1] = command
		end
		return true
	end,
	init_settings = function()
		Mods.option_menu:add_item("mod_settings", Mods.keyboard.SETTINGS, true)
	end
}

-- Sync settings
for _, command in pairs(Mods.settings.actions) do
	if type(command[1]) == "table" then
		Mods.keyboard.update[#Mods.keyboard.update+1] = command
	end
end

-- Start
Mods.keyboard.init()