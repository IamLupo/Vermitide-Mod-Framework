Mods.chat = {
	animation = nil,
	system = false,
	update = {},
	
	send = function(message)
		if Managers.chat:has_channel(1) then
			Mods.chat.system = true
			Managers.chat:send_chat_message(1, message)
			Mods.chat.system = false
		end
	end,
	
	on_message = function(message)
		-- Check message is command
		if string.sub(message, 1, 1) ~= "/" then
			return true
		end
		
		-- Check action settings
		for _, item in ipairs(Mods.chat.update) do
			if 
				"/" .. item[1] == string.sub(message, 1, item[1]:len() + 1) and

				--Check space after command
				not(
					item[1]:len() + 1 < message:len() and
					"/" .. item[1] .. " " ~= string.sub(message, 1, item[1]:len() + 2)
				)
			then
				local arg = {}
				
				-- Build arguments
				table.insert(arg, item[2]) -- File directory
				table.insert(arg, item[3]) -- File name
				
				-- Extract parameter
				if item[1]:len() + 1 < message:len() then
					local para = string.sub(message, item[1]:len()+3, message:len())
					
					table.insert(arg, {para}) -- Parameters
				end
				
				-- Execute
				local status, value = Mods.exec(unpack(arg))
				
				-- Check results
				return false
			end
		end
		
		EchoConsole("Unknown command " .. message)

		return false
	end,
	
	get = function(name)
		for _, command in pairs(Mods.chat.update) do
			if command[1] == name then
				return command
			end
		end
		return nil
	end,
	
	add = function(name, folder, script)
		local command = Mods.chat.get(name)
		
		if command then
			command = {name, folder, script}
		else
			local command = {name, folder, script}
			Mods.chat.update[#Mods.chat.update+1] = command
		end
		return true
	end,
}

-- Sync settings
for _, command in pairs(Mods.settings.actions) do
	if type(command[1]) == "string" then
		Mods.chat.update[#Mods.chat.update+1] = command
	end
end