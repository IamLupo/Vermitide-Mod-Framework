--[[
	Mutators
		- Adds options to Mutators group in Mod options
		- Mutators are added via the add_mutator function
		- Each mutator belongs to a predetermined group from group_infos
		- Only one mutator of each group can be active at a time
		- When a mutator is selected, disable function is executed for the current 
		  mutator of its group and enable function is executed for selected mutator
		- See CustomGameModeTemplate for example of a mutator

		- save_, restore_ and replace_data methods can be used to manipulate game variables
		- Values must be passed as strings, strings must be passed this way: "\"string\""
		- See Deathwish and Stormvermintide for examples of use

		- :set_servername and :restore_servername can be used to alter servername

	author: unshame
--]]

Mods.mutators = {
	SETTINGS = {

	},
	group_infos = {
		GAME_MODE = {
			save = "game_mode",
			text = "Game mode",
			tooltip = "Select game mode mutator"
		},
		TEST_GROUP = {
			save = "test_group",
			text = "Test group",
			tooltip = "Select test group mutator"
		},
	},
	empty_setting = {
		["save"] = "cb_mutators_empty_setting",
		["widget_type"] = "checkbox",
		["text"] = "No mutators loaded",
		["default"] = false
	},
	groups = {},
	current_mutators = {}
}

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings


-- Adds new mutators to the manager
Mods.mutators.add_mutator = function(self, group_name, mutator)

	--Check if this is the first mutator in a group
	self:_init_group(group_name)

	--Can't add mutators to groups that haven't been predetermined
	if not self.SETTINGS[group_name] then
		EchoConsole("[ERROR] Couldn't add " .. tostring(mutator.name) .. " to " .. tostring(group_name) .. ", as it doesn't exist")
	end

	local options = self.SETTINGS[group_name]["options"]
	local option = {text = mutator.name, value = #options + 1}
	table.insert(self.groups[group_name], mutator)
	table.insert(options, option)

	mutator.enabled = false
	--EchoConsole("[" .. group_name .. "] " .. mutator.name .. " added")
end

-- Adds hook to detect option change and creates options
Mods.mutators.run = function(self)
	self:_create_options()
	local last_update = 0
	Mods.hook.set("Mods.mutators", "MatchmakingManager.update", function(func, s, dt, t)
		func(s, dt, t)
		if t - last_update > 1 then
			last_update = t
			self:_update_current_mutators()
		end
	end)

end

-- Disables all mutators
Mods.mutators.disable = function(self)
	for group_name, group in pairs(self.groups) do
		if self.current_mutators[group_name] and self.current_mutators[group_name].disable then
			self.current_mutators[group_name]:disable()
		end
	end
end


--Server name
Mods.mutators.set_servername = function(self, name)
	local lobby_data = Managers.matchmaking.lobby:get_stored_lobby_data()
	local old_server_name = LobbyAux.get_unique_server_name()
	local server_name = name .. " " .. old_server_name
	lobby_data.unique_server_name = server_name
	Managers.matchmaking.lobby:set_lobby_data(lobby_data)
end

Mods.mutators.restore_servername = function(self)
	local lobby_data = Managers.matchmaking.lobby:get_stored_lobby_data()
	local server_name = LobbyAux.get_unique_server_name()
	lobby_data.unique_server_name = server_name
	Managers.matchmaking.lobby:set_lobby_data(lobby_data)
end


-- Detects changes in options
Mods.mutators._update_current_mutators = function(self)
	for group_name, group in pairs(self.groups) do

		local mutator_index = get(self.SETTINGS[group_name].save) - 1
		local mutator = group[mutator_index]
		local current_mutator = self.current_mutators[group_name]

		--Mutators can only be switched in the inn
		local is_in_inn = Managers.state and Managers.state.game_mode and Managers.state.game_mode._game_mode_key == "inn"

		if is_in_inn and (mutator or mutator_index == 0) and mutator ~= current_mutator then

			if current_mutator then 
				current_mutator.enabled = false
				if current_mutator.disable then current_mutator:disable() end
				Managers.chat:send_system_chat_message(1, "[" .. group_name .. "] " .. tostring(current_mutator.name) .. " deactivated", "", true)
			end

			self.current_mutators[group_name] = mutator

			if self.current_mutators[group_name] then
				EchoConsole("[" .. group_name .. "] " .. tostring(self.current_mutators[group_name].name))
			else
				EchoConsole("[" .. group_name .. "] DISABLED")
			end
		end

		--Enable/disable mutator based on conditions
		if is_in_inn and mutator then

			--Check mutator conditions
			local can_enable
			if mutator and mutator.check_conditions then 
				can_enable = mutator.check_conditions()
			else
				can_enable = true
			end

			if can_enable and not mutator.enabled then
				mutator.enabled = true
				if mutator.enable then mutator:enable() end
				Managers.chat:send_system_chat_message(1, "[" .. group_name .. "] " .. tostring(self.current_mutators[group_name].name) .. " activated", "", true)
			elseif not can_enable and mutator.enabled then
				mutator.enabled = false
				if mutator.disable then mutator:disable() end
				Managers.chat:send_system_chat_message(1, "[" .. group_name .. "] " .. tostring(self.current_mutators[group_name].name) .. " deactivated", "", true)
			end
		end
	end
end

-- Initialises a mutator group
Mods.mutators._init_group = function(self, group_name)
	local info = self.group_infos[group_name]
	if not info or self.SETTINGS[group_name] then
		return
	end
	self.SETTINGS[group_name] = {
		["save"] = "cb_mutators_" .. info.save,
		["widget_type"] = "dropdown",
		["text"] = info.text,
		["tooltip"] = info.tooltip,
		["value_type"] = "number",
		["options"] = {
			{text = "None", value = 1}
		},
		["default"] = 1,
	}
	set(self.SETTINGS[group_name].save, 1)
	save()
	self.groups[group_name] = {}
	self.current_mutators[group_name] = nil
end

-- Options
Mods.mutators._create_options = function(self)
	--Mods.option_menu:add_group("mutators", "Mutators")
	local counter = 0
	for _, settings in pairs(self.SETTINGS) do
		Mods.option_menu:add_item("mutators", settings, true)
		counter = counter + 1
	end
	if counter == 0 then
		Mods.option_menu:add_item("mutators", self.empty_setting, true)
	end
	
end

--Functions used to save, replace and restore game values

--Global variable used for loadstring
temp_storage = {}

Mods.mutators.save_data = function(mutator)
	--local count = 0
	mutator.saved = {}
	for k,_ in pairs(mutator.data) do
		local value = assert(loadstring("return " .. k))()
		mutator.saved[k] = value
		--count = count + 1
	end
	--EchoConsole("Saved " .. count .. " values")
end

Mods.mutators.replace_data = function(mutator)
	--local count = 0
	for k,v in pairs(mutator.data) do
		assert(loadstring(k .. " = " .. tostring(v) ))()
		--count = count + 1
	end
	--EchoConsole("Replaced " .. count .. " values")
end

Mods.mutators.restore_data = function(mutator)
	temp_storage = mutator.saved
	--local count = 0
	for k,_ in pairs(mutator.saved) do
		assert(loadstring(k .. " = temp_storage[\"" .. k .. "\"]" ))()
		--count = count + 1
	end
	temp_storage = {}
	--EchoConsole("Restored  " .. count .. " values")
end