
local mod_name = "Mods.ui"

Mods.ui = {
	
	theme = "hellshark",
	windows = {},
	
	-- ##### Create ###################################################################################################
	create_window = function(name, position, size, controls)
		-- Create window
		local window = table.clone(Mods.ui.controls.window)
		window.name = name or "name"
		window.position = position or {0, 0}
		window.size = size or {0, 0}
		window.controls = controls or {}
		Mods.ui.inc_z_orders(#Mods.ui.windows)
		window.z_order = 1
		
		-- Add window to list
		Mods.ui.windows[#Mods.ui.windows+1] = window

		mdod(Mods.ui.windows, "Mods.ui.windows", 10)
		
		return window
	end,
	
	inc_z_orders = function(changed_z)
		for z=changed_z, 1, -1 do
			for _, window in pairs(Mods.ui.windows) do
				if window.z_order == z then
					window.z_order = window.z_order + 1
				end
			end
		end
	end,
	
	dec_z_orders = function(changed_z)
		for z=changed_z, #Mods.ui.windows do
			for _, window in pairs(Mods.ui.windows) do
				if window.z_order == z then
					window.z_order = window.z_order - 1
				end
			end
		end
	end,
	
	-- ##### Update ###################################################################################################
	update = function()
		
		-- Click
		if stingray.Mouse.pressed(stingray.Mouse.button_id("left")) then
			Mods.ui.mouse.click(Mods.ui.mouse.cursor())
		elseif stingray.Mouse.released(stingray.Mouse.button_id("left")) then
			Mods.ui.mouse.release(Mods.ui.mouse.cursor())
		end
		
		-- Hover
		Mods.ui.mouse.hover(Mods.ui.mouse.cursor())
		
		-- Update windows
		if #Mods.ui.windows > 0 then
			for z=#Mods.ui.windows, 1, -1 do
				for _, window in pairs(Mods.ui.windows) do
					if window.visible then
						if window.z_order == z then
							window:update()
							window:render()
						end
					end
				end
			end
		end
		
	end,
	
	mouse = {
		click = function(position)
			for z=1, #Mods.ui.windows do
				for _, window in pairs(Mods.ui.windows) do
					if window.z_order == z then
						if Mods.ui.point_in_bounds(position, window:bounds()) then
							window:on_click(position)
							return
						end
					end
				end
			end
		end,
		release = function(position)
			for z=1, #Mods.ui.windows do
				for _, window in pairs(Mods.ui.windows) do
					if window.z_order == z then
						if Mods.ui.point_in_bounds(position, window:bounds()) then
							window:on_release(position)
							return
						end
					end
				end
			end
		end,
		hover = function(position)
			for z=1, #Mods.ui.windows do
				for _, window in pairs(Mods.ui.windows) do
					if window.z_order == z then
						local hovered, cursor = Mods.ui.point_in_bounds(position, window:bounds())
						if hovered then
							window:on_hover(cursor)
							return
						end
					end
				end
			end
			Mods.ui.mouse.un_hover_all()
		end,
		un_hover_all = function()
			for _, window in pairs(Mods.ui.windows) do
				if window.hovered then
					window:on_hover_exit()
				end
			end
		end,
		cursor = function()
			local cursor_axis_id = stingray.Mouse.axis_id("cursor")	-- retrieve the axis ID
			local value = stingray.Mouse.axis(cursor_axis_id)		-- use the ID to access to value
			return {value[1], value[2]}
		end,
	},
	
	to_bounds = function(position, size)
		return {position[1], position[1] + size[1], position[2], position[2] + size[2]}
	end,
	
	point_in_bounds = function(position, bounds)
		if position[1] >= bounds[1] and position[1] <= bounds[2] and position[2] >= bounds[3] and position[2] <= bounds[4] then
			return true, {position[1] - bounds[1], position[2] - bounds[3]}
		end
		return false, {0, 0}
	end,
	
	color = {
		box = function(a, r, g, b)
			return {a, r, g, b}
		end,
		unbox = function(box)
			return Color(box[1], box[2], box[3], box[4])
		end,
	},
	
}

Mods.ui.anchor = {

	styles = {
		--none = "bottom_left",
		bottom_left = "bottom_left",
		--left = "left",
		top_left = "top_left",
		--top = "top",
		top_right = "top_right",
		--right = "right",
		bottom_right = "bottom_right",
		--bottom = "bottom",
		--fill = "fill",
	},

	bottom_left = {
		position = function(window, control)
			return {window.position[1] + control.offset[1], window.position[2] + control.offset[2]}
		end,
	},
	top_left = {
		position = function(window, control)
			return {window.position[1] + control.offset[1], window.position[2] + window.size[2] - control.offset[2]}
		end,
	},
	top_right = {
		position = function(window, control)
			return {window.position[1] + window.size[1] - control.offset[1] - control.size[1], window.position[2] + window.size[2] - control.offset[2] - control.size[2]}
		end,
	},
	bottom_right = {
		position = function(window, control)
			return {window.position[1] + window.size[1] - control.offset[1], window.position[2] + control.offset[2]}
		end,
	},
	
}
local Anchors = Mods.ui.anchor

Mods.ui.themes = {
	
	-- Default
	metro = {
		window = {
			color = Mods.ui.color.box(255, 200, 200, 200),
			color_hover = Mods.ui.color.box(255, 255, 255, 255),
			shadow = {
				layers = 10,
				border = 5,
				color = {20, 10, 10, 10},
			},
		},
		title = {
			height = 30,
			color = Mods.ui.color.box(255, 40, 40, 40),
			color_hover = Mods.ui.color.box(255, 0, 0, 0),
			color_text = Mods.ui.color.box(255, 255, 255, 255),
			color_text_hover = Mods.ui.color.box(255, 200, 200, 200),
		},
		button = {
			color = Mods.ui.color.box(255, 150, 150, 150),
			color_hover = Mods.ui.color.box(255, 80, 80, 80),
			color_clicked = Mods.ui.color.box(255, 40, 40, 40),
			color_text = Mods.ui.color.box(255, 100, 100, 100),
			color_text_hover = Mods.ui.color.box(255, 200, 200, 200),
			color_text_clicked = Mods.ui.color.box(255, 255, 255, 255),
			shadow = {
				layers = 10,
				border = 0,
				color = {20, 10, 10, 10},
			},
		},
		resizer = {
			size = {40, 40},
			color = Mods.ui.color.box(255, 150, 150, 150),
			color_hover = Mods.ui.color.box(255, 80, 80, 80),
		},
		close_button = {
			size = {25, 25},
		},
	},
	
	hellshark = {
		window = {
			color = Mods.ui.color.box(200, 150, 150, 150), --
			color_hover = Mods.ui.color.box(255, 75, 75, 75), --
			shadow = {
				layers = 0,
				border = 0,
				color = {0, 255, 255, 255},
			},
		},
		title = {
			height = 20,
			color = Mods.ui.color.box(255, 40, 20, 20),
			color_hover = Mods.ui.color.box(255, 0, 0, 0),
			color_text = Mods.ui.color.box(255, 255, 127, 127),
			color_text_hover = Mods.ui.color.box(255, 200, 100, 100),
		},
		button = {
			color = Mods.ui.color.box(0, 65, 65, 65),
			color_hover = Mods.ui.color.box(200, 40, 30, 30),
			color_clicked = Mods.ui.color.box(200, 7, 5, 5),
			color_text = Mods.ui.color.box(100, 255, 168, 0),
			color_text_hover = Mods.ui.color.box(200, 255, 168, 0),
			color_text_clicked = Mods.ui.color.box(255, 255, 168, 0),
			shadow = {
				layers = 5,
				border = 0,
				color = {20, 10, 10, 10},
			},
		},
		resizer = {
			size = {20, 20},
			color = Mods.ui.color.box(255, 150, 75, 75),
			color_hover = Mods.ui.color.box(255, 80, 40, 40),
		},
		close_button = {
			size = {15, 15},
		},
	},
	
}

-- Mods.ui.color = {
	
	-- name = "",
	-- A = 0,
	-- R = 0,
	-- G = 0,
	-- B = 0,
	-- new = function()
		-- local color = 
	-- end,
	
-- }

Mods.ui.control = {

	name = "",
	position = {0, 0},
	size = {0, 0},
	_type = "",
	anchor = "",
	hovered = false,
	cursor = {0, 0},
	--colors = {},
	z_order = 0,
	
	init = function()
	end,
	update = function()
	end,
	render = function()
	end,
	on_click = function()
	end,
	
	bounds = function(self)
		return Mods.ui.to_bounds(self.position, self.size)
	end,
}

Mods.ui.controls = {
	
	window = {
		name = "",
		position = {0, 0},
		size = {0, 0},
		initialized = false,
		hovered = false,
		cursor = {0, 0},
		dragging = false,
		drag_offset = {0, 0},
		resizing = false,
		resize_offset = {0, 0},
		resize_origin = {0, 0},
		z_order = 0,
		controls = {},
		visible = true,
		color = nil,
		color_hover = nil,
		shadow = {
			layers = nil,
			border = nil,
			color = nil,
		},
		
		-- ##### Create controls ##########################################################################################
		--[[
			Create title bar
		--]]
		create_title = function(self, name, text, height)
			local control = self:create_control(name, nil, nil, "title")
			control.text = text
			control.height = height
			self.controls[#self.controls+1] = control
		end,
		--[[
			Create button
		--]]
		create_button = function(self, name, position, size, text, on_click, anchor, param)
			local control = self:create_control(name, position, size, "button", anchor)
			control.text = text
			control.click = on_click
			control.param = param
			self.controls[#self.controls+1] = control
		end,
		--[[
			Create resizer
		--]]
		create_resizer = function(self, name, size)
			local control = self:create_control(name, nil, size, "resizer")
			self.controls[#self.controls+1] = control
		end,
		--[[
			Create control
		--]]
		create_close_button = function(self, name)
			local control = self:create_control(name, {5, 0}, {25, 25}, "close_button", Anchors.styles.top_right)
			--local control = self:create_button(name, {30, 25}, {25, 25}, "X", on_click, Anchors.styles.top_right)
			control.text = "X"
			control.click = function(self)
				self.window:destroy()
			end
			self.controls[#self.controls+1] = control
		end,
		--[[
			Create control
		--]]
		create_control = function(self, name, position, size, _type, anchor)
		
			-- Create control
			local control = table.clone(Mods.ui.control)
			control.name = name or "name"
			control.position = position or {0, 0}
			control.offset = control.position
			control.size = size or {0, 0}
			control._type = _type or "button"
			control.window = self
			self:inc_z_orders(#self.controls)
			control.z_order = 1
			
			-- Set control functions
			control.init = Mods.ui.controls[_type].init
			control.update = Mods.ui.controls[_type].update
			control.render = Mods.ui.controls[_type].render
			control.render_shadow = Mods.ui.controls[_type].render_shadow
			control.render_text = Mods.ui.controls[_type].render_text
			control.on_click = Mods.ui.controls[_type].on_click
			control.on_release = Mods.ui.controls[_type].on_release
			control.position_z = Mods.ui.controls[_type].position_z
			
			-- Anchor
			control.anchor = anchor or Anchors.styles.bottom_left --Mods.ui.anchor[anchor] or Mods.ui.anchor.bottom_left
			-- control.anchor.window = self

			return control
		end,
		
		-- ##### Methods ##################################################################################################
		--[[
			Initialize window
		--]]
		init = function(self)
			-- Event
			self:on_init()
			
			-- Init controls
			if #self.controls > 0 then
				for _, control in pairs(self.controls) do
					control:init()
				end
			end
			
			self.initialized = true
		end,
		--[[
			Bring window to front
		--]]
		bring_to_front = function(self)
			if not self:has_focus() then
				Mods.ui.inc_z_orders(self.z_order)
				self.z_order = 1
			end
		end,
		--[[
			Destroy window
		--]]
		destroy = function(self)
			Mods.ui.dec_z_orders(self.z_order)
			table.remove(Mods.ui.windows, self:window_index())
		end,
		--[[
			Increase z orders
		--]]
		inc_z_orders = function(self, changed_z)
			--for z=changed_z, 1, -1 do
				for _, control in pairs(self.controls) do
					--if control.z_order == z then
						control.z_order = control.z_order + 1
					--end
				end
			--end
		end,
		--[[
			Decrease z orders
		--]]
		dec_z_orders = function(self, changed_z)
			--for z=changed_z, #self.controls do
				for _, control in pairs(self.controls) do
					--if control.z_order == z then
						control.z_order = control.z_order - 1
					--end
				end
			--end
		end,
		
		-- ################################################################################################################
		-- ##### Events ###################################################################################################
		-- ################################################################################################################
		--[[
			Window gets focus
		--]]
		on_init = function(self)
		end,
		--[[
			Window gets focus
		--]]
		on_focus = function(self)
			self:bring_to_front()
		end,
		--[[
			Window is hovered
		--]]
		on_hover = function(self, cursor)
			if not self.hovered then self:on_hover_enter() end
			self.cursor = cursor
		end,
		--[[
			Window starts being hovered
		--]]
		on_hover_enter = function(self)
			Mods.ui.mouse.un_hover_all()
			self.hovered = true
		end,
		--[[
			Window ends being hovered
		--]]
		on_hover_exit = function(self)
			self.hovered = false
		end,
		--[[
			Window is clicked
		--]]
		on_click = function(self, position)
			self:on_focus()
			for z=1, #self.controls do
				for _, control in pairs(self.controls) do
					if control.z_order == z then
						if Mods.ui.point_in_bounds(position, control:bounds()) then
							control:on_click()
							return
						end
					end
				end
			end
		end,
		
		on_release = function(self, position)
			self:on_focus()
			for z=1, #self.controls do
				for _, control in pairs(self.controls) do
					if control.z_order == z then
						if Mods.ui.point_in_bounds(position, control:bounds()) then
							control:on_release()
							return
						end
					end
				end
			end
		end,
		
		-- ################################################################################################################
		-- ##### Attributes ###############################################################################################
		-- ################################################################################################################
		--[[
			Check if window has focus
		--]]
		has_focus = function(self)
			return self.z_order == 1
		end,
		--[[
			Get window index
		--]]
		window_index = function(self)
			for i=1, #Mods.ui.windows do
				if Mods.ui.windows[i] == self then return i end
			end
			return 0
		end,
		--[[
			Window bounds
		--]]
		bounds = function(self)
			return Mods.ui.to_bounds(self.position, self.size)
		end,
		
		position_z = function(self)
			return #Mods.ui.windows - self.z_order
		end,
		
		-- ################################################################################################################
		-- ##### Cycle ####################################################################################################
		-- ################################################################################################################
		--[[
			Update window
		--]]
		update = function(self)
			if self.initialized then
				local got_focus = false
				
				-- Get cursor position
				local cursor = Mods.ui.mouse.cursor()
				
				if not self:has_focus() then
					return
				end
				
				-- Drag
				self:drag(cursor)
				
				-- Resize
				self:resize(cursor)

				-- Update controls
				self:update_controls()
				
			end
		end,
		--[[
			Resize window
		--]]
		drag = function(self, cursor)
			if self.dragging then
				self.position = {cursor[1] - self.drag_offset[1], cursor[2] - self.drag_offset[2]}
			end
		end,
		--[[
			Resize window
		--]]
		resize = function(self, cursor)
			if self.resizing then
				local new_size = {
					cursor[1] - self.resize_origin[1] + self.resize_offset[1], 
					self.resize_origin[2] - cursor[2] + self.resize_offset[2],
				}
				if new_size[1] < 100 then new_size[1] = 100 end
				if new_size[2] < 100 then new_size[2] = 100 end
				self.size = new_size
				self.position = {self.position[1], self.resize_origin[2] - new_size[2]}
			end
		end,
		--[[
			Update controls
		--]]
		update_controls = function(self)
			if #self.controls > 0 then
				for z=#self.controls, 1, -1 do
					for _, control in pairs(self.controls) do
						if control.z_order == z then
							control:update()
						end
					end
				end
			end
		end,
		--[[
			Render window
		--]]
		render = function(self)
			-- Render Shadow
			self:render_shadow()
			
			-- Render Window
			local position_z = #Mods.ui.windows - self.z_order
			local color = nil
			if self.hovered then
				if self.color_hover then
					color = Color(self.color_hover[1], self.color_hover[2], self.color_hover[3], self.color_hover[4])
				else
					color = Mods.ui.color.unbox(Mods.ui.themes[Mods.ui.theme].window.color_hover)
				end
			else
				if self.color then
					color = Color(self.color[1], self.color[2], self.color[3], self.color[4])
				else
					color = Mods.ui.color.unbox(Mods.ui.themes[Mods.ui.theme].window.color)
				end
			end
			Mods.gui.rect(self.position[1], self.position[2], position_z, self.size[1], self.size[2], color)
			
			-- Render controls
			self:render_controls()
		end,
		--[[
			Render shadow
		--]]
		render_shadow = function(self)
			-- Theme
			local layers = self.shadow.layers or Mods.ui.themes[Mods.ui.theme].window.shadow.layers
			local border = self.shadow.border or  Mods.ui.themes[Mods.ui.theme].window.shadow.border
			local cv = self.shadow.color or Mods.ui.themes[Mods.ui.theme].window.shadow.color
			-- Render
			for i=1, layers do
				local color = Color((cv[1]/layers)*i, cv[2], cv[3], cv[4])
				local mod = layers-i
				Mods.gui.rect(self.position[1]+mod-border, self.position[2]-mod-border, self:position_z(),
					self.size[1]-mod*2+border*2, self.size[2]+mod*2+border*2, color)
			end
			for i=1, layers do
				local color = Color((cv[1]/layers)*i, cv[2], cv[3], cv[4])
				local mod = layers-i
				Mods.gui.rect(self.position[1]-mod-border, self.position[2]+mod-border, self:position_z(),
					self.size[1]+mod*2+border*2, self.size[2]-mod*2+border*2, color)
			end
		end,
		--[[
			Render controls
		--]]
		render_controls = function(self)
			if #self.controls > 0 then
				for z=#self.controls, 1, -1 do
					for _, control in pairs(self.controls) do
						if control.z_order == z then
							control:render()
						end
					end
				end
			end
		end,
	
	},
	
	title = {
		init = function(self)
			self:update()
		end,
		update = function(self)
			self.size = {self.window.size[1], self.height or Mods.ui.themes[Mods.ui.theme].title.height}
			self.position = {self.window.position[1], self.window.position[2] + self.window.size[2] - self.size[2]}
			-- Hover
			local cursor = Mods.ui.mouse.cursor()
			local bounds = Mods.ui.to_bounds({self.position[1], self.position[2]}, self.size)
			self.hovered, self.cursor = Mods.ui.point_in_bounds(cursor, bounds)
			-- if self.hovered then
				-- if not self.window.dragging then
					-- --self.window.dragging = stingray.Mouse.pressed(stingray.Mouse.button_id("left"))
					-- self.window.drag_offset = self.window.cursor
				-- end
			-- end
			-- Drag
			if self.window.dragging then
				self.window.dragging = not stingray.Mouse.released(stingray.Mouse.button_id("left"))
			end
			-- Render
			--self:render()
		end,
		render = function(self)
			--local position_z = #Mods.ui.windows - self.window.z_order
			
			-- Render background
			local color = nil
			if self.hovered then
				color = Mods.ui.color.unbox(Mods.ui.themes[Mods.ui.theme].title.color_hover)
			else
				color = Mods.ui.color.unbox(Mods.ui.themes[Mods.ui.theme].title.color)
			end
			Mods.gui.rect(self.position[1], self.position[2], self:position_z(), self.size[1], self.size[2], color)
			
			-- Render text
			if self.hovered then
				color = Mods.ui.color.unbox(Mods.ui.themes[Mods.ui.theme].title.color_text_hover)
			else
				color = Mods.ui.color.unbox(Mods.ui.themes[Mods.ui.theme].title.color_text)
			end
			Mods.gui.text(self.text, self.position[1] + self.size[2]*0.2, self.position[2] + self.size[2]*0.2, self:position_z(), self.size[2], color)
		end,
		render_shadow = nil,
		render_text = nil,
		position_z = function(self)
			return #Mods.ui.windows - self.window.z_order
		end,
		on_click = function(self)
			self.window.dragging = true
			self.window.drag_offset = self.window.cursor
		end,
		on_release = function(self)
		end,
	},
	
	button = {
		init = function(self)
			self:update()
		end,
		update = function(self)
			-- Mouse position
			local cursor = Mods.ui.mouse.cursor()
			-- Set control position via anchor
			self.position = Anchors[self.anchor].position(self.window, self)
			-- Disabled
			if self.disabled then return end
			-- Check hovered
			self.hovered, self.cursor = Mods.ui.point_in_bounds(cursor, self:bounds())
			-- Clicked
			if self.clicked then
				self.clicked = self.hovered
			end
		end,
		render = function(self)
			-- Render shadow
			self:render_shadow()
			-- Render background
			local color = nil
			if self.clicked then
				color = Mods.ui.color.unbox(Mods.ui.themes[Mods.ui.theme].button.color_clicked)
			elseif self.hovered then
				color = Mods.ui.color.unbox(Mods.ui.themes[Mods.ui.theme].button.color_hover)
			else
				color = Mods.ui.color.unbox(Mods.ui.themes[Mods.ui.theme].button.color)
			end
			Mods.gui.rect(self.position[1], self.position[2], self:position_z(), self.size[1], self.size[2], color)
			-- Render text
			self:render_text()
		end,
		render_shadow = function(self)
			-- Theme
			local layers = Mods.ui.themes[Mods.ui.theme].button.shadow.layers
			local border = Mods.ui.themes[Mods.ui.theme].button.shadow.border
			local cv = Mods.ui.themes[Mods.ui.theme].button.shadow.color
			-- Render
			for i=1, layers do
				local mod = layers-i
				local color = Color((cv[1]/layers)*i, cv[2], cv[3], cv[4])
				Mods.gui.rect(self.position[1]+mod-border, self.position[2]-mod-border, self:position_z(),
					self.size[1]-mod*2+border*2, self.size[2]+mod*2+border*2, color)
			end
			for i=1, layers do
				local mod = layers-i
				local color = Color((cv[1]/layers)*i, cv[2], cv[3], cv[4])
				Mods.gui.rect(self.position[1]-mod-border, self.position[2]+mod-border, self:position_z(),
					self.size[1]+mod*2+border*2, self.size[2]-mod*2+border*2, color)
			end
		end,
		render_text = function(self)
			local color = nil
			if self.clicked then
				color = Mods.ui.color.unbox(Mods.ui.themes[Mods.ui.theme].button.color_text_clicked)
			elseif self.hovered then
				color = Mods.ui.color.unbox(Mods.ui.themes[Mods.ui.theme].button.color_text_hover)
			else
				color = Mods.ui.color.unbox(Mods.ui.themes[Mods.ui.theme].button.color_text)
			end
			Mods.gui.text(self.text, self.position[1] + self.size[2]*0.2, self.position[2] + self.size[2]*0.2, self:position_z(), self.size[2], color)
		end,
		position_z = function(self)
			return #Mods.ui.windows - self.window.z_order
		end,
		on_click = function(self)
			-- Disabled
			if self.disabled then return end
			self.clicked = true
		end,
		on_release = function(self)
			-- Disabled
			if self.disabled then return end
			if self.clicked then
				self.clicked = false
				if self.click then
					self:click(self, self.param)
				end
			end
		end,
	},
	
	resizer = {
		init = function(self)
			if self.size[1] == 0 and self.size[2] == 0 then
				self.size = Mods.ui.themes[Mods.ui.theme].resizer.size
			end
			self:update()
		end,
		update = function(self)
			self.size = Mods.ui.themes[Mods.ui.theme].resizer.size
			self.position = {self.window.position[1] + self.window.size[1] - self.size[1] - 5, self.window.position[2] + 5}
			-- Hover
			local cursor = Mods.ui.mouse.cursor()
			local bounds = Mods.ui.to_bounds({self.position[1], self.position[2]}, self.size)
			self.hovered, self.cursor = Mods.ui.point_in_bounds(cursor, bounds)
			-- if self.hovered then
				-- if not self.window.resizing then
					-- --self.window.resizing = stingray.Mouse.pressed(stingray.Mouse.button_id("left"))
					-- self.window.resize_offset = {self.window.size[1] - self.window.cursor[1], self.window.cursor[2]}
					-- self.window.resize_origin = {self.window.position[1], self.window.position[2] + self.window.size[2]}
				-- end
			-- end
			-- Drag
			if self.window.resizing then
				self.window.resizing = not stingray.Mouse.released(stingray.Mouse.button_id("left"))
			end
			-- Render
			--self:render()
		end,
		render = function(self)
			local color = nil
			if self.hovered then
				color = Mods.ui.color.unbox(Mods.ui.themes[Mods.ui.theme].resizer.color_hover)
			else
				color = Mods.ui.color.unbox(Mods.ui.themes[Mods.ui.theme].resizer.color)
			end
			Mods.gui.rect(self.position[1], self.position[2], self:position_z(), self.size[1], self.size[2], color)
		end,
		render_shadow = nil,
		render_text = nil,
		position_z = function(self)
			return #Mods.ui.windows - self.window.z_order
		end,
		on_click = function(self)
			self.window.resizing = true
			self.window.resize_offset = {self.window.size[1] - self.window.cursor[1], self.window.cursor[2]}
			self.window.resize_origin = {self.window.position[1], self.window.position[2] + self.window.size[2]}
		end,
		on_release = function(self)
		end,
	},
	
	close_button = {
		init = function(self)
			self.render = Mods.ui.controls.button.render
			self.render_shadow = Mods.ui.controls.button.render_shadow
			self.render_text = Mods.ui.controls.button.render_text
			self.position_z = Mods.ui.controls.button.position_z
			self.on_click = Mods.ui.controls.button.on_click
			self.on_release = Mods.ui.controls.button.on_release
			Mods.ui.controls.button.init(self)
		end,
		update = function(self)
			self.size = Mods.ui.themes[Mods.ui.theme].close_button.size
			Mods.ui.controls.button.update(self)
		end,
		render = nil,
		render_shadow = nil,
		render_text = nil,
		position_z = nil,
		on_click = nil,
		on_release = nil,
	},

}

Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, ...)
	func(...)
	if Mods.ui then
		Mods.ui.update()
	end
end)

local close_window = function(self)
	self.window:destroy()
end

local change_theme = function(self)
	if Mods.ui.theme == "metro" then
		Mods.ui.theme = "test"
	else
		Mods.ui.theme = "metro"
	end
	for _, window in pairs(Mods.ui.windows) do
		window:update()
		for _, control in pairs(window.controls) do
			control:update()
		end
	end
end

-- Test
for i=0, 1, -1 do
	local window = Mods.ui.create_window("test" .. tostring(i), {100*i, 100*i}, {300, 300})
	window:create_title("test_title" .. tostring(i), "Test " .. tostring(i))
	--window:create_button("test_button" .. tostring(i), {30, 25}, {25, 25}, "X", close_window, Anchors.styles.top_right)
	window:create_close_button("test_close" .. tostring(i))
	if i == 1 then window:create_button("test_button" .. tostring(i), {10, 10}, {85, 25}, "Theme", change_theme, Anchors.styles.bottom_left) end
	window:create_resizer("test_resizer" .. tostring(i))
	window:init()
end

-- i=2
-- local window2 = Mods.ui.create_window("test" .. tostring(i), {100*i, 100*i}, {300, 300})
-- window2:create_title("test_title" .. tostring(i), "Test 2")
-- window2:create_button("test_button" .. tostring(i), {30, 25}, {25, 25}, "X", close_window, Anchors.styles.top_right)
-- window2:create_resizer("test_resizer" .. tostring(i))
-- window2:init()

-- i=3
-- local window3 = Mods.ui.create_window("test" .. tostring(i), {100*i, 100*i}, {300, 300})
-- window3:create_title("test_title" .. tostring(i), "Test 2")
-- window3:create_button("test_button" .. tostring(i), {30, 25}, {25, 25}, "X", close_window, Anchors.styles.top_right)
-- window3:create_resizer("test_resizer" .. tostring(i))
-- window3:init()