Mods.manager = {
	_mods = {},
	
	init = function(self)
		-- Find all mod files and load them
		for _, mod_file in ipairs(self._get_mod_files()) do	
			local config = self._get_config_data(mod_file)
			
			if config and config.mod_name and config.version then
				-- Future feature is version check to overwrite old version
				config.mod_file = string.sub(mod_file, 1, -5)
				config.activated = false
				
				self._mods[config.mod_name] = config
			end
		end
		
		-- Debug
		mdod(self, "Mods.manager", 5)
	end,
	
	get = function(self, mod_name)
		if not self._mods[mod_name] then
			if mod_name and string.len(mod_name) > 0 then EchoConsole("[ERROR] No mod '" .. mod_name .. "' found") end
			return nil
		end
		return table.clone(self._mods[mod_name])
	end,
	
	run = function(self)
		local patched = true
		
		-- Keep activating mods till everything is activated or is missing requirements
		while patched do
			patched = false
			
			if self._mods then
				for mod_name, config in pairs(self._mods) do
					if config.activated == false and self:check_requirements(config) then
						--EchoConsole("[LOADED] " .. config.mod_name)
						
						-- Run function
						self:check_function(config)
						
						-- Run patches
						self:check_patches(config)
						
						-- Run chat
						self:check_chat(config)
						
						config.activated = true
						patched = true
					end
				end
			end
		end
		
		-- Detect missing requirements
		for mod_name, config in pairs(self._mods) do
			if config.activated == false then
				for _, requirement_name in ipairs(config.requirement) do
					EchoConsole("[ERROR] '" .. mod_name .. "' is missing requirement '" .. requirement_name .. "'")
					EchoConsole("Please install the '" .. requirement_name .. "'")
				end
			end
		end
		
		-- Debug
		mdod(self._mods, "mods", 6)
	end,
	
	check_requirements = function(self, config)
		if config.requirement then
			for _, mod_name in ipairs(config.requirement) do
				if self._mods[mod_name] == nil or self._mods[mod_name].activated == false then 
					return false
				end
			end
		end
		
		return true
	end,
	
	check_function = function(self, config)
		if config["function"] then
			for _, function_name in ipairs(config["function"]) do
				Mods.exec(config.mod_name, function_name)
			end
		end
	end,
	
	check_patches = function(self, config)
		if config.patch then
			for _, patch_name in ipairs(config.patch) do
				Mods.exec(config.mod_name, patch_name)
			end
		end
	end,
	
	check_chat = function(self, config)
		if config.chat then
			for _, setting in ipairs(config.chat) do
				-- Activate chat command
				Mods.chat.update[#Mods.chat.update+1] = {setting.command, config.mod_name, setting.execute}
				
				--Add autocorrection
				SystemChat.sense.register("/" .. setting.command, setting.description)
			end	
		end
	end,
	
	-- Scan mods directory for mods
	_get_mod_files = function()
		local list = {}
		
		for file in lfs.dir("./mods") do
			if string.find(file, "%.mod$") then
				table.insert(list, file)
			end
		end
		
		return list
	end,
	
	-- Read out config data
	_get_config_data = function(mod_file)
		-- Open zip file
		local zfile, zfile_err = zip.open("mods\\" .. mod_file)
		if zfile then
			-- Open config.json
			local config_file = zfile:open("config.json")
			
			if config_file then
				-- Read config.json
				local data = config_file:read("*all")
				
				-- Close config.json
				config_file:close()
				
				-- Convert json data to table
				local state, value = pcall(function()
					return json.decode(data)
				end)
				
				if state then
					zfile:close()
					
					return value
				else
					EchoConsole("Corrupted 'config.json' file in '" .. mod_file .. "'")
				end
			else
				EchoConsole("Missing 'config.json' file in '" .. mod_file .. "'")
			end
			zfile:close()
		else
			EchoConsole(zfile_err)
		end
		
		return nil
	end,
}

Mods.manager:init()