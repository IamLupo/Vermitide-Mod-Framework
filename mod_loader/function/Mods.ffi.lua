--[[
	Reference:
		https://raw.githubusercontent.com/moteus/lua-path/master/lua/path/win32/ffi/fs.lua
--]]
Mods.ffi = require("ffi")

Mods.ffi.cdef[[
	int MessageBoxA(void *w, const char *txt, const char *cap, int type);
	
	bool __stdcall CreateDirectoryA(const char *src, void* lpSecurityAttributes);
]]

local NULL = Mods.ffi.cast("void*", 0)

Mods.C = {
	MessageBox = function(text, title)
		Mods.ffi.C.MessageBoxA(nil, text, title, 0)
	end,
	
	CreateDirectory = function(src)
		local ret = Mods.ffi.C.CreateDirectoryA(src, NULL)
		
		return (ret ~= 0)
	end,
}