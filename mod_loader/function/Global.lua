EMPTY_FUNC = function(...) end

get_directory = function()
	local para = package.cpath
	local pattern = string.match(para, ';[^;]*')
	
	return string.sub(pattern, 2, #pattern - 5)
end

file_exists = function(name)
	local f = io.open(name,"r")
	
	if f ~= nil then
		io.close(f)
		return true
	else
		return false
	end
end

round = function(num, idp)
  return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end

safe_pcall = function(func, echo)
	local status, value = pcall(func)
	
	if status then
		return value
	else
		EchoConsole(tostring(value))
	end
end
