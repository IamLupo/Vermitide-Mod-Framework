local mutator_name = "CustomGameMode"
local mutator_group = "GAME_MODE"
local difficulty_name = "||Custom Difficulty||"

--Mutator's namespace
Mods.CustomGameMode = {}

--Shorthand
local gm = Mods.CustomGameMode

gm.name = mutator_name
gm.group = mutator_group
gm.difficulty_name = difficulty_name

--Values to be changed when mutator is activated
--You don't have to use this
gm.saved = {}
gm.data = {
	--As an example, we'll set Cataclysm clan rat hp to 999
	--You can specify any number of existing variables here
	["Breeds.skaven_clan_rat.max_health[5]"] = "999"
}

--What to do when mutator is activated
gm.enable = function(self)
	EchoConsole(self.name .. " enabled")
	Mods.hook.enable(true, mutator_name)
	Mods.mutators.replace_data(gm)
	Mods.mutators:set_servername(self.difficulty_name)
end

--What to do when mutator is deactivated
gm.disable = function(self)
	EchoConsole(self.name .. " disabled")
	Mods.hook.enable(false, mutator_name)
	Mods.mutators.restore_data(gm)
	Mods.mutators:restore_servername()
end

--Conditions that must be met before mutator can be activated
gm.check_conditions = function()
	return Managers.player.is_server
end

--[[ Hooks --]]

--Changing difficulty name
Mods.hook.set(mutator_name, "IngamePlayerListUI.set_difficulty_name", function (func, self, name)		
	local content = self.headers.content
	content.game_difficulty = name .. " " .. mutator_name
	return 
end)

--Setting servername
Mods.hook.set(mutator_name, "MatchmakingStateHostGame.host_game", function (func, self, ...)
	func(self, ...)

	Mods.mutators:set_servername(difficulty_name)
end)

--Don't forget to disable hooks by default
Mods.hook.enable(false, mutator_name)

--[[ Initialization --]]

--Remember the data before it's changed
Mods.mutators.save_data(gm)

--Adding mutator to Mod Settings -> Mutators -> mutator_group
Mods.mutators:add_mutator(mutator_group, gm)
