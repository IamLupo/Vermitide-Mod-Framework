--[[
	This will return a single function
	Example of what Mods.require can be used for
--]]

local hidden_value = 42

local function local_function()
	return hidden_value
end

local function export_function()
	return local_function()
end

return export_function