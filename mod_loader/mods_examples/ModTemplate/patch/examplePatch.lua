local mod_name = "ModTemplate" -- This will show up in chat if an error occurs while executing this file 

--[[
	ModTemplate
		- Adds a group with two settings to Mod Settings
		- Checks for setting change every second and remembers it locally
		- Use this as the starting point for your mod
--]]

--These functions are used to get, set and save mod settings
local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

--This is used for hotkeys
local oi = OptionsInjector

--Use this to load external scripts as "modules"
local externalModule = Mods.require(mod_name, "module/exampleModule")	

--This will be your mod's namespace
Mods.Modtemplate = {}

--Shorthand
local mod = Mods.Modtemplate

mod.settings = {
	STEPPER = {
		["save"] = "mod_example_stepper",	--key

		--How the setting will look in the menu
		--Can be stepper, slider, dropdown, checkbox or keybind 
		["widget_type"] = "stepper",	

		["text"] = "Stepper Example",	--title
		["tooltip"] = 					--mouseover description
			"Stepper Example\n" ..	
			"Description.",

		--Value type
		--Can be string, boolean or number
		["value_type"] = "boolean",

		["options"] = {						--possible options
			{text = "Off", value = false},
			{text = "On", value = true},
		},
		["default"] = 1,	--index of the default option
	},
	KEYBIND = {
		["save"] = "mod_example_keybind",
		["widget_type"] = "keybind",
		["text"] = "Keybind Example",
		["default"] = {
			"e",	--key code http://help.autodesk.com/view/Stingray/ENU/?guid=__lua_ref_ns_stingray_Keyboard_html
			oi.key_modifiers.NONE --key modifier
			--[[
			key_modifiers = {
				NONE = 1,
				CTRL = 2,
				SHIFT = 3,
				ALT = 4,
				CTRL_SHIFT = 5,
				CTRL_ALT = 6,
				SHIFT_ALT = 7,
			}
			--]]
		},

		--What file should be executed by this hotkey
		["exec"] = {mod_name, "action/exampleAction"}, 
	},
}

-- ####################################################################################################################
-- ##### Options ######################################################################################################
-- ####################################################################################################################
mod.create_options = function()

	Mods.option_menu:add_group("example_group", "Mod Template") --new group in Mod Settings

	--Adding mod settings items
	Mods.option_menu:add_item(
		"example_group",		--settings group
			--[[
				"mod_settings"
				"system",
				"system_chat",
				"mutators",
				"cheats",
				"items",
				"spawning",
				"movement",
				"hud",
			--]]
		mod.settings.STEPPER, --setting configuration we defined above
		true
	)
	Mods.option_menu:add_item("example_group", mod.settings.KEYBIND, true)
end

-- ####################################################################################################################
-- ##### Hooks ########################################################################################################
-- ####################################################################################################################

--Here we set required hooks
--This one will check for changed mod settings each second and remember them
local last_update = 0
local saved_setting = nil
Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, self, dt, t)

	func(self, dt, t) --execute original function

	--see if a second has passed
	if t - last_update > 1 then
		last_update = t
		local setting = get(mod.settings.STEPPER.save)
		if setting ~= saved_setting then
			saved_setting = setting
			--Here we can do what's necessary when settings are changed
			EchoConsole("New setting is: " .. tostring(setting))
		end
	end
end)

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
mod.create_options()