--[[
	Type in the chat: /clear
--]]

local args = {...}

if #args == 0 then
	SystemChat.clear_lines()

	return true
else
	return false
end