--[[
	Type in the chat: /itemlist
--]]

local args = {...}

if #args == 0 then
	local tFile = io.open("ItemList.txt", "w")
	local list = {}

	-- Collect all items
	for key, obj in pairs(ItemMasterList) do
		table.insert(list, tostring(key))
	end

	-- Sort the items 
	table.sort(list)

	-- Save results in file
	for _, item in ipairs(list) do
		tFile:write(item .. "\n")
	end

	tFile:close()

	EchoConsole("Succesfully created ItemList.txt in the binary folder.")

	return true
else
	return false
end