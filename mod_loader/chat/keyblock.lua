--[[
	Type in the chat: /keyblock
--]]

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

local option_string = Mods.keyboard.SETTINGS.save
local shortcuts_enabled = get(option_string)
set(option_string, not shortcuts_enabled)

if not shortcuts_enabled then
	EchoConsole("Keyboard shortcuts unsuspended")
else
	EchoConsole("Keyboard shortcuts suspended")
end

save()

return true