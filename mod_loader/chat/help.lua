--[[
	Type in the chat: /help
--]]

local args = {...}

if #args == 0 then
	Steam.open_url("file:///" .. get_directory() .. "doc/index.html")
	
	return true
else
	return false
end
