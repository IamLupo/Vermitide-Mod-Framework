--[[
	Save and restore hero equipment loadouts.
--]]
 
local args = {...}
local loadout_func_name = args[1]
local loadout_func = loadout_func_name and LoadoutSaver[loadout_func_name]
local loadout_number = args[2]
if not loadout_func or not loadout_number then
	EchoConsole("Error: invalid args for LoadoutSaver")
	return
end
 
local status, err = pcall(loadout_func, LoadoutSaver, loadout_number)
if err then
	EchoConsole(err)
end