Mods.settings = {
	version = "0.15.4",
	
	actions = {
		{"reload",		"action",			"reload"},
		{"help", 		"chat",				"help"},
		{"playmusic", 	"chat",				"playmusic"},
		{"clear",		"chat",				"clear"},
		{"keyblock",	"chat",				"keyblock"},
		{"itemlist",	"chat",				"itemlist"},
	},
}