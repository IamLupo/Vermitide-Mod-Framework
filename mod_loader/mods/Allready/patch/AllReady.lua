local mod_name = "AllReady"
--[[
	Patch all_peers_ready:
		This allows you to start the game without other people have to vote.
--]]

local oi = OptionsInjector

Mods.AllReady = {
	SETTINGS = {
		ACTIVE = {
			["save"] = "cb_all_ready",
			["widget_type"] = "stepper",
			["text"] = "All Ready",
			["tooltip"] =  "All Ready\n" ..
				"Toggle all ready on / off.\n\n" ..
				"Lets you start the game without waiting for other players to be ready.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
		},
	},
}
local me = Mods.AllReady

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Options ######################################################################################################
-- ####################################################################################################################
Mods.AllReady.create_options = function()
	Mods.option_menu:add_group("system", "System")
	
	Mods.option_menu:add_item("system", me.SETTINGS.ACTIVE, true)
end

-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "MatchmakingManager.all_peers_ready", function(func, ...)
	if get(me.SETTINGS.ACTIVE.save) then
		return true
	else
		return func(...)
	end
end)

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()