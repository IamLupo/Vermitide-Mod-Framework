This mod allows you to start the game without having to wait for other people to Ready up.
Toggle this mod on and off in Mod Settings -> System -> All Ready.
Off by default.