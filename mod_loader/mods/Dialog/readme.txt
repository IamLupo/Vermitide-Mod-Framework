Allows to play character dialogues via chat commands.
Chat commands:
	/dialoglist - generates a file with dialogue ids in binary folder
	/dialog [id] - plays dialogue with specified id