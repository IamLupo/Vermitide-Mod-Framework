local mod_name = "Dialog"

-- Bug fix: first time playing it looks for last_query that doesnt exist
Mods.hook.set(mod_name, "DialogueSystem.update_currently_playing_dialogues", function(func, ...)
	pcall(func, ...)
end)