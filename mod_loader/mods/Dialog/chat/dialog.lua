--[[
	Type in the chat: /dialog [ - | + | <nr> ]
--]]

-- Dialoge Story:		521 - 731
-- Dwarf Quest:			834 - 863
-- 						1049 - 1055
-- Elf Quest:			864 - 911
-- Soldier Quest:		912 - 961
-- Mage quest:			962 - 1001
-- 						1056 - 1060
-- Witchhunter quest:	1002 - 1045
-- 						1061 - 1064
-- Dwarf DLC:			2282 - 2317
-- Funny Dwarf:			2308

local args = {...}

if not DIALOG_ID then
	DIALOG_ID = 1
end

-- check parameter
if #args > 0 then
	local para = args[1]
	
	if para == "+" then
		DIALOG_ID = DIALOG_ID + 1
	elseif para == "-" then
		DIALOG_ID = DIALOG_ID - 1
	else
		pcall(function()
			local value = math.floor(tonumber(para))
			
			DIALOG_ID = value
		end)
	end
end

EchoConsole("DIALOG_ID: " .. tostring(DIALOG_ID))

-- Debug NetworkLookup.dialogues:
--mdod(NetworkLookup.dialogues, "NetworkLookup.dialogues", 5)

pcall(function()
	local player = Managers.player:local_player(1)
	local player_go_id = Managers.state.unit_storage:go_id(player.player_unit)
	
	local dialogue_name = NetworkLookup.dialogues[DIALOG_ID]
	
	-- Advanced info
	local dialogue_system = player.network_manager.entity_system.entity_manager._systems.dialogue_system
	local dialogue = dialogue_system.dialogues[dialogue_name]

	-- Debug dialogue:
	--mdod(dialogue, "dialogue", 5)
	
	if dialogue then
		-- Network call
		EchoConsole("play: " .. dialogue_name)
		Managers.state.network.network_transmit:send_rpc_all('rpc_play_dialogue_event', player_go_id, false, DIALOG_ID, 1)
	end
end)

return true