--[[
	Type in the chat: /dialoglist
--]]

local args = {...}

if #args == 0 then
	local tFile = io.open("DialogList.txt", "w")

	-- Collect all items
	for key, obj in pairs(NetworkLookup.dialogues) do
		if type(key) == "number" then
			tFile:write(tostring(key) .. " = " .. obj .. "\n")
		end
	end

	tFile:close()

	EchoConsole("Succesfully created DialogList.txt in the binary folder.")
	
	return true
else
	return false
end