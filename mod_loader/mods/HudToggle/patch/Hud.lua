local mod, mod_name, oi = Mods.new_mod("HudToggle")

--[[
	Settings
]]--
mod.setting_strings = {
	"vmf_hud_elements",
	"vmf_hud_objectives",
	"vmf_hud_outlines",
	"vmf_hud_crosshair",
	"vmf_hud_ping",
	"vmf_hud_feedback",
	"vmf_hud_weapon"
}

mod.settings_sorted  = {
	"ELEMENTS",

	"OBJECTIVES",

	"OUTLINES",

	"CROSSHAIR",

	"PING",
	"FEEDBACK",

	"WEAPON",
}

mod.widget_settings = {
	ELEMENTS = {
		["save"] = mod.setting_strings[1],
		["widget_type"] = "stepper",
		["text"] = "HUD Elements",
		["tooltip"] = "HUD Elements\n" ..
			"Whether to display HUD elements like equipment, health bars, stamina and overcharge.",
		["value_type"] = "boolean",
		["options"] = {
			{text = "Off", value = false},
			{text = "On", value = true},
		},
		["default"] = 2,
	},
	OBJECTIVES = {
		["save"] = mod.setting_strings[2],
		["widget_type"] = "stepper",
		["text"] = "Objectives",
		["tooltip"] = "Objectives\n" ..
			"Whether to display objective banner, markers and button prompts.",
		["value_type"] = "boolean",
		["options"] = {
			{text = "Off", value = false},
			{text = "On", value = true},
		},
		["default"] = 2,
	},
	OUTLINES = {
		["save"] = mod.setting_strings[3],
		["widget_type"] = "stepper",
		["text"] = "Outlines",
		["tooltip"] = "Outlines\n" ..
			"Whether to display player, object and item outlines.\n" ..
			"Overrides Player Outlines Always On setting.",
		["value_type"] = "boolean",
		["options"] = {
			{text = "Off", value = false},
			{text = "On", value = true},
		},
		["default"] = 2,
	},
	CROSSHAIR = {
		["save"] = mod.setting_strings[4],
		["widget_type"] = "stepper",
		["text"] = "Crosshair",
		["tooltip"] = "Crosshair\n" ..
			"Whether to display crosshair.",
		["value_type"] = "boolean",
		["options"] = {
			{text = "Off", value = false},
			{text = "On", value = true},
		},
		["default"] = 2,
	},
	PING = {
		["save"] = mod.setting_strings[5],
		["widget_type"] = "stepper",
		["text"] = "Ping",
		["tooltip"] = "Ping\n" ..
			"Whether enemies, players and items can be pinged.",
		["value_type"] = "boolean",
		["options"] = {
			{text = "Off", value = false},
			{text = "On", value = true},
		},
		["default"] = 2,
	},
	FEEDBACK = {
		["save"] = mod.setting_strings[6],
		["widget_type"] = "stepper",
		["text"] = "Feedback",
		["tooltip"] = "Feedback\n" ..
			"Whether damage indicators, special kills and assists are shown.\n",
		["value_type"] = "boolean",
		["options"] = {
			{text = "Off", value = false},
			{text = "On", value = true},
		},
		["default"] = 2,
	},
	WEAPON = {
		["save"] = mod.setting_strings[7],
		["widget_type"] = "stepper",
		["text"] = "Weapon Model",
		["tooltip"] = "Weapon Model\n" ..
			"Whether to display weapon model and hands.",
		["value_type"] = "boolean",
		["options"] = {
			{text = "Off", value = false},
			{text = "On", value = true},
		},
		["default"] = 2,
	},

	TOGGLE = {
		["save"] = "vmf_hud_toggle",
		["widget_type"] = "keybind",
		["text"] = "Toggle HUD",
		["default"] = {
			"h",
			oi.key_modifiers.ALT
		},
		["exec"] = {"HudToggle", "chat/toggle"},
	},
	MORE = {
		["save"] = "vmf_hud_more",
		["widget_type"] = "keybind",
		["text"] = "HUD+",
		["default"] = {
			"0",
			oi.key_modifiers.NONE
		},
		["exec"] = {"HudToggle", "chat/more"},
	},
	LESS = {
		["save"] = "vmf_hud_less",
		["widget_type"] = "keybind",
		["text"] = "HUD-",
		["default"] = {
			"9",
			oi.key_modifiers.NONE
		},
		["exec"] = {"HudToggle", "chat/less"},
	},
}

mod.settings_saved = {}

--[[
	Flags
]]--
mod.visible = true
mod.hud_set = false
mod.camera_set = false
mod.outlines_set = false

mod.mode = 0

--[[
	Internal methods
]]--

function mod.get_setting(key)
	if mod.widget_settings[key] then 
		return mod.get(mod.widget_settings[key].save)
	else
		return nil
	end
end

function mod.get_settings()
	local settings = {}
	for i,v in ipairs(mod.setting_strings) do
		table.insert(settings, mod.get(v))
	end
	return settings
end

function mod.set_setting(key, value)
	if mod.widget_settings[key] then 
		mod.set(mod.widget_settings[key].save, value)
		mod.save()
	end
end

function mod.set_setting_i(i, value)
	if mod.setting_strings[i] then 
		mod.set(mod.setting_strings[i], value)
		mod.save()
	end
end

function mod.set_all_settings(value)
	mod.visible = value
	for i,v in ipairs(mod.setting_strings) do
		mod.set(mod.setting_strings[i], not not value)
	end
	mod.save()
end

function mod.apply_settings()

	--Saving settings locally
	for i,v in ipairs(mod.settings_sorted) do
		mod.settings_saved[v] = mod.get_setting(v)
	end

	--Hiding beta overlay
	if(Managers.beta_overlay) then
		if(mod.get_setting("ELEMENTS")) then
			Managers.beta_overlay.widget.offset[1] = 0
		else
			Managers.beta_overlay.widget.offset[1] = 10000
		end
	end

	--Setting flags for update hooks
	mod.hud_set = false
	mod.camera_set = false
	mod.outlines_set = false
end

function mod.check_visibility()
	local visible = true
	for i,v in ipairs(mod.setting_strings) do
		local value = mod.get(v)
		if not value then
			visible = false
			break
		end
	end
	mod.visible = visible
	return visible
end

function mod.create_options()
	Mods.option_menu:add_group("hud_toggle", "HUD Visibility")
	Mods.option_menu:add_item("hud_toggle", mod.widget_settings["TOGGLE"], true)
	Mods.option_menu:add_item("hud_toggle", mod.widget_settings["MORE"], true)
	Mods.option_menu:add_item("hud_toggle", mod.widget_settings["LESS"], true)
	for i,v in ipairs(mod.settings_sorted) do
		Mods.option_menu:add_item("hud_toggle", mod.widget_settings[v], true)
	end
end

--[[
	Extrenal methods
]]--

--Toggles hud
function mod.toggle()
	mod.set_all_settings(not mod.visible)
	mod.mode = mod.visible and 0 or #mod.settings_sorted
	mod.apply_settings()
end

--Hud +/-
function mod.more()
	local mode = mod.mode
	local decrease = mode == 6 and 2 or 1
	if mode <= 0 then decrease = 0 end	
	for i = mode - decrease + 1, #mod.settings_sorted do
		mod.set_setting_i(i, true)
	end
	mod.mode = mode - decrease
	mod.apply_settings()
end

function mod.less()
	local mode = mod.mode
	local increase = mode == 4 and 2 or 1
	if mode >= #mod.settings_sorted then increase = 0 end
	for i = 1, mode + increase do
		mod.set_setting_i(i, false)
	end
	mod.mode = mode + increase
	mod.apply_settings()
end

--[[
	Hooks
]]--

--Settings watch
local last_update = 0
Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, self, dt, t)
	func(self, dt, t)
	if t - last_update > 1 then
		last_update = t
		for i,v in ipairs(mod.settings_sorted) do
			if mod.get_setting(v) ~= mod.settings_saved[v] then
				mod.apply_settings()
				break
			end
		end
	end
end)

--Altering hud toggle function
Mods.hook.set(mod_name, "IngameHud.set_visible", function(func, self, orig_visible)

	if mod.get_setting("ELEMENTS") then
		return func(self, orig_visible)
	end
	
	local visible = false
	if self.player_inventory_ui then
		self.player_inventory_ui:set_visible(visible)
	end

	if self.unit_frames_handler then
		self.unit_frames_handler:set_visible(visible)
	end

	if self.game_timer_ui then
		self.game_timer_ui:set_visible(visible)
	end

	if self.endurance_badge_ui then
		self.endurance_badge_ui:set_visible(visible)
	end

	local difficulty_unlock_ui = self.difficulty_unlock_ui

	if difficulty_unlock_ui then
		difficulty_unlock_ui.set_visible(difficulty_unlock_ui, visible)
	end

	local difficulty_notification_ui = self.difficulty_notification_ui

	if difficulty_notification_ui then
		difficulty_notification_ui.set_visible(difficulty_notification_ui, visible)
	end

	if self.boon_ui then
		self.boon_ui:set_visible(visible)
	end

	if self.contract_log_ui then
		self.contract_log_ui:set_visible(visible)
	end

	if self.tutorial_ui then
		self.tutorial_ui:set_visible(visible)
	end

	local observer_ui = self.observer_ui

	if observer_ui then
		local observer_ui_visibility = self.is_own_player_dead(self) and not self.ingame_player_list_ui.active and orig_visible

		if observer_ui and observer_ui.is_visible(observer_ui) ~= observer_ui_visibility then
			observer_ui.set_visible(observer_ui, observer_ui_visibility)
		end
	end

end)

--Hiding things that might show up later
Mods.hook.set(mod_name, "IngameUI.update", function(func, self, ...)
	func(self, ...)
	if not mod.hud_set then
		if self.ingame_hud and self.ingame_hud.set_visible then
			self.ingame_hud:set_visible(self, mod.get_setting("ELEMENTS"))
			mod.hud_set = true
		end
	end
	if not mod.get_setting("OBJECTIVES") and self.hud_visible then
		self.hud_visible = false
	end
end)

--Hiding contracts log
Mods.hook.set(mod_name, "ContractLogUI.update", function (func, ...)
	if mod.get_setting("ELEMENTS") then
		return func(...)
	end
end)

--Hiding stamina
Mods.hook.set(mod_name, "FatigueUI.update", function (func, ...)
	if mod.get_setting("ELEMENTS") then
		return func(...)
	end
end)

--Hiding overcharge bar
Mods.hook.set(mod_name, "OverchargeBarUI.update", function (func, ...)
	if mod.get_setting("ELEMENTS") then
		return func(...)
	end
end)

--Area indicators (?)
Mods.hook.set(mod_name, "AreaIndicatorUI.update", function (func, ...)
	if mod.get_setting("OBJECTIVES") then
		return func(...)
	end
end)

--Hiding interaction prompts
Mods.hook.set(mod_name, "InteractionUI.update", function (func, ...)
	if mod.get_setting("OBJECTIVES") then
		return func(...)
	end
end)

--Mission objectives
Mods.hook.set(mod_name, "MissionObjectiveUI.update", function (func, ...)
	if mod.get_setting("OBJECTIVES") then
		return func(...)
	end
end)

--Tutorial UI (?)
Mods.hook.set(mod_name, "TutorialUI.update", function (func, ...)
	if mod.get_setting("OBJECTIVES") then
		return func(...)
	end
end)

--Hiding crosshair
Mods.hook.set(mod_name, "CrosshairUI.update", function(func, self, ...)
	if mod.get_setting("CROSSHAIR") then
		return func(self, ...)
	end
end)


--Hiding hands and weapon
Mods.hook.set(mod_name, "PlayerUnitFirstPerson.update", function (func, self, unit, input, dt, context, t)
	func(self, unit, input, dt, context, t)
	
	if not mod.get_setting("WEAPON") then
		self.inventory_extension:show_first_person_inventory(false)
		self.inventory_extension:show_first_person_inventory_lights(false)
		Unit.set_unit_visibility(self.first_person_attachment_unit, false)

	elseif not mod.camera_set then

		local player_unit = Managers.player:player_from_peer_id(Network.peer_id()).player_unit
		local first_person_system = player_unit and ScriptUnit.extension(player_unit, "first_person_system")
		if not first_person_system or not first_person_system.first_person_mode then return end

		local is_third_person_mod = Mods.manager._mods["ThirdPerson"] and ThirdPerson.SETTINGS and ThirdPerson.SETTINGS.ACTIVE and ThirdPerson.SETTINGS.ACTIVE.save and mod.get(ThirdPerson.SETTINGS.ACTIVE.save)
		local is_first_person = first_person_system.first_person_mode and not is_third_person_mod


		self.inventory_extension:show_first_person_inventory(is_first_person)
		self.inventory_extension:show_first_person_inventory_lights(is_first_person)
		Unit.set_unit_visibility(self.first_person_attachment_unit, is_first_person)

		mod.camera_set = true
	end
end)

--Hiding outlines
Mods.hook.set(mod_name, "OutlineSystem.update", function(func, self, ...)

	if mod.get_setting("OUTLINES") then
		return func(self, ...)
	end

	if #self.units == 0 then
		return 
	end

	if script_data.disable_outlines then
		return 
	end

	local checks_per_frame = 4
	local current_index = self.current_index
	local units = self.units

	for i = 1, checks_per_frame, 1 do
		current_index = current_index + 1

		if not units[current_index] then
			current_index = 1
		end

		local unit = self.units[current_index]
		local extension = self.unit_extension_data[unit]

		if extension or false then
			local is_pinged = extension.pinged
			local method = "never"

			if self[method](self, unit, extension) then
				if not extension.outlined or extension.new_color or extension.reapply then
					local c = (is_pinged and OutlineSettings.colors.player_attention.channel) or extension.outline_color.channel
					local channel = Color(c[1], c[2], c[3], c[4])

					self.outline_unit(self, unit, extension.flag, channel, true, extension.apply_method, extension.reapply)

					extension.outlined = true
				end
			elseif extension.outlined or extension.new_color or extension.reapply then
				local c = extension.outline_color.channel
				local channel = Color(c[1], c[2], c[3], c[4])

				self.outline_unit(self, unit, extension.flag, channel, false, extension.apply_method, extension.reapply)

				extension.outlined = false
			end

			extension.new_color = false
			extension.reapply = false
		end
	end

	self.current_index = current_index
end)

--Disabling ping
Mods.hook.set(mod_name, "ContextAwarePingExtension.update", function (func, ...)
	if mod.get_setting("PING") then
		return func(...)
	end
end)

--Disabling positive reinforcement
Mods.hook.set(mod_name, "PositiveReinforcementUI.update", function (func, ...)
	if mod.get_setting("FEEDBACK") then
		return func(...)
	end
end)

--Hiding subtitles
Mods.hook.set(mod_name, "SubtitleGui.update", function(func, self, ...)
	if mod.get_setting("FEEDBACK") then
		return func(self, ...)
	end
end)

--Hide damage indicators
Mods.hook.set(mod_name, "DamageIndicatorGui.update", function(func, self, ...)
	if mod.get_setting("FEEDBACK") then
		return func(self, ...)
	end
end)

--[[
	Initialization
]]--
mod.create_options()
mod.check_visibility()
mod.apply_settings()