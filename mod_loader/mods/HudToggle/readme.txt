Makes it possible to turn off most hud elements and outlines.
These elements can be turned off separately:
	- HUD (equipment, character portraits, health bars, stamina, overcharge etc.)
	- Objectives (objective markers, banner and button prompts)
	- Outlines (players, items, objects)
	- Crosshair
	- Ping 
	- Feedback (damage indicators, special kills and assists)
	- Weapon model and hands
0 and 9 (by default) can be used to toggle elements one by one.
Note that Ping and Feedback are toggled together when doing it this way.
You can use Alt + H (by default) or /togglehud chat command to toggle all elements.
Go to Mod Settings -> HUD to toggle elements and change hotkeys.