local mod_name = "CheatProtect"
--[[ 
	Protect from Cheaters
	
	Author: IamLupo and Grimalackt
--]]

local oi = OptionsInjector

Mods.CheatProtect = {
	SETTINGS = {
		ACTIVE = {
			["save"] = "cb_cheat_protect_activate",
			["widget_type"] = "stepper",
			["text"] = "Active",
			["tooltip"] =  "Activate Cheat Protection\n" ..
				"This will protect yourself against cheaters.\n\n" ..
				"This will detect people try to spawn/duplicate items.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
	},
	
	-- Save items state
	item = {
		count = -1,
		person = nil,
	},
	
	-- Save position state
	position = nil,
	
	-- Save trade state
	trade = {
		pickup_name_id = -1,
		person = nil,
	},
}

local me = Mods.CheatProtect

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Option #######################################################################################################
-- ####################################################################################################################
Mods.CheatProtect.create_options = function()
	Mods.option_menu:add_group("cheat_protect", "Cheat Protection")
	
	Mods.option_menu:add_item("cheat_protect", me.SETTINGS.ACTIVE, true)
end

-- ####################################################################################################################
-- ##### Hooks ########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "InventorySystem.rpc_add_equipment", function(func, self, sender, go_id, slot_id, item_name_id)
	func(self, sender, go_id, slot_id, item_name_id)
	
	if get(me.SETTINGS.ACTIVE.save) then
		if slot_id == 7 or slot_id == 8 or slot_id == 9 then
			me.item.count = #Managers.state.unit_spawner.unit_storage.map_goid_to_unit
			me.item.person = sender
			
			local player = Managers.player:player_from_peer_id(sender, 1)
			me.position = POSITION_LOOKUP[player.player_unit]
		end
	end
end)

Mods.hook.set(mod_name, "PickupSystem.rpc_spawn_pickup", function(func, self, sender, pickup_name_id, position, rotation, spawn_type_id)
	if get(me.SETTINGS.ACTIVE.save) then
		local player = Managers.player:player_from_peer_id(sender, 1)

		if not (
			sender == Network.peer_id() or
			(me.item.count == #Managers.state.unit_spawner.unit_storage.map_goid_to_unit and me.item.person == sender)
		) then
			-- Show message
			Managers.chat:send_system_chat_message(
				1,
				"CheatProtect: Blocked player '" .. player._cached_name ..
					"' attempted RPC_SPAWN_PICKUP (without physics) for item " ..
					NetworkLookup.pickup_names[pickup_name_id] .. ".",
				0,
				true
			)
			
			return
		end
	end
	
	func(self, sender, pickup_name_id, position, rotation, spawn_type_id)
end)

Mods.hook.set(mod_name, "PickupSystem.rpc_spawn_pickup_with_physics", function(func, self, sender, pickup_name_id, position, rotation, spawn_type_id)
	if get(me.SETTINGS.ACTIVE.save) then
		local player = Managers.player:player_from_peer_id(sender, 1)
		
		if Unit.alive(player.player_unit) then
			local status_extension = ScriptUnit.extension(player.player_unit, "status_system")
			local position = POSITION_LOOKUP[player.player_unit]

			if not (
					sender == Network.peer_id() or
				   (status_extension and status_extension.dead) or
				   (me.position == position) or
				   (me.trade.pickup_name_id == pickup_name_id and me.trade.person == sender)
			) then
				-- Show message
				Managers.chat:send_system_chat_message(
					1,
					"CheatProtect: Blocked player '" .. player._cached_name ..
						"' attempted RPC_SPAWN_PICKUP_WITH_PHYSICS for item " ..
						NetworkLookup.pickup_names[pickup_name_id] .. ".",
					0,
					true
				)
				
				me.trade.pickup_name_id = -1
				me.trade.person = nil
				
				return
			end

			me.trade.pickup_name_id = -1
			me.trade.person = nil
		end
	end
	
	func(self, sender, pickup_name_id, position, rotation, spawn_type_id)
end)

Mods.hook.set(mod_name, "InventorySystem.rpc_give_equipment", function(func, self, sender, game_object_id, slot_id, item_name_id, position)
	func(self, sender, game_object_id, slot_id, item_name_id, position)
	
	if get(me.SETTINGS.ACTIVE.save) then
		local unit = self.unit_storage:unit(game_object_id)
		
		if Unit.alive(unit) and not ScriptUnit.extension(unit, "status_system"):is_dead() then
			local owner = Managers.player:owner(unit)
			
			if owner.remote then
				local item_name = NetworkLookup.item_names[item_name_id]
				local item_data = ItemMasterList[item_name]
				local pickup_name = BackendUtils.get_item_template(item_data).pickup_data.pickup_name
				local pickup_name_id = NetworkLookup.pickup_names[pickup_name]
				
				-- Save trade
				me.trade.pickup_name_id = pickup_name_id
				me.trade.person = owner.network_id(owner)
			end
		end
	end
end)

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()