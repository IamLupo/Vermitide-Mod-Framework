Adds keyboard shortcuts for spawning ammo boxes and some other weird stuff.
Requires SpawnItemFunc mod to function.
Go to Mod Settings -> Spawning -> Structures to see and change shortcuts.