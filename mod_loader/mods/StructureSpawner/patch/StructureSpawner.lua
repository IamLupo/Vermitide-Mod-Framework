local mod_name = "StructureSpawner"

local oi = OptionsInjector

StructureSpawner = {
	SETTINGS = {
		BUILD = {
			["save"] = "cb_spawning_build",
			["widget_type"] = "checkbox",
			["text"] = "Structures",
			["default"] = false,
			["hide_options"] = {
				{
					false,
					mode = "hide",
					options = {
						"cb_spawning_build_box",
						"cb_spawning_build_box_modifiers",
						"cb_spawning_build_ground",
						"cb_spawning_build_ground_modifiers",
						"cb_spawning_build_stairs",
						"cb_spawning_build_stairs_modifiers",
						"cb_spawning_build_stairs_big",
						"cb_spawning_build_stairs_big_modifiers",
						"cb_spawning_build_explosion",
						"cb_spawning_build_explosion_modifiers",
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_spawning_build_box",
						"cb_spawning_build_box_modifiers",
						"cb_spawning_build_ground",
						"cb_spawning_build_ground_modifiers",
						"cb_spawning_build_stairs",
						"cb_spawning_build_stairs_modifiers",
						"cb_spawning_build_stairs_big",
						"cb_spawning_build_stairs_big_modifiers",
						"cb_spawning_build_explosion",
						"cb_spawning_build_explosion_modifiers",
					}
				},
			},
		},
		HK_BUILD_BOX = {
			["save"] = "cb_spawning_build_box",
			["widget_type"] = "keybind",
			["text"] = "Ammo Box",
			["default"] = {
				"a",
				oi.key_modifiers.ALT,
			},
			["exec"] = {"StructureSpawner", "build/ammo_box"},
		},
		HK_BUILD_GROUND = {
			["save"] = "cb_spawning_build_ground",
			["widget_type"] = "keybind",
			["text"] = "Ammo Boxes: Platform",
			["default"] = {
				"f1",
				oi.key_modifiers.ALT,
			},
			["exec"] = {"StructureSpawner", "build/ground"},
		},
		HK_BUILD_STAIRS = {
			["save"] = "cb_spawning_build_stairs",
			["widget_type"] = "keybind",
			["text"] = "Ammo Boxes: Small Stairs",
			["default"] = {
				"f2",
				oi.key_modifiers.ALT,
			},
			["exec"] = {"StructureSpawner", "build/stairs_small"},
		},
		HK_BUILD_STAIRS_BIG = {
			["save"] = "cb_spawning_build_stairs_big",
			["widget_type"] = "keybind",
			["text"] = "Ammo Boxes: Big Stairs",
			["default"] = {
				"f3",
				oi.key_modifiers.ALT,
			},
			["exec"] = {"StructureSpawner", "build/stairs_big"},
		},
		HK_SPAWN_EXPLOSION = {
			["save"] = "cb_spawning_build_explosion",
			["widget_type"] = "keybind",
			["text"] = "Explosion",
			["default"] = {
				"f5",
				oi.key_modifiers.ALT,
			},
			["exec"] = {"StructureSpawner", "build/explosion"},
		},
	},
}

local me = StructureSpawner

-- ####################################################################################################################
-- ##### Option #######################################################################################################
-- ####################################################################################################################
StructureSpawner.create_options = function()	
	Mods.option_menu:add_item("spawning", me.SETTINGS.BUILD, true)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_BUILD_BOX)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_BUILD_GROUND)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_BUILD_STAIRS)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_BUILD_STAIRS_BIG)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_SPAWN_EXPLOSION)
end

-- ####################################################################################################################
-- ##### Start #######################################################################################################
-- ####################################################################################################################
me.create_options()