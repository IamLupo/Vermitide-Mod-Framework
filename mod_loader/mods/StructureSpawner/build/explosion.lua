local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

local player_unit = Managers.player:player_from_peer_id(Network.peer_id()).player_unit
local player_pos = Unit.local_position(player_unit, 0)
local player_rot = Unit.local_rotation(player_unit, 0)

local pickup = NetworkLookup.pickup_names["explosive_barrel"]
local pickup_spawn_type = NetworkLookup.pickup_spawn_types['dropped']

for x = 1, 10 do
	for y = 1, 10 do
		local final_pos = player_pos + Vector3((x * 0.5), (y * 0.5), -0.5)
		Managers.state.network.network_transmit:send_rpc_server(
			'rpc_spawn_pickup_with_physics',
			pickup,
			final_pos,
			player_rot,
			pickup_spawn_type)
	end
end

-- Feedback
if not get(ModSettings.SETTINGS.HK_FEEDBACK.save) then
	EchoConsole("Build: Explosion")
end