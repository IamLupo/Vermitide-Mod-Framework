local mod_name = "PlayerList"
--[[
	Patch Player List:
		Shows the ping and kick button in tab menu
--]]

local oi = OptionsInjector

Mods.PlayerList = {
	SETTINGS = {
		SHOW = {
			["save"] = "cb_player_list_show",
			["widget_type"] = "checkbox",
			["text"] = "Player List",
			["default"] = false,
			["hide_options"] = {
				{
					false,
					mode = "hide",
					options = {
						"cb_player_list_ping",
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_player_list_ping",
					}
				},
			},
		},
		PING = {
			["save"] = "cb_player_list_ping",
			["widget_type"] = "stepper",
			["text"] = "Share ping information",
			["tooltip"] = "Share ping information\n" ..
				"Share ping information with other modded clients\n\n" ..
				"Normally a client cant see ping information of other clients.\n" ..
				"The server will manage to send this information to all clients",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
		},
		PING_LIMIT = {
			["save"] = "cb_player_list_ping_limit",
			["widget_type"] = "slider",
			["text"] = "Refresh rate",
			["tooltip"] = "Refresh rate\n" .. 
						"Time in seconds how often the server refreshes the ping list\n",
			["range"] = {1, 10},
			["default"] = 3,
		},
	},

	ping = {},
}
local me = Mods.PlayerList

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Options ######################################################################################################
-- ####################################################################################################################
Mods.PlayerList.create_options = function()
	Mods.option_menu:add_group("player_list", "Player List")

	Mods.option_menu:add_item("player_list", me.SETTINGS.PING, true)
	Mods.option_menu:add_item("player_list", me.SETTINGS.PING_LIMIT, true)
end

-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "IngamePlayerListUI.update_player_information", function(func, self)
	-- Call orginal function
	func(self)
	
	local players = self.players
	for _, player in ipairs(players) do
		local ping = 0
		local widget = player.widget
		
		if player.is_server then -- clients can get server ping
			ping = round(Network.ping(player.peer_id) * 1000, 0)
		else -- other pings get from ping list
			safe_pcall(function()
				ping = me.ping[player.peer_id]
			end)
		end
		
		if ping ~= 0 then -- server get server ping results to ping 0
			widget.content.name = widget.content.name .. " (" .. tostring(ping) .. ")"
		end
	end
	
end)

Mods.hook.set(mod_name, "IngamePlayerListUI.update", function(func, self, dt)
	-- Call orginal function
	func(self, dt)
	
	-- Kick		
	if self.active then
		local players = self.players		
		if Managers.player.is_server then -- can only kick when server
			for i, player in ipairs(players) do
				if player.peer_id ~= Network.peer_id() then -- can't kick yourself
					local content = self.player_list_widgets[i].content				
					content.show_kick_button = true
					content.kick_button_hotspot.disable_button = false
					if content.kick_button_hotspot.input_pressed then -- button was pressed
						Managers.state.network.network_transmit:send_rpc("rpc_kick_peer", player.peer_id)
						content.kick_button_hotspot.input_pressed = false
						
						if Mods.ban then
							-- Save last player that has been kicked
							Mods.ban.kicked.peer_id = tostring(player.peer_id)
							Mods.ban.kicked.name = tostring(player.widget.content.name)
						end
					end
				end
			end
		end
	end
end)

local send_ping_t = 0
Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, self, dt, t)
	func(self, dt, t)
	
	-- Update Ping list
	if get(me.SETTINGS.PING.save) and Managers.player.is_server then -- Server manage the ping list
		if t - send_ping_t > get(me.SETTINGS.PING_LIMIT.save) then -- Once every second the ping list updates
			local players = Managers.player:human_players()
			
			-- Update player ping
			for _, player in pairs(players) do
				me.ping[player.peer_id] = round(Network.ping(player.peer_id) * 1000, 0)
			end
			
			-- Send ping list to all clients
			Mods.network.send_rpc_clients("rpc_playerlist_set_pings", me.ping)
			
			send_ping_t = t
		end
	end
end)

-- ####################################################################################################################
-- ##### Network #########################################################################################################
-- ####################################################################################################################
Mods.network.register("rpc_playerlist_set_pings", function(sender, ping_list)
	-- Only clients need the ping list
	if not Managers.player.is_server then
		me.ping = ping_list
	end
end)

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()