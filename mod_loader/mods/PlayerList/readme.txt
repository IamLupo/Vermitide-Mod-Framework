Shows ping and kick button in tab menu. Ping only shows for players if you're hosting or if they have enabled Share Ping option.
Always on.
Go to Mod Settings -> Player List to enable sharing ping to other clients and set ping refresh rate.