Displays healthbars for enemies.
Can be modified to show healthbars above and below enemies, show healthbars for specified enemies only and only show healthbars for damaged enemies.
Go to Mod Settings -> Enemy Health Bars to change settings.