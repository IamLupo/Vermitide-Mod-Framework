local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

local active = get(EnemyHealthBars.SETTINGS.ACTIVE.save)

if active == 1 then
	set(EnemyHealthBars.SETTINGS.ACTIVE.save, 2)
	EchoConsole("Enemy Health Bars: All")
elseif active == 2 then
	set(EnemyHealthBars.SETTINGS.ACTIVE.save, 3)
	EchoConsole("Enemy Health Bars: Specials")
elseif active == 3 then
	set(EnemyHealthBars.SETTINGS.ACTIVE.save, 4)
	EchoConsole("Enemy Health Bars: Ogre")
elseif active == 4 then
	set(EnemyHealthBars.SETTINGS.ACTIVE.save, 5)
	EchoConsole("Enemy Health Bars: Custom")
else
	EchoConsole("Enemy Health Bars: Off")
	set(EnemyHealthBars.SETTINGS.ACTIVE.save, 1)
end

save()