This mod allows you to ban players from your server after they have been kicked.
Keeps a ban list in mod_loader/Ban_list.lua.
Chat command: /ban