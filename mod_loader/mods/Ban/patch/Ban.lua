local mod_name = "Ban"
--[[
	Ban:
		This allows you to ban players that keeps annoying you.
--]]

local me = Mods.ban

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Options ######################################################################################################
-- ####################################################################################################################
Mods.ban.create_options = function()
	Mods.option_menu:add_group("ban", "Ban")
	
	Mods.option_menu:add_item("ban", me.SETTINGS.KICK_LEVEL, true)
	Mods.option_menu:add_item("ban", me.SETTINGS.KICK_LEVEL_NIGHTMARE)
	Mods.option_menu:add_item("ban", me.SETTINGS.KICK_LEVEL_CATACLYSM)
end

-- ####################################################################################################################
-- ##### Function #####################################################################################################
-- ####################################################################################################################
--
-- Check players how needs to get kicked
--
Mods.ban.check_players = function()
	local human_players = Managers.player:human_players()
		
	for _, player in pairs(human_players) do
		-- Check Ban list
		for _, ban_player in ipairs(me.list) do
			if ban_player.peer_id == tostring(player.peer_id) then
				Mods.chat.send("Automaticly kicked " .. ban_player.name .. " because player is banned.")
				Managers.state.network.network_transmit:send_rpc("rpc_kick_peer", player.peer_id)
			end
		end
		
		if get(me.SETTINGS.KICK_LEVEL.save) then
			-- Check Player level
			local player_level = ExperienceSettings.get_player_level(player)
			local player_name = tostring(player._cached_name)
			
			local difficulty_settings = Managers.state.difficulty:get_difficulty_settings()
			
			-- Minimum level
			local min_level = 0
			if difficulty_settings.rank == 4 then
				min_level = get(me.SETTINGS.KICK_LEVEL_NIGHTMARE.save)
			elseif difficulty_settings.rank == 5 then
				min_level = get(me.SETTINGS.KICK_LEVEL_CATACLYSM.save)
			end
			
			if player_level and player_level < min_level then
				Mods.chat.send(
					"Automaticly kicked " .. player_name .. " because player is level " .. tostring(player_level) .. " and thats to low. " ..
					"Minimum level requirement is " .. tostring(min_level)
				)
				Managers.state.network.network_transmit:send_rpc("rpc_kick_peer", player.peer_id)
			end
		end
	end
end

-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
local check_time = 0
Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, self, dt, t)
	-- Call orginal function
	func(self, dt, t)
	
	if Managers.player.is_server then -- can only ban when server
		if t - check_time > 1 then -- check once ever second to ban
			me.check_players()
		
			check_time = t
		end
	end
end)

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()

-- Load ban list
me.load()