--[[
	Type in the chat: /ban
--]]

local args = {...}

local me = Mods.ban

if #args == 0 then
	if me.kicked.peer_id ~= "" then
		me.add_player(me.kicked.peer_id, me.kicked.name)
		
		-- Intimidate other players :D
		Mods.chat.send("Banned player " .. me.kicked.name)
		
		-- remove data
		me.kicked.peer_id = ""
		me.kicked.name = ""
	else
		EchoConsole("Kick a player before you can ban.")
	end
	
	return true
else
	return false
end