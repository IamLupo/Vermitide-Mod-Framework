Mods.ban = {
	SETTINGS = {
		KICK_LEVEL = {
			["save"] = "cb_ban_kick_level",
			["widget_type"] = "stepper",
			["text"] = "Kick low level players",
			["tooltip"] =  "Kick low level players\n" ..
				"Toggle Kick low level players on / off.\n\n" ..
				"This allows you to kick low level people automaticly.\n",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
			["hide_options"] = {
				{
					false,
					mode = "hide",
					options = {
						"cb_ban_kick_level_nightmare",
						"cb_ban_kick_level_cataclysm",
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_ban_kick_level_nightmare",
						"cb_ban_kick_level_cataclysm",
					}
				},
			},
		},
		KICK_LEVEL_NIGHTMARE = {
			["save"] = "cb_ban_kick_level_nightmare",
			["widget_type"] = "slider",
			["text"] = "Nightmare",
			["tooltip"] = "Minimum Level\n\n" .. 
						"Minimum player level it needs to have to not be kicked",
			["range"] = {1, 200},
			["default"] = 30,
		},
		KICK_LEVEL_CATACLYSM = {
			["save"] = "cb_ban_kick_level_cataclysm",
			["widget_type"] = "slider",
			["text"] = "Cataclysm",
			["tooltip"] = "Minimum Level\n\n" .. 
						"Minimum player level it needs to have to not be kicked",
			["range"] = {1, 200},
			["default"] = 50,
		},
	},
	
	filename = "Ban_list",
	
	list = {},
	
	-- Last player kicked
	kicked = {
		peer_id = "",
		name = "",
	},
}

local me = Mods.ban

local ban_player_template = {
	peer_id = "",
	name = "",
}

--
-- Add Player to ban list
--
Mods.ban.add_player = function(peer_id, name)
	if peer_id ~= Network.peer_id() then -- can't ban yourself
		if not me.exist(peer_id) then -- can't ban players twice
			local ban_player = table.clone(ban_player_template)
			
			ban_player.peer_id = tostring(peer_id)
			ban_player.name = name
			
			-- insert player into ban list
			table.insert(me.list, ban_player)
			
			-- Save Ban list
			me.save()
		end
	end
end

--
-- Check Player is already banned
--
Mods.ban.exist = function(peer_id)
	for _, player in ipairs(me.list) do
		if player.peer_id == tostring(peer_id) then
			return true
		end
	end
	
	return false
end

--
-- Save ban list
--
Mods.ban.save = function()
	local file_path = "mod_loader/" .. me.filename .. ".lua"
	
	local file = io.open(file_path, "w+")
	if file ~= nil then
		file:write("Mods.ban.list = {\n")
		
		for _, player in ipairs(me.list) do
			file:write("	{\n")
			file:write("		peer_id = \"" .. player.peer_id .. "\",\n")
			file:write("		name = \"" .. player.name .. "\",\n")
			file:write("	},\n")
		end
		
		file:write("}\n")
		file:close()
	end
end

--
-- Load ban list
--
Mods.ban.load = function()
	local file = io.open("mod_loader/" .. me.filename .. ".lua", "r")
	
	if file then
		Mods.exec("", me.filename)
	end
end