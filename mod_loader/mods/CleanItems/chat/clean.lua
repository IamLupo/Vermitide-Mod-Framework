--[[
	Type in the chat: /clean
--]]

local args = {...}

if #args == 0 then
	if Managers.player.is_server then
		local unit_storage = Managers.state.unit_spawner.unit_storage
		local items = unit_storage.map_goid_to_unit
		
		for go_id, unit in pairs(items) do
			local gotype = unit_storage.map_goid_to_gotype[go_id]
			
			if gotype == "pickup_torch_unit" or gotype == "pickup_unit" then
				if not Managers.state.unit_spawner:is_marked_for_deletion(unit) then
					Managers.state.unit_spawner:mark_for_deletion(unit)
				end
			end
		end
		
		EchoConsole("Remove all items")
	else
		EchoConsole("Only host can remove items")
	end

	return true
else
	return false
end