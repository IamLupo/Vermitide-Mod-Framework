Adds commands to display animated text in chat (with ■ and Z symbols).
Use sparingly.
Chat commands:
	/gg - gg animation
	/msg <message> - animates message