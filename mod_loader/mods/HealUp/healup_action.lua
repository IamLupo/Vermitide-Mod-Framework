local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

local player_unit = Managers.player:player_from_peer_id(Network.peer_id()).player_unit
local unit_id = Managers.state.network:unit_game_object_id(player_unit)

Managers.state.network.network_transmit:send_rpc_server(
	"rpc_request_heal",
	unit_id,
	150,
	NetworkLookup.heal_types.healing_draught
)

-- Feedback
if not get(ModSettings.SETTINGS.HK_FEEDBACK.save) then
	EchoConsole("Player: Healed")
end