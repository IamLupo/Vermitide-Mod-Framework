local mod_name = "HealUp"

local oi = OptionsInjector

HealUp = {
	SETTINGS = {
		HEAL = {
			["save"] = "cb_cheats_hotkey_heal",
			["widget_type"] = "keybind",
			["text"] = "Heal up",
			["default"] = {
				"h",
				oi.key_modifiers.CTRL_SHIFT,
			},
			["exec"] = {"HealUp", "healup_action"},
		},
	},
}

HealUp.create_options = function()	
	Mods.option_menu:add_item("cheats", HealUp.SETTINGS.HEAL, true)
end
HealUp.create_options()