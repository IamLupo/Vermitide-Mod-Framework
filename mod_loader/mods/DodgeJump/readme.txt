Allows you to dodge-jump up walls, like you could before v1.6.
Go to Mod Settings -> Movement -> Dodge Jump to toggle.
Off by default.