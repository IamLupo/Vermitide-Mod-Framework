Disables item consumption.
Keyboard shortcut to toggle (default): Ctrl + Shift + U
Go to Mod Settings -> Items -> Unlimited Items to change settings and shortcut.