local mod_name = "UnlimitedItems"
--[[
	Unlimited items
		- Items like bombs, potions and such that are used up can be made unlimited.
	
	author: grasmann
--]]

local oi = OptionsInjector

UnlimitedItems = {
	SETTINGS = {
		ACTIVE = {
			["save"] = "cb_unlimited_items",
			["widget_type"] = "checkbox",
			["text"] = "Unlimited Items",
			["default"] = false,
			["hide_options"] = {
				{
					false,
					mode = "hide",
					options = {
						"cb_unlimited_items_mode",
						"cb_unlimited_items_hotkey_toggle",
						"cb_unlimited_items_hotkey_toggle_modifiers",
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_unlimited_items_mode",
						"cb_unlimited_items_hotkey_toggle",
						"cb_unlimited_items_hotkey_toggle_modifiers",
					}
				}
			},
		},
		MODE = {
			["save"] = "cb_unlimited_items_mode",
			["widget_type"] = "dropdown",
			["text"] = "Mode",
			["tooltip"] = "Unlimited Items\n" ..
				"Toggle unlimited items on / off.\n\n" ..
				"This will picked up items unlimited.",
			["value_type"] = "number",
			["options"] = {
				{text = "Off", value = 1},
				{text = "All", value = 2},
				{text = "Custom", value = 3},
			},
			["default"] = 1,
			["hide_options"] = {
				{
					1,
					mode = "hide",
					options = {
						"cb_unlimited_items_bomb",
						"cb_unlimited_items_potion",
						"cb_unlimited_items_kit",
					}
				},
				{
					2,
					mode = "hide",
					options = {
						"cb_unlimited_items_bomb",
						"cb_unlimited_items_potion",
						"cb_unlimited_items_kit",
					}
				},
				{
					3,
					mode = "show",
					options = {
						"cb_unlimited_items_bomb",
						"cb_unlimited_items_potion",
						"cb_unlimited_items_kit",
					}
				},
			},
		},
		BOMB = {
			["save"] = "cb_unlimited_items_bomb",
			["widget_type"] = "checkbox",
			["text"] = "Bomb",
			["default"] = false,
		},
		POTION = {
			["save"] = "cb_unlimited_items_potion",
			["widget_type"] = "checkbox",
			["text"] = "Potion",
			["default"] = false,
		},
		KIT = {
			["save"] = "cb_unlimited_items_kit",
			["widget_type"] = "checkbox",
			["text"] = "Med Kit",
			["default"] = false,
		},
		HK_TOGGLE = {
			["save"] = "cb_unlimited_items_hotkey_toggle",
			["widget_type"] = "keybind",
			["text"] = "Toggle",
			["default"] = {
				"u",
				oi.key_modifiers.CTRL_SHIFT,
			},
			["exec"] = {"UnlimitedItems", "action/no_item_use"},
		},
	},
}
local me = UnlimitedItems

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Option #######################################################################################################
-- ####################################################################################################################
UnlimitedItems.create_options = function()
	Mods.option_menu:add_item("items", me.SETTINGS.ACTIVE, true)
	Mods.option_menu:add_item("items", me.SETTINGS.MODE)
	Mods.option_menu:add_item("items", me.SETTINGS.BOMB)
	Mods.option_menu:add_item("items", me.SETTINGS.POTION)
	Mods.option_menu:add_item("items", me.SETTINGS.KIT)
	Mods.option_menu:add_item("items", me.SETTINGS.HK_TOGGLE, true)
end
	
-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "GenericAmmoUserExtension.use_ammo", function(func, self, ammo_used)
	local mode = get(me.SETTINGS.MODE.save)
	if mode and mode > 1 then
		local potion = get(me.SETTINGS.POTION.save)
		local bomb = get(me.SETTINGS.BOMB.save)
		local kit = get(me.SETTINGS.KIT.save)
		if (string.find(self.item_name, "potion") and (mode == 2 or (mode == 3 and potion)) or
			string.find(self.item_name, "grenade") and (mode == 2 or (mode == 3 and bomb)) or
			string.find(self.item_name, "healthkit") and (mode == 2 or (mode == 3 and kit))) then
			ammo_used = 0
		end
	end
	
	func(self, ammo_used)
end)

set(me.SETTINGS.MODE.save, 1)
save()

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()