local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

pcall(function()
	if get(UnlimitedItems.SETTINGS.MODE.save) ~= 2 then
		set(UnlimitedItems.SETTINGS.MODE.save, 2)
		EchoConsole("Unlimited Items All")
	else
		set(UnlimitedItems.SETTINGS.MODE.save, 1)
		EchoConsole("Unlimited Items Off")
	end
	save()
end)