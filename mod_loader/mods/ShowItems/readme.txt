Adds the ability to display icons above items, including items inside chests.
Go to Mod Settings -> Items -> Show Item Icons to tweak settings to your liking.
Changelog:
v1.0.1
	- Fixed grim title in options