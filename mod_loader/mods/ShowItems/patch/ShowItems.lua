local mod_name = "ShowItems"

local oi = OptionsInjector

ShowItems = {
	SETTINGS = {
		MODE = {
			["save"] = "cb_show_items_mode",
			["widget_type"] = "dropdown",
			["text"] = "Show Item Icons",
			["tooltip"] = "Show Items\n" ..
				"Shows icons above items.\n\n" ..
				"-- OFF --\n" ..
				"No Item icons will be visable.\n\n" ..
				"-- ALL --\n" ..
				"All items icons will be visable\n\n" ..
				"-- CUSTOM --\n" ..
				"Selected items will be only be visable",
			["value_type"] = "number",
			["options"] = {
				{text = "Off", value = 1},
				{text = "All", value = 2},
				{text = "Custom", value = 3},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
			["hide_options"] = {
				{
					1,
					mode = "hide",
					options = {
						"cb_show_items_range",
						"cb_show_items_size",
						"cb_show_items_custom_health",
						"cb_show_items_custom_potion",
						"cb_show_items_custom_grenade",
						"cb_show_items_custom_grim",
						"cb_show_items_custom_tome",
						"cb_show_items_custom_lorebook_page",
					}
				},
				{
					2,
					mode = "hide",
					options = {
						"cb_show_items_custom_health",
						"cb_show_items_custom_potion",
						"cb_show_items_custom_grenade",
						"cb_show_items_custom_grim",
						"cb_show_items_custom_tome",
						"cb_show_items_custom_lorebook_page",
					}
				},
				{
					2,
					mode = "show",
					options = {
						"cb_show_items_range",
						"cb_show_items_size",
					}
				},
				{
					3,
					mode = "show",
					options = {
						"cb_show_items_range",
						"cb_show_items_size",
						"cb_show_items_custom_health",
						"cb_show_items_custom_potion",
						"cb_show_items_custom_grenade",
						"cb_show_items_custom_grim",
						"cb_show_items_custom_tome",
						"cb_show_items_custom_lorebook_page",
					}
				},
			},
		},
		RANGE = {
			["save"] = "cb_show_items_range",
			["widget_type"] = "slider",
			["text"] = "Distance",
			["tooltip"] = "Distance\n" ..
				"Set the maximum distance to show items.",
			["range"] = {2, 1000},
			["default"] = 100,
		},
		SIZE = {
			["save"] = "cb_show_items_size",
			["widget_type"] = "slider",
			["text"] = "Icon Size",
			["tooltip"] = "Icon Size\n" ..
				"Set the size of the icons.",
			["range"] = {16, 128},
			["default"] = 64,
		},
		CUSTOM = {
			HEALTH = {
				["save"] = "cb_show_items_custom_health",
				["widget_type"] = "checkbox",
				["text"] = "Health",
				["default"] = false,
			},
			POTION = {
				["save"] = "cb_show_items_custom_potion",
				["widget_type"] = "checkbox",
				["text"] = "Potion",
				["default"] = false,
			},
			GRENADE = {
				["save"] = "cb_show_items_custom_grenade",
				["widget_type"] = "checkbox",
				["text"] = "Grenade",
				["default"] = false,
			},
			GRIM = {
				["save"] = "cb_show_items_custom_grim",
				["widget_type"] = "checkbox",
				["text"] = "Grimoire",
				["default"] = false,
			},
			TOME = {
				["save"] = "cb_show_items_custom_tome",
				["widget_type"] = "checkbox",
				["text"] = "Tome",
				["default"] = false,
			},
			LOREBOOK_PAGE = {
				["save"] = "cb_show_items_custom_lorebook_page",
				["widget_type"] = "checkbox",
				["text"] = "Lorebook Page",
				["default"] = false,
			},
		}
	},
	
	items = {
		healing_draught = {
			icon = "consumables_potion_01_lit",
			setting = "HEALTH"
		},
		first_aid_kit = {
			icon = "consumables_medpack_lit",
			setting = "HEALTH"
		},
		damage_boost_potion = {
			icon = "consumables_strength_lit",
			setting = "POTION"
		},
		speed_boost_potion = {
			icon = "consumables_speed_lit",
			setting = "POTION"
		},
		fire_grenade_t1 = {
			icon = "consumables_fire_lit",
			setting = "GRENADE"
		},
		fire_grenade_t2 = {
			icon = "consumables_fire_lit",
			setting = "GRENADE"
		},
		frag_grenade_t1 = {
			icon = "consumables_frag_lit",
			setting = "GRENADE"
		},
		frag_grenade_t2 = {
			icon = "consumables_frag_lit",
			setting = "GRENADE"
		},
		smoke_grenade_t1 = {
			icon = "consumables_smoke_lit",
			setting = "GRENADE"
		},
		smoke_grenade_t2 = {
			icon = "consumables_smoke_lit",
			setting = "GRENADE"
		},
		tome = {
			icon = "consumables_book_lit",
			setting = "TOME"
		},
		grimoire = {
			icon = "consumables_grimoire_lit",
			setting = "GRIM"
		},
		lorebook_page = {
			icon = "consumables_book_lit",
			setting = "LOREBOOK_PAGE"
		},
	}
}
local me = ShowItems

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Options ######################################################################################################
-- ####################################################################################################################
ShowItems.create_options = function()
	Mods.option_menu:add_item("items", me.SETTINGS.MODE, true)
	Mods.option_menu:add_item("items", me.SETTINGS.CUSTOM.HEALTH)
	Mods.option_menu:add_item("items", me.SETTINGS.CUSTOM.POTION)
	Mods.option_menu:add_item("items", me.SETTINGS.CUSTOM.GRENADE)
	Mods.option_menu:add_item("items", me.SETTINGS.CUSTOM.TOME)
	Mods.option_menu:add_item("items", me.SETTINGS.CUSTOM.GRIM)
	Mods.option_menu:add_item("items", me.SETTINGS.CUSTOM.LOREBOOK_PAGE)
	Mods.option_menu:add_item("items", me.SETTINGS.RANGE)
	Mods.option_menu:add_item("items", me.SETTINGS.SIZE)
end

local create_icon = function(unit, pickup_name)
	local player = Managers.player:local_player()
	local world = Managers.world:world("level_world")
	local viewport = ScriptWorld.viewport(world, player.viewport_name)
	local camera = ScriptViewport.camera(viewport)
	
	local item_settings = me.items[pickup_name]
	
	if get(me.SETTINGS.MODE.save) == 1 or item_settings == nil then
		return
	end
	
	local setting = me.SETTINGS.CUSTOM[item_settings.setting]
	if get(me.SETTINGS.MODE.save) == 3 and get(setting.save) == false then
		return
	end
	
	if POSITION_LOOKUP[unit] and POSITION_LOOKUP[player.player_unit] then
		local distance = Vector3.distance(POSITION_LOOKUP[unit], POSITION_LOOKUP[player.player_unit])
		local position2d, depth = Camera.world_to_screen(camera, POSITION_LOOKUP[unit])
		
		if distance > 2 and distance < get(me.SETTINGS.RANGE.save) and depth < 1 then
			local pos = Vector3(position2d[1], position2d[2], 200)
			
			safe_pcall(function()
				local size = Vector2(get(me.SETTINGS.SIZE.save), get(me.SETTINGS.SIZE.save))
				Mods.gui.draw_icon(item_settings.icon, pos, nil, size)
			end)
		end
	end
end

-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "OutlineSystem.update", function(func, self, ...)
	if Managers.matchmaking.ingame_ui.hud_visible then
		for _, unit in pairs(self.units) do
			if ScriptUnit.has_extension(unit, "pickup_system") then
				local pickup_system = ScriptUnit.extension(unit, "pickup_system")
				
				create_icon(unit, pickup_system.pickup_name)
			end
		end
	end
	
	func(self, ...)
	
	return
end)

Mods.hook.set(mod_name, "OutlineSystem.outline_unit", function(func, ...)
	pcall(func, ...)
	
	return 
end)

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()