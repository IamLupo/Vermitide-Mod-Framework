local mod_name = "MissionControl"

local oi = OptionsInjector

MissionControl = {
	SETTINGS = {
		END_MISSION = {
			["save"] = "cb_cheats_end_mission",
			["widget_type"] = "checkbox",
			["text"] = "End Mission",
			["default"] = false,
			["hide_options"] = {
				{
					false,
					mode = "hide",
					options = {
						"cb_cheats_hotkey_win",
						"cb_cheats_hotkey_win_modifiers",
						"cb_cheats_hotkey_win_grim",
						"cb_cheats_hotkey_win_grim_modifiers",
						"cb_cheats_hotkey_fail",
						"cb_cheats_hotkey_fail_modifiers",
						
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_cheats_hotkey_win",
						"cb_cheats_hotkey_win_modifiers",
						"cb_cheats_hotkey_win_grim",
						"cb_cheats_hotkey_win_grim_modifiers",
						"cb_cheats_hotkey_fail",
						"cb_cheats_hotkey_fail_modifiers",
					}
				},
			},
		},
		HK_WIN = {
			["save"] = "cb_cheats_hotkey_win",
			["widget_type"] = "keybind",
			["text"] = "Win",
			["default"] = {
				"f1",
				oi.key_modifiers.CTRL_SHIFT,
			},
			["exec"] = {"MissionControl", "action/win"},
		},
		HK_WIN_GRIM = {
			["save"] = "cb_cheats_hotkey_win_grim",
			["widget_type"] = "keybind",
			["text"] = "Win with 7 Grims",
			["default"] = {
				"f2",
				oi.key_modifiers.CTRL_SHIFT,
			},
			["exec"] = {"MissionControl", "action/win_7_grim"},
		},
		HK_FAIL = {
			["save"] = "cb_cheats_hotkey_fail",
			["widget_type"] = "keybind",
			["text"] = "Fail",
			["default"] = {
				"f3",
				oi.key_modifiers.CTRL_SHIFT,
			},
			["exec"] = {"MissionControl", "action/fail"},
		},
	},
}

MissionControl.create_options = function()	
	Mods.option_menu:add_item("cheats", MissionControl.SETTINGS.END_MISSION, true)
	Mods.option_menu:add_item("cheats", MissionControl.SETTINGS.HK_WIN)
	Mods.option_menu:add_item("cheats", MissionControl.SETTINGS.HK_WIN_GRIM)
	Mods.option_menu:add_item("cheats", MissionControl.SETTINGS.HK_FAIL)
end
MissionControl.create_options()