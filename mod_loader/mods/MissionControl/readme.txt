Adds keyboard shortcuts and chat commands to instantly win and lose missions.
Keyboard shortcuts (default):
	Ctrl + Shift + F1 - instantly win the mission
	Ctrl + Shift + F2 - instantly win the mission with 7 grims
	Ctrl + Shift + F3 - instantly lose the mission
Go to Mod Settings -> Cheats -> Mission Control to change shortcuts.
Chat commands:
	/win - instantly win the mission
	/superwin - instantly win the mission with 7 grims
	/concede or /fail - instantly lose the mission