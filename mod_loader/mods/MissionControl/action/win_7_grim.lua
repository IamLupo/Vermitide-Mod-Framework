if Managers.state.game_mode._level_key ~= "inn_level" then
	local mission_system = Managers.state.entity:system("mission_system")
	
	-- add 6 grims
	for i = 1, 7 do
		mission_system.request_mission(mission_system, "grimoire_hidden_mission", nil, Network.peer_id())
		mission_system.update_mission(mission_system, "grimoire_hidden_mission", true, nil, Network.peer_id(), nil, true)
	end
	
	Managers.state.game_mode:complete_level()
	
	EchoConsole("Win the Game with 7 Grims")
end