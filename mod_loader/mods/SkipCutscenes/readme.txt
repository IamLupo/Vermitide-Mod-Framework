Allows you to skip cutscenes by pressing any button.
Go to Mod Settings -> System -> Skip Level Cutscenes to toggle.