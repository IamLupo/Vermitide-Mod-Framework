local mod_name = "Melting"
--[[
	Patch Melting:
		This allows me to melt promo items in the forge
--]]

Mods.hook.set(mod_name, "ForgeView.activate_melt_view", function(func, self, ignore_sound)
	-- Call Orginal
	func(self, ignore_sound)
	
	-- Added Promo is accepted
	local ui_pages = self.ui_pages
	local items_page = ui_pages.items
	local accepted_rarities = {
		common = true,
		plentiful = true,
		exotic = true,
		rare = true,
		unique = true,
		promo = true
	}
	
	items_page:clear_disabled_backend_ids()
	
	local item_filter = "slot_type == trinket or slot_type == melee or slot_type == ranged or slot_type == hat"
	self:_apply_item_filter(item_filter, true)
	
	items_page:set_accepted_rarities(accepted_rarities)
	items_page:set_rarity(nil)
	
end)

--- ##### ScriptBackendItem_is_salvageable ##### ----
Mods.hook.set(mod_name, "ScriptBackendItem.is_salvageable", function(func, backend_id)
	local unequipped = not ScriptBackendItem.is_equipped(backend_id)

	return unequipped
end)

--- ##### Promo Settings ##### ----
ForgeSettings.melt_reward.promo = {
	token_texture = "token_icon_04",
	min = 0,
	token_type = "gold_tokens",
	max = 0
}

VaultForgeMeltKeyTable = {}
for key, val in pairs(ForgeSettings.melt_reward) do
	local min_max = {
		min = "melt_reward." .. key .. ".min",
		max = "melt_reward." .. key .. ".max"
	}
	VaultForgeMeltKeyTable[key] = min_max

	Vault.deposit_single(min_max.min, val.min)
	Vault.deposit_single(min_max.max, val.max)
end