--[[ WE EXPORT MELEE
	Export function for melee weapons
--]]

local findAttackSequences = Mods.require(WE.name, "function/findAttackSequences")
local findRefireDelay = Mods.require(WE.name, "function/findRefireDelay")

local function saveMelee(heroesString, hero, weapName, templateName, weapType, attack, kind, maxTargets, stats, stamina, dodge, push, movement, attackSpeed)
	local entry = {}
	entry.name = weapName
	entry.hero = hero
	entry.heroesString = heroesString
	entry.template = templateName
	entry.type = weapType
	entry.attack = attack
	entry.attackKind = kind
	entry.maxTargets = maxTargets
	entry.stats = stats

	if(WE.settings.attackFormat == "short") then
		entry.attackLocalized = WE.attackTypesShort[attack] or attack
	elseif(WE.settings.attackFormat == "long") then
		entry.attackLocalized = WE.attackTypesLong[attack] or attack
	else
		entry.attackLocalized = attack
	end 

	entry.stamina = stamina
	entry.dodge = dodge
	entry.push = push
	entry.movement = movement
	entry.attackSpeed = attackSpeed
	return entry 
end

local function exportMelee(templateName, weapon, names, heroes, heroesString, dodge, movement)

	--Actions that the weapon has
	local attacks = {}
	attacks = table.create_copy(attacks, weapon.actions.action_one)

	--Add special actions to our list of attacks
	if weapon.actions.action_three then
		for k,v in pairs(weapon.actions.action_three) do
			attacks["special_" .. k] = table.create_copy(attacks["special_" .. k], v)
			attacks["special_" .. k].original_name = k
		end
	end

	--Stamina
	local stamina = weapon.max_fatigue_points

	--Setting weapon type for that one weapon where they didn't specify what it is
	local weaponType = 2

	--Getting values of the push
	local push = WE.get.stats(attacks.push.attack_template, "no_damage", nil, false)	
	push.radius = attacks.push.push_radius
	push.template = attacks.push.attack_template

	--Get a list of attacks that are possible after a push
	push.chainActions = {}
	for k,action in pairs(attacks.push.allowed_chain_actions) do
	 	table.insert(push.chainActions,action.sub_action)
	 end 

	--Creating the sequences of attacks of the weapon
	findAttackSequences(templateName, attacks)

	--Iterating through all actions to get the needed information
	for attackName,attack in pairs(attacks) do

		--We only need actions that aren't dummy actions or pushes (i.e. actual attacks)
		if(attack.kind ~= "dummy" and attackName ~= "push") then	

			--Find refire delay
			local attackSpeed = nil
			local storage = WE.meleeAttackPreTime[templateName]
			if storage and weapon.actions["action_one"][attackName] then
				WE.writeDebug("Refire", "\n" .. names[1] .. " " .. "action_one" .. " " .. attackName, true)
				WE.writeDebug("PrepTimes", "\n" .. names[1] .. " " .. "action_one" .. " " .. attackName, true)
				attackSpeed = findRefireDelay(weapon.actions, "action_one", attackName, storage)
				if not attackSpeed then
					attackSpeed = findRefireDelay(weapon.actions, "action_one", attackName)
				end
			elseif attack.original_name then
				WE.writeDebug("Refire", "\n" .. names[1] .. " " .. "action_three" .. " " .. attackName, true)
				attackSpeed = findRefireDelay(weapon.actions, "action_three", attack.original_name)
			else
				attackSpeed = "N/A"
			end

			--Figuring out maximum amount of targets
			--max_penetrations is for WH's off-hand pistol on Rapier
			local maxPenetrations = nil
			if(attack.max_penetrations) then
				maxPenetrations = attack.max_penetrations + 1
			end
			local maxTargets = tonumber(attack.max_targets) or tonumber(attack.max_penetrations) or "∞"
			if(tostring(maxTargets) == "1.#INF") then
				maxTargets = "∞"
			end

			--Creating a table for all info about current attack
			local stats = {}

			--First we see if the info is stored in the attack itself
			if(attack.attack_template) then	
				stats[1] = WE.get.stats(attack.attack_template,attack.attack_template_damage_type,attack.kind,false)
				if(maxTargets == "∞") then
					for i=1,WE.constants.MAX_TARGETS do									
						stats[i + 1] = WE.get.stats(attack.attack_template,"no_damage",attack.kind,false)
					end	
				end
			--Otherwise, the info is stored in targets and default_targets
			else							
				--Check if targets exist
				local hasTargets = attack.targets and table.getn(attack.targets) > 0

				--If they do, we get the info about them
				if(hasTargets) then
					for tn,target in pairs(attack.targets) do
						--We only need the info about targets that don't exceed our max target amount
						if((type(maxTargets) == "number" and maxTargets - table.getn(stats) > 0) or maxTargets == "∞") then
							table.insert(stats, WE.get.stats(target.attack_template,target.attack_template_damage_type,attack.kind,false))
						end											
					end
				end

				--Now we get info about the default target. The default target is used for all targets for which a target isn't specified
				--We only need the info about targets that don't exceed our max target amount
				if((type(maxTargets) == "number" and maxTargets - table.getn(stats) > 0) or maxTargets == "∞") then
					
					--We only show up to MAX_TARGETS targets, with MAX_TARGETS+1 meaning all targets after MAX_TARGETS
					local to
					if(maxTargets == "∞") then
						to = WE.constants.MAX_TARGETS
					else
						to = maxTargets - 1
					end			

					for i=table.getn(stats),to do									
						stats[i + 1] = WE.get.stats(attack.default_target.attack_template,attack.default_target.attack_template_damage_type,attack.kind,false)
					end																		
				end
			end	

			--Now we put all of the info into a table, once for each hero this weapon can be used by
			for k,hero in pairs(heroes) do
				local meleeWeapon = saveMelee(heroesString, hero, names[k],templateName,weaponType,attackName,attack.kind,maxTargets,stats,stamina, dodge, push, movement, attackSpeed)
				table.insert(WE.meleeWeapons, meleeWeapon)								
				table.insert(WE.combinedWeapons, meleeWeapon)								
			end
		end
	end

end

return exportMelee