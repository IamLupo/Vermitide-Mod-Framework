--[[ WE EXPORT RANGED
	Export function for ranged weapons
--]]

local findRefireDelay = Mods.require(WE.name, "function/findRefireDelay")
local function saveRanged(heroesString, hero, weapName, templateName, weapType, action, attack, kind, stats, ammoData, dodge, movement, aoe, timedAoe)
	local entry = {}
	entry.name = weapName
	entry.hero = hero
	entry.heroesString = heroesString
	entry.template = templateName
	entry.type = weapType
	entry.action = action
	entry.attack = attack
	entry.attackKind = kind
	entry.stats = stats
	if not WE.rangedNamesLocalized[weapName] or not WE.rangedNamesLocalized[weapName][action] then
	EchoConsole("Localization not found: " .. weapName .. " " .. action .. " " .. attack)
	end
	if(WE.settings.attackFormat == "short" or WE.settings.attackFormat == "long") then
		entry.attackLocalized = WE.rangedNamesLocalized[weapName] and WE.rangedNamesLocalized[weapName][action] and WE.rangedNamesLocalized[weapName][action][attack] or attack
		if entry.attackLocalized ~= attack and WE.rangedNamesLocalized[weapName][action][attack .. "_alt"] then
			entry.attackLocalizedAlt = WE.rangedNamesLocalized[weapName][action][attack .. "_alt"]
		end
	else
		entry.attackLocalized = attack
	end 

	entry.ammoData = ammoData
	entry.dodge = dodge
	entry.movement = movement
	entry.aoe = aoe
	entry.timedAoe = timedAoe
	return entry 
end

--Generates stats for conflag staff
local function generateManyStats(actions, stats, attackTemplateList, kind, ff)

	if not attackTemplateList then return nil end
	
	--Get radius from the targeting action
	local targetingAction = actions.action_two.default
	local radiusMin = targetingAction.min_radius
	local radiusMax = targetingAction.max_radius
	local manyStats = {}
	stats.split = true

	--Get new stats for each of the AoE attack's part
	for i,templateName in pairs(attackTemplateList) do
		manyStats[i] = WE.get.stats(templateName, "no_damage", kind, ff)
		manyStats[i].overcharge = stats.overcharge
		manyStats[i].index = i
		manyStats[i].split = true
		manyStats.templateName = templateName
		manyStats[i].radiusMax = {}
		manyStats[i].radiusMin = {}

		--Determine the radius of each part of AoE (unused)
		if(i == 1) then
			manyStats[i].radiusMax.max = radiusMax
			manyStats[i].radiusMax.min = radiusMax * 0.75
			manyStats[i].radiusMin.max = radiusMin
			manyStats[i].radiusMin.min = radiusMin * 0.75
		elseif(i == 2) then
			manyStats[i].radiusMax.max = radiusMax * 0.75
			manyStats[i].radiusMax.min = radiusMax * 0.25
			manyStats[i].radiusMin.max = radiusMin * 0.75
			manyStats[i].radiusMin.min = radiusMin * 0.25
		else
			manyStats[i].radiusMax.max = radiusMin * 0.25
			manyStats[i].radiusMax.min = 0
			manyStats[i].radiusMin.max = radiusMin * 0.25
			manyStats[i].radiusMin.min = 0
		end

		--Total radius
		manyStats[i].radiusMax.value = radiusMax
		manyStats[i].radiusMin.value = radiusMin
	end
	return manyStats
end

local function exportRanged(templateName, weapon, names, heroes, heroesString, dodge, movement)
	--Weapon type is ranged
	local weaponType = 3

	local actions = weapon.actions

	--Going through each action and attack
	for actionName,action in pairs(actions) do
		for attackName,attack in pairs(action) do

			local ammo = {}

			--Attack archetype
			local kind = attack.kind

			--Is a valid ranged attack
			if WE.rangedAttacks[kind] then

				--Ammo data
				local ammoData = weapon.ammo_data							
				if(ammoData and kind ~="shield_slam") then

					ammo.amount = ammoData.max_ammo
					ammo.clipSize = ammoData.ammo_per_reload or ammoData.ammo_per_clip or ammoData.max_ammo
					ammo.reloadTime = ammoData.reload_time
					ammo.isSingleClip = ammoData.single_clip or false
					ammo.ammoUsage = attack.num_projectiles
					--No reload time for WH's pistols
					if(ammoData.ammo_immediately_available) then
						ammo.reloadTime = 0
					end
				else
					ammoData = nil
				end

				--Active reload
				ammo.noActiveReload = attack.no_active_reload --Don't think this exists anymore
				ammo.activeReloadTime = attack.active_reload_time or "-"

				--Attack and damage templates
				local attackTemplate = attack.attack_template
				local damageTemplate = attack.attack_template_damage_type or "no_damage"
				local attackTemplateList = attack.attack_template_list

				local maxPenetrations = nil
				local maxTargets = nil
				local lifeSpan = nil

				local stats = nil
				local manyStats = nil
				local aoe = nil
				local aoeName = attack.aoe_name
				local timedAoe = nil							

				--Projectile info
				local projectileInfo = attack.projectile_info
				if(projectileInfo) then	

					--What happens upon impact
					local impactData = projectileInfo.impact_data

					--What happens at the end of lifespan
					local timedData = projectileInfo.timed_data

					--Damage and attack template of the projectile
					if(impactData.damage) then
						local target = impactData.damage.enemy_unit_hit.default_target
						attackTemplate = target.attack_template
						damageTemplate = target.attack_template_damage_type
					end

					maxTargets = impactData.targets

					--Projectile AoE
					local aoeTemplate = impactData.aoe
					if(aoeTemplate) then
						aoeTemplate = aoeTemplate.explosion or aoeTemplate.aoe
						if(aoeTemplate) then
							aoe = WE.get.aoe(aoeTemplate)
						else
							EchoConsole("Unexpected type of aoe")
						end
					end

					--Timed AoE
					--We only need lifeSpan as timed aoe is so rarely comes in play it's more confusing than useful information 
					if(timedData) then
						lifeSpan = timedData.life_time
						-- if(timedData.aoe) then
						-- 	local timedAoeTemplate = timedData.aoe.explosion or timedData.aoe.aoe
						-- 	if(timedAoeTemplate) then
						-- 		timedAoe = WE.get.aoe(timedAoeTemplate)
						-- 	else
						-- 		EchoConsole("Unexpected type of timed aoe")
						-- 	end
						-- end
					end
				end

				--AoE
				if(aoeName) then
					local aoeTemplate = ExplosionTemplates[aoeName]
					if(aoeTemplate) then
						aoeTemplate = aoeTemplate.explosion or aoeTemplate.aoe
						if(aoeTemplate) then
							aoe = WE.get.aoe(aoeTemplate)
						else
							EchoConsole("Unexpected type of aoe")
						end
					else

						EchoConsole("Couldn't find aoe")
					end
				end

				--Friendly fire
				local ff = (kind ~= "shield_slam")

				--Get stats for chosen attack and damage templates 
				if(attackTemplate) then
					stats = WE.get.stats(attackTemplate, damageTemplate, kind, ff)

					--Find refire delay
					WE.writeDebug("Refire", "\n" .. names[1] .. " " .. actionName .. " " .. attackName, true)
					ammo.refireDelay = findRefireDelay(actions, actionName, attackName)
				end

				if not stats then
					EchoConsole("No attack template for " .. names[1] .. " " .. actionName .. " " .. attackName)
					stats = {}
				end

				--Overcharge
				stats.overcharge = WE.get.overcharge(weapon, action, attack, actionName, attackName)

				--Refire delay for beam
				if stats.overcharge and stats.overcharge.damageInterval then
					ammo.refireDelay = stats.overcharge.damageInterval
				end

				--Bullshit for conflag staff
				local manyStats = generateManyStats(actions, stats, attackTemplateList, kind, ff)

				--Max amount of penetrations
				maxPenetrations = attack.max_penetrations or 0	

				--Range, shots per fire and save lifespan
				stats.range = attack.range
				stats.shots = attack.shot_count or attack.num_projectiles or not not ammo.amount and 1 or "-" --ammo.amount ~= nil -> 1
				stats.lifeSpan = lifeSpan or "-"

				--Wether or not the attack uses all ammo upon firing
				ammo.usesAll = attack.special_ammo_thing or ammo.ammoUsage == stats.shots

				--Archetype-specific stuff
				if(kind == "bullet_spray") then
					stats.range = WE.constants.SPRAY_RANGE
					maxTargets = WE.constants.BULLET_SPRAY_MAX_TARGETS
				elseif(not stats.range and stats.lifeSpan == "-" and kind ~= "shield_slam") then
					stats.range = "∞"
				elseif(kind == "shield_slam") then
					stats.range = attack.push_radius
					maxPenetrations = "-"
					stats.shots = "-"
					maxTargets = "∞"
				elseif not(stats.range) then
					stats.range = "-"
				end

				if(kind == "geiser") then
					stats.damages = nil
					maxTargets = "∞"
					maxPenetrations = nil
					stats.dotDamages = aoe and aoe.dotDamages
					stats.dotProps = aoe and aoe.dotProps
					stats.range = "-"
					stats.lifeSpan = "-"
				end

				--Adding target counts to stats
				stats.maxPenetrations = maxPenetrations
				stats.maxTargets = maxTargets

				--Saving data
				for k,hero in pairs(heroes) do

					--Conflag bullshit AoE
					if(manyStats) then
						for _,stat in ipairs(manyStats) do
							if(stat.index) then
								local rangedWeapon = saveRanged(heroesString, hero, names[k], templateName, weaponType, actionName, attackName .. stat.index, kind, stat, ammo, dodge, movement, aoe, timedAoe)
								table.insert(WE.rangedWeapons, rangedWeapon)	
								table.insert(WE.combinedWeapons, rangedWeapon)	
							end
						end
						local firepatchStats = {}
						firepatchStats = table.create_copy(firepatchStats, stats)
						firepatchStats.overcharge.cost = firepatchStats.overcharge.costHeavy
						firepatchStats.overcharge.shouldScale = false
						firepatchStats.damages = {}
						-- local firepatchAoe = {}
						-- firepatchAoe = table.create_copy(firepatchAoe, aoe)
						local rangedWeapon = saveRanged(heroesString, hero, names[k], templateName, weaponType, actionName, "aoe_firepatch", kind, firepatchStats, ammo, dodge, movement, aoe, timedAoe)
						table.insert(WE.rangedWeapons, rangedWeapon)
						table.insert(WE.combinedWeapons, rangedWeapon)	
					--Everything else
					else
						local rangedWeapon = saveRanged(heroesString, hero, names[k], templateName, weaponType, actionName, attackName, kind, stats, ammo, dodge, movement, aoe, timedAoe)
						table.insert(WE.rangedWeapons, rangedWeapon)
						table.insert(WE.combinedWeapons, rangedWeapon)		
					end
				end	
			end
		end
	end
end

return exportRanged