--[[ WE EXPORT WEAPONS
	Base function for exporting weapon info
	Looks for values applicable to both ranged and melee weapons and passes them to corresponding functions
--]]

local exportMelee = Mods.require(WE.name, "export/exportMelee")
local exportRanged = Mods.require(WE.name, "export/exportRanged")
local writeRangedTree = Mods.require(WE.name, "write/writeRangedTree")

-- Which weapons to extract values of
local function isRightWeapon(templateName)
	
	local rarity = WE.settings.rarity
	
	--Exotic and veteran
	if rarity == "exotic" or rarity == "veteran" or rarity == "orange" or rarity == "red" then
		return string.find(templateName, "t3") and string.find(templateName, "_un") == nil 

	--Rare
	elseif rarity == "rare" or rarity == "blue" then
		return string.find(templateName, "t2")
	
	--Green and white (can fail)
	elseif rarity == "common" or rarity == "plentiful" or rarity == "white" or rarity == "green" then
		return (string.find(templateName, "_un") == nil and string.find(templateName, "_co") == nil and string.find(templateName, "_t3") == nil and string.find(templateName, "_t2") == nil)
	
	--Incorrect rarity
	else
		return false
	end
end

local function exportWeapons(templateName, weapon)

	--Grenades
	if weapon.wield_anim == "to_grenade" then

		--Remove tier tag from grenade names since all tiers are the same
		local tier = templateName:match('([_][^_]+)$')
		if tier then
			local name = string.gsub(templateName, tier, "")
			if not WE.grenades[name] then

				local projInfo = weapon.actions.action_one.throw.projectile_info
				local impactData = projInfo.impact_data

				local explosion = WE.get.aoe(impactData.aoe.explosion)
				local aoe = WE.get.aoe(impactData.aoe.aoe)

				if explosion or aoe then
					local grenade = {}
					grenade.name = name
					grenade.aoe = aoe
					grenade.explosion = explosion
					WE.grenades[name] = grenade
				end
			end
		end

	--Selecting only weapons of required rarity
	elseif isRightWeapon(templateName) then		

		--Weapon names for each hero
		local names = {}
		--All hero names as a string (unused)
		local heroesString = ""
		--Hero names
		local heroes = {}

		--Finding the names of the weapon
		local isFirstName = true
		for k,v in pairs(ItemMasterList) do
			if(v.template == templateName and not table.indexOf(heroes,Localize(v.can_wield[1]))) then
				table.insert(names, Localize(v.item_type))
				table.insert(heroes, Localize(v.can_wield[1]))
				if(isFirstName) then
					isFirstName = false
					heroesString = Localize(v.can_wield[1])
				else
					heroesString = heroesString .. ", " .. Localize(v.can_wield[1])
				end
			end
		end

		local isUnavailable = table.indexOf(WE.unavailableWeapons, names[1])

		--If a name is found, it's a weapon available in game (or flamewave)
		if 
			not WE.settings.shouldWriteUnavailabe and names[1] and not isUnavailable or 
			WE.settings.shouldWriteUnavailabe and (not names[1] or isUnavailable)
		then	

			if not names[1] then
				names[1] = templateName
				heroes[1] = "N/A"
				heroesString = "N/A"
			end

			--*Per-weapon values as oppoed to per-attack values*--

			--Dodge-related stuff
			local dodge = {}
			dodge.distance = weapon.dodge_distance or 1
			dodge.speed = weapon.dodge_speed or 1
			dodge.count = weapon.dodge_count or 2

			--Movement with weapons (only speed when blocking/chargin/aiming is saved for now)
			local movement = {}
			local chargeZoom = weapon.actions.action_two and weapon.actions.action_two.default
			
			if(chargeZoom and chargeZoom.kind ~= "shield_slam" and chargeZoom.buff_data) then
				movement.chargeZoomSlowdown = chargeZoom.buff_data[1].external_multiplier
			end

			--Weapon type
			local weaponType = nil
			weaponType = weapon.buff_type

			--*/*--

			--Melee
			if(weaponType == 2 or names and table.indexOf(names, "Two-handed Sword")) then

				--Melee export
				exportMelee(templateName, weapon, names, heroes, heroesString, dodge, movement)

			--Ranged
			else
				--Ranged export
				exportRanged(templateName, weapon, names, heroes, heroesString, dodge, movement)

				--Ranged tree write
				writeRangedTree(names, templateName, weapon.actions)

			end	
		end	
	end
end

return exportWeapons