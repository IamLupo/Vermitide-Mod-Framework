--[[ WE EXPORT ITEMS
	Export function for items
--]]

local function exportItem(key, obj, items, kinds)

	--List of item types for debug
	if not kinds[obj.item_type] then 
		kinds[obj.item_type] = 1
	else
		kinds[obj.item_type] = kinds[obj.item_type] + 1
	end

	--Item strings
	local item = {}
	item.canWield = obj.can_wield
	item.name = obj.display_name
	item.description = obj.description
	item.rarity = obj.rarity
	item.kind = obj.item_type
	item.key = key
	item.traits = obj.traits

	--Type
	if obj.template and Weapons[obj.template] and obj.rarity then
		item.type = "weapon"
	elseif obj.item_type == "trinket" then
		item.type = "trinket"
	elseif obj.item_type == "hat" then
		item.type = "hat"
	else
		item.type = "unknown"
	end

	--Localized strings
	item.localized = {}

	item.localized.name = item.name and Localize(item.name)

	item.localized.rarity = item.rarity and WE.raritiesLocalized[item.rarity]

	item.localized.description = item.description and Localize(item.description)

	item.localized.kind = item.kind and Localize(item.kind)

	if item.canWield then
		item.localized.canWield = {}
		for i,v in ipairs(item.canWield) do
			table.insert(item.localized.canWield, Localize(v))
		end
	end

	if item.traits then
		item.localized.traits = {}
		for i,v in ipairs(item.traits) do
			table.insert(item.localized.traits, Localize(BuffTemplates[v].display_name))
		end
	end

	table.insert(items, item)
end

return exportItem