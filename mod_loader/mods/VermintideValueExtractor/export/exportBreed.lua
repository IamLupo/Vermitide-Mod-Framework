--[[ WE EXPORT BREEDS
	Export function for breed info
--]]

local function exportBreed(breedTag)
	local breed = Breeds[breedTag]
	local breedName = WE.breedNames[breedTag]
	if(breed) then		
		local poisonResistance = breed.poison_resistance or 0
		local armorCategory = WE.armorTypes[breed.armor_category]
		local hp = breed.max_health
		WE.write("Breeds", breedName .. ";" .. armorCategory  .. ";" .. poisonResistance .. ";")	
		for k,health in pairs(hp) do
			WE.write("Breeds", health .. ";")					
		end
		WE.write("Breeds", "\n")
	else
		EchoConsole("Wrong breed name!")
	end
end

return exportBreed