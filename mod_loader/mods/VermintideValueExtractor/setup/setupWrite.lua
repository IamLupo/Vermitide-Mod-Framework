--[[ WE WRITE SETUP
	Functions to simplify writing to files
--]]

--Writes to files
WE.write = function(filename, str, endl)
	local file = WE.files[filename]
	if not file then 
		EchoConsole("File not found: " .. filename)
	else
		if endl then str = str .. "\n" end
		file:write(str)
	end
end

--Writes to debug files
WE.writeDebug = function(filename, str, endl)
	if not WE.settings.debug then return end
	local file = WE.files[filename]
	if not file then 
		EchoConsole("Debug file not found: " .. filename)
	else
		if endl then str = str .. "\n" end
		file:write(str)
	end
end