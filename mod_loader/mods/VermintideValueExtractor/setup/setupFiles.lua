--[[ WE SETUP FILES
	Creates files and specifies headers
--]]

function openFiles(filelist, path, ext, headers)
	for _,filename in ipairs(filelist) do
		local filepath = path .. filename .. ext
		WE.files[filename] = io.open(filepath, "w")	
		if WE.files[filename] then
			WE.numOfFiles = WE.numOfFiles + 1
			if headers and headers[filename] and WE.settings.shouldWriteHeaders then
				WE.files[filename]:write(headers[filename])
			end
		else
			EchoConsole("Couln't open file \"" .. filepath .. "\"")
		end
		WE.numOfFilesRequired = WE.numOfFilesRequired + 1
	end
end

WE.fileNames = {}
WE.files = {}
WE.headers = {}

--[[ FILES --]]

--Main files
WE.fileNames.main = {	
	"Melee", 	
	"MeleeCompare",		
	"Ranged",			
	"RangedCompare",	
	"Push",
	"Misc",		
	"Breeds",
	"Overcharge",				
	"Grenades",
	"MeleeOverheads",
	"ItemsWeapons",			
	"ItemsTrinkets",
	"ItemsHats"		
}

--Either unused or unusable files
WE.fileNames.bad = {
	--"MeleeOld",		--Bad formatting
	--"MeleeStagger",			--Not very useful info and bad formatting
	--"MeleeCompareOld",	--Bad formatting
}

--Debug files
WE.fileNames.debug = {
	"Debug",				--General debug file
	"MeleeWeirdAttacks",	--Contains info about stray attacks (attacks which don't fall into light or heavy attack patterns)
	"RangedTree",			--An uncomplete ranged value tree I used to figure out what data I need to extract
	"PlainRangedTable",		--All collected values for ranged weapons in a plain text format
	"PlainMeleeTable",
	"Refire",				--Log of the findRefireDelay function
	"PrepTimes",
	"PlainWETable"
}

--[[
--Weapon attack damage but separately for each armor type
--Outdated
WE.fileNames.separate = {
	"Unarmored",
	"Armored",
	"Resistant"
}
--]]

--Headers
WE.headers.main = {
	["Melee"] = "Class;Weapon;Attack;Duration;Targets;HS Unarmored;1;2;3;4;5;6;∞;HS Armrd Dmg;HS Armrd No Dmg;1;2;3;4;5;6;∞;HS Resistant;1;2;3;4;5;6;∞;\n", 	
	["MeleeOverheads"] = "Class;Weapon;Attack;Targets;1;2;3;4;5;6;∞;\n", 	
	["MeleeCompare"] = "Class;Weapon;Attack;Duration;Targets;;Headshot;1;2;3;4;5;6;∞;1;2;3;4;5;6;∞;1;2;3;4;5;6;∞;\n",		
	["Ranged"] = "Class;Weapon;Attack;Attack archetype;Targets;Shots per fire;Lifespan;Max range;Damage dropoff range;Unarmored;Armored;Resistant;Allies;HS Unarmored;HS Armored;HS Resistant;Ammo;Clip size;Reload time;Reload delay;Minimum fire interval;Interrupts overheads?;Applied To;Duration;Interval;Unarmored;Armored;Resistant;Allies;Radius;Max damage radius;Unarmored;Armored;Resistant;Allies;\n",			
	["RangedCompare"] = "Class;Weapon;Attack;Attack archetype;Targets;Headshot modifier;Max range;Damage dropoff range;Unarmored;Armored;Resistant;Allies;Ammo;Reload time;Reload delay;Minimum fire interval;Interrupts overheads?;\n",	
	["Push"] = "Class;Weapon;Push radius;Interrupts Overheads?;Distance multiplier;Power;Time;Power;Time;Interrupts Overheads?;Distance multiplier;Power;Time;Power;Time;\n",
	["Misc"] = "Class;Weapon;;Charge\\zoom\\block movement speed;Distance;;Speed;Efficient count;Stamina;;Template name;\n",		
	["Breeds"] = "Breed;Armor type;Poison resistance;Easy;Normal;Hard;Nightmare;Cataclysm;\n",
	["Overcharge"] = "Class;Weapon;Attack;Cost;Charge cost;Decrease rate;Decrease value;Threshold;\n",			
	["Grenades"] = "Grenade type;Radius;Max damage radius;Unarmored;Armored;Resistant;Allies;Radius;Duration;Interval;Unarmored;Armored;Resistant;Allies;Applied to;Duration;Interval;Unarmored;Armored;Resistant;Allies;\n",					
	["ItemsWeapons"] = "Rarity;Class;Weapon;Trait 1;Trai 2;Trait 3;Name;Key;\n",
	["ItemsTrinkets"] = "Rarity;Trinket;Key;Trait;Effect;\n",
	["ItemsHats"] = "Rarity;Class;Hat;Key;\n",
}
WE.headers.additional = {
	["MeleeDoT"] = "Class;Weapon;Attack;Damage;Each;Over;Damage;Each;Over;Damage;Each;Over;\n",
	["RangedCompareDoT"] = "Class;Weapon;Attack;Duration;Interval;Unarmored;Armored;Resistant;Allies;\n",
	["RangedCompareAoE"] = "Class;Weapon;Attack;Radius;Max damage radius;Unarmored;Armored;Resistant;Allies;\n",
	["OverchargeExplosions"] = "Class;Radius;Max damage radius;Unarmored;Armored;Resistant;Allies;Duration;Interval;Unarmored;Armored;Resistant;Allies;\n",			

}

--Generate folders
Mods.C.CreateDirectory([[.\WE]])
Mods.C.CreateDirectory([[.\WE\]] .. WE.settings.folder)
if #WE.fileNames.bad > 0 then
	Mods.C.CreateDirectory([[.\WE\]] .. WE.settings.folder .. [[\Bad]])
end
if WE.settings.debug then
	Mods.C.CreateDirectory([[.\WE\]] .. WE.settings.folder .. [[\Debug]])
end

--Opening files
WE.numOfFiles = 0
WE.numOfFilesRequired = 0
openFiles(WE.fileNames.main, [[.\WE\]] .. WE.settings.folder .. [[\]], ".csv", WE.headers.main)
openFiles(WE.fileNames.bad, [[.\WE\]] .. WE.settings.folder .. [[\Bad\]], ".csv")
if WE.settings.debug then
	openFiles(WE.fileNames.debug, [[.\WE\]] .. WE.settings.folder .. [[\Debug\]], ".txt")
end

--[[ /FILES --]]