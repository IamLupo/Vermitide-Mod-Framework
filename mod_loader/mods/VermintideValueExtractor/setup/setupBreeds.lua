--[[ WE SETUP BREEDS
	Stuff used to write breed info
--]]

--[[ BREEDS --]]

WE.breedNames = {
	skaven_slave= "Slave Rat",
	skaven_clan_rat= "Clan Rat",
	skaven_storm_vermin= "Stormvermin",
	skaven_ratling_gunner= "Ratling Gunner",
	skaven_pack_master= "Packmaster",
	skaven_gutter_runner= "Gutter Runner",
	skaven_poison_wind_globadier= "Poison Wind Globadier",
	skaven_rat_ogre= "Rat Ogre",
	skaven_loot_rat= "Sack Rat",
	critter_rat= "Critter Rat",
	critter_pig= "Critter Pig"
}
WE.breedRoster = {
	"skaven_slave",
	"skaven_clan_rat",
	"skaven_storm_vermin",
	"skaven_ratling_gunner",
	"skaven_pack_master",
	"skaven_gutter_runner",
	"skaven_poison_wind_globadier",
	"skaven_rat_ogre",
	"skaven_loot_rat",
	"critter_rat",
	"critter_pig"
}

--[[ /BREEDS --]]