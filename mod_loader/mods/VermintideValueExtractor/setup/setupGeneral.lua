--[[ WE SETUP GENERAL
	Stuff that's used throughout the script
--]]

WE.get = Mods.require(WE.name, "function/get")

--[[ GENERAL --]]
WE.raritiesSorted = {
	"unique", 
	"exotic", 
	"rare",
	"common",
	"plentiful",
	"promo"
}

WE.raritiesLocalized = {
	["unique"] = "Veteran",
	["exotic"] = "Exotic",
	["rare"] = "Rare",
	["common"] = "Common",
	["plentiful"] = "Plentiful",
	["promo"] = "Promo"
}

WE.defaultAction = {
	action = "action_one",
	attack = "default"
}

WE.weaponTypes = {
	"",
	"MELEE",
	"RANGED"
}
WE.armorTypes = {
	"Unarmored",
	"Armored",
	"Resistant",
	"Friendly fire"
}

WE.unavailableWeapons = {
	"Flamewave Staff",
}

--[[ /GENERAL --]]