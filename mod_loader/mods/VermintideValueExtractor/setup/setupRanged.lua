--[[ WE SETUP RANGED
	Stuff that's used for ranged weapons
--]]

--[[ RANGED --]]

WE.rangedActions = {
	"action_one",
	"action_two",
	"action_three"
}
WE.rangedMainActions = {
	"action_one",
	"action_two"
}
WE.rangedAttrs = {
	"kind",
	"max_penetrations",
	"attack_template",
	"attack_template_damage_type"
}
WE.rangedAttacks = {
	handgun = "Handgun",				--Hitscan
	shotgun = "Shotgun",				--Hitscan
	bullet_spray = "Cone Blast",		--Hitscan
	shield_slam = "Melee",				--Push attack	
	true_flight_bow = "Trueflight",		--Projectile
	crossbow = "Crossbow",				--Projectile
	bow = "Bow",						--Projectile
	charged_projectile = "Projectile",	--Projectile
	geiser = "Geiser",					--AOE
	beam = "Beam"						--Beam
}

WE.overchargeExplosions = {
	["overcharge_explosion_brw"] = "Bright Wizard",
	["overcharge_explosion_dwarf"] = "Dwarf Ranger"
}
WE.grenadeNames = {
	["frag_grenade"] = "Frag Grenade",
	["fire_grenade"] = "Fire Grenade",
	["smoke_grenade"] = "Smoke Grenade", -- removed from the game pretty much
}

--Ranged attack names
WE.rangedNamesLocalized = {
	["Beam Staff"] = {
		action_one = {
			default = "Blast",
			charged_beam = "Beam Blast"
		},
		action_two = {
			default = "Beam"
		}
	},
	["Bolt Staff"] = {
		action_one = {
			default = "Spark right",
			rapid_left = "Spark left",
			shoot_charged = "Charged lvl 1",
			shoot_charged_2 = "Charged lvl 2",
			shoot_charged_3 = "Charged lvl 3"
		}
	},
	["Conflagration Staff"] = {
		action_one = {
			default = "Fireball",
			geiser_launch1 = "AoE1 Edge",
			geiser_launch1_alt = "Geiser",
			geiser_launch2 = "AoE2 Main part",
			geiser_launch3 = "AoE3 Center",
			aoe_firepatch = "AoE4 Firepatch",
			aoe_firepatch_alt = "Firepatch"
		}
	},
	["Fireball Staff"] = {
		action_one = {
			default = "Fireball",
			shoot_charged = "Charged Fireball"
		}
	},
	["Crossbow"] = {
		action_one = {
			default = "Shot",
			zoomed_shot = "Zoomed Shot"
		}
	},
	["Drakefire Pistols"] = {
		action_one = {
			default = "Fireball",
			shoot_charged = "Charged Blast"
		}
	},
	["Grudge-raker"] = {
		action_one = {
			default = "Shot",
		},
		action_two = {
			default = "Push"
		}
	},
	["Handgun"] = {
		action_one = {
			default = "Shot",
			zoomed_shot = "Zoomed Shot"
		}
	},
	["Blunderbuss"] = {
		action_one = {
			default = "Shot"
		},
		action_two = {
			default = "Push"
		}
	},
	["Repeater Handgun"] = {
		action_one = {
			bullet_spray = "Rapid Fire",
			default = "Single Shot"
		}
	},
	["Hagbane Swift Bow"] = {
		action_one = {
			default = "Shot",
			shoot_charged = "Charged Shot"
		}
	},
	["Longbow"] = {
		action_one = {
			default = "Shot",
			shoot_charged = "Charged Shot"
		}
	},
	["Swift Bow"] = {
		action_one = {
			default = "Shot",
			shoot_charged = "Charged Shot"
		}
	},
	["Trueflight Longbow"] = {
		action_one = {
			default = "Shot",
			shoot_charged = "Charged Shot"
		}
	},
	["Brace of Pistols"] = {
		action_one = {
			default = "Single Shot",
			fast_shot = "Rapid Fire"
		}
	},
	["Repeater Pistol"] = {
		action_one = {
			bullet_spray = "Bullet Spray",
			default = "Single Shot"
		}
	},
	["Volley Crossbow"] = {
		action_one = {
			default = "Shot",
			zoomed_shot = "Zoomed Shot"
		}
	}
}

--[[ /RANGED --]]
