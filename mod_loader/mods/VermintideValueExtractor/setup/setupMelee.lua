--[[ WE SETUP MELEE
	Stuff for melee weapons
--]]

--[[ MELEE --]]

WE.sequenceModifiers = {
	"charged"
}

WE.allowedPrefixes = {
	"light",
	"heavy",
	"push",
	"special"
}

WE.allowedPrefixesAsSequences = {
	"light",
	"heavy"
}

WE.allowedSequences = {
	"light_attack",
	"heavy_attack",
	"heavy_attack_charged"
}

--Used for attack sequences' names
WE.seqPrefixMatch = '([^_]+)' --light_attack_right -> light
WE.seqNameMatch = '([^_]+_attack)' --light_right_attack -> light_attack
WE.seqModifierMatch = '[_]([^_]+)$' --light_attack_right -> right

WE.attackTypesShort = {
	heavy_attack_left = "Heavy ←",
	heavy_attack_right = "Heavy →",
	heavy_attack = "Heavy",
	heavy_attack_2 = "Heavy 2",
	heavy_attack_down_first = "Heavy ↓ 1",
	heavy_attack_down_second = "Heavy ↓ 2",
	heavy_attack_charged = "Heavy charged",
	heavy_attack_left_charged = "Heavy charged ←",
	heavy_attack_right_charged = "Heavy charged →",
	light_attack_left = "Light ←",
	light_attack_left_last = "Light ← last",
	light_attack_right = "Light →",
	light_attack_right_first = "Light → 1",
	light_attack_right_second = "Light → 2",
	light_attack_last = "Light last",
	light_attack_down = "Light ↓",
	light_attack_upper = "Light ↑",
	light_attack_stab_left = "Light stab ←",
	light_attack_up_left = "Light ↖",
	light_attack_left_upward = "Light ↖",
	light_attack_up_right = "Light ↗",
	light_attack_right_upward = "Light ↗",
	light_attack_stab = "Light stab",
	light_attack_quick_left = "Light quick ←",
	push_stab = "Push-stab",
	special_block_shot = "Pistol block shot",
	special_default = "Pistol shot",

}

WE.attackTypesLong = {
	heavy_attack_left = "Heavy left",
	heavy_attack_right = "Heavy right",
	heavy_attack = "Heavy",
	heavy_attack_2 = "Heavy 2",
	heavy_attack_down_first = "Heavy down 1",
	heavy_attack_down_second = "Heavy down 2",
	heavy_attack_charged = "Heavy charged",
	heavy_attack_left_charged = "Heavy charged left",
	heavy_attack_right_charged = "Heavy charged right",
	light_attack_left = "Light left",
	light_attack_left_last = "Light left last",
	light_attack_right = "Light right",
	light_attack_right_first = "Light right 1",
	light_attack_right_second = "Light right 2",
	light_attack_last = "Light last",
	light_attack_down = "Light down",
	light_attack_upper = "Light up",
	light_attack_stab_left = "Light stab left",
	light_attack_up_left = "Light up left",
	light_attack_left_upward = "Light left up",
	light_attack_up_right = "Light up right",
	light_attack_right_upward = "Light right up",
	light_attack_stab = "Light stab",
	light_attack_quick_left = "Light quick left",
	push_stab = "Push-stab",
	special_block_shot = "Pistol block shot",
	special_default = "Pistol shot",
}

--[[ /MELEE --]]