--[[ WE SETUP STAGGER
	Stuff that's used for writing stagger info
--]]

--[[ STAGGER --]]

WE.staggerTypes = {}

--Unarmored
WE.staggerTypes[1] = {
	"P1",
	"P2",
	"P3",
	"P2",
	"P2",
	"P4",
	"P1"
}
--Armored
WE.staggerTypes[2] = {
	"P1",
	"P2",
	"P2",
	"P1",
	"P1",
	"P3",
	"P2"
}
--Interrupts overhead?
WE.staggerTypes[3] = {
	false,
	true,
	true,
	false,
	false,
	true,
	true
}

--[[ /STAGGER --]]