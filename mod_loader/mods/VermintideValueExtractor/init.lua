--[[ WE INIT
	Preferences and things that need to be adjusted manually as the game is updated
	/weset [option] [value] for preferences and options
	WEExtrenalSettings.lua in mod_loader folder contains more settings
--]]

WE = {
	name = "VermintideValueExtractor",
	info = {
		title = "Vermintide Value Extractor (WE) by UnShame",
		header = "/we <command> [<args>]",
		offset = "    ",
		footer = "See WEExtrenalSettings.lua in mod_loader folder for more settings",
	},
	actions = {
		["export"] = {
			action = "actionExport",
			info = "export [-a] [<folder> | <option> <value>]",
			description = {
				"Exports values with current options",
				"Options can be set before exporting",
				"<folder> will set folder option",
				"<option> <value> will set other options, see /we help set",
				"-a will export all rarities of weapons"
			}
		},
		["set"] = {
			action = "actionSet",
			info = "set <option> | -d | -de <value> | -d",
			description = {
				"Changes or resets options",
				"Run without <option> for list of options",
				"Run without <value> for list of values",
				"-d resets all or one option to default",
				"-de resets external settings to default"
			}
		},
		["reload"] = {
			action = "actionReload",
			info = "reload",
			description = {
				"Reloads mod"
			}
		},
		["help"] = {
			action = "actionHelp",
			info = "help [<command>]",
			description = {
				"There are no easter eggs down here.",
				"Go away."
			}
		}
	},
	
	--[[ PREFERENCES --]]
	settings = {},
	settingsInfo = {

		--Folder to output to
		["folder"] = {
			ref = "folder",
			setting = "we_settings_folder",
			default = "Current"
		},

		--Rarity of the weapons to output
		["rarity"] = {
			ref = "rarity",
			setting = "we_settings_rarity",
			indexable = true,
			type = "string",
			values = {
				"plentiful",
				"common",
				"rare",
				"exotic", 
				"veteran",
				"white",
				"green",
				"blue",
				"orange", 
				"red"
			},
			default = "exotic"
		},

		--Whether or not to write headers
		["headers"] = {
			ref = "shouldWriteHeaders",
			setting = "we_settings_headers",
			type = "boolean",
			indexable = false,
			values = {
				true,
				false
			},
			default = true
		},

		--Debug output
		["debug"] = {
			ref = "debug",
			setting = "we_settings_debug",
			type = "boolean",
			indexable = false,
			values = {
				true,
				false
			},
			default = false
		},

		--Attack output format
		["attackFormat"] = {
			ref = "attackFormat",
			setting = "we_settings_attack_format",
			indexable = true,
			type = "string",
			values = {
				"short",
				"long",
				"plain"
			},
			default = "short"
		},

		--Weapon name format
		["nameFormat"] = {
			ref = "weaponNameFormat",
			setting = "we_settings_name_format",
			type = "string",
			indexable = true,
			values = {
				"localized",
				"plain"
			},
			default = "localized"
		},

		["shortestRefire"] = {
			ref = "shortestRefire",
			setting = "we_settings_refire",
			type = "boolean",
			indexable = false,
			values = {
				true,
				false
			},
			default = false
		},

		-- --Changes whether or not dodge modifiers are shown (they aren't actually used pre 1.5)
		-- ["dodgeModifiers"] = {
		-- 	ref = "useDodgeModifiers",
		-- 	setting = "we_settings_dodge_modifiers",
		-- 	type = "boolean",
		-- 	indexable = true,
		-- 	values = {
		-- 		true,
		-- 		false
		-- 	},
		-- 	default = true
		-- },

		--Do we need to write attacks that aren't actually doable?
		["unreachable"] = {
			ref = "shouldWriteUnreachableAttacks",
			setting = "we_settings_write_unreachable",
			type = "boolean",
			indexable = false,
			values = {
				true,
				false
			},
			default = true
		},

		--Write only unavailavble in game weapons
		["unavailable"] = {
			ref = "shouldWriteUnavailabe",
			setting = "we_settings_write_unavailable",
			type = "boolean",
			indexable = false,
			values = {
				true,
				false
			},
			default = false
		},

		--Prefix for plus sign so it doesn't mess up formatting
		["plusPrefix"] = {
			ref = "plusPrefix",
			setting = "we_settings_plus_prefix",
			type = "string",
			default = "'"
		}
	},
	--[[ /PREFERENCES --]]
	
	--[[ STUFF THAT MIGHT NEED ADJUSTMENT BASED ON GAME'S VERSION --]]
	constantsString = [[
--External settings for Vermintide Value Extractor
--/wereload to load changed settings
--/weresetext to reset this to default values
{
	--What dev blow trait upgrades pushes to
	--From the source code
	pushUpgrades = {
		basic_sweep_push = "heavy_sweep_push",
		dagger_sweep_push = "heavy_sweep_push",
		heavy_sweep_push = "super_heavy_sweep_push",
		weak_sweep_push = "heavy_sweep_push"  
	},

	--Maximum amount of targets to show other than infinite (Two-handed sword has the maximum with 6)
	MAX_TARGETS = 6,

	--Weapons from the new DLC. We don't want to display them in comparison lists.
	newWeapons = {

	},

	--Max beam\drakefire shotgun range
	--Appears to be 6 (from source code and testing) but more testing is required
	SPRAY_RANGE = 6,

	--Max amount of targets a beam\drakefire shotgun can hit
	--Taken from the source code
	BULLET_SPRAY_MAX_TARGETS = 10,

	--Modifier to get minimum overcharge value
	--See player_unit_overcharge_extension.lua
	MIN_CHARGE_OVERCHARGE_MODIFIER = 0.4
}
	]]
	--[[  /STUFF THAT MIGHT NEED ADJUSTMENT BASED ON GAME'S VERSION --]]
}

WE.setOption = Mods.require(WE.name, "function/setOption")

--Constants
local input = io.open([[.\mod_loader\]] .. "WEExternalSettings.lua", "r")
local ExternalSettings
if input then
	ExternalSettings = loadstring(
		[[return ]] .. input:read("*all")
	)()
end
if not ExternalSettings then	
	local output = io.open([[.\mod_loader\]] .. "WEExternalSettings.lua", "w")

	WE.constants = loadstring(
		[[return ]] .. WE.constantsString
	)()
	if output then
		output:write(WE.constantsString)
		output:close()
	end
else
	WE.constants = ExternalSettings
end


local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

--Loading settings or applying default settings
for k,info in pairs(WE.settingsInfo) do
	local value = get(info.setting)
	if value ~= nil then
		WE.settings[info.ref] = value
	else
		WE.settings[info.ref] = info.default
		set(info.setting, info.default)
	end
end
save()