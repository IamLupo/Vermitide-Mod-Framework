Vermintide Value Extractor (WE) 
Author: UnShame

Extracts values for:
	Melee weapons
	Ranged weapons
	Grenades
	Weapons as items
	Trinkets
	Hats
	Breeds
Puts them into CSV format without headers
Finished spreadsheet: https://docs.google.com/spreadsheets/d/1h3GnI6sIS77oXnCzE05k7KTkOoGJ3UezeVi24miBBoM

After first initialization, WEExternalSettings.lua will appear in mod_loader folder.
It contains settings that might need adjustments as the game is updated.

Type /we into chat for usage information.