--[[ WE findRefireDelay
	Finds the lowest amount of time to do the same attack again (ranged and some melee)
--]]

--Compares attack sequences
local function sameSequence(n1, n2)

	local seq1 = n1:match(WE.seqPrefixMatch)
	local seq2 = n2:match(WE.seqPrefixMatch)

	if not table.indexOf(WE.allowedPrefixesAsSequences, seq1) or not table.indexOf(WE.allowedPrefixesAsSequences, seq2) then
		return false
	end

	local m1 = n1:match(WE.seqModifierMatch)
	local m2 = n2:match(WE.seqModifierMatch)

	m1 = table.indexOf(WE.sequenceModifiers, m1) and m1
	m2 = table.indexOf(WE.sequenceModifiers, m2) and m2

	return (seq1 == seq2) and (m1 == m2)
end

--Finds a sequence of actions that leads to the same attack (melee and ranged)
--@actions - all actions
--@actionName, attackName - the action and attack we're starting from and trying to come back to
--@_actionName, _attackName - current action and attack
--@sequence - sequence of actions we're building
--@deadEnds - actions that don't lead to our starting attack
--@limitBySequence - whether or not to look for the same sequence of attacks instead of for the same attack
--					 also limits the search to the same action (melee only)
--@failsafe - for debug, isn't necessary
--Returns assembled sequence and dead ends
local function getRefireSequence(actions, actionName, attackName, _actionName, _attackName, sequence, deadEnds, limitBySequence, failsafe)
	
	--Initial iteration
	if not(sequence) then
		sequence = {}
		WE.writeDebug("Refire", "Original attack - " .. actionName .. " " .. attackName, true)
	end

	failsafe = failsafe - 1
	if(failsafe < 0) then
		EchoConsole("Endless loop detected (Shouldn't happen)")
		WE.writeDebug("Refire", "Endless loop detected (Shouldn't happen)\n")
		return nil
	end

	local _action = actions[_actionName]
	local _attack = _action[_attackName]

	--Actions that can follow the current one
	local followingActions = _attack.allowed_chain_actions
	local pair = {_actionName, _attackName}

	--Looking for the original action and attack first
	for i,chainAction in pairs(followingActions) do		
		if(
			not(#sequence == 0 and table.indexOf(deadEnds,chainAction)) and 
			(
				(
					actionName == chainAction.action and 
					attackName == chainAction.sub_action 
				)
				or
				(
					limitBySequence and
					chainAction.action == actionName and
					sameSequence(attackName, chainAction.sub_action)
				)
			)
		) then
			WE.writeDebug("Refire", "Found original attack - " .. chainAction.action .. " " .. chainAction.sub_action, true)
			local step = {
				action = pair[1],
				attack = pair[2],
				chain = chainAction
			}
			table.insert(sequence, step)
			return sequence, deadEnds
		end 
	end

	--Looking for a valid next action
	for i,chainAction in pairs(followingActions) do
		local nextPair = {chainAction.action, chainAction.sub_action}	
		local step = {
			action = pair[1],
			attack = pair[2],
			chain = chainAction
		}
		if(
			not table.indexOf(deadEnds,chainAction) and
			table.indexOf(WE.rangedActions, chainAction.action) and
			not (
				chainAction.action == _actionName and
				chainAction.sub_action == _attackName
			) and
			not table.indexOfByKey(sequence,step) and 
			(
				not limitBySequence or 
				chainAction.action == actionName and
				chainAction.sub_action:match(WE.seqPrefixMatch) == "default"
			)
		) then
			--Start a new iteration with the found action and attack
			WE.writeDebug("Refire", "Going to attack - " .. chainAction.action .. " " .. chainAction.sub_action .. " " .. #sequence, true)	

			table.insert(sequence, step)
			return getRefireSequence(actions, actionName, attackName, chainAction.action, chainAction.sub_action, sequence, deadEnds, limitBySequence, failsafe)
		end

	end

	--We ran out of possible sequences
	if(#sequence < 1) then
		WE.writeDebug("Refire", "Couldn't find a refire sequence\n")
		return nil
	end

	--We couldn't find a valid next action, so mark current as dead end,
	--delete previous action from the chain and iterate back to it to look for another
	table.insert(deadEnds,sequence[#sequence].chain)
	local prevActionName = sequence[#sequence].action
	local prevAttackName = sequence[#sequence].attack
	sequence[#sequence] = nil
	WE.writeDebug("Refire", "Dead end, going back to - " .. prevActionName .. " " .. prevAttackName, true)
	return getRefireSequence(actions, actionName, attackName, prevActionName, prevAttackName, sequence, deadEnds, limitBySequence, failsafe) 
end

--Iterates through prepared sequence and finds refire delay and prepare time
local function getRefireDelay(sequences, limitBySequence)

	local refireDelay = math.huge
	local totalDelay = math.huge
	local prepareTime = 0

	local hasShortestSequence = false
	local targetChainAction = nil

	if #sequences == 0 then return refireDelay, prepareTime, hasShortestSequence, targetChainAction end

	--Find the sequence with the lowest duration
	WE.writeDebug("Refire", "-\nLooking for lowest delay\n")
	for i,sequence in pairs(sequences) do

		WE.writeDebug("Refire", "Sequnce " .. i .. " #" .. #sequence, true)

		local currentRefireDelay = 0
		local offset = limitBySequence and 1 or 0
		local isShortestSequence = (#sequence == 1) and not WE.settings.shortestRefire

		--Add up the start times of all actions in the sequence
		for n = 1 + offset,#sequence do

			local chainAction = sequence[n].chain

			WE.writeDebug("Refire", tostring(chainAction.input), true)
			WE.writeDebug("Refire", chainAction.start_time, true)
			currentRefireDelay = currentRefireDelay + chainAction.start_time
		end

		local currentTotalDelay = currentRefireDelay
		local currentPrepareTime = 0
		if limitBySequence then		
			local chainAction = sequence[1].chain
			currentPrepareTime = chainAction.start_time
			currentTotalDelay = currentTotalDelay + currentPrepareTime
		end

		--Compare found duration to the current lowest one
		WE.writeDebug("Refire", "= " .. currentTotalDelay .. " <-> " .. totalDelay, true)
		if 
			not hasShortestSequence and (isShortestSequence or currentTotalDelay < totalDelay) or
			isShortestSequence and currentTotalDelay < totalDelay
		then
			totalDelay = currentTotalDelay
			refireDelay = currentRefireDelay
			if limitBySequence then					
				targetChainAction = sequence[#sequence].chain
				prepareTime = currentPrepareTime
				WE.writeDebug("Refire", "Instead: " .. prepareTime, true)
			end
		end

		if isShortestSequence then
			hasShortestSequence = true
			WE.writeDebug("Refire", "Shortest sequence\n")
		end
	end
	return refireDelay, prepareTime, hasShortestSequence, targetChainAction
end

--Iterates through action sequences until it runs out of options
--Returns sequences
local function findRefireSequences(actions, followingActions, actionName, attackName, startActionName, startAttackName, prepareStorage)

	local sequences = {}
	local deadEnds = {}
	local sequence = nil

	local searchIndex = 0
	for i,chainAction in pairs(followingActions) do

		--We're only looking at the action_one and action_two
		if(table.indexOf(WE.rangedActions, chainAction.action)) then

			sequence = nil
			searchIndex = searchIndex + 1

			WE.writeDebug("Refire", "Iteration " .. searchIndex, true)
			sequence, deadEnds = getRefireSequence(actions, actionName, attackName, startActionName, startAttackName, sequence, deadEnds, prepareStorage and true, 25)
			if(sequence) then
				local pair = {chainAction.action, chainAction.sub_action}
				table.insert(sequences, sequence)
				table.insert(deadEnds, chainAction)

			--We ran out of sequences
			else
				break
			end
		end
	end
	return sequences
end

--Finds the lowest amount of time to do the same attack again (ranged and some melee)
--or the lowest time to do an attack from the same sequence (melee)
--@actions - all actions
--@actionName, attackName - the action and attack we're looking for
--@prepareStorage - where to save preparation time for next attack of the same sequence
--					also acts as a flag to look for sequence instead of exact attack
--					and to limit search to specified action (melee only)
local function findRefireDelay(actions, actionName, attackName, prepareStorage)

	--We're trying to find the lowest value, so initialize it with the biggest possible
	local action = actions[actionName]
	local attack = action[attackName]
	local followingActions = attack.allowed_chain_actions

	local sequences = findRefireSequences(actions, followingActions, actionName, attackName, actionName, attackName, prepareStorage)

	local refireDelay, prepareTime, hasShortestSequence, targetChainAction = getRefireDelay(sequences, prepareStorage)

	--Saving prepare time
	WE.writeDebug("PrepTimes", attackName .. " - ")
	if prepareStorage and targetChainAction then

		local storage = prepareStorage[WE.get.sequenceName(attackName)]

		if storage and storage[targetChainAction.sub_action] then

			--Swap times so the time between attacks is saved for later
			refireDelay, prepareTime = prepareTime, refireDelay
			
			storage[targetChainAction.sub_action] = prepareTime
			WE.writeDebug("PrepTimes", targetChainAction.sub_action .. " " .. prepareTime, true)
		else
			WE.writeDebug("PrepTimes", targetChainAction.sub_action .. " ?", true)
			for k,v in pairs(storage) do
				WE.writeDebug("PrepTimes", "	" .. k .. " " .. v, true)
			end
		end
	else
		WE.writeDebug("PrepTimes", "\n")
	end

	--When no other option is presented, we use the animation time of the attack
	local animTime = attack.total_time

	if (not hasShortestSequence and refireDelay ~= math.huge) then
		WE.writeDebug("Refire", "Attack can't repeat\n")
		animTime = math.huge
	end

	if(refireDelay == math.huge and not prepareStorage) then
		WE.writeDebug("Refire", "Refire delay not found, retrying from default action\n")
		local startActionName = WE.defaultAction.action
		local startAttackName = WE.defaultAction.attack
		local followingActions = actions[startActionName][startAttackName].allowed_chain_actions
		local sequences = findRefireSequences(actions, followingActions, actionName, attackName, startActionName, startAttackName, prepareStorage)
		if #sequences > 0 then
			refireDelay = getRefireDelay(sequences, false)
		else		
			refireDelay = animTime
			WE.writeDebug("Refire", "ANIMATION TIME USED\n")
		end
	end

	if prepareStorage and refireDelay == math.huge then
		refireDelay = nil
	end

	WE.writeDebug("Refire", "----------------\nREFIRE DELAY = " .. (refireDelay or "?"), true)
	return refireDelay
end

--[[
--see findRefireDelay
WE.findRealReloadTime = function(weaponActions, actionName, attackName)
	local action = weaponActions[actionName]
	local attack = action[attackName]
	local chain = attack.allowed_chain_actions
	local reloadTime = nil
	local beforeAvailableTime = nil
	for i,chainAction in pairs(chain) do
		if(chainAction.action == actionName) then
			if(weaponActions[actionName][chainAction.sub_action] == attack and WE.rangedAttacks[weaponActions[actionName][chainAction.sub_action].kind]) then
				reloadTime = chainAction.start_time
			end
			if(not beforeAvailableTime or chainAction.start_time and chainAction.start_time < tonumber(beforeAvailableTime)) then
				beforeAvailableTime = chainAction.start_time
			end
		end
	end
	if not(beforeAvailableTime) then
		beforeAvailableTime = ActionTemplates.wield.default.wield_cooldown
	end
	return reloadTime, beforeAvailableTime
end
--]]

return findRefireDelay