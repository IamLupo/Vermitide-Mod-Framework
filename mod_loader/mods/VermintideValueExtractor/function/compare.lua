--[[ WE COMPARE FUNCTIONS ]]--

local compare = {

	--Sorts melee weapons by class, then name, then attack sequence
	melee = function(a,b)
		if a.hero == b.hero then
			if a.name == b.name then
				local seqA = WE.get.sequenceName(a.attack)
				local seqB = WE.get.sequenceName(b.attack)
				if (seqA == seqB) then
					local aa = table.indexOf(WE.meleeAttackSequence[a.template][seqA],a.attack)
					local bb = table.indexOf(WE.meleeAttackSequence[b.template][seqB],b.attack)
					WE.writeDebug("Debug", a.name .. " - " .. a.attack .. ":" .. tostring(aa) .. " / " .. b.attack .. ":" .. tostring(bb), true)
					if(not aa) then
						aa = math.huge
					end
					if(not bb) then
						bb = math.huge
					end
					return aa < bb
				else
					if string.len(seqA) == 0 then return false end
					if string.len(seqB) == 0 then return true end
					return seqA < seqB
				end
			else
				return a.name < b.name
			end
		else
			return a.hero < b.hero
		end
	end,

	--Sorts ranged weapons by class, then name, then action, then attack
	ranged = function(a,b)
		if a.hero == b.hero then
			if a.name == b.name then
				if a.action == b.action then
					return a.attackLocalized < b.attackLocalized
				else
					return a.action < b.action
				end
			else
				return a.name < b.name
			end
		else
			return a.hero < b.hero
		end
	end,

	--Sorts combined weapons by class, then name
	combined = function(a,b)
		if a.hero == b.hero then
			return a.name < b.name
		else
			return a.hero < b.hero
		end
	end,

	items = function(a, b)
		if a.type == b.type then
			if a.type == "weapon" then
				local rarityA = table.indexOf(WE.raritiesSorted, a.rarity)
				local rarityB = table.indexOf(WE.raritiesSorted, b.rarity)
				if rarityA == rarityB then
					if a.localized.canWield[1] == b.localized.canWield[1] then
						if a.localized.kind == b.localized.kind then
							if #a.localized.traits ~= #b.localized.traits then
								return #a < #b 
							else
								for i=1, #a.localized.traits do
									if a.localized.traits[i] ~= b.localized.traits[i] then
										return a.localized.traits[i] < b.localized.traits[i]
									end
								end
							end
						else
							return a.localized.kind < b.localized.kind
						end
					else
						return a.localized.canWield[1] < b.localized.canWield[1]
					end
				else
					return rarityA < rarityB
				end
			elseif a.type == "trinket" then
				local rarityA = table.indexOf(WE.raritiesSorted, a.rarity)
				local rarityB = table.indexOf(WE.raritiesSorted, b.rarity)
				if rarityA == rarityB then
					return a.key < b.key
				else
					return rarityA < rarityB
				end
			elseif a.type == "hat" then
				local rarityA = table.indexOf(WE.raritiesSorted, a.rarity)
				local rarityB = table.indexOf(WE.raritiesSorted, b.rarity)
				if rarityA == rarityB then
					if a.localized.canWield[1] == b.localized.canWield[1] then
						return a.localized.name < b.localized.name
					else
						return a.localized.canWield[1] < b.localized.canWield[1]
					end
				else
					return rarityA < rarityB
				end
			else
				return a.name < b.name
			end
		else
			return table.indexOf(WE.itemTypes, a.type) < table.indexOf(WE.itemTypes, b.type)
		end
	end
}

return compare