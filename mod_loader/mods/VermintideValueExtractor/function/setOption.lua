--[[ WE SET OPTION FUNCTION
	Allows user to modify internal settings of the mod
--]]

--This lets us silently change options, only successful change will be echoed
local function proxiedEchoConsole(str, noEcho)
	if not noEcho then
		EchoConsole(str)
	end
end

--Lists all available options
local function listOptions(str, noEcho)	
	if type(str) == "string" then
		proxiedEchoConsole(str, noEcho)
	end

	for option,info in pairs(WE.settingsInfo) do
		local value = tostring(WE.settings[info.ref])
		str = WE.info.offset .. option .. " = "
		str = str .. "\"" .. value .. "\""
		proxiedEchoConsole(str, noEcho)
	end
end

--Lists possible values for specific option
local function listValues(info, str, noEcho)
	if type(str) == "string" then
		proxiedEchoConsole(str)
	end
	if(info.values) then
		for i,v in ipairs(info.values) do
			local str = WE.info.offset
			if(info.indexable) then
				str = str .. i .. ":"
			end
			str = str .. tostring(v)
			if v == info.default then
				str = str .. " - default"
			end
			if v == WE.settings[info.ref] then
				str = str .. " - current"
			end
			proxiedEchoConsole(str, noEcho)
		end
	else
		proxiedEchoConsole(WE.info.offset .. "Any string")
		proxiedEchoConsole(WE.info.offset .. "\"" .. tostring(WE.settings[info.ref]) .. "\" - current", noEcho)
		proxiedEchoConsole(WE.info.offset .. "\"" .. tostring(info.default) .. "\" - default", noEcho)
	end
end

local function setOption(option, value, noEcho)

	--Initialize settings if they don't already exist
	if not WE or not WE.settings then
		Mods.exec("VermintideValueExtractor", "init")
	end

	--No input, list options
	if not option then
		EchoConsole("Usage:")
		EchoConsole("    /we " .. WE.actions["set"].info)
		listOptions("Options:", noEcho)
		return
	end

	if option == "-d" then
		Mods.exec(WE.name, "action/actionReset")
		return
	elseif option == "-de" then
		Mods.exec(WE.name, "action/actionResetExt")
		return
	end

	local info = WE.settingsInfo[option]

	--When using /we command user may specify just value without option, it will be folder
	if noEcho and option and not value then
		value = option
		option = "folder"
		info = WE.settingsInfo[option]

	--Check if option exists
	elseif not option or not info then
		listOptions("Incorrect option", noEcho)
		return
	end

	--Check if value has been enetered
	if not value then
		listValues(info, "Values for \"" .. option .. "\":", noEcho)
		return
	end

	if value == "-d" then
		value = info.default
	end

	--Check if value can be used to index option
	local index = tonumber(value)
	if not(index ~= index) and info.indexable and info.values[index] ~= nil then
		value = info.values[index]
	end

	--Attempt to convert value into the right type
	if info.type == "boolean" and type(value) ~= "boolean" then
		if value == "0" or value == "false" then
			value = false
		elseif value == "1" or value == "true" then
			value = true
		end
	elseif info.type == "number" and type(value) ~= "number" then
		value = tonumber(value)
		if value ~= value then 
			listValues(info, "Incorrect value", noEcho)
			return
		end
	end

	--Check if value is correct now
	if info.values and not table.indexOf(info.values, value) then
		listValues(info, "Incorrect value", noEcho)
		return
	end

	--Set value, save setting, notify user
	local set = Application.set_user_setting
	local save = Application.save_user_settings

	set(info.setting, value)
	WE.settings[info.ref] = value
	save()

	local char = ""
	if type(value) == "string" and string.len(tostring(value)) > 1 then char = "\"" end
	proxiedEchoConsole("Option \"" .. option .. "\" set to " .. char .. tostring(value) .. char, noEcho)
end

return setOption