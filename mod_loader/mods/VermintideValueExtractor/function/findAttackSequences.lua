--[[ WE findAttackSequences
	Finds a sequence of actions that leads to the same attack (melee and ranged)
--]]

--Checks if all of an action's allowed to chain actions are default actions
local function isDefaultOnly(entry)	
	local allDefault = true
	for k,action in pairs(entry) do
		if(action.sub_action ~= "default") then
			allDefault = false
		end
	end
	return allDefault
end

--Iterates through a chain of actions and assembles a sequence of actions (melee)
--@templateName - name of weapon template by which sequence and preparation times will be found
--@attacks - all actions
--@attackName - name of the current action
--@seq - the name of the sequence we're filling in
--Returns assembled sequence
local function getAttackSequence(templateName, attacks, attackName, seq, failsafe)

	local sequence = WE.meleeAttackSequence[templateName]
	local preTimes = WE.meleeAttackPreTime[templateName]

	--Limiting the number of iterations
	--(used for debug, now works without infinite loops, but I let it stay)
	failsafe = failsafe - 1
	if(failsafe < 0) then
		EchoConsole("Endless loop (shouldn't happen)")
		return sequence
	end

	local attack = attacks[attackName]
	local actions = attack.allowed_chain_actions
	local nextAction = nil

	WE.writeDebug("Debug", "-----------" .. failsafe .. "------------\n")
	for k,v in pairs(sequence[seq]) do
		WE.writeDebug("Debug", k .. ": " .. seq .. " " .. v, true)
	end

	--Current action belongs to our sequence
	local newSeq = WE.get.sequenceName(attackName)
	if(not table.indexOf(sequence[seq],attackName) and sequence[newSeq]) then
		WE.writeDebug("Debug", "Added action:" .. attackName, true)

		--Addding it to sequence
		table.insert(sequence[seq], attackName)
		preTimes[seq][attackName] = 0

		--The action has only default sub-actions, so it's the last one
		--This is checked at the end of previous iteration most of the time
		if(isDefaultOnly(actions)) then
			WE.writeDebug("Debug", "Is last\n")
			return sequence
		end
	end

	--Used for checking if weapon has attacks that getAttackSequence between each other without going to the default action
	local endlessLoop = false

	--Looking for the next action
	for k,action in pairs(actions) do		
		if(action.sub_action ~= nil) then

			--Check if attack belongs to our sequence
			local newSeq = WE.get.sequenceName(action.sub_action)
			if(newSeq == seq) then

				--Check if attack hasn't been added already, otherwise sequence is endless
				if(table.indexOf(sequence[seq],action.sub_action)) then
					endlessLoop = true
					--there really should be a break here but I guess it doesn't matter
				else
					endlessLoop = false

					--Remember next action
					nextAction = action.sub_action

					WE.writeDebug("Debug", "Next action chosen:" .. nextAction, true)
					break
				end
			end	
		end	
	end

	--If it's an endless attack loop, return the sequence
	if(endlessLoop) then
		WE.writeDebug("Debug", "Weapon has endless attack loop, exiting\n")
		return sequence
	end

	--If we found next action
	if(nextAction) then		

		--If it's the last action
		if(isDefaultOnly(attacks[nextAction].allowed_chain_actions)) then	

			--Add it and return the sequence
			table.insert(sequence[seq], nextAction)
			preTimes[seq][nextAction] = 0

			WE.writeDebug("Debug", "Next action is last.\n")
			WE.writeDebug("Debug", "-----------" .. failsafe .. "------------\n")
			for k,v in pairs(sequence[seq]) do
				WE.writeDebug("Debug", k .. ": " .. seq .. " " .. v, true)
			end
			WE.writeDebug("Debug", "Added action:" .. nextAction, true)
			WE.writeDebug("Debug", "\n")

			return sequence					
		else
			WE.writeDebug("Debug", "Recursing to next action \n")

			--Otherwise start a new iteration with the next action
			return getAttackSequence(templateName, attacks, nextAction, seq, failsafe)
		end

	--If no next action was found then we need to find default action we can go to to find our next action
	else
		for k,action in pairs(actions) do
			if(action.sub_action) then

				--Action shouldn't be THE default action and shouldn't belong to any of the sequences
				if(action.sub_action ~= "default" and sequence[action.sub_action:match(WE.seqPrefixMatch)] == nil) then
					WE.writeDebug("Debug", "Recursing to default action \n")

					--Start a new iteration with the default action
					return getAttackSequence(templateName, attacks, action.sub_action, seq, failsafe)
				end
			end
		end

		--If no default actions were found, return sequence (doesn't actually happen)
		WE.writeDebug("Debug", "Exiting \n")
		return sequence
	end
end

local function findAttackSequences(templateName, attacks)
	--Creating the sequences of attacks of the weapon
	WE.meleeAttackSequence[templateName] = {}	
	WE.meleeAttackPreTime[templateName] = {}

	--We start from iterating through the default action's allowed chain actions
	for k,action in pairs(attacks.default.allowed_chain_actions) do
		if(attacks[action.sub_action]) then

			local seq = WE.get.sequenceName(action.sub_action)

			--For each action that is not a dummy action (i.e. an actual attack) we create a new sub-sequence of attacks
			if(table.indexOf(WE.allowedSequences, seq) and WE.meleeAttackSequence[templateName][seq] == nil) then
				--Then we go through the chain of actions and generate the rest of the sequence	
				WE.meleeAttackSequence[templateName][seq] = {action.sub_action}
				WE.meleeAttackPreTime[templateName][seq] = {[action.sub_action] = 0}
				
				WE.writeDebug("Debug", "====" .. templateName .. "====\n")
				WE.meleeAttackSequence[templateName] = getAttackSequence(templateName, attacks, action.sub_action, seq, 25)						
				WE.writeDebug("Debug", "\n")
			end
		end
	end
end

return findAttackSequences