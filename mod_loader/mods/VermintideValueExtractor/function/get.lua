--[[ WE GET FUNCTIONS --]]

local get = {

	--Stats are stored this way: weapon.stats[targetNumber].key[armorType] for melee
	--This function returns a single stats[targetNumber], so it is run targetNumber times
	--For ranged there's only one instance of stats per attack but it's run for initial hit, aoe and dot
	--Headshot modifier, stagger length and DoT properties are stored once for each targetNumber
	--There's also aoe and upgraded push here now
	--Works also for getting dot, aoe and push stats
	stats = function(attackTemplateName, damageTemplateName, attackKind, shouldWriteFF)
		local stats = {}

		local attackTemplate = AttackTemplates[attackTemplateName]
		if not attackTemplate then return nil end
		local dot = attackTemplate.dot_template_name
		local hs = attackTemplate.headshot_multiplier 
		local damageTemplate = AttackDamageValues[damageTemplateName]

		--Save headshot
		stats.headshot = {}

		--Account for attacks that can't do headshots
		if(attackKind == "shield_slam" or attackKind == "bullet_spray" or attackKind == "geiser") then
			stats.headshot.modifier = ""
			stats.headshot.value = "N/A"

		--Account for specified headshot modifiers
		elseif(hs) then

			--Account for bugged -1 modifier
			if(hs == -1 ) then
					stats.headshot.value = 0
					stats.headshot.modifier = "x"

			--Modifier for cases where it's properly specified
			else
				stats.headshot.value = tonumber(hs)
				stats.headshot.modifier = "x"
			end

		--Default headshot modifier is +1
		else
			stats.headshot.modifier = "+"		
			stats.headshot.value = 1
		end

		--Save damage values
		if(attackTemplate.damage) then
			stats.damages = {}
			for key,value in pairs(attackTemplate.damage) do
				if((shouldWriteFF == true or key ~= 4) and key ~= "lookup_id" and key < 5) then
					table.insert(stats.damages, tostring(value))
				end
			end
		elseif(damageTemplate) then
			stats.damages = {}
			for key,value in pairs(damageTemplate) do
				if((shouldWriteFF == true or key ~= 4) and key ~= "lookup_id" and key < 5) then
					table.insert(stats.damages, tostring(value))
				end
			end
		end

		--Save DoT damage values and properties
		if(dot) then
			local buff = BuffTemplates[dot].buffs[1]
			local dotTable = AttackDamageValues[buff.attack_damage_template]
			stats.dotDamages = {}
			for key,value in pairs(dotTable) do
				if((shouldWriteFF == true or key ~= 4) and key ~= "lookup_id" and key < 5) then
					table.insert(stats.dotDamages, tostring(value))
				end
			end
			stats.dotProps = {
				duration = buff.duration,
				interval = buff.time_between_dot_damages
			}
		end

		--Save ranged weapon damage

		if(attackTemplate.damage_near) then
			stats.damages = nil
			stats.damageNear = {}
			for key,value in pairs(attackTemplate.damage_near) do
				if((shouldWriteFF == true or key ~= 4) and key ~= "lookup_id" and key < 5) then
					table.insert(stats.damageNear, tonumber(value))
				end
			end
		end

		if(attackTemplate.damage_far) then
			stats.damageFar = {}
			for key,value in pairs(attackTemplate.damage_far) do
				if((shouldWriteFF == true or key ~= 4) and key ~= "lookup_id" and key < 5) then
					table.insert(stats.damageFar, tonumber(value))
				end
			end
		end

		if(attackTemplate.range_dropoff_settings) then
			stats.damageDropoff = {}
			stats.damageDropoff._start = attackTemplate.range_dropoff_settings.dropoff_start
			stats.damageDropoff._end = attackTemplate.range_dropoff_settings.dropoff_end
		end

		--Save stagger properties
		--I don't have a good format for these...
		stats.stagger = {}

		if(attackTemplate.stagger_impact) then
			stats.stagger.impact = {}
			for key,value in pairs(attackTemplate.stagger_impact) do
				if(key ~= "lookup_id" and key < 4) then
					table.insert(stats.stagger.impact, tonumber(value))
				end
			end
		end

		if(attackTemplate.stagger_duration) then
			stats.stagger.time = {}
			for key,value in pairs(attackTemplate.stagger_duration) do
				if(key ~= "lookup_id" and key < 4) then
					table.insert(stats.stagger.time, tonumber(value))
				end
			end
		end

		if(attackTemplate.stagger_length) then
			stats.stagger.distance = attackTemplate.stagger_length
		end

		--Push with Dev blow
		if(WE.constants.pushUpgrades[attackTemplateName]) then
			local pushTemplate = AttackTemplates[WE.constants.pushUpgrades[attackTemplateName]]
			if not(pushTemplate) then
				EchoConsole("Wrong template in WE.constants.pushUpgrades")
				return stats
			end
			stats.upgradedPush = {}
			stats.upgradedPush.stagger = {}
			stats.upgradedPush.stagger.impact = {}
			stats.upgradedPush.stagger.time = {}
			for key,value in pairs(pushTemplate.stagger_impact) do
				if(key ~= "lookup_id" and key < 4) then
					table.insert(stats.upgradedPush.stagger.impact, tonumber(value))
					table.insert(stats.upgradedPush.stagger.time, tonumber(pushTemplate.stagger_duration[key]))
				end
			end
			stats.upgradedPush.stagger.distance = pushTemplate.stagger_length
		end

		return stats
	end,

	--Creates aoe stats
	aoe = function(aoeTemplate)

		if not aoeTemplate then return nil end

		local aoe = {}
		local ff = not aoeTemplate.no_friendly_fire
		local aoeDamageTemplate = aoeTemplate.attack_template_damage_type or "no_damage"

		aoe = WE.get.stats(aoeTemplate.attack_template, aoeDamageTemplate, nil, ff)
		if not aoe then return nil end
		aoe.radius = aoeTemplate.radius
		aoe.duration = aoeTemplate.duration
		aoe.interval = aoeTemplate.damage_interval

		if (not aoe.radius) and (aoeTemplate.radius_min or aoeTemplate.radius_max) then
			
			aoe.radiusMin = aoeTemplate.radius_min
			aoe.radiusMax = aoeTemplate.radius_max

			aoe.maxDamageRadiusMin = aoeTemplate.max_damage_radius_min
			aoe.maxDamageRadiusMax = aoeTemplate.max_damage_radius_max

			aoe.damages = nil

			aoe.damagesMin = aoeTemplate.damage_min
			aoe.damagesMax = aoeTemplate.damage_max

		elseif(aoeTemplate.damage) then

			aoe.damages = aoeTemplate.damage
			aoe.maxDamageRadius = aoeTemplate.max_damage_radius

		end

		return aoe
	end,

	--Overcharge data
	overcharge = function(weapon, action, attack, actionName, attackName)
		
		if not attack.overcharge_type then return nil end
		local overcharge = {}
		local actions = weapon.actions
		overcharge.damageInterval = attack.damage_interval
		overcharge.interval = attack.overcharge_interval
		overcharge.type = attack.overcharge_type
		overcharge.cost = PlayerUnitStatusSettings.overcharge_values[attack.overcharge_type]
		overcharge.shouldScale = attack.scale_overcharge or attack.kind == "geiser"

		overcharge.typeHeavy = attack.overcharge_type_heavy
		overcharge.costHeavy = PlayerUnitStatusSettings.overcharge_values[attack.overcharge_type_heavy]
		local chargeAction = actions["action_two"]["default"]
		local isBeam = (attack.kind == "beam")
		local isHandgun = (attack.kind == "handgun")
		if chargeAction and chargeAction.overcharge_type then
			local followingActions = chargeAction.allowed_chain_actions
			for _,chainAction in ipairs(followingActions) do
				if isBeam or chainAction.action == actionName and chainAction.sub_action == attackName and not isHandgun then
					overcharge.chargeType = isBeam and "charging" or chargeAction.overcharge_type
					overcharge.chargingCost = PlayerUnitStatusSettings.overcharge_values[overcharge.chargeType]
					break
				end
			end
		end
		if overcharge.shouldScale then
			overcharge.costMin = overcharge.cost * WE.constants.MIN_CHARGE_OVERCHARGE_MODIFIER
		end
		overcharge.explosionName = weapon.overcharge_data.explosion_template
		overcharge.explosion = overcharge.explosionName and WE.get.aoe(ExplosionTemplates[overcharge.explosionName].explosion)
		overcharge.threshold = weapon.overcharge_data.overcharge_threshold
		overcharge.decreaseRate = weapon.overcharge_data.time_until_overcharge_decreases
		overcharge.decreaseValue = weapon.overcharge_data.overcharge_value_decrease_rate

		return overcharge
	end,

	--Returns sequence name from action name
	sequenceName = function(name)
		local seq = name:match(WE.seqNameMatch) or ""
		local mod = name:match(WE.seqModifierMatch)
		if mod and table.indexOf(WE.sequenceModifiers, mod) then
			seq = seq .. "_" .. mod
		end
		return seq
	end
}

return get