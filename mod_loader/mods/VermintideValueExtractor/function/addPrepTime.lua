--[[ WE ADD PREP TIME FUNCTION
	Add preparation time to melee weapons' attackSpeed
]]--

local function addPrepTime(entry)
	local seq = WE.get.sequenceName(entry.attack)
	local storage = WE.meleeAttackPreTime[entry.template][seq]
	if storage and storage[entry.attack] then
		local speed = entry.attackSpeed
		entry.attackSpeed = entry.attackSpeed + storage[entry.attack]
		WE.writeDebug("PrepTimes", entry.name .. " " .. entry.attack .. " " .. speed .. " + " .. storage[entry.attack] .. " = " .. entry.attackSpeed, true)
	end
end

return addPrepTime