--[[ WE HEADSHOT HANDLER ]]--

local headshotHandler = {

	--Compares headshot modifiers taking into account plus prefix
	modifiersDiffer = function(m1, m2, plusPrefix)
		local mod1 = m1.modifier
		local mod2 = m2.modifier
		if(mod1 == plusPrefix .. mod2) then
			mod1 = mod2
		elseif(mod2 == plusPrefix .. mod1) then
			mod2 = mod1
		end
		return m2.value ~= m1.value or mod2 ~= mod1
	end,


	--Returns actual headshot modifier based on code from damage_utils.lua
	getRealModifier = function(modifier, value, armorType, plusPrefix)
		if(type(plusPrefix) ~= "string") then
			plusPrefix = ""
		end

		local headshot = {
			modifier = modifier,
			value = value
		}
		local headshotNoDamage = nil
		
		if (armorType == 2) then 
			if (modifier == "+" and value == 1) then
				headshot.value = 0.5
			end
			headshotNoDamage = {
				modifier = "",
				value = value
			}	
			if(string.len(modifier) ~= 0) then
				headshotNoDamage.modifier = plusPrefix .. "+"
			end
		elseif (armorType == 3) then
			if (modifier == "+" and value == 1) then
				headshot.modifier = "x"
				headshot.value = 1.5
			end
		end

		if(headshot.modifier == "+") then
			headshot.modifier = plusPrefix .. "+"
		end

		return headshot, headshotNoDamage
	end
}

return headshotHandler