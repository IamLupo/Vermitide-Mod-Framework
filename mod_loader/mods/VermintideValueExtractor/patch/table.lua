--[[ WE TABLE
	table extentions
]]

--Index of an element o in a table t
table.indexOf = function(t, o)
	if type(t) ~= "table" then
		return nil
	end
	for i,v in ipairs(t) do
		if o == v then
			return i
		end
	end
	return nil
end

--Index of table in table if all keys are the same
table.indexOfByKey = function(t1, t2)
	if type(t1) ~= "table" then
		return nil
	end
	for i,v in ipairs(t1) do
		local equals = true
		for k,vv in pairs(v) do
			if vv ~= t2[k] then
				equals = false
			end
		end
		if equals then
			return i
		end
	end
	return nil
end