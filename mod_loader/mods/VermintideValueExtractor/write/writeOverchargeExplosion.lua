--[[ WE WRITE OVERCHARGE EXPLOSION --]]

local function writeOverchargeExplosion(explosions, entry)

	local stats = entry.stats
	local overcharge = stats.overcharge
	if not overcharge then return end
	local explosionName = overcharge.explosionName
	if table.indexOf(explosions, explosionName) then
		return
	else
		table.insert(explosions, explosionName)
	end
	local aoe = overcharge.explosion
	local dotDamages = aoe.dotDamages
	local dotProps = aoe.dotProps

	local stringToWrite = WE.overchargeExplosions[explosionName] .. ";"

	--Area of effect
	stringToWrite = stringToWrite .. aoe.radius .. ";"
	stringToWrite = stringToWrite .. aoe.maxDamageRadius .. ";"
	for i=1,4 do
		if(aoe.damages[i]) then
			stringToWrite = stringToWrite .. aoe.damages[i] .. ";"
		else
			stringToWrite = stringToWrite .. "0;"
		end
	end

	--Damage over time
	local duration = dotProps.duration
	local interval = dotProps.interval

	stringToWrite = stringToWrite .. duration .. ";" .. interval .. ";"
	for i=1,4 do
		if(dotDamages[i]) then
			stringToWrite = stringToWrite .. dotDamages[i] .. ";"
		else
			stringToWrite = stringToWrite .. "-;"
		end
	end

	WE.write("Overcharge", stringToWrite, true)
end

return writeOverchargeExplosion