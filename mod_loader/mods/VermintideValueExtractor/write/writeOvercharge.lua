--[[ WE WRITE OVERCHARGE --]]

local function writeOvercharge(entry)
	
	local stats = entry.stats
	local overcharge = stats.overcharge
	if not overcharge or stats.split and stats.index and stats.index > 1 then return end

	--Class, weapon name, attack name
	local stringToWrite = entry.hero .. ";"

	if(WE.settings.weaponNameFormat == "localized") then
		stringToWrite = stringToWrite .. entry.name .. ";"
	else
		stringToWrite = stringToWrite .. entry.template .. ";"
	end

	stringToWrite = stringToWrite .. (entry.attackLocalizedAlt or entry.attackLocalized) .. ";"

	local cost = overcharge.cost
	if cost then
		if overcharge.shouldScale then
			cost = overcharge.costMin .. " - " .. cost
		end

		stringToWrite = stringToWrite .. cost .. ";"
	else
		stringToWrite = stringToWrite .. "N/A;"
	end

	if overcharge.chargingCost then
		stringToWrite = stringToWrite .. overcharge.chargingCost .. ";"
	else
		stringToWrite = stringToWrite .. "-;"
	end
	
	stringToWrite = stringToWrite .. overcharge.decreaseRate .. ";" .. overcharge.decreaseValue .. ";" .. overcharge.threshold .. ";"


	WE.write("Overcharge", stringToWrite, true)
end

return writeOvercharge