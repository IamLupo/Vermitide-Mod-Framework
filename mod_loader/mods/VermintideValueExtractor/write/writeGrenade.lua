--[[ WE WRITE GRENADE --]]

local function writeGrenade(entry)

	local name = entry.name
	local explosion = entry.explosion
	local dotDamages = explosion.dotDamages
	local dotProps = explosion.dotProps
	local aoe = entry.aoe

	local stringToWrite = WE.grenadeNames[name] .. ";"

	--Explosion
	stringToWrite = stringToWrite .. explosion.radius .. ";"
	stringToWrite = stringToWrite .. explosion.maxDamageRadius .. ";"
	for i=1,4 do
		if(explosion.damages[i]) then
			stringToWrite = stringToWrite .. explosion.damages[i] .. ";"
		else
			stringToWrite = stringToWrite .. "0;"
		end
	end

	--AoE
	if aoe then
		stringToWrite = stringToWrite .. aoe.radius .. ";"
		stringToWrite = stringToWrite .. aoe.duration .. ";"
		stringToWrite = stringToWrite .. aoe.interval .. ";"
		for i=1,4 do
			if(explosion.damages[i]) then
				stringToWrite = stringToWrite .. aoe.damages[i] .. ";"
			else
				stringToWrite = stringToWrite .. "0;"
			end
		end
	end

	--Damage over time
	if dotDamages then
		local appliedTo = "Explosion"
		if(aoe) then
			appliedTo = appliedTo .. ", AoE"			
		end
		stringToWrite = stringToWrite .. appliedTo .. ";"

		local duration = dotProps.duration
		local interval = dotProps.interval

		stringToWrite = stringToWrite .. duration .. ";" .. interval .. ";"
		for i=1,4 do
			if(dotDamages[i]) then
				stringToWrite = stringToWrite .. dotDamages[i] .. ";"
			else
				stringToWrite = stringToWrite .. "-;"
			end
		end
	end

	WE.write("Grenades", stringToWrite, true)
end

return writeGrenade