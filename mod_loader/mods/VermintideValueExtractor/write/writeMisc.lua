--[[ WE WRITE MISC
	Writes movement speed when blocking, dodge info, stamina and template
--]]

local function writeMisc(entry)
	local chargeZoomSlowdown = entry.movement.chargeZoomSlowdown or "1"

	local stringToWrite = entry.hero .. ";" .. entry.name

	stringToWrite = stringToWrite .. ";x;" .. chargeZoomSlowdown

	stringToWrite = stringToWrite .. ";" .. entry.dodge.distance*2
	stringToWrite = stringToWrite .. ";x;" .. entry.dodge.speed

	stringToWrite = stringToWrite .. ";" .. entry.dodge.count

	if(entry.stamina) then
		local stamina = 2
		local staminaString = ""
		while(entry.stamina >= stamina) do
			staminaString = staminaString .. "■ "
			stamina = stamina + 2
		end
		if(stamina - entry.stamina == 1) then
			staminaString = staminaString .. "□ "
		end
		staminaString = staminaString .. ";" .. entry.stamina
		stringToWrite = stringToWrite .. ";" .. staminaString
	else
		stringToWrite = stringToWrite .. ";;" 
	end
	stringToWrite = stringToWrite .. ";" .. entry.template
	stringToWrite = stringToWrite .. "\n"

	WE.write("Misc", stringToWrite)
end

return writeMisc