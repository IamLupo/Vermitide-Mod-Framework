--[[ WE WRITE MELEE BETTER
	Writes melee weapon damage numbers neatly. Also writes DoT for melee weapons.
--]]

local headshotHandler = Mods.require(WE.name, "function/headshot")

local function writeMeleeBetter(entry, toCompare, writeDot)

	if(writeDot and not entry.stats[1].dotDamages or toCompare and table.indexOf(WE.constants.newWeapons,entry.name)) then
		return
	end

	local damNumbers = ""
	if not(writeDot) then
		for i=1,3 do
			local headshot, headshotNoDamage = headshotHandler.getRealModifier(entry.stats[1].headshot.modifier, entry.stats[1].headshot.value, i, WE.settings.plusPrefix)
			if not(toCompare) then	
				
				local dmgApplicable = false
				local noDmgApplicable = false

				for m=1,WE.constants.MAX_TARGETS+1 do
					if (entry.stats[m]) then
						if(entry.stats[m].damages[i] == "0") then
							noDmgApplicable = true
						else
							dmgApplicable = true
						end
					end
					if(dmgApplicable and noDmgApplicable) then
						break
					end
				end		

				if(dmgApplicable) then
					damNumbers = damNumbers .. headshot.modifier .. tostring(headshot.value) .. ";"
				else
					damNumbers = damNumbers .. "-;"
				end

				if(headshotNoDamage) then
					if(noDmgApplicable) then
						damNumbers = damNumbers .. headshotNoDamage.modifier .. tostring(headshotNoDamage.value) .. ";"
					else
						damNumbers = damNumbers .. "-;"
					end
				end
			end
			for m=1,WE.constants.MAX_TARGETS+1 do
				local stat = entry.stats[m]
				if(stat) then
					local targetHeadshot, targetHeadshotNoDamage = headshotHandler.getRealModifier(stat.headshot.modifier, stat.headshot.value, i)
					damNumbers = damNumbers .. stat.damages[i]
					if(
						not(toCompare) and
						tonumber(stat.damages[i]) ~= 0 and 
						headshotHandler.modifiersDiffer(headshot, targetHeadshot, WE.settings.plusPrefix) 
					) then
						damNumbers = damNumbers .. "(" .. targetHeadshot.modifier .. tostring(targetHeadshot.value) .. ")"				
					end
					damNumbers = damNumbers .. ";"
				else
					damNumbers = damNumbers .. ";"
				end
			end
		end
	else
		for i=1,3 do
			damNumbers = damNumbers .. entry.stats[1].dotDamages[i] .. ";" .. entry.stats[1].dotProps.interval .. ";" .. entry.stats[1].dotProps.duration .. ";"

		end
	end

	local maxTargets
	if(toCompare and entry.maxTargets == "∞") then
		maxTargets = 999
	else
		maxTargets = entry.maxTargets
	end
	
	local formattedHeadshotModifier = nil
	if(toCompare) then
		if(entry.stats[1].headshot.modifier == "+") then
			formattedHeadshotModifier = WE.settings.plusPrefix .. "+"
		else
			formattedHeadshotModifier = entry.stats[1].headshot.modifier
		end
	end

	local stringToWrite = entry.hero .. ";" 
	if(WE.settings.weaponNameFormat == "localized") then
		stringToWrite = stringToWrite .. entry.name
	else
		stringToWrite = stringToWrite .. entry.template
	end

	stringToWrite = stringToWrite .. ";" .. entry.attackLocalized


	if not writeDot then
		--stringToWrite = stringToWrite .. ";" .. math.floor((1 / entry.attackSpeed) * 100 + 0.5) / 100 --attacks per second
		stringToWrite = stringToWrite .. ";" .. entry.attackSpeed
		stringToWrite = stringToWrite .. ";" .. maxTargets 
	end
	if(formattedHeadshotModifier) then
		stringToWrite = stringToWrite .. ";" .. formattedHeadshotModifier .. ";" .. entry.stats[1].headshot.value
	end
	stringToWrite = stringToWrite .. ";" .. damNumbers .. "\n"

	if(toCompare) then
		WE.write("MeleeCompare", stringToWrite)
	else
		WE.write("Melee", stringToWrite)
	end
end

return writeMeleeBetter