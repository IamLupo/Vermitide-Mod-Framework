--[[ WE WRITE RANGED
	Write ranged weapon values 
--]]

local headshotHandler = Mods.require(WE.name, "function/headshot")

local function writeRanged(entry, toCompare, partToWrite)

	local stats = entry.stats
	local ammo = entry.ammoData
	local timedAoe = entry.timedAoe
	local aoe = entry.aoe or timedAoe
	local dodge = entry.dodge
	local radiusMin = stats.radiusMin
	local radiusMax = stats.radiusMax
	local dotDamages = stats.dotDamages or aoe and aoe.dotDamages or timedAoe and timedAoe.dotDamages
	local dotProps = stats.dotProps or aoe and aoe.dotProps or timedAoe and timedAoe.dotProps
	
	local shots = stats.shots or "-"
	if ammo.usesAll then
		shots = shots .. " (" .. shots .. ")"
	elseif ammo.ammoUsage then
		shots = shots .. " (" .. ammo.ammoUsage .. ")"
	end 

	local lifeSpan = stats.lifeSpan or "-"
	local range = stats.range or "-"
	if toCompare then
		WE.writeDebug("Debug", entry.name .. " " .. partToWrite ..  "\n")
	end
	if( 
		toCompare and
		(
			(partToWrite == 2 and not dotDamages) or 
			(partToWrite == 3 and not aoe and not radiusMax) or
			table.indexOf(WE.constants.newWeapons,entry.name)
		)
	) then
		return
	end

	--Class, weapon name, attack name
	local stringToWrite = entry.hero .. ";"

	if(WE.settings.weaponNameFormat == "localized") then
		stringToWrite = stringToWrite .. entry.name .. ";"
	else
		stringToWrite = stringToWrite .. entry.template .. ";"
	end

	stringToWrite = stringToWrite .. entry.attackLocalized .. ";"

	--Main values
	if(not toCompare or partToWrite == 1) then

		--Archetype
		stringToWrite = stringToWrite .. WE.rangedAttacks[entry.attackKind] .. ";"

		--Max targets/max penetrations
		if(stats.maxTargets) then
			stringToWrite = stringToWrite .. stats.maxTargets .. ";" 
		elseif(stats.maxPenetrations) then
			stringToWrite = stringToWrite .. stats.maxPenetrations .. ";" 
		else
			stringToWrite = stringToWrite .. "∞;" 
		end

		--Shots per fire
		if not(toCompare) then
			stringToWrite = stringToWrite .. shots .. ";"
		end

		--Single headshot modifier
		if(toCompare) then
			if stats.headshot then
				local formattedHeadshotModifier = stats.headshot.modifier
				if(formattedHeadshotModifier == "+") then
					formattedHeadshotModifier = WE.settings.plusPrefix .. formattedHeadshotModifier
				end
				stringToWrite = stringToWrite .. formattedHeadshotModifier .. stats.headshot.value .. ";"
			else
				stringToWrite = stringToWrite .. "N/A;"
			end
		end

		--Lifespan
		if not(toCompare) then
			stringToWrite = stringToWrite .. lifeSpan .. ";"
		end

		--Range
		if(radiusMax) then
			stringToWrite = stringToWrite .. "-;"
		else
			stringToWrite = stringToWrite .. range .. ";"
		end

		--Damage
		if(stats.damages and not radiusMax) then
			stringToWrite = stringToWrite .. "-;"
			for i=1,4 do
				if(stats.damages[i]) then
					stringToWrite = stringToWrite .. stats.damages[i] .. ";"
				else
					stringToWrite = stringToWrite .. "-;"
				end
			end
		elseif(stats.damageDropoff) then
			stringToWrite = stringToWrite .. stats.damageDropoff._start .. " - " .. stats.damageDropoff._end .. ";"
			for i=1,4 do
				stringToWrite = stringToWrite .. stats.damageNear[i] .. " -> " .. stats.damageFar[i] .. ";"
			end
		else
			stringToWrite = stringToWrite .. "-;-;-;-;-;"
		end 

		--Headshot modifiers for different armor types
		if not toCompare then
			if stats.headshot then
				for i=1,3 do
					local headshot, headshotNoDamage = headshotHandler.getRealModifier(stats.headshot.modifier, stats.headshot.value, i, WE.settings.plusPrefix)
					if headshotNoDamage and i == 2 and 
					headshotHandler.modifiersDiffer(headshot, headshotNoDamage, WE.settings.plusPrefix) and
					(
						stats.damages and not radiusMax and tonumber(stats.damages[i]) == 0 or 
						stats.damageDropoff and (stats.damageNear[i] == 0 or stats.damageFar[i] == 0)
					) then

						if stats.damageDropoff then
							if stats.damageNear[i] == 0 and stats.damageFar[i] == 0 then
								stringToWrite = stringToWrite .. headshotNoDamage.modifier .. headshotNoDamage.value .. ";"
							elseif stats.damageNear[i] == 0 then
								stringToWrite = stringToWrite .. headshotNoDamage.modifier .. headshotNoDamage.value .. " -> "
								headshot.modifier = string.gsub(headshot.modifier, WE.settings.plusPrefix, "")
								stringToWrite = stringToWrite .. headshot.modifier .. headshot.value .. ";"
							else
								stringToWrite = stringToWrite .. headshot.modifier .. headshot.value .. " -> "
								headshotNoDamage.modifier = string.gsub(headshotNoDamage.modifier, WE.settings.plusPrefix, "")
								stringToWrite = stringToWrite .. headshotNoDamage.modifier .. headshotNoDamage.value .. ";"
							end
						else
							stringToWrite = stringToWrite .. headshotNoDamage.modifier .. headshotNoDamage.value .. ";"
						end
					else
						stringToWrite = stringToWrite .. headshot.modifier .. headshot.value .. ";"
					end
				end
			else
				stringToWrite = stringToWrite .. "N/A;N/A;N/A;"
			end
		end

		--Ammo related stuff
		if(ammo.amount) then
			stringToWrite = stringToWrite .. ammo.amount .. ";"
			if not(toCompare) then
				stringToWrite = stringToWrite .. ammo.clipSize .. ";" 
			end
			if(ammo.reloadTime) then
				stringToWrite = stringToWrite .. ammo.reloadTime .. ";"
			else
				stringToWrite = stringToWrite .. ";"
			end
			if(ammo.activeReloadTime) then
				stringToWrite = stringToWrite .. ammo.activeReloadTime .. ";"
			else
				stringToWrite = stringToWrite .. ";"
			end
			if ammo.refireDelay then
				if(ammo.clipSize == 1 or ammo.usesAll) then
					stringToWrite = stringToWrite .. ammo.refireDelay + ammo.reloadTime .. ";"
				else
					stringToWrite = stringToWrite .. ammo.refireDelay .. ";"
				end
			else
				stringToWrite = stringToWrite .. "N/A;"
			end
		elseif(ammo.refireDelay) then
			if not(toCompare) then
				stringToWrite = stringToWrite .. "-;"
			end
			stringToWrite = stringToWrite .. "-;-;-;"
			stringToWrite = stringToWrite .. ammo.refireDelay .. ";"
		else
			if not(toCompare) then
				stringToWrite = stringToWrite .. "-;"
			end
			stringToWrite = stringToWrite .. "-;-;-;-;"
		end

		--Overhead interruption
		local staggerImpact = stats.stagger and stats.stagger.impact[2]
		local canStagger = staggerImpact and (WE.staggerTypes[3][staggerImpact] and WE.settings.plusPrefix .. "+" or "-") or "N/A"
		stringToWrite = stringToWrite .. canStagger .. ";"
	end

	--Damage over time
	if(not toCompare or partToWrite == 2) then
		if(dotDamages) then 
			local duration = dotProps.duration
			local interval = dotProps.interval
			local dotDuration =  aoe and aoe.dotProps and aoe.dotProps.duration
			if dotDuration and aoe and aoe.duration and not radiusMax then
				dotDuration = dotDuration .. "*" .. aoe.duration
			end

			if not(toCompare) then
				local appliedTo = ""
				if(stats.damages and not radiusMax or stats.damageDropoff) then
					appliedTo = appliedTo .. "Init"
				end
				if(aoe or radiusMax) then
					if(string.len(appliedTo) ~= 0) then
						appliedTo = appliedTo .. ", "
					end
					appliedTo = appliedTo .. "AoE"
				end
				stringToWrite = stringToWrite .. appliedTo .. ";"
			end

			if(not radiusMax and aoe and dotDuration and dotDuration ~= dotProps.duration) then
				duration = duration .. "/" .. dotDuration
			end
			if(not radiusMax and aoe and aoe.dotProps and aoe.dotProps.interval and aoe.dotProps.interval ~= dotProps.interval) then
				interval = interval .. "/" .. aoe.dotProps.interval
			end
			stringToWrite = stringToWrite .. duration .. ";" .. interval .. ";"
			for i=1,4 do
				if(dotDamages[i]) then
					local damage = math.floor(dotDamages[i] * 100 + 0.5) / 100
					stringToWrite = stringToWrite .. damage .. ";"
				else
					stringToWrite = stringToWrite .. "-;"
				end
			end
		else
			stringToWrite = stringToWrite .. ";;;;;;"
		end
	end

	--Area of effect
	if(not toCompare or partToWrite == 3) then
		if(aoe or radiusMax) then
			if(radiusMax) then	
				--[[
				if(not toCompare) then
					stringToWrite = stringToWrite .. radiusMin.value .. ";"
				elseif(radiusMin.min == 0) then
					stringToWrite = stringToWrite .. radiusMin.max .. ";"
				else
					stringToWrite = stringToWrite .. radiusMin.min .. " - " .. radiusMin.max .. ";"
				end	
				if(not toCompare) then
					stringToWrite = stringToWrite .. radiusMax.value .. ";"
				elseif(radiusMax.min == 0) then
					stringToWrite = stringToWrite .. radiusMax.max .. ";"
				else
					stringToWrite = stringToWrite .. radiusMax.min .. " - " .. radiusMax.max .. ";"
				end
				--]]
				stringToWrite = stringToWrite .. radiusMin.value .. " - " .. radiusMax.value .. ";-;"
				for i=1,4 do
					if(stats.damages[i]) then
						stringToWrite = stringToWrite .. stats.damages[i] .. ";"
					else
						stringToWrite = stringToWrite .. "-;"
					end
				end
			elseif(aoe.damages) then	

				-- if aoe.duration then
				-- 	stringToWrite = stringToWrite .. aoe.duration .. ";" .. aoe.interval .. ";"
				-- else
				-- 	stringToWrite = stringToWrite .. ";;"
				-- end

				if(aoe.maxDamageRadius) then
					stringToWrite = stringToWrite .. ";" .. aoe.maxDamageRadius .. ";"
				elseif(aoe.radius) then
					stringToWrite = stringToWrite .. aoe.radius .. ";-;"
				else
					stringToWrite = stringToWrite .. "-;-;"
				end
				for i=1,4 do
					if(aoe.damages[i]) then
						stringToWrite = stringToWrite .. aoe.damages[i] .. ";"
					else
						stringToWrite = stringToWrite .. "0;"
					end
				end
			elseif(aoe.damagesMin) then
				stringToWrite = stringToWrite .. aoe.radiusMin .. " - " .. aoe.radiusMax .. ";"
				if(aoe.maxDamageRadiusMax) then
					stringToWrite = stringToWrite .. aoe.maxDamageRadiusMin .. " - " .. aoe.maxDamageRadiusMax .. ";"
				else
					stringToWrite = stringToWrite .. ";"
				end
				for i=1,4 do
					stringToWrite = stringToWrite .. aoe.damagesMin[i] .. " - " .. aoe.damagesMax[i] .. ";"
				end
			end
		end
	end

	if(toCompare) then
		stringToWrite = string.gsub(stringToWrite, "-;", ";")
		WE.write("RangedCompare", stringToWrite, true)
	else
		WE.write("Ranged", stringToWrite, true)
	end
end

return writeRanged