--[[ WE WRITE ITEMS --]]

local write = {

	weapon = function(item)
		local str = 
			WE.raritiesLocalized[item.rarity] .. ";" .. 
			item.localized.canWield[1] .. ";" .. 
			item.localized.kind .. ";"
		for i=1,3 do
			if item.localized.traits[i] then
				str = str .. item.localized.traits[i] .. ";"
			else
				str = str .. ";"
			end
		end
		str = str .. item.localized.name .. ";" .. item.key .. ";"
		WE.write("ItemsWeapons", str, true)
	end,

	trinket = function(item)
		local str = item.localized.rarity .. ";" .. 
					item.localized.name .. ";" .. 
					item.key .. ";" .. 
					(
						item.localized.traits[1] and (
							item.localized.traits[1] .. ";" .. 
							BackendUtils.get_trait_description(item.traits[1], item.rarity) .. ";"
						) or 
						"None;Cosmetic trinket from Collector's Edition;"
					) 
		WE.write("ItemsTrinkets", str, true)
	end,

	hat = function(item)
		local str = item.localized.rarity .. ";" .. 
					item.localized.canWield[1] .. ";" .. 
					item.localized.name .. ";" .. 
					item.key .. ";"
		WE.write("ItemsHats", str, true)
	end,
	
	unknown = function(item)
		return
	end,
}

local function writeItems(item)
	write[item.type](item)
end

return writeItems