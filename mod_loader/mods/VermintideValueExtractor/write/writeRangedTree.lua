--[[ WE writeRangedTree 
	Info tree, was used to look for relevant keys, now unused
]]--

local writeTable = Mods.require(WE.name, "write/writeTable")

local function writeRangedTree(names, templateName, actions)
	WE.writeDebug("RangedTree", names[1] .. " " .. templateName, true)
	for i=1,#WE.rangedMainActions do
		WE.writeDebug("RangedTree", "	" .. WE.rangedMainActions[i], true)
		if actions[WE.rangedMainActions[i]] then
			for actionName,action in pairs(actions[WE.rangedMainActions[i]]) do
				if(WE.rangedAttacks[action.kind]) then
					WE.writeDebug("RangedTree", "		" .. actionName, true)
					for sAname,subAction in pairs(action) do
						if(table.indexOf(WE.rangedAttrs,sAname)) then
							WE.writeDebug("RangedTree", "			" .. sAname .. " " .. tostring(subAction), true)
						elseif(sAname == "projectile_info") then
							WE.writeDebug("RangedTree", "			" .. sAname, true)
							for projKey,projVal in pairs(subAction) do
								if(projKey == "timed_data") then
									WE.writeDebug("RangedTree", "				" .. projKey , true)
									if(projVal.aoe) then
										WE.writeDebug("RangedTree", "					" .. "aoe" .. " " .. tostring(projVal.aoe) , true)
									end
									if(projVal.life_time) then	
										WE.writeDebug("RangedTree", "					" .. "life_time" .. " " .. tostring(projVal.life_time) , true)
									end
								elseif(projKey == "impact_data") then
									WE.writeDebug("RangedTree", "				" .. projKey , true)
									writeTable(projVal, WE.writeDebug, "RangedTree", "					")
								end
							end
						end
					end
				end
			end
		else
			WE.writeDebug("RangedTree", "		None", true)
		end
	end
	WE.writeDebug("RangedTree", "\n")
end

return writeRangedTree