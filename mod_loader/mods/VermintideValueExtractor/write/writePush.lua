--[[ WE WRITE PUSH
	Writes info about pushing
--]]

local function writePush(entry)
	local interrupts, upgradedInterrupts
	if not(entry.push.upgradedPush) then
		EchoConsole("No upgraded push info")
		return
	end

	if(WE.staggerTypes[3][entry.push.stagger.impact[2]]) then
		interrupts = "Yes"
	else
		interrupts = "No"
	end
	if(WE.staggerTypes[3][entry.push.upgradedPush.stagger.impact[2]]) then
		upgradedInterrupts = "Yes"
	else
		upgradedInterrupts = "No"
	end

	local stringToWrite = 	entry.hero .. ";" .. 
							entry.name .. ";" .. 
							entry.push.radius .. ";" ..
							interrupts .. ";" .. 
							entry.push.stagger.distance .. ";" .. 
							WE.staggerTypes[1][entry.push.stagger.impact[1]] .. ";" .. 
							entry.push.stagger.time[1] .. ";" .. 
							WE.staggerTypes[2][entry.push.stagger.impact[2]] .. ";" .. 
							entry.push.stagger.time[2]

	stringToWrite = stringToWrite .. ";" .. 
					upgradedInterrupts .. ";" .. 
					entry.push.upgradedPush.stagger.distance .. ";" .. 
					WE.staggerTypes[1][entry.push.upgradedPush.stagger.impact[1]] .. ";" ..
					entry.push.upgradedPush.stagger.time[1] .. ";" ..  
					WE.staggerTypes[2][entry.push.upgradedPush.stagger.impact[2]] .. ";" ..
					entry.push.upgradedPush.stagger.time[2] .. ";" ..  "\n"

	WE.write("Push", stringToWrite)
end

return writePush