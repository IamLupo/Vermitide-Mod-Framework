--[[ WE WRITE TABLE 
	Recursively writes nested tables to filename via write function with proper tabulation
--]]
local function writeTable(t, write, filename, tabs, maxDepth)
	if maxDepth then
		maxDepth = maxDepth - 1
	end
	for k,v in pairs(t) do
		if(type(v) == "table") then
			write(filename, tabs .. tostring(k), true)
			if maxDepth and maxDepth == 0 then
				write(filename, tabs .. "	Max depths exceeded", true)
			else
				writeTable(v, write, filename, tabs .. "	", maxDepth)
			end
		else
			local val = type(v) ~= "number" and tostring(v) or v
			write(filename, tabs .. tostring(k) .. " " .. val, true)
		end
	end
end
return writeTable