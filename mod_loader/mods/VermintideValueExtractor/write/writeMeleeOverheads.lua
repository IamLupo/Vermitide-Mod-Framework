--[[ WE WRITE MELEE OVERHEADS --]]

local function writeStagger(entry)
	local staggerNumbers = ""
	for k,stat in pairs(entry.stats) do

		local staggerImpact = stat.stagger.impact[2]
		local canStagger = WE.staggerTypes[3][staggerImpact] and "+" or "-"

		staggerNumbers = staggerNumbers .. canStagger .. ";"
	end

	local stringToWrite = entry.hero .. ";" .. entry.name .. ";"  .. entry.attackLocalized .. ";" .. entry.maxTargets  .. ";" .. staggerNumbers .. "\n"
	WE.write("MeleeOverheads", stringToWrite)
end

return writeStagger