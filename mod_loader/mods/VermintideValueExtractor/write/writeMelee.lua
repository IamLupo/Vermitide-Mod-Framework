--[[ WE WRITE MELEE
	Old write melee damage numbers function
--]]

local function writeMelee(entry,toCompare,separate)
	for i=1,3 do
		local damNumbers = ""
		for k,stat in pairs(entry.stats) do
			damNumbers = damNumbers .. stat.damages[i]
			if((stat.headshot.value ~= entry.stats[1].headshot.value and stat.headshot.modifier == entry.stats[1].headshot.modifier or stat.headshot.modifier ~= entry.stats[1].headshot.modifier) and not toCompare) then
				damNumbers = damNumbers .. "(" .. stat.headshot.modifier .. tostring(stat.headshot.value) .. ")"				
			end
			damNumbers = damNumbers .. ";"
		end
		if(entry.stats[1].dotDamages) then
			if(toCompare) then
				damNumbers = damNumbers .. "DOT;" .. entry.stats[1].dotDamages[i] .. ";" .. entry.stats[1].dotProps.interval .. ";" .. entry.stats[1].dotProps.duration .. ";"
			else
				damNumbers = damNumbers .. entry.stats[1].dotDamages[i] .. " each " .. entry.stats[1].dotProps.interval .. "sec for " .. entry.stats[1].dotProps.duration .. "sec;"
			end
		end

		local stamina = 2
		local staminaString = ""
		while(entry.stamina >= stamina) do
			staminaString = staminaString .. "■ "
			stamina = stamina + 2
		end
		if(math.fmod(entry.stamina,2) ~= 0) then
			staminaString = staminaString .. "□ "
		end

		local maxTargets
		if(toCompare and entry.maxTargets == "∞") then
			maxTargets = 999
		else
			maxTargets = entry.maxTargets
		end
		
		local stringToWrite = entry.hero .. ";" .. entry.name

		if not(toCompare) then
			stringToWrite = stringToWrite .. " " .. staminaString  .. ";" .. entry.template
		end

		stringToWrite = stringToWrite .. ";" .. entry.attackLocalized  .. ";" .. maxTargets

		if (toCompare) then
			stringToWrite = stringToWrite .. ";" .. entry.stats[1].headshot.modifier .. ";" .. entry.stats[1].headshot.value
		else
			stringToWrite = stringToWrite .. ";" .. entry.stats[1].headshot.modifier .. entry.stats[1].headshot.value
		end

		
		if not(separate) then
			stringToWrite = stringToWrite .. ";" .. WE.armorTypes[i]
		end
		stringToWrite = stringToWrite .. ";" .. damNumbers .. "\n"

		if(toCompare) then
			if not(separate) then
				WE.write("MeleeCompare", stringToWrite)
			else
				WE.files[WE.fileNames.separate[i]]:write(stringToWrite)
			end
		else
			if not(separate) then
				WE.write("MeleeDamage", stringToWrite)
			else
				WE.files[WE.fileNames.separate[i]]:write(stringToWrite)
			end
		end
	end
end

return writeMelee