--[[ WE WRITE STAGGER
	Unfinished format for stagger
--]]

local function writeStagger(entry)
	for i=1,2 do

		local staggerNumbers = ""
		for k,stat in pairs(entry.stats) do

			local staggerDistance
			if(stat.stagger.impact[i] == 4 or stat.stagger.impact[i] == 1) then
				staggerDistance = 0
			else
				staggerDistance = stat.stagger.distance
			end

			local staggerImpact, staggerTime
			if(stat.stagger.impact[i] == 0) then
				staggerImpact = "-"
				staggerTime = "-"
				staggerDistance = "-"
			else
				staggerImpact = stat.stagger.impact[i]
				staggerTime = stat.stagger.time[i]
			end

			staggerNumbers = staggerNumbers .. staggerImpact .. ";" .. staggerTime .. ";" .. staggerDistance .. ";"
		end

		local stringToWrite = entry.hero .. ";" .. entry.name .. ";"  .. entry.attackLocalized .. ";" .. entry.maxTargets  .. ";" .. WE.armorTypes[i] .. ";" .. staggerNumbers .. "\n"
		WE.write("MeleeStagger", stringToWrite)
	end
end

return writeStagger