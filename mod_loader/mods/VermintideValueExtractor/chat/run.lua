--[[ WE RUN
	Type in the chat: /we
	
	Vermintide Value Extractor
--]]
local args = {...}
if not WE or not WE.settings or not WE.constants then
	Mods.exec("VermintideValueExtractor", "init")
end

local function showInfo()
	EchoConsole(WE.info.title)
	EchoConsole("Usage:")
	EchoConsole(WE.info.offset .. WE.info.header)
	EchoConsole("Commands:")
	for k,v in pairs(WE.actions) do
		EchoConsole(WE.info.offset .. v.info)
	end
	EchoConsole(WE.info.footer)
end

local command, option, value1, value2
if args[1] then
	for v in string.gmatch(args[1], "%S+") do
	   if not command then command = v elseif not option then option = v elseif not value1 then value1 = v elseif not value2 then value2 = v else break end
	end
end
if not command then
	showInfo()
	return
end
local actionInfo = WE.actions[command]
if not actionInfo then
	EchoConsole("Invalid command")
	showInfo()
	return
end
Mods.exec(WE.name, "action/" .. actionInfo.action, {option, value1, value2})