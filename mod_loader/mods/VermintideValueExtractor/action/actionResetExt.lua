--[[ WE RESET
	Resets WE
]]--
local ExtrenalSettingsFile = io.open([[.\mod_loader\]] .. "WEExternalSettings.lua", "w")
if ExtrenalSettingsFile then
	ExtrenalSettingsFile:close()
end

Mods.exec(WE.name, "init")

EchoConsole("External settings reset")