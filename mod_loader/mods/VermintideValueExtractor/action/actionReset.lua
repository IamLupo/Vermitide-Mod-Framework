--[[ WE RESET
	Resets WE
]]--
local ExtrenalSettingsFile = io.open([[.\mod_loader\]] .. "WEExternalSettings.lua", "w")
if ExtrenalSettingsFile then
	ExtrenalSettingsFile:close()
end

Mods.exec(WE.name, "init")

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

--Applying default settings
for k,info in pairs(WE.settingsInfo) do
	WE.settings[info.ref] = info.default
	set(info.setting, info.default)
end
save()

EchoConsole("Options reset")