--[[ WE HELP --]]

local args = {...}
local actionInfo = WE.actions[args[1]]
if actionInfo then
	EchoConsole("Usage:")
	EchoConsole(WE.info.offset .. "/we " .. actionInfo.info)
	EchoConsole("Info:")
	for i,v in ipairs(actionInfo.description) do
		EchoConsole(WE.info.offset .. v)
	end
else
	Mods.exec(WE.name, "chat/run")
end