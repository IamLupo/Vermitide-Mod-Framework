--[[ WE EXPORT
	Runs the main script
--]]
local args = {...}

if args[1] == "-a" then
	local folder = WE.settings.folder
	local prefix = ""
	local option, value
	if args[2] then
		if args[3] and args[2] ~= "folder" then
			option = args[2]
			value = args[3]
		else
			prefix = args[3] or args[2]
		end
	end
	WE.setOption("rarity", "white", true)
	WE.setOption("folder", prefix .. "PlentifulCommon", true)
	Mods.exec(WE.name, "main", {option, value})
	WE.setOption("rarity", "blue", true)
	WE.setOption("folder", prefix .. "Rare", true)
	Mods.exec(WE.name, "main", {option, value})
	WE.setOption("rarity", "orange", true)
	WE.setOption("folder", prefix .. "ExoticVeteran", true)
	Mods.exec(WE.name, "main", {option, value})
	WE.setOption("folder", "folder", true)
	EchoConsole("All rarities exported")
else
	Mods.exec(WE.name, "main", args)
end