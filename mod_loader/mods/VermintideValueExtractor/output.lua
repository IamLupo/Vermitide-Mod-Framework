--[[ WE OUTPUT
	Prints everything
	Some output formats are "bad" i.e. I either didn't finish them or made new better formats
--]]

local writeTable = Mods.require(WE.name, "write/writeTable")	

local writeMelee = Mods.require(WE.name, "write/writeMelee")
local writeMeleeBetter = Mods.require(WE.name, "write/writeMeleeBetter")
local writeMeleeOverheads = Mods.require(WE.name, "write/writeMeleeOverheads")

local writeRanged = Mods.require(WE.name, "write/writeRanged")

local writePush = Mods.require(WE.name, "write/writePush")
local writeMisc = Mods.require(WE.name, "write/writeMisc")
local writeGrenade = Mods.require(WE.name, "write/writeGrenade")

local writeOvercharge = Mods.require(WE.name, "write/writeOvercharge")	
local writeOverchargeExplosion = Mods.require(WE.name, "write/writeOverchargeExplosion")	

local writeItem = Mods.require(WE.name, "write/writeItem")	

--Writing all stuff from WE for shits and giggles
writeTable(WE, WE.writeDebug, "PlainWETable", "")

local lastWeapon = nil
for i,entry in ipairs(WE.meleeWeapons) do

	local seq = WE.get.sequenceName(entry.attack)

	--Attack has a sequence associated with it
	local canBeInSequence = WE.meleeAttackSequence[entry.template][seq]

	--Attack is in a sequence
	local isInSequence = table.indexOf(WE.meleeAttackSequence[entry.template][seq],entry.attack)

	--Attack is doable after a push
	local isAfterPush = table.indexOf(entry.push.chainActions, entry.attack)

	--Allowed attack
	local isAllowed = table.indexOf(WE.allowedPrefixes, entry.attack:match(WE.seqPrefixMatch))

	--List unreachable attacks and attacks not belonging to heavy\light sequences (MeleeWeirdAttacks file)
	if not isInSequence then
		WE.writeDebug("MeleeWeirdAttacks", entry.hero .. " " .. entry.name .. " " .. entry.attack .. " " .. seq .. " " .. entry.attackLocalized)
		local astrx = ""
		if not isAllowed then
			astrx = "***"
			WE.writeDebug("MeleeWeirdAttacks", " - ATTACK NOT ALLOWED BY WE!")
		elseif canBeInSequence then
			if isAfterPush then
				astrx = "*"
				WE.writeDebug("MeleeWeirdAttacks", " - attack after push")
			else
				astrx = "**"
				entry.attackSpeed = "N/A"
				WE.writeDebug("MeleeWeirdAttacks", " - ATTACK UNREACHABLE!")
			end
		else
			WE.writeDebug("MeleeWeirdAttacks", " - push-stab or special attack")
		end
		entry.attackLocalized = entry.attackLocalized .. astrx
		WE.writeDebug("MeleeWeirdAttacks", "\n")		
	end

	if isInSequence or isAllowed and (not canBeInSequence or isAfterPush or WE.settings.shouldWriteUnreachableAttacks) then
		writeMeleeBetter(entry,false)	
		writeMeleeBetter(entry,true)
		writeMeleeOverheads(entry)
	end

	--Write push
	--TODO: add shield bash info function to here	
	if(lastWeapon ~= entry.template) then
		lastWeapon = entry.template
		writePush(entry)
	end	

	--Write bad formats
	--writeMelee(entry)
	--writeMelee(entry,true)
	--writeMelee(entry,true,true)
end
WE.write("Melee", "\n")


--Melee DoT
if WE.settings.shouldWriteHeaders then
	WE.write("Melee", WE.headers.additional["MeleeDoT"])
end
for i,entry in ipairs(WE.meleeWeapons) do
	writeMeleeBetter(entry,false,true)

	--DoT for compare. There's literally one line so fuck it
	--writeMeleeBetter(entry,true,true)
end

--Melee weapons data in plain format
for i,v in ipairs(WE.meleeWeapons) do
	WE.writeDebug("PlainMeleeTable", v.name, true)
	writeTable(v, WE.writeDebug, "PlainMeleeTable", "	")
	WE.writeDebug("PlainMeleeTable", "\n")
end

--Ranged weapons data in plain format
for i,v in ipairs(WE.rangedWeapons) do
	WE.writeDebug("PlainRangedTable", v.name, true)
	writeTable(v, WE.writeDebug, "PlainRangedTable", "	")
	WE.writeDebug("PlainRangedTable", "\n")
end

--Ranged
for i,entry in ipairs(WE.rangedWeapons) do
	writeRanged(entry, false)
	writeOvercharge(entry)
end

--Overcharge explosions
WE.write("Overcharge", "\n" .. WE.headers.additional["OverchargeExplosions"])
local explosions = {}
for i,entry in ipairs(WE.rangedWeapons) do
	writeOverchargeExplosion(explosions, entry)
end

--Ranged compare
--We write initial hit, dot and aoe separately
for i=1,3 do
	if(i > 1) then
		WE.write("RangedCompare", "\n")
	end
	if WE.settings.shouldWriteHeaders then
		if i == 2 then
			WE.write("RangedCompare", WE.headers.additional["RangedCompareDoT"])
		elseif i == 3 then
			WE.write("RangedCompare", WE.headers.additional["RangedCompareAoE"])
		end
	end
	for _,entry in ipairs(WE.rangedWeapons) do
		writeRanged(entry, true, i)
	end
end

--Write misc
lastWeapon = nil
for i,entry in ipairs(WE.combinedWeapons) do		
	if(lastWeapon ~= entry.template) then
		lastWeapon = entry.template
		writeMisc(entry)
	end
end

--Grenades
for k,grenade in pairs(WE.grenades) do
	writeGrenade(grenade)
end

--Items
for _, item in ipairs(WE.items) do
	writeItem(item)
end
--Debug
for a,b in pairs(WE.meleeAttackSequence) do
	for k,v in pairs(b) do
		WE.writeDebug("Debug", a, true)
		for i,val in pairs(v) do
			WE.writeDebug("Debug", i .. " " .. val, true)
		end
		if WE.settings.debug then
			for k,v in pairs(WE.meleeAttackPreTime[a][k]) do
				WE.writeDebug("Debug", "	" .. k .. " " .. v, true)
			end
		end
	end
end

--Close files
for key,file in pairs(WE.files) do	file:close() end

EchoConsole("Data successfully exported into " .. WE.numOfFiles .. " files in \"\\WE\\" .. WE.settings.folder .. "\\\" directory")