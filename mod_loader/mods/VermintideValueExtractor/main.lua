--[[ WE MAIN
	Vermintide Value Extractor (WE) 
	Author: UnShame

	Extracts values for:
		Melee weapons
		Ranged weapons
		Weapons as items
		Trinkets
		Hats
		Breeds
	Puts them into CSV format without headers
	Finished spreadsheet: https://docs.google.com/spreadsheets/d/1h3GnI6sIS77oXnCzE05k7KTkOoGJ3UezeVi24miBBoM

	After first initialization, WEExternalSettings.lua will appear in mod_loader folder.
	It contains settings that might need adjustments as the game is updated.

	Type /we into chat for usage information.
--]]

--[[  INIT  --]]

local args = {...}

--Only initialize mod if it hasn't been loaded
if not WE or not WE.settings or not WE.constants then
	Mods.exec("VermintideValueExtractor", "init")
end

--Apply settings from command line
if #args ~= 0 then
	WE.setOption(args[1], args[2], true)
end
--[[  /INIT  --]]

--[[  SETUP  --]]
local exportWeapons = Mods.require(WE.name, "export/exportWeapons")	
local exportBreed = Mods.require(WE.name, "export/exportBreed")	
local exportItem = Mods.require(WE.name, "export/exportItem")	
local addPrepTime = Mods.require(WE.name, "function/addPrepTime")	
local compare = Mods.require(WE.name, "function/compare")	

Mods.exec(WE.name, "setup/setupFiles")
if WE.numOfFiles ~= WE.numOfFilesRequired then
	EchoConsole("Some files couldn't be opened, aborting")
	return
end

Mods.exec(WE.name, "setup/setupGeneral")
Mods.exec(WE.name, "setup/setupWrite")
Mods.exec(WE.name, "setup/setupMelee")
Mods.exec(WE.name, "setup/setupRanged")
Mods.exec(WE.name, "setup/setupStagger")
Mods.exec(WE.name, "setup/setupBreeds")
Mods.exec(WE.name, "setup/setupItems")
--[[  /SETUP  --]]

--[[  EXPORT --]]

--Stores info about all weapon attacks
WE.meleeWeapons = {}
WE.rangedWeapons = {}
WE.combinedWeapons = {}

--Stores sequences of attacks for each weapon
WE.meleeAttackSequence = {}
WE.meleeAttackPreTime = {}

--Grenades
WE.grenades = {}

--Items
WE.items = {}
WE.itemKinds = {}

-- Iterating through all weapon templates
for templateName, weapon in pairs(Weapons) do 
	exportWeapons(templateName, weapon)
end

--Breeds
for k,breedTag in pairs(WE.breedRoster) do
	exportBreed(breedTag)	
end

--Add preparation times to attack speeds of melee weapons
for key,entry in pairs(WE.meleeWeapons) do
	addPrepTime(entry)
end

--Items
for key, obj in pairs(ItemMasterList) do
	exportItem(key, obj, WE.items, WE.itemKinds)
end

--[[  /EXPORT --]]

--[[  SORTING --]]
table.sort(WE.meleeWeapons, compare.melee)
table.sort(WE.rangedWeapons, compare.ranged)
table.sort(WE.combinedWeapons, compare.combined)
table.sort(WE.items, compare.items)
--[[  /SORTING --]]

--[[  OUTPUT --]]
Mods.exec(WE.name, "output")
--[[  /OUTPUT --]]