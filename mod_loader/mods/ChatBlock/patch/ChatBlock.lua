local mod_name = "ChatBlock"
--[[
	Add Defend while in chat:
		When you are typing in the chat the charter will automaticly
		block every attack.
--]]

local oi = OptionsInjector

local states = {
	NOT_BLOCKING = 0,
	SHOULD_BLOCK = 1,
	BLOCKING = 2,
	SHOULD_PUSH = 3
}

local settings = {
	OFF = 0,
	SIMPLE = 1,
	NO_PUSH = 2,
	PUSH = 3
}

Mods.ChatBlock = {
	SETTINGS = {
		SETTING = {
			["save"] = "cb_chat_block",
			["widget_type"] = "stepper",
			["text"] = "Chat Block",
			["tooltip"] =  "Chat Block\n" ..
				"Toggle chat block on / off.\n\n" ..
				"Automatically block when you're chatting.\n" ..
				"Your block will still break if your stamina runs out.\n\n" ..
				"-- ANIMATION --\n Blocking animation will play, works only with melee weapons.\n\n" ..
				"-- ANIMATION AND PUSH --\nYou will also push when closing the chat window.\n\n" ..
				"-- NO ANIMATION --\nNo blocking animation, works with ranged weapons.",
			["value_type"] = "number",
			["options"] = {
				{text = "Off", value = settings.OFF},
				{text = "Animation", value = settings.NO_PUSH},
				{text = "Animation and push", value = settings.PUSH},
				{text = "No animation", value = settings.SIMPLE},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
			["cursor_offset"] = {-10, 150},
		}
	},
	
	state = states.NOT_BLOCKING,
	chat_defence = false,
}
local me = Mods.ChatBlock

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Options ######################################################################################################
-- ####################################################################################################################
Mods.ChatBlock.create_options = function()
	Mods.option_menu:add_group("system", "System")
	
	Mods.option_menu:add_item("system", me.SETTINGS.SETTING, true)
end

-- ####################################################################################################################
-- ##### Set Block ####################################################################################################
-- ####################################################################################################################
Mods.ChatBlock.set_block = function(player_unit, status_extension, value)
	if LevelHelper:current_level_settings().level_id == "inn_level" then
		return
	end
	
    local go_id = Managers.state.unit_storage:go_id(player_unit)
   
    if Managers.player.is_server then
        Managers.state.network.network_transmit:send_rpc_clients("rpc_set_blocking", go_id, value)
    else
        Managers.state.network.network_transmit:send_rpc_server("rpc_set_blocking", go_id, value)
    end
   
    status_extension.set_blocking(status_extension, value)
end

-- ####################################################################################################################
-- ##### Chat manager hook ############################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "ChatManager.update", function(func, self, dt, t, ...)
	local setting = get(me.SETTINGS.SETTING.save)
	if setting ~= settings.OFF then
		local player = Managers.player:player_from_peer_id(Network.peer_id())
		
		if player and player.player_unit then		
			if setting ~= settings.SIMPLE then

				if self.chat_gui.chat_focused and me.state == states.NOT_BLOCKING then				
					me.state = states.SHOULD_BLOCK
					--EchoConsole(tostring(me.state))
				elseif not self.chat_gui.chat_focused and me.state == states.BLOCKING then
					if setting == settings.PUSH then
						me.state = states.SHOULD_PUSH
					else
						me.state = states.NOT_BLOCKING
					end
					--EchoConsole(tostring(me.state))
				elseif not self.chat_gui.chat_focused and me.state ~= states.NOT_BLOCKING then
					me.state = states.NOT_BLOCKING
					--EchoConsole(tostring(me.state))
				end

			else

			    local player = Managers.player:player_from_peer_id(Network.peer_id())
			   
			    if player and player.player_unit then
			        local player_unit = player.player_unit
			        local status_extension = ScriptUnit.extension(player_unit, "status_system")
			   
			        if me.chat_defence == true then
			            if self.chat_gui.chat_focused == false then
			                me.set_block(player_unit, status_extension, false)
			                me.chat_defence = false
			            elseif self.chat_gui.chat_focused == true and status_extension.fatigue > 99 then -- When stamina runs out
			                me.set_block(player_unit, status_extension, false)
			            end
			        else
			            if self.chat_gui.chat_focused == true then
			                me.set_block(player_unit, status_extension, true)
			                me.chat_defence = true
			            end
			        end
			    end
			end
		end
	end
	
	return func(self, dt, t, ...)
end)

-- Weapon action update hook
-- And I'm not doing this fancy over the top unnessesary commenting thing, no way
Mods.hook.set(mod_name, "CharacterStateHelper.update_weapon_actions",
function(func, t, unit, input_extension, inventory_extension, damage_extension)
	local player_unit = Managers.player:local_player().player_unit

	--Check if local player and not in inn
	if(player_unit ~= unit or LevelHelper:current_level_settings().level_id == "inn_level") then
		func(t, unit, input_extension, inventory_extension, damage_extension)
		return
	end

	local status_extension = player_unit and ScriptUnit.extension(player_unit, "status_system")

	--Get data about weapon
	local item_data, right_hand_weapon_extension, left_hand_weapon_extension = CharacterStateHelper._get_item_data_and_weapon_extensions(inventory_extension)
	local new_action, new_sub_action, current_action_settings, current_action_extension, current_action_hand = nil
	current_action_settings, current_action_extension, current_action_hand = CharacterStateHelper._get_current_action_data(left_hand_weapon_extension, right_hand_weapon_extension)
	if not(item_data) or not(status_extension) then
		func(t, unit, input_extension, inventory_extension, damage_extension)
		return
	end
	local item_template = BackendUtils.get_item_template(item_data)
	if not(item_template) then
		func(t, unit, input_extension, inventory_extension, damage_extension)
		return
	end

	--Check if weapon can block
	new_action = "action_two"
	new_sub_action = "default"
	local new_action_template = item_template.actions[new_action]
	local new_sub_action_template = new_action_template and item_template.actions[new_action][new_sub_action] 
	if not(new_sub_action_template) or (not right_hand_weapon_extension and not left_hand_weapon_extension) or (new_sub_action_template.kind ~= "block") then
		func(t, unit, input_extension, inventory_extension, damage_extension)
		return
	end

	--Block
	if (me.state == states.SHOULD_BLOCK) then
		me.state = states.BLOCKING
		if(left_hand_weapon_extension) then
			left_hand_weapon_extension.start_action(left_hand_weapon_extension, new_action, new_sub_action, item_template.actions, t)				
		end
		if(right_hand_weapon_extension) then
			right_hand_weapon_extension.start_action(right_hand_weapon_extension, new_action, new_sub_action, item_template.actions, t)
		end
		return

	--Push
	elseif(me.state == states.SHOULD_PUSH and not status_extension.fatigued(status_extension)) then
		new_action = "action_one"
		new_sub_action = "push"
		if(item_template.actions[new_action][new_sub_action]) then
			if(left_hand_weapon_extension) then
				left_hand_weapon_extension.start_action(left_hand_weapon_extension, new_action, new_sub_action, item_template.actions, t)				
			end
			if(right_hand_weapon_extension) then
				right_hand_weapon_extension.start_action(right_hand_weapon_extension, new_action, new_sub_action, item_template.actions, t)
			end
		end

		--Reset block state
		me.state = states.NOT_BLOCKING
	end

	--Continue blocking
	if not(me.state == states.BLOCKING) then
		func(t, unit, input_extension, inventory_extension, damage_extension)
	end
end)

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()
