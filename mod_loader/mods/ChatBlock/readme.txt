This mods adds automatic blocking when opening the chat window while holding a melee weapon. When closing the window, a push is performed.
To enable go to Mod Settings -> System -> Chat Block.
Off by default.