local me = Mods.ChestInfo

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

set(me.SETTINGS.PING.save, not get(me.SETTINGS.PING.save))
save()

--Feedback
if get(me.SETTINGS.PING.save) then
	EchoConsole("Enable Chest ping")
else
	EchoConsole("Disable Chest ping")
end