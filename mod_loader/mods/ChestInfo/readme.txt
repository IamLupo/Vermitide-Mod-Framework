Displays the percentage of getting a loot die when opening chests.
Go to Mod Settings -> Items -> Show Loot Die Chance to toggle.
Off by default.