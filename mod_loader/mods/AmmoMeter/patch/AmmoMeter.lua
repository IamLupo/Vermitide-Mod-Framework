local mod_name = "AmmoMeter"

Mods.AmmoMeter = {
	SETTINGS = {
		AMMO = {
			["save"] = "cb_hud_ammo_meter",
			["widget_type"] = "stepper",
			["text"] = "Ammo Meters",
			["tooltip"] = "Ammo Meters\n" ..
				"Show an ammo meter on the HUD for each team member who has equipped a ranged " ..
				"weapon that uses ammo.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
	},
	
	ammo = {},
	
	refresh = 0,
}

local me = Mods.AmmoMeter

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

--[[
	Ammo meter widget definition, based on code from scripts/ui/views/player_inventory_ui_definitions.lua
	(it's a very simplified version of the overcharge meter shown to the right of combustion-based
	weapons' icons on the HUD).
--]]
local ammo_meter_widget = {
	scenegraph_id = "pivot",
	offset = { 225, -79, -2 },
	element = {
		passes = {
			{
				pass_type = "texture",
				style_id = "ammo_bar_fg",
				texture_id = "ammo_bar_fg",
			},
			{
				style_id = "ammo_bar",
				pass_type = "texture_uv_dynamic_color_uvs_size_offset",
				content_id = "ammo_bar",
				dynamic_function = function (content, style, size, dt)
					local bar_value = content.bar_value
					local uv_start_pixels = style.uv_start_pixels
					local uv_scale_pixels = style.uv_scale_pixels
					local uv_pixels = uv_start_pixels + uv_scale_pixels*bar_value
					local uvs = style.uvs
					local uv_scale_axis = style.scale_axis
					local offset = style.offset
					uvs[1][uv_scale_axis] = 1 - uv_pixels/(uv_start_pixels + uv_scale_pixels)
					size[uv_scale_axis] = uv_pixels
					return nil, uvs, size, offset
				end
			},
		},
	},
	content = {
		ammo_bar_fg = "stance_bar_frame",
		ammo_bar = {
			bar_value = 0,
			texture_id = "stance_bar_blue",
		},
	},
	style = {
		ammo_bar_fg = {
			color = { 255, 255, 255, 255 },
			offset = { 0, 0, 1 },
			size = { 32, 128 },
		},
		ammo_bar = {
			uv_start_pixels = 0,
			uv_scale_pixels = 67,
			offset_scale = 1,
			scale_axis = 2,
			offset = { 9, 15, 0 },
			size = { 9, 0 },
			uvs = {
				{ 0, 0 },
				{ 1, 1 }
			},
		},
	},
}

-- ####################################################################################################################
-- ##### Option #######################################################################################################
-- ####################################################################################################################
Mods.AmmoMeter.create_options = function()
	Mods.option_menu:add_group("hud", "HUD")
	
	Mods.option_menu:add_item("hud", me.SETTINGS.AMMO, true)
end

-- ####################################################################################################################
-- ##### Functions ####################################################################################################
-- #################################################################################################################### 
--[[
	Helper function to retrieve the ammo extension from the given slot data.
--]]
Mods.AmmoMeter.get_ammo_extension = function(slot_data)
	if slot_data then
		local right_unit = slot_data.right_unit_1p
		local left_unit = slot_data.left_unit_1p
		
		return (right_unit and ScriptUnit.has_extension(right_unit, "ammo_system")) or
			(left_unit and ScriptUnit.has_extension(left_unit, "ammo_system"))
	end
	
	return nil
end
 
--[[
	Returns the current ammo and the maximum ammo from the given ammo.  Based on
	SimpleInventoryExtension.current_ammo_status, which we can't use because it doesn't give the max
	ammo we want (it gives the 'raw' max ammo for the weapon type without the Ammo Holder trait).
--]]
Mods.AmmoMeter.current_ammo_status = function(inventory_extn)
	local slot_data = inventory_extn:equipment().slots["slot_ranged"]
	
	if slot_data then
		local item_data = slot_data.item_data
		local item_template = BackendUtils.get_item_template(item_data)
		local ammo_data = item_template.ammo_data
 
		if ammo_data then
			local ammo_extn = me.get_ammo_extension(slot_data)
			
			return ammo_extn:total_remaining_ammo(), ammo_extn.max_ammo
		end
	end
	
	return nil, nil
end

--[[
	Collect ammo data from local player and store in the ammo list
]]--
Mods.AmmoMeter.update_ammo = function(player_unit, peer_id, local_player_id)
	local id = tostring(peer_id) .. ":" .. tostring(local_player_id)
	
	if ScriptUnit.has_extension(player_unit, "inventory_system") then
		local inventory_extn = ScriptUnit.has_extension(player_unit, "inventory_system")
		local current_ammo, max_ammo = me.current_ammo_status(inventory_extn)
		
		if current_ammo then
			me.ammo[id] = {
				["current"] = current_ammo,
				["max"] = max_ammo
			}
		end
	end
end

-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "UnitFramesHandler.update", function(func, self, dt, t, my_player)
	local player_unit = self.my_player.player_unit
	
	if player_unit then
		-- Save player_unit to unit frame
		for _, unit_frame in ipairs(self._unit_frames) do
			if unit_frame and unit_frame.player_data and unit_frame.player_data.player then
				unit_frame.data.player_unit = unit_frame.player_data.player_unit
				unit_frame.data.player_ui_id = unit_frame.player_data.player_ui_id
			end
		end
	end
	
	return func(self, dt, t, my_player)
end)
 
Mods.hook.set(mod_name, "UnitFrameUI.draw", function(func, self, dt)
	local data = self.data
	
	if self._is_visible then
		local ui_renderer = self.ui_renderer
		local input_service = self.input_manager:get_service("ingame_menu")
		
		UIRenderer.begin_pass(ui_renderer, self.ui_scenegraph, input_service, dt, nil, self.render_settings)
		
		-- Draw Ammo bars
		local ammo = me.ammo[self.data.player_ui_id]
		if ammo then
			local widget = self._ammo_widget
			
			if not widget then
				widget = UIWidget.init(ammo_meter_widget)
				self._ammo_widget = widget
			end
			
			local ammo_bar = widget.content.ammo_bar
			
			ammo_bar.bar_value = math.max(0, math.min(ammo.current / ammo.max, 1))
			
			UIRenderer.draw_widget(ui_renderer, widget)
		end
		
		UIRenderer.end_pass(ui_renderer)
	end
 
	return func(self, dt)
end)

-- ####################################################################################################################
-- ##### Network ######################################################################################################
-- ####################################################################################################################
--[[
	Update the ammo list and send the ammo list over the network
--]]
Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, self, dt, t)
	func(self, dt, t)
	
	if me.refresh + 1 < t then
		local local_player = Managers.player:local_player()
		if local_player then
			me.update_ammo(local_player.player_unit, local_player.peer_id, local_player._local_player_id)
			
			if Managers.player.is_server then
				local local_bots = Managers.player:bots()
				
				if local_bots then
					for _, local_bot in pairs(local_bots) do
						me.update_ammo(local_bot.player_unit, local_bot.peer_id, local_bot._local_player_id)
					end
				end
				
				Mods.network.send_rpc_clients("rpc_hud_update_ammo", me.ammo)
			else
				Mods.network.send_rpc_server("rpc_hud_update_ammo", me.ammo)
			end
		end
		
		me.refresh = t
	end
end)

--[[
	RPC call to sync ammo data in a list
]]--
Mods.network.register("rpc_hud_update_ammo", function(sender_id, ammo)
	if Managers.player.is_server then
		local id = tostring(sender_id) .. ":1"
		
		-- Update record
		me.ammo[id] = ammo[id]
	else
		-- Update list
		me.ammo = table.clone(ammo)
	end
end)

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()