pcall(function ()
	local player_unit = Managers.player:local_player(1).player_unit
	
	if player_unit and Unit.alive(player_unit) then
		local player_unit_id = Managers.state.network:unit_game_object_id(player_unit)
		
		Managers.state.network.network_transmit:send_rpc_server("rpc_ping_unit", player_unit_id, player_unit_id)
	end
end)