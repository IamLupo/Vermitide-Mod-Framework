Overrides the "ping self" voiceover.
This causes a predetermined line of dialogue to play when you ping yourself,
instead of a random line of "help me" dialogue.
Options in Mod Settings -> Ping Self Voiceover.
Off by default.
Home key (default) will ping self.