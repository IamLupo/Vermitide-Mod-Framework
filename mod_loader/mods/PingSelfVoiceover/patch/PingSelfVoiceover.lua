local mod_name = "PingSelfVoiceover"
--[[
	Override the "ping self" voiceover:
		This causes a predetermined line of dialogue to play when you ping yourself, instead
		of a random line of "help me" dialogue.
		
	author: walterr
--]]
 
 local oi = OptionsInjector
 
PingSelfVoiceover = {
	SETTINGS = {
		DISABLED = {
			["save"] = "cb_ping_self_voiceover_off",
			["widget_type"] = "stepper",
			["text"] = "Override Ping Self Voiceover",
			["tooltip"] =  "Override Ping Self Voiceover\n" ..
				"Toggle override of voiceover when pinging self on / off.\n\n" ..
				"Changes your voiceover when you ping yourself to a \"This way\" message (only works" ..
				"if the host has this mod enabled).",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default second option is enabled. In this case On
		},
		HK_TOGGLE = {
			["save"] = "cb_ping_self_hotkey_ping",
			["widget_type"] = "keybind",
			["text"] = "Ping",
			["default"] = {
				"home",
				oi.key_modifiers.NONE,
			},
			["exec"] = {"PingSelfVoiceover", "action/ping_self"},
		},
	},
	
	vo = {
		witch_hunter = { dialogue_name = "pwh_objective_correct_path_this_way", dialogue_index = 1 },
		empire_soldier = { dialogue_name = "pes_objective_correct_path_this_way", dialogue_index = 1 },
		dwarf_ranger = { dialogue_name = "pdr_objective_correct_path_this_way", dialogue_index = 3 },
		bright_wizard = { dialogue_name = "pbw_objective_correct_path_this_way", dialogue_index = 1 },
		wood_elf = { dialogue_name = "pwe_objective_correct_path_this_way", dialogue_index = 1 }
	},
}
local me = PingSelfVoiceover

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings
 
-- ####################################################################################################################
-- ##### Options ######################################################################################################
-- ####################################################################################################################
PingSelfVoiceover.create_options = function()
	Mods.option_menu:add_group("ping_self", "Ping Self Voiceover")

	Mods.option_menu:add_item("ping_self", me.SETTINGS.DISABLED, true)
	Mods.option_menu:add_item("ping_self", me.SETTINGS.HK_TOGGLE, true)
end
 
-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "PingSystem.play_ping_vo", function (original_func, self, pinger_unit, pinged_unit)
	-- note that this function is only called if we are the server
	if pinger_unit == pinged_unit and get(me.SETTINGS.DISABLED.save) then
		local pinger_dialogue_ext = ScriptUnit.extension(pinger_unit, "dialogue_system")
		local pinger_name = pinger_dialogue_ext.context.player_profile
		local vo_info = pinger_name and me.vo[pinger_name]
		if vo_info then
			-- play dialogue locally
			local dialogue = self.entity_manager:system("dialogue_system").dialogues[vo_info.dialogue_name]
			assert(dialogue)
			local index = vo_info.dialogue_index
			pinger_dialogue_ext.input:play_voice(dialogue.sound_events[index])
			
			-- play dialogue on clients
			local dialogue_id = NetworkLookup.dialogues[vo_info.dialogue_name]
			local network_manager = Managers.state.network
			local go_id, is_level_unit = network_manager:game_object_or_level_id(pinger_unit)
			network_manager.network_transmit:send_rpc_clients("rpc_play_dialogue_event", go_id, is_level_unit, dialogue_id, index)
		end
	else
		-- call original function
		original_func(self, pinger_unit, pinged_unit)
	end
end)
 
-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()