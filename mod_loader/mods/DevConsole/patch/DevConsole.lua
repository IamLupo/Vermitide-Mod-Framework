local mod_name = "DevConsole"
--[[
	Development Console Window:
		Pop up a console window with debugging information
--]]

local oi = OptionsInjector

Mods.DevConsole = {
	SETTINGS = {
		ACTIVE = {
			["save"] = "cb_dev_console_window",
			["widget_type"] = "stepper",
			["text"] = "Development Console Window",
			["tooltip"] =  "Development Console Window\n" ..
				"Toggle development console window on / off.\n\n" ..
				"Pop up a console window with debugging information.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
		},
	},
	enabled = false
}
local me = Mods.DevConsole

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Options ######################################################################################################
-- ####################################################################################################################
Mods.DevConsole.create_options = function()
	Mods.option_menu:add_group("system", "System")

	Mods.option_menu:add_item("system", me.SETTINGS.ACTIVE, true)
end

-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "print", function(func, ...)
	if get(me.SETTINGS.ACTIVE.save) then
		CommandWindow.print(...)
	else
		func(...)
	end
end)

Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, ...)
	safe_pcall(function()
		-- Open Command Window
		if get(me.SETTINGS.ACTIVE.save) == true and me.enabled == false then
			CommandWindow.open("Development command window")
			
			me.enabled = true
		end
		
		-- Close Command Window
		if get(me.SETTINGS.ACTIVE.save) == false and me.enabled == true then
			CommandWindow.close()
			
			me.enabled = false
		end
	end)
	
	func(...)
end)

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()