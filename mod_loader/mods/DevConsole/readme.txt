Opens a command line window that the game prints some debug information into.
print() function can be used to display text on the console via lua scripts.
Go to Mod Settings -> System -> Development Console Window to toggle.
Off by default.