Adds a chat command that puts an item with specified id into your inventory.
Chat command:
	/giveitem [item_id]
You can use this spreadsheet to find right ids (Key column):
	https://docs.google.com/spreadsheets/d/1phNRw5rXvGQdfcExFH_2jgtemiNeA-bDieCTsSMPods/edit?usp=sharing
