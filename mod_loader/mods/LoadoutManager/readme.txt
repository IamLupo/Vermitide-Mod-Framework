This mod adds additional buttons to the inventory screen for saving and restoring character loadouts.
Also provides shortcuts for restoring loadouts for all bots (by default):
	Ctrl + Alt + F1-F9
Go to Mod Settings -> Loadout Manager to change shortcuts.