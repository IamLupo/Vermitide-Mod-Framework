local mod_name = "LoadoutSaver"
--[[ 
	LoadoutSaver
		- Saves load out settings
	
	Author: walterr
--]]


local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

local LOADOUT_SETTING = "cb_saved_loadouts"

local oi = OptionsInjector

LoadoutSaver = {
	_equipment_queue = {},

	SETTINGS = {},

	create_options = function()
		for i=1,9 do
			local setting = {
				["save"] = "cb_loadouts_" .. i,
				["widget_type"] = "keybind",
				["text"] = "Load loadout " .. i .. " for bots",
				["default"] = {
					"f" .. i,
					oi.key_modifiers.CTRL_ALT,
				},
				["exec"] = {"LoadoutManager", "loadout/restore" .. i},
			}
			LoadoutSaver.SETTINGS["HK_LOADOUT_" .. i] = setting
		end
		Mods.option_menu:add_group("loadouts", "Loadout Manager")
		for i=1,9 do
			Mods.option_menu:add_item("loadouts", LoadoutSaver.SETTINGS["HK_LOADOUT_" .. i], true)
		end
	end,
	
	save_loadout = function (self, loadout_number)
		local inventory_ui = self:_get_inventory_ui()
		if not inventory_ui then
			EchoConsole("Error: loadouts can only be saved from Inventory view")
			return
		end
 
		local items_page = inventory_ui.ui_pages.items
		local profile_name = items_page:current_profile_name()
		local loadout = {}
 
		for slot_name, _ in pairs(InventorySettings.slots_by_name) do
			local item_backend_id = ScriptBackendItem.get_loadout_item_id(profile_name, slot_name)
			if item_backend_id then
				loadout[slot_name] = item_backend_id
			end
		end
 
		set(LOADOUT_SETTING, profile_name, loadout_number, loadout)
		save()
		
		-- Feedback
		if not get(ModSettings.SETTINGS.HK_FEEDBACK.save) then
			EchoConsole("Loadout #" .. loadout_number .. " saved for hero " .. profile_name)
		end
	end,
 
	restore_loadout = function (self, loadout_number)
		if self._original_update_resync_loadout then
			EchoConsole("Error: restore already in progress")
			return
		end
 
		local inventory_ui = self:_get_inventory_ui()
		if not inventory_ui then
			EchoConsole("Error: individual loadout can only be restored from Inventory view")
			return
		end
 
		local pages = inventory_ui.ui_pages
		local profile_name = pages.items:current_profile_name()
		local loadout = get(LOADOUT_SETTING, profile_name, loadout_number)
		if not loadout then
			EchoConsole("Error: loadout #" .. loadout_number .. " not found for hero " .. profile_name)
			return
		end
 
		-- Part of each equipment change may be done asynchronously, so we set up a
		-- queue to perform the next change only when the current one is finished.
		self:_set_up_update_resync_loadout_queue()
 
		local equipment_page = pages.equipment
		local items_page = pages.items
		local equipment_queue = self._equipment_queue
 
		-- Note that this ordering does slot_hat last, which is important because the
		-- new hat doesn't appear on the player model unless it's done last (for some
		-- reason that I dont have time to figure out right now).
		for _, slot in ipairs(InventorySettings.slots_by_inventory_button_index) do
			local item_backend_id = loadout[slot.name]
			if item_backend_id then
				local current_item = BackendUtils.get_loadout_item(profile_name, slot.name)
				if not current_item or current_item.backend_id ~= item_backend_id then
					equipment_queue[#equipment_queue + 1] = {
						equipment_page = equipment_page,
						items_page = items_page,
						item_backend_id = item_backend_id,
						slot = slot
					}
				end
			end
		end
		
		-- Feedback
		if not get(ModSettings.SETTINGS.HK_FEEDBACK.save) then
			EchoConsole("Loadout #" .. loadout_number .. " restored for hero " .. profile_name)
		end
	end,
 
	restore_background_loadouts = function (self, loadout_number)
		local current_profile = Managers.player:local_player():profile_display_name()
		local inventory_ui = self:_get_inventory_ui()
		local selected_profile = inventory_ui and inventory_ui.ui_pages.items:current_profile_name()
 
		if not DamageUtils.is_in_inn then
			EchoConsole("Error: can only restore other heroes' loadouts in Red Moon Inn")
			return
		end
 
		for _, profile in ipairs(SPProfiles) do
			local profile_name = profile.display_name
			if profile_name ~= current_profile and profile_name ~= selected_profile then
				local loadout = get(LOADOUT_SETTING, profile_name, loadout_number)
				if loadout then
					for slot_name, backend_id in pairs(loadout) do
						local current_backend_id = ScriptBackendItem.get_loadout_item_id(profile_name, slot_name)
						if backend_id ~= current_backend_id then
							ScriptBackendItem.set_loadout_item(backend_id, profile_name, slot_name)
						end
					end
					EchoConsole("Loadout #" .. loadout_number .. " restored for hero " .. profile_name)
				end
			end
		end
 
		if selected_profile and selected_profile ~= current_profile then
			self:restore_loadout(loadout_number)
		end
	end,
 
	_get_inventory_ui = function (self)
		local matchmaking_manager = Managers.matchmaking
		local ingame_ui = matchmaking_manager and matchmaking_manager.ingame_ui
		local inventory_is_active = ingame_ui and ingame_ui.current_view == "inventory_view"
		return inventory_is_active and ingame_ui.views["inventory_view"]
	end,
 
	_set_up_update_resync_loadout_queue = function (outer_self)
		local player_unit = Managers.player:local_player().player_unit
		local inventory_extn = ScriptUnit.extension(player_unit, "inventory_system")
		
		-- Save orginal function
		outer_self._original_update_resync_loadout = inventory_extn.update_resync_loadout
 
		inventory_extn.update_resync_loadout = function (self)
			outer_self:_process_equipment_queue(self)
			
			-- Check if there are still items to sync
			if self._item_to_spawn then
				-- Sync item
				outer_self._original_update_resync_loadout(self)
			else
				-- Restore orginal function
				self.update_resync_loadout = outer_self._original_update_resync_loadout
				outer_self._original_update_resync_loadout = nil
			end
		end
	end,
 
	_process_equipment_queue = function (self, inventory_extn)
		local equipment_queue = self._equipment_queue
		
		while equipment_queue[1] and not inventory_extn._item_to_spawn do
			local next_equip = equipment_queue[1]
			table.remove(equipment_queue, 1)
			
			-- Check if item_backend_id still exist
			if ScriptBackendItem.get_key(next_equip.item_backend_id) then
				local item = BackendUtils.get_item_from_masterlist(next_equip.item_backend_id)
				if item then
					local success = next_equip.equipment_page:equip_inventory_item(item, next_equip.slot.inventory_button_index)
					
					if success then
						next_equip.items_page:refresh_items_status()
					end
				end
			end
		end
	end,
	
	create_window = function(self)
		local scale = UIResolutionScale()
		self.window = Mods.ui.create_window("loadout_saver_window", {50*scale, 30*scale}, {1360*scale, 80*scale})
		self.window.color = {0, 0, 0, 0}
		self.window.color_hover = {0, 0, 0, 0}
		local size = {40*scale, 36*scale}
		local border = 2*scale
		local on_click = function(self)
			local id = self.param[1]
			local mode = self.param[2]
			if mode == "load" then
				LoadoutSaver:restore_loadout(id)
			elseif mode == "save" then
				LoadoutSaver:save_loadout(id)
				LoadoutSaver:reload_window()
			end
		end
		self.window:create_button("loadout_saver_load_text", {0, 40*scale+border}, {size[1], size[2]}, "Load", nil, "bottom_left")
		self.window.controls[#self.window.controls].disabled = true
		self.window:create_button("loadout_saver_save_text", {0, 0}, {size[1], size[2]}, "Save", nil, "bottom_left")
		self.window.controls[#self.window.controls].disabled = true
		for i=1, 9 do
			if self:loadout_exists(i) then
				self.window:create_button("loadout_saver_load" .. tostring(i), {40+(size[1]+border)*(i), 40*scale+border},
					{size[1], size[2]}, tostring(i), on_click, "bottom_left", {i, "load"})
			end
			self.window:create_button("loadout_saver_save" .. tostring(i), {40+(size[1]+border)*(i), 0},
				{size[1], size[2]}, tostring(i), on_click, "bottom_left", {i, "save"})
		end
		--self.window.visible = false
		self.window:init()
	end,
	
	reload_window = function(self)
		if LoadoutSaver.window ~= nil then
			LoadoutSaver.window:destroy()
		end
		LoadoutSaver:create_window()
	end,
	
	loadout_exists = function(self, loadout_number)
		local inventory_ui = self:_get_inventory_ui()
		if inventory_ui ~= nil then
			local pages = inventory_ui.ui_pages
			if pages ~= nil then
				local profile_name = pages.items:current_profile_name()
				
				if profile_name ~= nil then
					return get(LOADOUT_SETTING, profile_name, loadout_number) ~= nil
				end
			end
		end
		return false
	end,
	
	last_profile_name = "",
	
	current_profile_name = function(self)
		local inventory_ui = self:_get_inventory_ui()
		if inventory_ui ~= nil then
			local pages = inventory_ui.ui_pages
			if pages ~= nil then
				return pages.items:current_profile_name()
			end
		end
		
		return ""
	end,
}

Mods.hook.set(mod_name, "InventoryView.on_enter", function(func, self)
	func(self)
	
	self.input_manager:device_unblock_service("keyboard", 1, "Mods")
end)

Mods.hook.set(mod_name, "InventoryView.unsuspend", function(func, self)
	func(self)
	
	self.input_manager:device_unblock_service("keyboard", 1, "Mods")
end)

Mods.hook.set(mod_name, "InventoryView.on_exit", function(func, ...)
	func(...)
	
	if LoadoutSaver.window then
		LoadoutSaver.window:destroy()
	end
	
	-- No profile has been selected anymore when closing the inventory menu
	LoadoutSaver.last_profile_name = ""
end)

Mods.hook.set(mod_name, "InventoryView.exit", function(func, ...)
	-- Can not exit before Loadout Restore is finished else game crash
	if not LoadoutSaver._original_update_resync_loadout then
		func(...)
	end
end)

Mods.hook.set(mod_name, "InventoryView.update_animations", function(func, ...)
	func(...)
	
	-- Check profile name that has been selected
	local name = LoadoutSaver:current_profile_name()
	if LoadoutSaver.last_profile_name ~= name then
		LoadoutSaver:reload_window()
		
		LoadoutSaver.last_profile_name = name
	end
end)

LoadoutSaver.create_options()