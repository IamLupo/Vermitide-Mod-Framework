local mod_name = "ChatHistory"

-- Keymap
ChatHistoryKeyMap = {
	win32 = {
		["up_pressed"]			= {"keyboard", "up", "pressed"},
		["down_pressed"]		= {"keyboard", "down", "pressed"},
		["enter"]				= {"keyboard", "enter", "pressed"},
	},
}
ChatHistoryKeyMap.xb1 = ChatHistoryKeyMap.win32

if not ChatHistory then
	ChatHistory = {
		messages = {},
		position = 0
	}
end

local me = ChatHistory

-- ####################################################################################################################
-- ##### Functions ####################################################################################################
-- ####################################################################################################################
ChatHistory.init = function()
	Managers.input:create_input_service("ChatHistory", "ChatHistoryKeyMap")
	Managers.input:map_device_to_service("ChatHistory", "keyboard")
	Managers.input:map_device_to_service("ChatHistory", "mouse")
	Managers.input:map_device_to_service("ChatHistory", "gamepad")
end

ChatHistory.pressed = function(key)
	local input_service = Managers.input:get_service(mod_name)
	
	if input_service then
		return input_service.get(input_service, key)
	else
		return false
	end
end

ChatHistory.block = function()
	local input_service = Managers.input:get_service(mod_name)
	
	if input_service then
		for name, obj in pairs(Managers.input.input_devices) do
			obj.blocked_access["ChatHistory"] = true
		end
		input_service:set_blocked(true)
	end
end

ChatHistory.unblock = function()
	local input_service = Managers.input:get_service(mod_name)
	
	if input_service then
		for name, obj in pairs(Managers.input.input_devices) do
			obj.blocked_access["ChatHistory"] = nil
		end
		input_service:set_blocked(nil)
	end
end

-- ####################################################################################################################
-- ##### Hooks ########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "ChatGui.update", function(func, self, ...)
	if me.pressed("up_pressed") then
		if #me.messages > me.position then
			me.position = me.position + 1

			self.chat_index = 1
			self.chat_message = me.messages[me.position]
			self.chat_index = string.len(self.chat_message) + 1
		end
	elseif me.pressed("down_pressed") then
		self.chat_index = 1
		if me.position > 1 then
			me.position = me.position - 1	
					
			self.chat_message = me.messages[me.position]			
		else
			self.chat_message = ""
			me.position = 0
		end
		self.chat_index = string.len(self.chat_message) + 1
	elseif me.pressed("enter") then
		me.position = 0

		if self.chat_message ~= "" and me.messages[1] ~= self.chat_message then
			table.insert(me.messages, 1, self.chat_message)
			
			if #me.messages > 10 then
				table.remove(me.messages, 11)
			end
		end
	end

	func(self, ...)
end)

Mods.hook.set(mod_name, "ChatGui.block_input", function(func, ...)
	func(...)
	
	me.unblock()
end)

Mods.hook.set(mod_name, "ChatGui.unblock_input", function(func, ...)
	func(...)
	
	me.block()
end)

Mods.hook.set(mod_name, "StateInGameRunning.event_game_started", function(func, ...)
	func(...)
	
	me.init()
end)

Mods.hook.set(mod_name, "StateInGameRunning.event_game_actually_starts", function(func, ...)
	func(...)
	
	me.init()
end)

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.init()
