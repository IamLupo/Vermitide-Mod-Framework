local mod_name = "SmokeGrenade"
--[[
	Smoke grenade
		- Unlock a locked grenade type.
		- Blocks paths for enemies
		
	author: grasmann
--]]
SmokeGrenade = {
	SETTINGS = {
		ACTIVE = {
			["save"] = "cb_spawning_smoke_grenade_lott_table",
			["widget_type"] = "stepper",
			["text"] = "Game Can Spawn Smoke Grenades",
			["tooltip"] = "Allow game to spawn Smoke Grenades\nSmoke Grenades can be found in chests and supply drops",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default second option is enabled. In this case On
		},
		ACTIVE_OLD = false,
	},
}
local me = SmokeGrenade

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Options ######################################################################################################
-- ####################################################################################################################
SmokeGrenade.create_options = function()
	Mods.option_menu:add_item("spawning", ItemSpawner.SETTINGS.ACTIVE, true)
end

-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, self, dt, t)
	func(self, dt, t)
	local active = get(me.SETTINGS.ACTIVE.save)
	if active ~= me.SETTINGS.ACTIVE_OLD then
		if active then
			Pickups.grenades.smoke_grenade_t1.spawn_weighting = 0.2
			Pickups.improved_grenades.smoke_grenade_t2.spawn_weighting = 0.2
			
			LootRatPickups["smoke_grenade_t1"] = 0.058823529411764705
			LootRatPickups["smoke_grenade_t2"] = 0.058823529411764705
		else
			Pickups.grenades.smoke_grenade_t1.spawn_weighting = 0
			Pickups.improved_grenades.smoke_grenade_t2.spawn_weighting = 0
			
			LootRatPickups["smoke_grenade_t1"] = nil
			LootRatPickups["smoke_grenade_t2"] = nil
		end
		me.SETTINGS.ACTIVE_OLD = active
	end
end)

-- Pickups.grenades.smoke_grenade_t1.spawn_weighting = 0.2
-- Pickups.improved_grenades.smoke_grenade_t2.spawn_weighting = 0.2

-- ####################################################################################################################
-- ##### Explosion template ###########################################################################################
-- ####################################################################################################################
ExplosionTemplates.smoke_grenade.explosion = {
	damage_type = "grenade",
	radius = 5,
	max_damage_radius = 2,
	attack_template_glance = "drakegun_glance",
	damage_type_glance = "grenade_glance",
	alert_enemies_radius = 15,
	attack_template = "drakegun",
	sound_event_name = "player_combat_weapon_smoke_grenade_explosion",
	alert_enemies = true,
	effect_name = "fx/wpnfx_smoke_grenade_impact",
	damage = {
		1, --6,
		0.5, --3,
		5, --30,
		2, --10
	}	
}

-- ####################################################################################################################
-- ##### AOE Template #################################################################################################
-- ####################################################################################################################
ExplosionTemplates.smoke_grenade.aoe = {
	--extra_effect_name = "fx/chr_gutter_foff",
	radius = 5,
	nav_tag_volume_layer = "smoke_grenade",
	create_nav_tag_volume = true,
	sound_event_name = "player_combat_weapon_smoke_grenade_explosion",
	damage_interval = 1,
	duration = 10,
	area_damage_template = "explosion_template_aoe",
	effect_name = "fx/wpnfx_smoke_grenade_impact"
}

-- Add to AllPickups table
AllPickups["smoke_grenade_t1"] = Pickups.grenades.smoke_grenade_t1
AllPickups["smoke_grenade_t2"] = Pickups.improved_grenades.smoke_grenade_t2

-- Add to LootRatPickups Procentage table
-- LootRatPickups["smoke_grenade_t1"] = 0.058823529411764705
-- LootRatPickups["smoke_grenade_t2"] = 0.058823529411764705

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()