local mod_name = "ItemSpawner"

local oi = OptionsInjector

ItemSpawner = {
	SETTINGS = {
		SPAWN_PICKUPS = {
			["save"] = "cb_spawning_spawn_pickups",
			["widget_type"] = "checkbox",
			["text"] = "Items",
			["default"] = false,
			["hide_options"] = {
				{
					false,
					mode = "hide",
					options = {
						"cb_spawning_spawn_badges",
						"cb_spawning_spawn_badges_modifiers",
						"cb_spawning_spawn_package",
						"cb_spawning_spawn_package_modifiers",
						"cb_spawning_spawn_speed",
						"cb_spawning_spawn_speed_modifiers",
						"cb_spawning_spawn_kit",
						"cb_spawning_spawn_kit_modifiers",
						"cb_spawning_spawn_strength",
						"cb_spawning_spawn_strength_modifiers",
						"cb_spawning_spawn_frag",
						"cb_spawning_spawn_frag_modifiers",
						"cb_spawning_spawn_fire",
						"cb_spawning_spawn_fire_modifiers",
						"cb_spawning_spawn_lorebook_page",
						"cb_spawning_spawn_lorebook_page_modifiers",
						"cb_spawning_spawn_torch",
						"cb_spawning_spawn_torch_modifiers",
						"cb_spawning_spawn_grim",
						"cb_spawning_spawn_grim_modifiers",
						"cb_spawning_spawn_tome",
						"cb_spawning_spawn_tome_modifiers",
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_spawning_spawn_badges",
						"cb_spawning_spawn_badges_modifiers",
						"cb_spawning_spawn_package",
						"cb_spawning_spawn_package_modifiers",
						"cb_spawning_spawn_speed",
						"cb_spawning_spawn_speed_modifiers",
						"cb_spawning_spawn_kit",
						"cb_spawning_spawn_kit_modifiers",
						"cb_spawning_spawn_strength",
						"cb_spawning_spawn_strength_modifiers",
						"cb_spawning_spawn_frag",
						"cb_spawning_spawn_frag_modifiers",
						"cb_spawning_spawn_fire",
						"cb_spawning_spawn_fire_modifiers",
						"cb_spawning_spawn_lorebook_page",
						"cb_spawning_spawn_lorebook_page_modifiers",
						"cb_spawning_spawn_torch",
						"cb_spawning_spawn_torch_modifiers",
						"cb_spawning_spawn_grim",
						"cb_spawning_spawn_grim_modifiers",
						"cb_spawning_spawn_tome",
						"cb_spawning_spawn_tome_modifiers",
					}
				},
			},
		},
		HK_SPAWN_BADGES = {
			["save"] = "cb_spawning_spawn_badges",
			["widget_type"] = "keybind",
			["text"] = "Badges",
			["default"] = {
				"b",
				oi.key_modifiers.CTRL,
			},
			["exec"] = {"ItemSpawner", "spawn/badges"},
		},
		HK_SPAWN_PACKAGE = {
			["save"] = "cb_spawning_spawn_package",
			["widget_type"] = "keybind",
			["text"] = "Supply Drop",
			["default"] = {
				"f1",
				oi.key_modifiers.CTRL,
			},
			["exec"] = {"ItemSpawner", "spawn/package"},
		},
		HK_SPAWN_KIT = {
			["save"] = "cb_spawning_spawn_kit",
			["widget_type"] = "keybind",
			["text"] = "First Aid Kit",
			["default"] = {
				"f2",
				oi.key_modifiers.CTRL,
			},
			["exec"] = {"ItemSpawner", "spawn/first_aid_kit"},
		},
		HK_SPAWN_SPEED = {
			["save"] = "cb_spawning_spawn_speed",
			["widget_type"] = "keybind",
			["text"] = "Speed Potion",
			["default"] = {
				"f3",
				oi.key_modifiers.CTRL,
			},
			["exec"] = {"ItemSpawner", "spawn/speed_boost_potion"},
		},
		HK_SPAWN_STRENGTH = {
			["save"] = "cb_spawning_spawn_strength",
			["widget_type"] = "keybind",
			["text"] = "Strength Potion",
			["default"] = {
				"f4",
				oi.key_modifiers.CTRL,
			},
			["exec"] = {"ItemSpawner", "spawn/damage_boost_potion"},
		},
		HK_SPAWN_FRAG = {
			["save"] = "cb_spawning_spawn_frag",
			["widget_type"] = "keybind",
			["text"] = "Frag Grenade",
			["default"] = {
				"f5",
				oi.key_modifiers.CTRL,
			},
			["exec"] = {"ItemSpawner", "spawn/frag_grenade_t2"},
		},
		HK_SPAWN_FIRE = {
			["save"] = "cb_spawning_spawn_fire",
			["widget_type"] = "keybind",
			["text"] = "Fire Grenade",
			["default"] = {
				"f6",
				oi.key_modifiers.CTRL,
			},
			["exec"] = {"ItemSpawner", "spawn/fire_grenade_t2"},
		},
		HK_SPAWN_LOREBOOK_PAGE = {
			["save"] = "cb_spawning_spawn_lorebook_page",
			["widget_type"] = "keybind",
			["text"] = "Lorebook page",
			["default"] = {
				"f7",
				oi.key_modifiers.CTRL,
			},
			["exec"] = {"ItemSpawner", "spawn/lorebook_page"},
		},
		HK_SPAWN_TORCH = {
			["save"] = "cb_spawning_spawn_torch",
			["widget_type"] = "keybind",
			["text"] = "Torch",
			["default"] = {
				"f8",
				oi.key_modifiers.CTRL,
			},
			["exec"] = {"ItemSpawner", "spawn/torch"},
		},
		HK_SPAWN_GRIM = {
			["save"] = "cb_spawning_spawn_grim",
			["widget_type"] = "keybind",
			["text"] = "Grim",
			["default"] = {
				"f9",
				oi.key_modifiers.CTRL,
			},
			["exec"] = {"ItemSpawner", "spawn/grimoire"},
		},
		HK_SPAWN_TOME = {
			["save"] = "cb_spawning_spawn_tome",
			["widget_type"] = "keybind",
			["text"] = "Tome",
			["default"] = {
				"f10",
				oi.key_modifiers.CTRL,
			},
			["exec"] = {"ItemSpawner", "spawn/tome"},
		},
		-- HK_SPAWN_SMOKE = {
		-- 	["save"] = "cb_spawning_spawn_smoke",
		-- 	["widget_type"] = "keybind",
		-- 	["text"] = "Smoke Grenade",
		-- 	["default"] = {
		-- 		"f11",
		-- 		oi.key_modifiers.CTRL,
		-- 	},
		-- 	["exec"] = {"ItemSpawner", "spawn/smoke_grenade_t2"},
		-- },
	},
}

local me = ItemSpawner

-- ####################################################################################################################
-- ##### Option #######################################################################################################
-- ####################################################################################################################
ItemSpawner.create_options = function()	
	Mods.option_menu:add_item("spawning", me.SETTINGS.SPAWN_PICKUPS, true)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_SPAWN_BADGES)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_SPAWN_PACKAGE)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_SPAWN_KIT)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_SPAWN_SPEED)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_SPAWN_STRENGTH)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_SPAWN_FRAG)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_SPAWN_FIRE)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_SPAWN_LOREBOOK_PAGE)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_SPAWN_TORCH)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_SPAWN_GRIM)
	Mods.option_menu:add_item("spawning", me.SETTINGS.HK_SPAWN_TOME)
	--Mods.option_menu:add_item("spawning", me.SETTINGS.HK_SPAWN_SMOKE)
end

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()