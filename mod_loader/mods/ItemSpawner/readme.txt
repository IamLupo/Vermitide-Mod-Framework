Adds keyboard shortcuts for spawning pickup items (medkits, tomes etc.).
Requires SpawnItemFunc mod to function.
Go to Mod Settings -> Spawning -> Items to see and change shortcuts.