local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

Mods.spawnItem("speed_boost_potion")

-- Feedback
if not get(ModSettings.SETTINGS.HK_FEEDBACK.save) then
	EchoConsole("Spawn: Speed Potion")
end