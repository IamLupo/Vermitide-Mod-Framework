local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

Mods.spawnItem("fire_grenade_t2")

-- Feedback
if not get(ModSettings.SETTINGS.HK_FEEDBACK.save) then
	EchoConsole("Spawn: Fire Grenade")
end