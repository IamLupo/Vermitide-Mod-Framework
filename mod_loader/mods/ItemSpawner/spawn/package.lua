local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

local list = {
	"all_ammo_small",
	"fire_grenade_t2",
	"frag_grenade_t2",
	"healing_draught",
	"first_aid_kit",
	"speed_boost_potion",
	"damage_boost_potion"
}

local player_unit = Managers.player:player_from_peer_id(Network.peer_id()).player_unit
local player_pos = Unit.local_position(player_unit, 0)
local player_rot = Unit.local_rotation(player_unit, 0)

local pickup_spawn_type = NetworkLookup.pickup_spawn_types['dropped']

for y = 1, 4 do
	for x = 1, #list do
		local final_pos = player_pos + Vector3((x * 0.3) - 0.9, (y * 0.3) - 0.9, 0)
		
		Managers.state.network.network_transmit:send_rpc_server(
			'rpc_spawn_pickup_with_physics',
			NetworkLookup.pickup_names[list[x]],
			final_pos,
			player_rot,
			pickup_spawn_type
		)
	end
end

-- Feedback
if not get(ModSettings.SETTINGS.HK_FEEDBACK.save) then
	EchoConsole("Spawn Package")
end