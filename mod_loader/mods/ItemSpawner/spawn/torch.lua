local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

Mods.spawnItem("torch")

-- Feedback
if not get(ModSettings.SETTINGS.HK_FEEDBACK.save) then
	EchoConsole("Spawn: Torch")
end