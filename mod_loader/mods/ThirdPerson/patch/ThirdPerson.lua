local mod_name = "ThirdPerson"
--[[ 
	Third person mode
		- Does the necessary positioning of the camera
		- Applies different fixes to certain situations
	
	Author: grasmann
--]]

local oi = OptionsInjector

ThirdPerson = {

	reset = true,
	
	-- Store the person mode before zooming
	first_person_zoom = {
		active = false,
		end_zoom = false,
		wait_time = 0,
	},
	
	firstperson = {
		unit = false,
		value = false,
	},
	
	offset = { x = 0.6, y = -0.8, z = 0.1 },
	
	zoom = {
		default = { default = 30, medium = 40, low = 50, off = 65, },
		increased = { default = 16, medium = 30, low = 45, off = 65, },
	},
	
	SETTINGS = {
		ACTIVE = {
			["save"] = "cb_third_person_active",
			["widget_type"] = "stepper",
			["text"] = "Active",
			["tooltip"] =  "Third Person\n" ..
				"Toggle third person on / off.\n\n" ..
				"Play the game in first or third person.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
			["hide_options"] = {
				{
					false,
					mode = "hide",
					options = {
						"cb_third_person_side",
						"cb_third_person_offset",
						"cb_third_person_zoom",
						"cb_third_person_first_person_zoom",
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_third_person_side",
						"cb_third_person_offset",
						"cb_third_person_zoom",
						"cb_third_person_first_person_zoom",
					}
				},
			},
		},
		SIDE = {
			["save"] = "cb_third_person_side",
			["widget_type"] = "stepper",
			["text"] = "Side",
			["tooltip"] = "Third Person Side\n" ..
				"Toggle side for third person left and right.\n\n" ..
				"Choose if the camera is to left or right of your character.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Left", value = false},
				{text = "Right", value = true}
			},
			["default"] = 1, -- Default first option is enabled. In this case Left
		},
		OFFSET = {
			["save"] = "cb_third_person_offset",
			["widget_type"] = "slider",
			["text"] = "Offset",
			["tooltip"] = "Third Person Offset\n" ..
				"Set camera offset for third person.\n\n" ..
				"Change the distance between the camera and the character.",
			["range"] = {50, 400},
			["default"] = 100,
		},
		ZOOM = {
			["save"] = "cb_third_person_zoom",
			["widget_type"] = "dropdown",
			["text"] = "Zoom",
			["tooltip"] = "Third Person Zoom\n" ..
				"Set camera zoom for third person.\n\n" ..
				"Change the zoom strength for third person.",
			["value_type"] = "number",
			["options"] = {
				{text = "Default", value = 1},
				{text = "Medium", value = 2},
				{text = "Low", value = 3},
				{text = "Off", value = 4},
			},
			["default"] = 1, -- Default first option is enabled. In this case Default
		},
		FIRST_PERSON_ZOOM = {
			["save"] = "cb_third_person_first_person_zoom",
			["widget_type"] = "stepper",
			["text"] = "First Person Zoom",
			["tooltip"] = "First Person Zoom\n" ..
				"Toggle first person zoom off or on.\n\n" ..
				"When your in third person mode and you aim it " ..
				"will automaticly switch to first person.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
		},
		HK_TOGGLE = {
			["save"] = "cb_third_person_hotkey_toggle",
			["widget_type"] = "keybind",
			["text"] = "Toggle On / Off",
			["default"] = {
				"mouse_middle",
				oi.key_modifiers.CTRL,
			},
			["exec"] = {"ThirdPerson", "action/view/third_person"},
		},
		HK_SIDE = {
			["save"] = "cb_third_person_hotkey_side",
			["widget_type"] = "keybind",
			["text"] = "Toggle Left / Right",
			["default"] = {
				"mouse_middle",
				oi.key_modifiers.CTRL_SHIFT,
			},
			["exec"] = {"ThirdPerson", "action/view/third_person_side"},
		},
		HK_OFFSET = {
			["save"] = "cb_third_person_hotkey_offset",
			["widget_type"] = "keybind",
			["text"] = "Change Offset",
			["default"] = {
				"mouse_middle",
				oi.key_modifiers.SHIFT,
			},
			["exec"] = {"ThirdPerson", "action/view/third_person_offset"},
		},
		HK_ZOOM = {
			["save"] = "cb_third_person_hotkey_zoom",
			["widget_type"] = "keybind",
			["text"] = "Change Zoom",
			["default"] = {
				"mouse_middle",
				oi.key_modifiers.NONE,
			},
			["exec"] = {"ThirdPerson", "action/view/third_person_zoom"},
		},
	},
	
}
local me = ThirdPerson


local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Option #######################################################################################################
-- ####################################################################################################################
ThirdPerson.create_options = function()
	Mods.option_menu:add_group("third_person", "Third Person")
	
	Mods.option_menu:add_item("third_person", me.SETTINGS.ACTIVE, true)
	Mods.option_menu:add_item("third_person", me.SETTINGS.SIDE)
	Mods.option_menu:add_item("third_person", me.SETTINGS.OFFSET)
	Mods.option_menu:add_item("third_person", me.SETTINGS.ZOOM)
	Mods.option_menu:add_item("third_person", me.SETTINGS.FIRST_PERSON_ZOOM)
	Mods.option_menu:add_item("third_person", me.SETTINGS.HK_TOGGLE, true)
	Mods.option_menu:add_item("third_person", me.SETTINGS.HK_SIDE, true)
	Mods.option_menu:add_item("third_person", me.SETTINGS.HK_OFFSET, true)
	Mods.option_menu:add_item("third_person", me.SETTINGS.HK_ZOOM, true)
end

-- ####################################################################################################################
-- ##### Hooks ########################################################################################################
-- ####################################################################################################################
--[[
	Fix to make mission objectives visible in third person
--]]
Mods.hook.set(mod_name, "TutorialUI.update", function(func, self, ...)
	if get(me.SETTINGS.ACTIVE.save) then
		if self._first_person_extension then self._first_person_extension.first_person_mode = true end
		func(self, ...)
		if self._first_person_extension then self._first_person_extension.first_person_mode = me.firstperson.value end
	else
		func(self, ...)
	end
end)
--[[
	MAIN FUNCTION - Camera positioning
--]]
Mods.hook.set(mod_name, "CameraManager.post_update", function(func, self, dt, t, viewport_name)
	
	-- ##### Original function ########################################################################################
	func(self, dt, t, viewport_name)			
	
	if get(me.SETTINGS.ACTIVE.save) then
		-- ##### Check side ###########################################################################################
		local offset = nil
		local mult = get(me.SETTINGS.OFFSET.save) / 100
		if mult == nil then mult = 1 end		
		if not get(me.SETTINGS.SIDE.save) then
			offset = Vector3(me.offset.x, me.offset.y * mult, me.offset.z)
		else
			offset = Vector3(-me.offset.x, me.offset.y * mult, me.offset.z)
		end
	
		-- ##### Get data #############################################################################################
		local viewport = ScriptWorld.viewport(self._world, viewport_name)
		local camera = ScriptViewport.camera(viewport)
		local shadow_cull_camera = ScriptViewport.shadow_cull_camera(viewport)
		local camera_nodes = self._camera_nodes[viewport_name]
		local current_node = self._current_node(self, camera_nodes)
		local camera_data = self._update_transition(self, viewport_name, camera_nodes, dt)	

		camera_data.position = self._calculate_sequence_event_position(self, camera_data, offset)
		
		-- ##### Change zoom #####
		me.set_zoom_values(current_node)
		
		-- ##### Update camera ########################################################################################		
		self._update_camera_properties(self, camera, shadow_cull_camera, current_node, camera_data, viewport_name)
		self._update_sound_listener(self, viewport_name)		
		ScriptCamera.force_update(self._world, camera)		
	end
	
end)
--[[
	Fix to apply camera offset to ranged weapons
--]]
Mods.hook.set(mod_name, "PlayerUnitFirstPerson.current_position", function(func, self)
	if get(me.SETTINGS.ACTIVE.save) then
		-- ##### Get data #############################################################################################
		local position = Unit.world_position(self.first_person_unit, 0) --+ Vector3(0, 0, 1.5)
		local current_rot = Unit.local_rotation(self.first_person_unit, 0)

		-- ##### Counter offset #######################################################################################
		local offset = {}
		if not get(me.SETTINGS.SIDE.save) then
			offset = Vector3(me.offset.x, 0, me.offset.z)
		else
			offset = Vector3(-me.offset.x, 0, me.offset.z)
		end
		
		-- ##### Change position ######################################################################################
		local x = offset.x * Quaternion.right(current_rot)
		local y = offset.y * Quaternion.forward(current_rot)
		local z = Vector3(0, 0, offset.z)
		position = position + x + y + z	
		return position
	end
	
	-- ##### Original function ########################################################################################
	return func(self)
end)
--[[
	MAIN FUNCTION - Set first / third person mode - Hide first person ammo
--]]
Mods.hook.set(mod_name, "PlayerUnitFirstPerson.update", function(func, self, unit, input, dt, context, t)
	me.firstperson.unit = self

	if me.reset then
		self.set_first_person_mode(self, not get(me.SETTINGS.ACTIVE.save))
		me.reset = false
	end
	
	-- ##### Original function ########################################################################################
	func(self, unit, input, dt, context, t)
	
	if not me.is_first_person_blocked(self.unit) then
		if get(me.SETTINGS.ACTIVE.save) then
			-- ##### Disable first person #############################################################################
			if me.firstperson.value then
				self.set_first_person_mode(self, false)
				me.firstperson.value = false
			end
			
			-- ##### Hide first person ammo ###########################################################################
			local inventory_extension = ScriptUnit.extension(self.unit, "inventory_system")
			local slot_data = inventory_extension.get_slot_data(inventory_extension, "slot_ranged")
			if slot_data then
				if slot_data.right_ammo_unit_1p then Unit.set_unit_visibility(slot_data.right_ammo_unit_1p, false) end
				if slot_data.left_ammo_unit_1p then Unit.set_unit_visibility(slot_data.left_ammo_unit_1p, false) end	
			end
		else
			-- ##### Enable first person ##############################################################################
			if not me.firstperson.value then
				self.set_first_person_mode(self, true)
				me.firstperson.value = true
			end
		end
	end
	
end)

-- ####################################################################################################################
-- ##### Third Person - First Person Zoom #############################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "ActionAim.client_owner_start_action", function(func, ...)
	func(...)
	
	me.first_person_zoom.active = get(me.SETTINGS.ACTIVE.save)
	if get(me.SETTINGS.FIRST_PERSON_ZOOM.save) then
		set(me.SETTINGS.ACTIVE.save, false)
	end
end)

Mods.hook.set(mod_name, "ActionAim.finish", function(func, ...)
	func(...)
	
	me.first_person_zoom.end_zoom = true
end)

Mods.hook.set(mod_name, "ActionTrueFlightBowAim.client_owner_start_action", function(func, ...)
	func(...)
	
	me.first_person_zoom.active = get(me.SETTINGS.ACTIVE.save)
	if get(me.SETTINGS.FIRST_PERSON_ZOOM.save) then
		set(me.SETTINGS.ACTIVE.save, false)
	end
end)

Mods.hook.set(mod_name, "ActionTrueFlightBowAim.finish", function(func, ...)
	local chain_action_data = func(...)
	
	me.first_person_zoom.end_zoom = true
	
	return chain_action_data
end)

Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, self, dt, t)
	func(self, dt, t)
	
	-- Check if end zoom has triggered
	if me.first_person_zoom.end_zoom then
		-- Save the now time to wait 1 second to end first person mode
		me.first_person_zoom.wait_time = t
		me.first_person_zoom.end_zoom = false
	end
	
	-- After 1 second
	if me.first_person_zoom.wait_time ~= 0 and me.first_person_zoom.wait_time + 0.3 < t then
		if get(me.SETTINGS.FIRST_PERSON_ZOOM.save) then
			me.first_person_zoom.wait_time = 0
			
			set(me.SETTINGS.ACTIVE.save, me.first_person_zoom.active)
		end
	end
end)

-- ####################################################################################################################
-- ##### Reset ########################################################################################################
-- ####################################################################################################################
--[[
	A game was started
--]]
Mods.hook.set(mod_name, "StateInGameRunning.event_game_started", function(func, self)
	func(self)
	me.reset = true
end)
--[[
	A game was actually started ... lol
--]]
Mods.hook.set(mod_name, "StateInGameRunning.event_game_actually_starts", function(func, self)
	func(self)
	me.reset = true
end)
--[[
	Set first person mode for cutscenes
--]]
Mods.hook.set(mod_name, "CutsceneSystem.set_first_person_mode", function(func, self, enabled)
	func(self, enabled)
	
	if enabled then
		me.reset = true
	end
end)
--[[
	Reset view after character change
--]]
Mods.hook.set(mod_name, "ProfileView.on_exit", function(func, ...)
	func(...)
	
	if get(me.SETTINGS.ACTIVE.save) then
		me.reset = true
	end
end)
--[[
	Reset view after equipment change
--]]
Mods.hook.set(mod_name, "InventoryView.on_exit", function(func, self)
	func(self)
	if get(me.SETTINGS.ACTIVE.save) then
		me.reset = true
	end
end)

-- ####################################################################################################################
-- ##### Projectiles ##################################################################################################
-- ####################################################################################################################
--[[
	Fix to apply camera offset to projectiles
--]]
Mods.hook.set(mod_name, "ActionUtils.spawn_player_projectile", function(func, owner_unit, position, rotation, scale, angle, 
	target_vector, speed, item_name, item_template_name, action_name, sub_action_name)
	if get(me.SETTINGS.ACTIVE.save) then
		-- ##### Get data #############################################################################################
		local first_person_extension = ScriptUnit.extension(owner_unit, "first_person_system")
		local first_person_unit = first_person_extension.get_first_person_unit(first_person_extension)	
		position = Unit.world_position(first_person_unit, 0) --+ Vector3(0, 0, 1.5)
		local current_rot = Unit.local_rotation(first_person_unit, 0)

		-- ##### Counter offset #######################################################################################
		local offset = {}
		if not get(me.SETTINGS.SIDE.save) then
			offset = Vector3(me.offset.x, -me.offset.y*2, me.offset.z)
		else
			offset = Vector3(-me.offset.x, -me.offset.y*2, me.offset.z)
		end
		
		-- ##### Change position ######################################################################################
		local x = offset.x * Quaternion.right(current_rot)
		local y = offset.y * Quaternion.forward(current_rot)
		local z = Vector3(0, 0, offset.z)
		position = position + x + y + z	
	end
	
	-- ##### Original function ########################################################################################
	func(owner_unit, position, rotation, scale, angle, target_vector, speed, item_name, 
		item_template_name, action_name, sub_action_name)
end)

Mods.hook.set(mod_name, "ActionUtils.spawn_true_flight_projectile", function(func, owner_unit, target_unit, true_flight_template_id, 
	position, rotation, angle, target_vector, speed, item_name, item_template_name, action_name, sub_action_name, scale)
	
	if get(me.SETTINGS.ACTIVE.save) then
		-- ##### Get data #############################################################################################
		local first_person_extension = ScriptUnit.extension(owner_unit, "first_person_system")
		local first_person_unit = first_person_extension.get_first_person_unit(first_person_extension)	
		position = Unit.world_position(owner_unit, 0) + Vector3(0, 0, 1.5)
		local current_rot = Unit.local_rotation(first_person_unit, 0)
		
		-- ##### Create offset ########################################################################################
		local offset = {}
		if not get(me.SETTINGS.SIDE.save) then
			offset = Vector3(me.offset.x, -me.offset.y*2, me.offset.z)
		else
			offset = Vector3(-me.offset.x, -me.offset.y*2, me.offset.z)
		end
		
		-- ##### Change position ######################################################################################
		local x = offset.x * Quaternion.right(current_rot)
		local y = offset.y * Quaternion.forward(current_rot)
		local z = Vector3(0, 0, offset.z)
		position = position + x + y + z	
	end
	
	func(owner_unit, target_unit, true_flight_template_id, position, rotation, angle, 
		target_vector, speed, item_name, item_template_name, action_name, sub_action_name, scale)
end)

--[[
	Set zoom values
--]]
ThirdPerson.set_zoom_values = function(current_node)
	local degrees_to_radians = math.pi/180
	local zoom_fov = 65
	local zoom_setting = get(me.SETTINGS.ZOOM.save)
	if current_node._name == "zoom_in" then
		if zoom_setting == 2 then
			zoom_fov = me.zoom.default.medium
		elseif zoom_setting == 3 then
			zoom_fov = me.zoom.default.low
		elseif zoom_setting == 4 then
			zoom_fov = me.zoom.default.off
		else
			zoom_fov = me.zoom.default.default
		end
		current_node._vertical_fov = zoom_fov*degrees_to_radians				
	elseif current_node._name == "increased_zoom_in" then
		if zoom_setting == 2 then
			zoom_fov = me.zoom.increased.medium
		elseif zoom_setting == 3 then
			zoom_fov = me.zoom.increased.low
		elseif zoom_setting == 4 then
			zoom_fov = me.zoom.increased.off
		else
			zoom_fov = me.zoom.increased.default
		end
		current_node._vertical_fov = zoom_fov*degrees_to_radians
	end	
end
--[[
	Check if first person is blocked
--]]
ThirdPerson.is_first_person_blocked = function(unit)
	local blocked = false
	local state_system = ScriptUnit.extension(unit, "character_state_machine_system")
	if state_system ~= nil then
		blocked = blocked or state_system.state_machine.state_current.name == "dead"
		blocked = blocked or state_system.state_machine.state_current.name == "grabbed_by_pack_master"
		blocked = blocked or state_system.state_machine.state_current.name == "inspecting"
		blocked = blocked or state_system.state_machine.state_current.name == "interacting"
		blocked = blocked or state_system.state_machine.state_current.name == "knocked_down"
		--blocked = blocked or state_system.state_machine.state_current.name == "leave_ledge_hanging_falling"
		--blocked = blocked or state_system.state_machine.state_current.name == "leave_ledge_hanging_pull_up"
		blocked = blocked or state_system.state_machine.state_current.name == "ledge_hanging"
		blocked = blocked or state_system.state_machine.state_current.name == "pounced_down"
		blocked = blocked or state_system.state_machine.state_current.name == "waiting_for_assisted_respawn"
	end
	return blocked
end

-- ##### Start ########################################################################################################
me.create_options()