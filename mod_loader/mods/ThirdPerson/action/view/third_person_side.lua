local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- Set
set(ThirdPerson.SETTINGS.SIDE.save, not get(ThirdPerson.SETTINGS.SIDE.save))
save()

-- Feedback
if not get(ModSettings.SETTINGS.HK_FEEDBACK.save) then
	if get(ThirdPerson.SETTINGS.SIDE.save) then
		EchoConsole("View: Third person - Right side")
	else
		EchoConsole("View: Third person - Left side")
	end
end