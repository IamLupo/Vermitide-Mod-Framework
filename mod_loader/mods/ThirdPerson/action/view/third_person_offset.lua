local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

local offset = get(ThirdPerson.SETTINGS.OFFSET.save)

-- Set
if offset == 100 then
	set(ThirdPerson.SETTINGS.OFFSET.save, 200)
elseif offset == 200 then
	set(ThirdPerson.SETTINGS.OFFSET.save, 400)
else
	set(ThirdPerson.SETTINGS.OFFSET.save, 100)
end
save()

-- Feedback
if not get(ModSettings.SETTINGS.HK_FEEDBACK.save) then
	if offset == 100 then
		EchoConsole("View: Third person - Default offset")	
	elseif offset == 200 then
		EchoConsole("View: Third person - Medium offset")
	else
		EchoConsole("View: Third person - Far offset")
	end
end