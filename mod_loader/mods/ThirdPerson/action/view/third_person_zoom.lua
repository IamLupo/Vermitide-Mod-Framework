local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

local zoom = get(ThirdPerson.SETTINGS.ZOOM.save)

-- Set
if zoom == 1 then
	set(ThirdPerson.SETTINGS.ZOOM.save, 2)
elseif zoom == 2 then
	set(ThirdPerson.SETTINGS.ZOOM.save, 3)
elseif zoom == 3 then
	set(ThirdPerson.SETTINGS.ZOOM.save, 4)
else
	set(ThirdPerson.SETTINGS.ZOOM.save, 1)
end
save()

-- Feedback
if not get(ModSettings.SETTINGS.HK_FEEDBACK.save) then
	if zoom == 1 then
		EchoConsole("View: Third person - Default Zoom")
	elseif zoom == 2 then
		EchoConsole("View: Third person - Medium Zoom")
	elseif zoom == 3 then
		EchoConsole("View: Third person - Low Zoom")
	else
		EchoConsole("View: Third person - No Zoom")
	end
end