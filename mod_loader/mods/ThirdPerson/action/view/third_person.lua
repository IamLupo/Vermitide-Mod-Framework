local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- Set
set(ThirdPerson.SETTINGS.ACTIVE.save, not get(ThirdPerson.SETTINGS.ACTIVE.save))
save()

-- Feedback
if not get(ModSettings.SETTINGS.HK_FEEDBACK.save) then
	if get(ThirdPerson.SETTINGS.ACTIVE.save) then
		EchoConsole("View: Third person")
	else
		EchoConsole("View: First person")
	end
end