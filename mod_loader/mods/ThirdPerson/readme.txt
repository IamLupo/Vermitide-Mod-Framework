Adds third person camera mode.
Go to Mod Settings -> Third Person to tweak to your liking.
Keyboard shortcuts (default):
	Ctrl + Middle Mouse Button - toggle first person/third person mode
	Ctrl + Shift + Middle Mouse Button - switch between left/right camera position
	Shift + Offset - change camera offset
	Middle Mouse - change zoom level