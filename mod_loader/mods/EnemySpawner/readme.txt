Provides keyboard shortcuts for spawning and removing enemies from the map.
Keyboard shortcuts (default):
	o - cycle through breeds
	p - spawn enemy
	i - despawn all enemies in a large radius
Go to Mod Settings -> Spawning -> Enemies to change shortcuts.