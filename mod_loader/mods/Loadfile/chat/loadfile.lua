--[[
	Type in the chat: /loadfile <filename>
--]]

local args = {...}

if #args == 1 then
	local file = args[1]

	if file_exists("mod_loader/loadfile/" .. file .. ".lua") then
		EchoConsole("Execute: " .. file)
		Mods.exec("loadfile", file)
	else
		EchoConsole("File not exist!")
	end

	return true
else
	return false
end