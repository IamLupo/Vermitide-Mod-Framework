local mutator_name = "Deathwish"
local mutator_group = "GAME_MODE"
local difficulty_name = "||Deathwish Difficulty||"

Deathwish = {}

Deathwish.name = mutator_name
Deathwish.group = mutator_group
Deathwish.difficulty_name = difficulty_name
Deathwish.saved = {}
Deathwish.data = {
	["Breeds.skaven_clan_rat.max_health[5]"] = "18",
	["Breeds.skaven_slave.max_health[5]"] = "9",
	["Breeds.skaven_gutter_runner.max_health[5]"] = "30",
	["Breeds.skaven_loot_rat.max_health[5]"] = "200",
	["Breeds.skaven_pack_master.max_health[5]"] = "72",
	["Breeds.skaven_poison_wind_globadier.max_health[5]"] = "30",
	["Breeds.skaven_ratling_gunner.max_health[5]"] = "24",
	["Breeds.skaven_storm_vermin.max_health[5]"] = "50",
	["Breeds.skaven_storm_vermin_commander.max_health[5]"] = "50",

	["BreedActions.skaven_clan_rat.first_attack.difficulty_damage.hardest"] = "{20,10,5}",
	["BreedActions.skaven_clan_rat.running_attack.difficulty_damage.hardest"] = "{20,10,5}",
	["BreedActions.skaven_clan_rat.normal_attack.difficulty_damage.hardest"] = "{20,10,5}",

	["BreedActions.skaven_gutter_runner.target_pounced.difficulty_damage.hardest"] = "{10,4,1}",

	["BreedActions.skaven_poison_wind_globadier.throw_poison_globe.aoe_init_damage[5]"] = "{20,2,0}",
	["BreedActions.skaven_poison_wind_globadier.throw_poison_globe.aoe_dot_damage[5]"] = "{30,0,0}",
	["BreedActions.skaven_poison_wind_globadier.suicide_run.aoe_init_damage[5]"] = "{70,5,0}",
	["BreedActions.skaven_poison_wind_globadier.suicide_run.aoe_dot_damage[5]"] = "{20,0,0}",


	["BreedActions.skaven_rat_ogre.melee_slam.difficulty_damage.hardest"] = "{60,30,22.5}",
	["BreedActions.skaven_rat_ogre.melee_slam.blocked_difficulty_damage.hardest"] = "{25,20,15}",
	["BreedActions.skaven_rat_ogre.melee_shove.difficulty_damage.hardest"] = "{100,90,90}",

	["BreedActions.skaven_storm_vermin.special_attack_sweep.difficulty_damage.hardest"] = "{75,45,30}",
	["BreedActions.skaven_storm_vermin_commander.special_attack_sweep.difficulty_damage.hardest"] = "{75,45,30}",
	["BreedActions.skaven_storm_vermin.special_attack_cleave.difficulty_damage.hardest"] = "{150,75,45}",
	["BreedActions.skaven_storm_vermin_commander.special_attack_cleave.difficulty_damage.hardest"] = "{150,75,45}",

	["BreedActions.skaven_ratling_gunner.shoot_ratling_gun.attack_template_damage_type[5]"] = "\"sniper_shot_AP_t2\"",

	["DifficultySettings.hardest.amount_storm_vermin_patrol"] = "24"
}

Deathwish.enable = function(self)
	Mods.hook.enable(true, mutator_name)
	Mods.mutators.replace_data(Deathwish)
	Mods.mutators:set_servername(self.difficulty_name)
end

Deathwish.disable = function(self)
	Mods.hook.enable(false, mutator_name)
	Mods.mutators.restore_data(Deathwish)
	Mods.mutators:restore_servername()
end

Deathwish.check_conditions = function()
	return Managers.player.is_server and Managers.state.difficulty:get_difficulty_rank() == 5
end

--Hooks

--Player dies when knocked down
Mods.hook.set(mutator_name, "PlayerUnitHealthExtension._knock_down", function (func, self, unit)
	PlayerUnitHealthExtension.die(self, unit)
end)

--Tweaking damage calculation to allow for more friendly fire and less damage done to ogre
Mods.hook.set(mutator_name, "DamageUtils.add_damage_network", function (func, victim_unit, attacker_unit, damage_amount, hit_zone_name, damage_type, damage_direction, damage_source)
	local damage_amountfixed = (damage_amount / 1.5)
	local damage_amountff = (damage_amount * 1.5)
	local breed = Unit.get_data(victim_unit, "breed")

	if breed ~= nil then
		if breed.name == "skaven_rat_ogre" then
			func(victim_unit, attacker_unit, damage_amountfixed, hit_zone_name, damage_type, damage_direction, damage_source)
		else
			func(victim_unit, attacker_unit, damage_amount, hit_zone_name, damage_type, damage_direction, damage_source)
		end
	elseif (Unit.get_data(attacker_unit, "breed") == nil and damage_type ~= "damage_over_time") then
		func(victim_unit, attacker_unit, damage_amountff, hit_zone_name, damage_type, damage_direction, damage_source)
	else
		func(victim_unit, attacker_unit, damage_amount, hit_zone_name, damage_type, damage_direction, damage_source)
	end
end)

--Making respawn longer
Mods.hook.set(mutator_name, "RespawnHandler.update", function (func, self, dt, t, player_statuses)
	for _, status in ipairs(player_statuses) do
		if status.health_state == "dead" and not status.ready_for_respawn and not status.respawn_timer then
			local peer_id = status.peer_id
			local local_player_id = status.local_player_id
			local respawn_time = 60

			if peer_id or local_player_id then
				local player = Managers.player:player(peer_id, local_player_id)
				local player_unit = player.player_unit

				if Unit.alive(player_unit) then
					local buff_extension = ScriptUnit.extension(player_unit, "buff_system")
					respawn_time = buff_extension.apply_buffs_to_value(buff_extension, respawn_time, StatBuffIndex.FASTER_RESPAWN)
				end
			end

			status.respawn_timer = t + respawn_time
		elseif status.health_state == "dead" and not status.ready_for_respawn and status.respawn_timer < t then
			status.respawn_timer = nil
			status.ready_for_respawn = true
		end
	end

	return 
end)

--Adding bonus grims
Mods.hook.set(mutator_name, "GameModeManager.complete_level", function (func, ...)
	local mission_system = Managers.state.entity:system("mission_system")
	for i = 1,2 do
		mission_system.request_mission(mission_system, "grimoire_hidden_mission")
		mission_system.update_mission(mission_system, "grimoire_hidden_mission", true, nil, true)
	end

	func(...)
end)

--Changing difficulty name
Mods.hook.set(mutator_name, "IngamePlayerListUI.set_difficulty_name", function (func, self, name)		
	local content = self.headers.content
	content.game_difficulty = mutator_name
	return 
end)

--Setting servername
Mods.hook.set(mutator_name, "MatchmakingStateHostGame.host_game", function (func, self, ...)
	func(self, ...)

	Mods.mutators:set_servername(difficulty_name)
end)

Mods.hook.enable(false, mutator_name)

Mods.mutators.save_data(Deathwish)

--Adding mutator to mutator manager
Mods.mutators:add_mutator(mutator_group, Deathwish)