Deathwish custom game mode. A higher level of difficulty than Cataclysm.
"A callback to the april's fool video, turned to reality. However, unlike Fatshark's video depicted, this mod does not spawn (much) extra specials at all. It is however a new devastating difficulty that will make cataclysm feel like child's play.
All skaven have received another major health increase compared to cataclysm, giving them resilience against most things that would otherwise dispose of them instantly. Stormvermins especially will not go down without coordinated effort, or large expense of ressources.
Common skaven have seen their damage slightly increased in comparison to cataclysm. They remain incapable of taking out heroes in less than 3 hits (grimoires excluded), but each strike will be felt.
Special skaven have seen their damage massively increased in comparison to cataclysm, with ratling gunners taking the crown with a 433% increase in bullet damage.
Stormvermin patrols have received reinforcements in order to truly strike fear in the heart of heroes.
In addition to all that, heroes have become more frail. They may not be knocked down anymore, they will instead die instantly."
By Grimalackt
Go to Mod Settings -> Mutators -> Game Mode and pick Deathwish from the dropdown menu.
This game mode works correctly only on Cataclysm difficulty.