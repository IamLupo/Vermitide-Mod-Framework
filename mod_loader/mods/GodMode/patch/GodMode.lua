local mod_name = "GodMode"
--[[
	GodMode
		- Refills your life when it hits 0
	
	author: grasmann
--]]

local oi = OptionsInjector

Mods.GodMode = {
	SETTINGS = {
		ACTIVE = {
			["save"] = "cb_god_mode",
			["widget_type"] = "stepper",
			["text"] = "God Mode",
			["tooltip"] =  "God Mode\n" ..
				"Toggle god mode on / off.\n\n" ..
				"Refills your life when it hits 0.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
			["hide_options"] = {
				{
					false,
					mode = "show",
					options = {
						"cb_god_mode_hotkey_toggle",
						"cb_god_mode_hotkey_toggle_modifiers",
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_god_mode_hotkey_toggle",
						"cb_god_mode_hotkey_toggle_modifiers",
					}
				},
			},
		},
		HK_TOGGLE = {
			["save"] = "cb_god_mode_hotkey_toggle",
			["widget_type"] = "keybind",
			["text"] = "Toggle",
			["default"] = {
				"g",
				oi.key_modifiers.CTRL_SHIFT,
			},
			["exec"] = {"GodMode", "action/god"},
		},
	},
}
local me = Mods.GodMode

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Option #######################################################################################################
-- ####################################################################################################################
Mods.GodMode.create_options = function()
	Mods.option_menu:add_group("cheats", "Cheats")
	
	Mods.option_menu:add_item("cheats", me.SETTINGS.ACTIVE, true)
	Mods.option_menu:add_item("cheats", me.SETTINGS.HK_TOGGLE, true)
end

-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "PlayerUnitHealthExtension._knock_down", function(func, self, unit)
	if get(me.SETTINGS.ACTIVE.save) then
		local unit_id = Managers.state.network:unit_game_object_id(unit)		
		
		-- Reset knocked down
		StatusUtils.set_knocked_down_network(unit, false)
		StatusUtils.set_revived_network(unit, true, unit)
		
		-- Reset health
		Managers.state.network.network_transmit:send_rpc_server(
			"rpc_request_heal",
			unit_id,
			150,
			NetworkLookup.heal_types.healing_draught
		)
	else
		func(self, unit)
	end
	
end)

set(me.SETTINGS.ACTIVE.save, false)
save()

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()