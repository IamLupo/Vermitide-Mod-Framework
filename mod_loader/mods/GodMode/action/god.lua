local me = Mods.GodMode

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

pcall(function()
	set(me.SETTINGS.ACTIVE.save, not get(me.SETTINGS.ACTIVE.save))
	save()
	
	-- Feedback
	if get(me.SETTINGS.ACTIVE.save) then
		EchoConsole("God Mode On")
	else
		EchoConsole("God Mode Off")
	end
end)