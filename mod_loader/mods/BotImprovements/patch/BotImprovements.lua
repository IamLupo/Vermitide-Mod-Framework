local mod_name = "BotImprovements"
--[[
	authors: grimalackt, iamlupo, walterr
 
	Bot Improvements.
--]]
 
local MANUAL_HEAL_DISABLED = 1
local MANUAL_HEAL_OPTIONAL = 2
local MANUAL_HEAL_MANDATORY = 3
 
 local oi = OptionsInjector
 
BotImprovements = {
	SETTINGS = {
		PREFER_SALTZBOT = {
			["save"] = "cb_bot_improvements_prefer_saltzbot",
			["widget_type"] = "stepper",
			["text"] = "Choose Saltzpyre as Bot",
			["tooltip"] = "Choose Saltzpyre as Bot\n" ..
				"Choose Saltzpyre as a bot in preference to Sienna.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
		STAY_CLOSER = {
			["save"] = "cb_bot_improvements_stay_closer",
			["widget_type"] = "stepper",
			["text"] = "Bots Stay Closer During Hordes",
			["tooltip"] = "Bots Stay Closer During Hordes\n" ..
				"Bots will stay closer to humans when many rats are aggro'd.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
		NO_CHASE_RATLING = {
			["save"] = "cb_bot_improvements_no_chase_ratling",
			["widget_type"] = "stepper",
			["text"] = "Bots Should Not Chase Ratlings",
			["tooltip"] = "Bots Should Not Chase Ratlings\n" ..
				"Prevents bots from running off to melee Ratling Gunners.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
		},
		NO_CHASE_GLOBADIER = {
			["save"] = "cb_bot_improvements_no_chase_globadier",
			["widget_type"] = "stepper",
			["text"] = "Bots Should Not Chase Globadiers",
			["tooltip"] = "Bots Should Not Chase Globadiers\n" ..
				"Prevents bots from running off to melee Globadiers.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
		},
		NO_SEEK_COVER = {
			["save"] = "cb_bot_improvements_no_seek_cover",
			["widget_type"] = "stepper",
			["text"] = "Bots Should Ignore Ratling Fire",
			["tooltip"] = "Bots Should Ignore Ratling Fire\n" ..
				"Prevents bots from running off to seek cover from Ratling " ..
				"Gunner fire, even if they are being shot (use with caution).",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
		},
		BETTER_MELEE = {
			["save"] = "cb_bot_improvements_better_melee",
			["widget_type"] = "stepper",
			["text"] = "Improved Bot Melee Choices",
			["tooltip"] = "Improved Bot Melee Choices\n" ..
				"Improves bots' decision-making about which melee attack to use.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
		FASTER_AIM = {
			["save"] = "cb_bot_improvements_faster_aim",
			["widget_type"] = "stepper",
			["text"] = "Faster Bot Aiming",
			["tooltip"] = "Faster Bot Aiming\n" ..
				"Drastically improves bots' aiming speed and will to fire.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
		MANUAL_HEAL = {
			["save"] = "cb_bot_improvements_manual_heal",
			["widget_type"] = "dropdown",
			["text"] = "Manual Control of Bot Healing",
			["tooltip"] = "Manual Control of Bot Healing\n" ..
				"Allows manual control of bot healing.\n\n" ..
				"Holding numpad 0 will make one of the bots heal you, if it has a medkit.\n" ..
				"Holding numpad 1, 2, or 3 will make a bot heal the hero at the 1st, 2nd, or 3rd " ..
				"position on the HUD (which may be itself).\n\n" ..
				"-- OFF --\nThe feature is disabled, bots heal automatically as usual.\n\n" ..
				"-- AS WELL AS AUTO --\nBots will still heal automatically, but you can also " ..
					"manually force them to heal.\n\n" ..
				"-- INSTEAD OF AUTO --\nBots will only heal if you manually force them to do so.",
			["value_type"] = "number",
			["options"] = {
				{text = "Off", value = MANUAL_HEAL_DISABLED},
				{text = "As Well as Auto", value = MANUAL_HEAL_OPTIONAL},
				{text = "Instead of Auto", value = MANUAL_HEAL_MANDATORY},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
		},
		GIVE_POTIONS = {
			["save"] = "cb_bot_improvements_give_potions",
			["widget_type"] = "stepper",
			["text"] = "Bots Give Potions to Humans",
			["tooltip"] = "Bots Give Potions to Humans\n" ..
				"Allows bots to give potions to humans.\n\n" ..
					"If a bot has a potion, it will give it to you if it is standing " ..
					"near you, and your potion slot is empty.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
		GIVE_GRENADES = {
			["save"] = "cb_bot_improvements_give_grenades",
			["widget_type"] = "stepper",
			["text"] = "Bots Give Grenades to Humans",
			["tooltip"] = "Bots Give Grenades to Humans\n" ..
				"Allows bots to give grenades to humans.\n\n" ..
				"If a bot has a grenade, it will give it to you if it is standing " ..
				"near you, and your grenade slot is empty.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
		GIVE_DRAUGHTS = {
			["save"] = "cb_bot_improvements_give_draughts",
			["widget_type"] = "stepper",
			["text"] = "Bots Give Draughts to Low Health Humans And Bots",
			["tooltip"] = "Bots Give Draughts to Low Health Humans And Bots\n" ..
				"Allows bots to give healing draughts to humans or other bots who are in need.\n\n" ..
				"If a bot has a healing draught, it will give it to you if it is standing " ..
				"near you, and your healing slot is empty, and you are wounded.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
		LOOT_GRIMOIRES = {
			["save"] = "cb_bot_improvements_loot_grimoires",
			["widget_type"] = "stepper",
			["text"] = "Bots Pick Up Pinged Grimoires",
			["tooltip"] = "Bots Pick Up Pinged Grimoires\n" ..
				"Allows bots to pick up a grimoire when a human player pings the grimoire.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
		LOOT_TOMES = {
			["save"] = "cb_bot_improvements_loot_tomes",
			["widget_type"] = "stepper",
			["text"] = "Bots Pick Up Pinged Tomes",
			["tooltip"] = "Bots Pick Up Pinged Tomes\n" ..
				"Allows bots to pick up a tome when a human player pings the tome.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
		LOOT_POTIONS = {
			["save"] = "cb_bot_improvements_loot_potions",
			["widget_type"] = "stepper",
			["text"] = "Bots Pick Up Pinged Potions",
			["tooltip"] = "Bots Pick Up Pinged Potions\n" ..
				"Allows bots to pick up a potion when a human player pings the potion. " ..
				"Bots will only pick up potions when no human has an empty potion slot.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
		LOOT_GRENADES = {
			["save"] = "cb_bot_improvements_loot_grenades",
			["widget_type"] = "stepper",
			["text"] = "Bots Pick Up Pinged Grenades",
			["tooltip"] = "Bots Pick Up Pinged Grenades\n" ..
				"Allows bots to pick up a grenade when a human player pings the grenade. " ..
				"Bots will only pick up grenades when no human has an empty grenade slot.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
		KEEP_TOMES = {
			["save"] = "cb_bot_improvements_keep_tomes",
			["widget_type"] = "stepper",
			["text"] = "Bots Keep Tomes",
			["tooltip"] = "Bots Keep Tomes\n" ..
				"Stop bots from exchanging tomes for healing items, \n"..
				"unless they are wounded enough to use them or the healing item is pinged.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
		PING_STORMVERMINS = {
			["save"] = "cb_bot_improvements_ping_stormvermins",
			["widget_type"] = "stepper",
			["text"] = "Bots Ping Attacking Stormvermins",
			["tooltip"] = "Bots Ping Attacking Stormvermins\n" ..
				"Allows bots to ping stormvermins that are targeting them. " ..
				"Limited to one stormvermin at a time.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
		FIX_REVIVE = {
			["save"] = "cb_bot_improvements_fix_revive",
			["widget_type"] = "stepper",
			["text"] = "Fix Revive",
			["tooltip"] = "Fix Revive\n" ..
				"Bots will no longer fail to revive when targeted by a stormvermin.\n",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
	},
}
 
local me = BotImprovements
 
local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings
 
-- ############################################################################################################
-- ##### Options ##############################################################################################
-- ############################################################################################################
--[[
	Create options
--]]
BotImprovements.create_options = function()
	Mods.option_menu:add_group("bot_improvements", "Bot Improvements")
	
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.PREFER_SALTZBOT, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.STAY_CLOSER, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.NO_CHASE_RATLING, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.NO_CHASE_GLOBADIER, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.NO_SEEK_COVER, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.BETTER_MELEE, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.FASTER_AIM, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.MANUAL_HEAL, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.GIVE_POTIONS, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.GIVE_GRENADES, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.GIVE_DRAUGHTS, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.LOOT_GRIMOIRES, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.LOOT_TOMES, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.LOOT_POTIONS, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.LOOT_GRENADES, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.KEEP_TOMES, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.PING_STORMVERMINS, true)
	Mods.option_menu:add_item("bot_improvements", me.SETTINGS.FIX_REVIVE, true)
end
 
-- ####################################################################################################################
-- ##### Implementation ###############################################################################################
-- ####################################################################################################################
--[[
	Prioritize Saltzpyre bot over Sienna when choosing which bots will be spawned. By Walterr.
 
	Only one line of this function is modified, but I have to replace the whole thing ...
--]]
SpawnManager._update_available_profiles = function (self, profile_synchronizer, available_profile_order, available_profiles)
	local delta = 0
	local bots = 0
	local humans = 0
 
	-- This is the modified line (1=WH, 2=BW, 3=DR, 4=WE, 5=ES).
	local profile_indexes = (get(me.SETTINGS.PREFER_SALTZBOT.save) and {2,1,3,4,5}) or {1,2,3,4,5}
	for _,profile_index in ipairs(profile_indexes) do
		local owner_type = profile_synchronizer.owner_type(profile_synchronizer, profile_index)
 
		if owner_type == "human" then
			humans = humans + 1
			local index = table.find(available_profile_order, profile_index)
 
			if index then
				table.remove(available_profile_order, index)
 
				available_profiles[profile_index] = false
				delta = delta - 1
			end
		elseif (owner_type == "available" or owner_type == "bot") and not table.contains(available_profile_order, profile_index) then
			table.insert(available_profile_order, 1, profile_index)
 
			available_profiles[profile_index] = true
			delta = delta + 1
		end
 
		if owner_type == "bot" then
			bots = bots + 1
		end
	end
 
	if self._forced_bot_profile_index then
		local forced_bot_profile_index = self._forced_bot_profile_index
		local index = table.find(available_profile_order, forced_bot_profile_index)
		local available = (index and profile_synchronizer.owner_type(profile_synchronizer, forced_bot_profile_index) == "available") or false
 
		fassert(available, "Bot profile (%s) is not available!", SPProfilesAbbreviation[forced_bot_profile_index])
 
		if index ~= 1 then
			available_profile_order[index] = available_profile_order[1]
			available_profile_order[1] = available_profile_order[index]
		end
	end
 
	return delta, humans, bots
end
 
--[[
	Will make the bots stick closer to players, especially during swarms. By Walterr.
--]]
Mods.hook.set(mod_name, "AISystem.update_brains", function (func, self, ...)
	local result = func(self, ...)
	
	if get(me.SETTINGS.STAY_CLOSER.save) then
		local fight_melee = BotActions.default.fight_melee
		local aggro_level = (self.number_ordinary_aggroed_enemies * 2 / 3)
		fight_melee.engage_range_near_follow_pos = math.max(3, 15 - aggro_level)
		fight_melee.engage_range = math.max(2, 10 - aggro_level)
	end
	
	return result
end)
 
Mods.hook.set(mod_name, "PlayerBotBase._enemy_path_allowed", function (func, self, enemy_unit)
	local breed = Unit.get_data(enemy_unit, "breed")
	
	if (breed == Breeds.skaven_ratling_gunner and get(me.SETTINGS.NO_CHASE_RATLING.save)) or
			(breed == Breeds.skaven_poison_wind_globadier and get(me.SETTINGS.NO_CHASE_GLOBADIER.save)) then
		return false
	end
	
	return func(self, enemy_unit)
end)
 
Mods.hook.set(mod_name, "PlayerBotBase._in_line_of_fire", function(func, ...)
	if get(me.SETTINGS.NO_SEEK_COVER.save) then
		return false, false
	end
	
	return func(...)
end)
 
--[[
	Allow bots to trade potions to human players who currently have none. By Walterr.
	Allow bots to trade bombs to human players who currently have none. Patch 1.5+ only. By Grimalackt.
	Allow bots to trade healing draughts to human players or other bots who are low health/wounded and have no healing item. By Grimalackt.
--]]
BotActions.default.switch_to_potion = {
	name = "switch_to_potion",
	wanted_slot = "slot_potion",
	action_weight = 1
}
BotActions.default.switch_to_grenade = {
	name = "switch_to_grenade",
	wanted_slot = "slot_grenade",
	action_weight = 1
}
BotActions.default.switch_to_healing = {
	name = "switch_to_healing",
	wanted_slot = "slot_healthkit",
	action_weight = 1
}
BotActions.default.give_potion_to_human = {
	name = "give_potion_to_human",
	aim_node = "j_hips"
}
BotActions.default.give_grenade_to_human = {
	name = "give_grenade_to_human",
	aim_node = "j_hips"
}
BotActions.default.give_healing_kit_to_human = {
	name = "give_healing_kit_to_human",
	aim_node = "j_hips"
}
 
local check_inventory_state = function (unit, is_giving, slot_name)
	local inventory_ext = ScriptUnit.extension(unit, "inventory_system")
	local potion_slot_data = inventory_ext:get_slot_data(slot_name)
	local template = potion_slot_data and inventory_ext:get_item_template(potion_slot_data)
	return (is_giving and template and template.can_give_other) or not (is_giving or template)
end

local wasjusttraded = nil
local check_ally_available = function (ally_unit, blackboard)
	if not ally_unit then
		return false
	end

	local network_manager = Managers.state.network
	local self_unit = blackboard.unit
	local self_unit_id = network_manager.unit_game_object_id(network_manager, self_unit)
 
	if 5 < blackboard.ally_distance and self_unit_id == wasjusttraded then
		wasjusttraded = nil
	end
	if 4 < blackboard.ally_distance or self_unit_id == wasjusttraded then
		return false
	end
 
	local owner_player = Managers.player:owner(ally_unit)
	local target_is_bot = owner_player and owner_player.bot_player
	if target_is_bot then
		return false
	end

	for _, enemy_unit in pairs(blackboard.proximite_enemies) do
		if Unit.alive(enemy_unit) and Unit.get_data(enemy_unit, "blackboard").target_unit == self_unit then
			return false
		end
	end
 
	return true
end
Mods.hook.set(mod_name, "InventorySystem.rpc_give_equipment", function(func, self, sender, game_object_id, slot_id, item_name_id, position)
	func(self, sender, game_object_id, slot_id, item_name_id, position)
	local unit = self.unit_storage:unit(game_object_id)
	
	if Unit.alive(unit) and not ScriptUnit.extension(unit, "status_system"):is_dead() then
		local owner = Managers.player:owner(unit)
		if not owner.remote and owner.bot_player then
			local network_manager = Managers.state.network
			wasjusttraded = network_manager.unit_game_object_id(network_manager, unit)
		end
	end
end)
 
BTConditions.can_give_potion_to_human = function (blackboard)
	if not get(me.SETTINGS.GIVE_POTIONS.save) then
		return false
	end
 
	local ally_unit = blackboard.target_ally_unit
	if not check_ally_available(ally_unit, blackboard) then
		return false
	end
 
	if not (check_inventory_state(blackboard.unit, true, "slot_potion") and
			check_inventory_state(ally_unit, false, "slot_potion")) then
		return false
	end
 
	blackboard.interaction_unit = ally_unit
	return true
end
 
BTConditions.can_give_grenade_to_human = function (blackboard)
	if not get(me.SETTINGS.GIVE_GRENADES.save) then
		return false
	end
 
	local ally_unit = blackboard.target_ally_unit
	if not check_ally_available(ally_unit, blackboard) then
		return false
	end
 
	if not (check_inventory_state(blackboard.unit, true, "slot_grenade") and
			check_inventory_state(ally_unit, false, "slot_grenade")) then
		return false
	end
 
	blackboard.interaction_unit = ally_unit
	return true
end
BTConditions.can_give_healing_to_human = function (blackboard)
	if not get(me.SETTINGS.GIVE_DRAUGHTS.save) then
		return false
	end
	-- Avoids possible crash
	if blackboard.unit == nil then
		return false
	end
	local ally_unit = nil
	for i, player in pairs(Managers.player:human_and_bot_players()) do
		if Unit.alive(player.player_unit) then
			local health_extension = ScriptUnit.extension(player.player_unit, "health_system")
			local status_extension = ScriptUnit.extension(player.player_unit, "status_system")
			local current_health = 1		
			if not status_extension.is_knocked_down(status_extension) then
				current_health = health_extension.current_health_percent(health_extension)
			end
			
			if current_health < 0.5 or status_extension.wounded then
				ally_unit = player.player_unit
			end
		end
	end
	if ally_unit then
		local ally_posn = POSITION_LOOKUP[ally_unit]
		local self_unit = blackboard.unit
		if ally_posn and (4.0 < Vector3.distance(POSITION_LOOKUP[self_unit], ally_posn)) then
			return false
		end
		for _, enemy_unit in pairs(blackboard.proximite_enemies) do
			if Unit.alive(enemy_unit) and Unit.get_data(enemy_unit, "blackboard").target_unit == self_unit then
				return false
			end
		end
		if not (check_inventory_state(self_unit, true, "slot_healthkit") and
				check_inventory_state(ally_unit, false, "slot_healthkit")) then
			return false
		end
		if Unit.alive(ally_unit) then
			local health_extension = ScriptUnit.extension(ally_unit, "health_system")
			local status_extension = ScriptUnit.extension(ally_unit, "status_system")
			local current_health = 1		
			if status_extension.is_knocked_down(status_extension) then
				return false
			else
				current_health = health_extension.current_health_percent(health_extension)
			end
			if current_health > 0.5 and not status_extension.wounded then
				return false
			end 
		end
		blackboard.interaction_unit = ally_unit
		
		return true
	end
 
	return false
end
 
local give_potion_action = {
	"BTSequence",
	{
		"BTBotInventorySwitchAction",
		name = "switch_to_potion",
		action_data = BotActions.default.switch_to_potion
	},
	{
		"BTBotInteractAction",
		name = "give_potion_to_human",
		action_data = BotActions.default.give_potion_to_human
	},
	name = "give_potion_to_human_sequence",
	condition = "can_give_potion_to_human"
}
 
local give_grenade_action = {
	"BTSequence",
	{
		"BTBotInventorySwitchAction",
		name = "switch_to_grenade",
		action_data = BotActions.default.switch_to_grenade
	},
	{
		"BTBotInteractAction",
		name = "give_grenade_to_human",
		action_data = BotActions.default.give_grenade_to_human
	},
	name = "give_grenade_to_human_sequence",
	condition = "can_give_grenade_to_human"
}
local give_healing_action = {
	"BTSequence",
	{
		"BTBotInventorySwitchAction",
		name = "switch_healing_kit",
		action_data = BotActions.default.switch_to_healing
	},
	{
		"BTBotInteractAction",
		name = "give_healing_kit_to_human",
		action_data = BotActions.default.give_healing_kit_to_human
	},
	name = "give_healing_to_human_sequence",
	condition = "can_give_healing_to_human"
}
 
local insert_bt_node = function(lua_node)
	local lua_tree = BotBehaviors.default
	for i = 1, math.huge, 1 do
		if not lua_tree[i] then
			EchoConsole("ERROR: insertion point not found")
			return
		elseif lua_tree[i].name == lua_node.name then
			--EchoConsole("ERROR: bt node " .. lua_node.name .. " already inserted")
			return
		elseif lua_tree[i].name == "in_combat" then
			table.insert(lua_tree, i, lua_node)
			return
		end
	end
end
insert_bt_node(give_potion_action)
insert_bt_node(give_grenade_action)
insert_bt_node(give_healing_action)
 
--[[
	Improve bot melee behaviour. Glaive equipped Kerillian especially. By Grimalackt.
--]]
Mods.hook.set(mod_name, "BTBotMeleeAction._attack", function(func, self, unit, blackboard, input_ext, target_unit)
	if not get(me.SETTINGS.BETTER_MELEE.save) then
		return func(self, unit, blackboard, input_ext, target_unit)
	end
 
	local num_enemies = #blackboard.proximite_enemies
	local outnumbered = 1 < num_enemies
	local massively_outnumbered = 3 < num_enemies
	local target_breed = Unit.get_data(target_unit, "breed")
	local target_armor = (target_breed and target_breed.armor_category) or 1
	local inventory_ext = blackboard.inventory_extension
	local wielded_slot_name = inventory_ext.get_wielded_slot_name(inventory_ext)
	local slot_data = inventory_ext.get_slot_data(inventory_ext, wielded_slot_name)
	local item_data = slot_data.item_data
	local item_template = BackendUtils.get_item_template(item_data)
	local DEFAULT_ATTACK_META_DATA = {
		tap_attack = {
			penetrating = false,
			arc = 0
		},
		hold_attack = {
			penetrating = true,
			arc = 2
		}
	}
	local weapon_meta_data = item_template.attack_meta_data or DEFAULT_ATTACK_META_DATA
	local best_utility = -1
	local best_attack_input = nil
 
	for attack_input, attack_meta_data in pairs(weapon_meta_data) do
		local utility = 0
 
		if attack_meta_data.no_damage and massively_outnumbered and 1 < attack_meta_data.arc then
			utility = utility + 2
		elseif not attack_meta_data.no_damage and ((outnumbered and 1 < attack_meta_data.arc) or (not outnumbered and attack_meta_data.arc == 0)) then
			utility = utility + 4
		end
 
		if target_armor ~= 2 or attack_meta_data.penetrating then
			utility = utility + 8
		end
 
		if best_utility < utility then
			best_utility = utility
			best_attack_input = attack_input
		end
	end
 
	input_ext[best_attack_input](input_ext)
	return
end)

Mods.hook.set(mod_name, "BTBotMeleeAction._defend", function(func, self, unit, target_unit, input_ext, t)
	if self._is_attacking_me(self, unit, target_unit) then
		if (Unit.get_data(target_unit, "breed") and
			Unit.get_data(target_unit, "breed").name == "skaven_rat_ogre") or not get(me.SETTINGS.BETTER_MELEE.save) then
			input_ext.defend(input_ext)
		else
			input_ext.melee_push(input_ext)
		end
		return true
	else
		return false
	end

	return 
end)
 
--[[
	Drastically improves bots' aiming speed and will to fire. By Grimalackt.
--]]
Mods.hook.set(mod_name, "BTBotShootAction._calculate_aim_speed",
function(func, self, self_unit, dt, current_yaw, current_pitch, wanted_yaw, wanted_pitch, current_yaw_speed, current_pitch_speed)
	if not get(me.SETTINGS.FASTER_AIM.save) then
		return func(self, self_unit, dt, current_yaw, current_pitch, wanted_yaw, wanted_pitch, current_yaw_speed, current_pitch_speed)
	end
 
	local pi = math.pi
	local yaw_offset = (wanted_yaw - current_yaw + pi)%(pi*2) - pi
	local pitch_offset = wanted_pitch - current_pitch
	local yaw_offset_sign = math.sign(yaw_offset)
	local yaw_speed_sign = math.sign(current_yaw_speed)
	local has_overshot = yaw_speed_sign ~= 0 and yaw_offset_sign ~= yaw_speed_sign
	local wanted_yaw_speed = yaw_offset*math.pi*10
	local new_yaw_speed = nil
	local acceleration = 30
	local deceleration = 40
 
	if has_overshot and 0 < yaw_offset_sign then
		new_yaw_speed = math.min(current_yaw_speed + deceleration*dt, 0)
	elseif has_overshot then
		new_yaw_speed = math.max(current_yaw_speed - deceleration*dt, 0)
	elseif 0 < yaw_offset_sign then
		if current_yaw_speed <= wanted_yaw_speed then
			new_yaw_speed = math.min(current_yaw_speed + acceleration*dt, wanted_yaw_speed)
		else
			new_yaw_speed = math.max(current_yaw_speed - deceleration*dt, wanted_yaw_speed)
		end
	elseif wanted_yaw_speed <= current_yaw_speed then
		new_yaw_speed = math.max(current_yaw_speed - acceleration*dt, wanted_yaw_speed)
	else
		new_yaw_speed = math.min(current_yaw_speed + deceleration*dt, wanted_yaw_speed)
	end
 
	local lerped_pitch_speed = pitch_offset/dt
 
	return new_yaw_speed, lerped_pitch_speed
end)
Mods.hook.set(mod_name, "BTBotShootAction._aim_good_enough", function(func, self, dt, t, shoot_blackboard, yaw_offset, pitch_offset)
	if not get(me.SETTINGS.FASTER_AIM.save) then
		return func(self, dt, t, shoot_blackboard, yaw_offset, pitch_offset)
	end
	local bb = shoot_blackboard
	if not bb.reevaluate_aim_time then
		bb.reevaluate_aim_time = 0
	end
	local aim_data = bb.aim_data
	if bb.reevaluate_aim_time < t then
		local offset = math.sqrt(pitch_offset*pitch_offset + yaw_offset*yaw_offset)
		if (aim_data.max_radius / 50) < offset then
			bb.aim_good_enough = false
			dprint("bad aim - offset:", offset)
		else
			local success = nil
			local num_rolls = bb.num_aim_rolls + 1
			if offset < aim_data.min_radius then
				success = Math.random() < aim_data.min_radius_pseudo_random_c*num_rolls
			else
				local prob = math.auto_lerp(aim_data.min_radius, aim_data.max_radius, aim_data.min_radius_pseudo_random_c, aim_data.max_radius_pseudo_random_c, offset)*num_rolls
				success = Math.random() < prob
			end
			
			success = true
			if success then
				bb.aim_good_enough = true
				bb.num_aim_rolls = 0
				dprint("fire! - offset:", offset, " num_rolls:", num_rolls)
			else
				bb.aim_good_enough = false
				bb.num_aim_rolls = num_rolls
				dprint("not yet - offset:", offset, " num_rolls:", num_rolls)
			end
		end
		bb.reevaluate_aim_time = t + 0.1
	end
	return bb.aim_good_enough
end)
Mods.hook.set(mod_name, "BTBotShootAction._is_shot_obstructed", function(func, self, physics_world, from, direction, self_unit, target_unit, actual_aim_position, collision_filter)
	if not get(me.SETTINGS.FASTER_AIM.save) then
		return func(self, physics_world, from, direction, self_unit, target_unit, actual_aim_position, collision_filter)
	end	
	local INDEX_POSITION = 1
	local INDEX_DISTANCE = 2
	local INDEX_NORMAL = 3
	local INDEX_ACTOR = 4
	local max_distance = Vector3.length(actual_aim_position - from)
	PhysicsWorld.prepare_actors_for_raycast(physics_world, from, direction, 0.01, 0.5, max_distance*max_distance)
	local raycast_hits = PhysicsWorld.immediate_raycast(physics_world, from, direction, max_distance, "all", "collision_filter", collision_filter)
	if not raycast_hits then
		return false
	end
	local num_hits = #raycast_hits
	for i = 1, num_hits, 1 do
		local hit = raycast_hits[i]
		local hit_actor = hit[INDEX_ACTOR]
		local hit_unit = Actor.unit(hit_actor)
		if hit_unit == target_unit then
			return false
		elseif hit_unit ~= self_unit then
			local obstructed_by_static = Actor.is_static(hit_actor)
			if script_data.debug_unit == self_unit and script_data.debug_bot_obstruction then
				QuickDrawerStay:line(from, hit[INDEX_POSITION])
				QuickDrawerStay:sphere(hit[INDEX_POSITION], 0.05, Color(255, 0, 0))
			end
			if obstructed_by_static then
				return obstructed_by_static, max_distance - hit[INDEX_DISTANCE], obstructed_by_static
			end
		end
	end
	return false
end)
 
--[[
	Allow bots to loot pinged items. By Walterr, IamLupo and Grimalackt.
--]]
local pinged_grim = nil
local pinged_tome = nil
local pinged_potion = nil
local pinged_grenade = nil
local pinged_heal = nil
local pinged_storm = nil
 
Mods.hook.set(mod_name, "PingTargetExtension.set_pinged", function (func, self, pinged)
	if pinged then
		local pickup_extension = ScriptUnit.has_extension(self._unit, "pickup_system")
		local pickup_settings = pickup_extension and pickup_extension:get_pickup_settings()
		local is_inventory_item = pickup_settings and (pickup_settings.type == "inventory_item")
		local health_extension = ScriptUnit.has_extension(self._unit, "health_system") and 
			Unit.get_data(self._unit, "breed") and 
			(Unit.get_data(self._unit, "breed").name == "skaven_storm_vermin_commander" 
			or Unit.get_data(self._unit, "breed").name == "skaven_storm_vermin") and 
			ScriptUnit.extension(self._unit, "health_system")
 
		if is_inventory_item and (pickup_settings.item_name == "wpn_grimoire_01") and
				get(me.SETTINGS.LOOT_GRIMOIRES.save) then
			pinged_grim = self._unit
		else
			pinged_grim = nil
		end
 
		if is_inventory_item and (pickup_settings.item_name == "wpn_side_objective_tome_01") and
				get(me.SETTINGS.LOOT_TOMES.save) then
			pinged_tome = self._unit
		else
			pinged_tome = nil
		end
 
		if is_inventory_item and (pickup_settings.item_name == "potion_speed_boost_01" or
				pickup_settings.item_name == "potion_damage_boost_01") and
				get(me.SETTINGS.LOOT_POTIONS.save) then
			pinged_potion = self._unit
		else
			pinged_potion = nil
		end
 
		if is_inventory_item and (pickup_settings.item_name == "grenade_frag_01" or
				pickup_settings.item_name == "grenade_frag_02" or
				pickup_settings.item_name == "grenade_fire_01" or
				pickup_settings.item_name == "grenade_fire_02") and
				get(me.SETTINGS.LOOT_GRENADES.save) then
			pinged_grenade = self._unit
		else
			pinged_grenade = nil
		end
		
		if is_inventory_item and (pickup_settings.item_name == "potion_healing_draught_01" or
				pickup_settings.item_name == "healthkit_first_aid_kit_01") and
				get(me.SETTINGS.KEEP_TOMES.save) then
			pinged_heal = self._unit
		else
			pinged_heal = nil
		end

		if health_extension then
			pinged_storm = self._unit
		end
 
	elseif self._unit == pinged_grim then
		pinged_grim = nil
	elseif self._unit == pinged_tome then
		pinged_tome = nil
	elseif self._unit == pinged_potion then
		pinged_potion = nil
	elseif self._unit == pinged_grenade then
		pinged_grenade = nil
	elseif self._unit == pinged_heal then
		pinged_heal = nil
	elseif self._unit == pinged_storm then
		pinged_storm = nil
	end
 
	return func(self, pinged)
end)
 
local can_loot_grim = function (self_unit)
	-- Check distance to grim
	local grim_posn = POSITION_LOOKUP[pinged_grim]
	return grim_posn and (Vector3.distance(POSITION_LOOKUP[self_unit], grim_posn) < 20.0)
end
 
local can_loot_tome = function (self_unit)
	local inventory_ext = ScriptUnit.extension(self_unit, "inventory_system")
	local health_slot_data = inventory_ext.get_slot_data(inventory_ext, "slot_healthkit")
 
	-- Check if bot already has a tome
	if health_slot_data ~= nil then
		local item = inventory_ext.get_item_template(inventory_ext, health_slot_data)
		if item.name == "wpn_side_objective_tome_01" then
			return false
		end
	end
 
	-- Check distance to tome
	local tome_posn = POSITION_LOOKUP[pinged_tome]
	return tome_posn and (Vector3.distance(POSITION_LOOKUP[self_unit], tome_posn) < 8.0)
end
 
local can_loot_potion = function (self_unit)
	-- Check if close-by players have free potion slot
	local potion_posn = POSITION_LOOKUP[pinged_potion]
	for i, player in pairs(Managers.player:human_players()) do
		if player.player_unit == nil then
			return false
		end
		local player_inventory_ext = ScriptUnit.extension(player.player_unit, "inventory_system")
		if not player_inventory_ext.get_slot_data(player_inventory_ext, "slot_potion") and potion_posn and (25.0 > Vector3.distance(POSITION_LOOKUP[player.player_unit], potion_posn)) then
			return false
		end
	end
 
	-- Check if bot already has a potion or grimoire
	local inventory_ext = ScriptUnit.extension(self_unit, "inventory_system")
	if inventory_ext.get_slot_data(inventory_ext, "slot_potion") then
		return false
	end
 
	-- Check distance to potion
	return potion_posn and (Vector3.distance(POSITION_LOOKUP[self_unit], potion_posn) < 8.0)
end
 
local can_loot_grenade = function (self_unit)
	-- Check if close-by players have free grenade slot
	local grenade_posn = POSITION_LOOKUP[pinged_grenade]
	for i, player in pairs(Managers.player:human_players()) do
		if player.player_unit == nil then
			return false
		end
		local player_inventory_ext = ScriptUnit.extension(player.player_unit, "inventory_system")
		if not player_inventory_ext.get_slot_data(player_inventory_ext, "slot_grenade") and grenade_posn and (25.0 > Vector3.distance(POSITION_LOOKUP[player.player_unit], grenade_posn)) then
			return false
		end
	end
 
	-- Check if bot already has a grenade
	local inventory_ext = ScriptUnit.extension(self_unit, "inventory_system")
	if inventory_ext.get_slot_data(inventory_ext, "slot_grenade") then
		return false
	end
 
	-- Check distance to grenade
	return grenade_posn and (Vector3.distance(POSITION_LOOKUP[self_unit], grenade_posn) < 8.0)
end
 
BTConditions.can_loot_pinged_item = function (blackboard)
	-- Avoids possible crash
	local self_unit = blackboard.unit
	if self_unit == nil then
		return false
	end
 
	-- Test whether we can loot a pinged item
	local lootable_item = nil
	if pinged_grim and can_loot_grim(self_unit) then
		lootable_item = pinged_grim
	elseif pinged_tome and can_loot_tome(self_unit) then
		lootable_item = pinged_tome
	elseif pinged_potion and can_loot_potion(self_unit) then
		lootable_item = pinged_potion
	elseif pinged_grenade and can_loot_grenade(self_unit) then
		lootable_item = pinged_grenade
	else
		return false
	end
 
	-- Check if bot is being attacked
	for _, enemy_unit in pairs(blackboard.proximite_enemies) do
		if Unit.alive(enemy_unit) and Unit.get_data(enemy_unit, "blackboard").target_unit == self_unit then
			return false
		end
	end
 
	blackboard.interaction_unit = lootable_item
	return true
end
 
local loot_pinged_item_action = {
	"BTBotInteractAction",
	condition = "can_loot_pinged_item",
	name = "loot_pinged_item"
}
insert_bt_node(loot_pinged_item_action)
 
--[[
	Allow bots to ping stormvermins attacking them. By Grimalackt.
--]]
BTConditions.can_ping_storm = function (blackboard)
	if not get(me.SETTINGS.PING_STORMVERMINS.save) then
		return false
	end
	-- Avoids possible crash
	if blackboard.unit == nil then
		return false
	end
	local self_unit = blackboard.unit
	if pinged_storm then
		local health_extension = ScriptUnit.has_extension(pinged_storm, "health_system") and ScriptUnit.extension(pinged_storm, "health_system")
		
		if health_extension and health_extension.current_health_percent(health_extension) <= 0 then
			pinged_storm = nil
		end
	end
		
	for _, enemy_unit in pairs(blackboard.proximite_enemies) do
		if Unit.alive(enemy_unit) and Unit.get_data(enemy_unit, "blackboard").target_unit == self_unit and 
			(Unit.get_data(enemy_unit, "breed").name == "skaven_storm_vermin_commander" or Unit.get_data(enemy_unit, "breed").name == "skaven_storm_vermin") and 
			ScriptUnit.extension(enemy_unit, "health_system").current_health_percent(ScriptUnit.extension(enemy_unit, "health_system")) > 0 and not pinged_storm then
			blackboard.interaction_unit = enemy_unit
			local network_manager = Managers.state.network
			local self_unit_id = network_manager.unit_game_object_id(network_manager, self_unit)
			local enemy_unit_id = network_manager.unit_game_object_id(network_manager, enemy_unit)
			network_manager.network_transmit:send_rpc_server("rpc_ping_unit", self_unit_id, enemy_unit_id)
			return true
		end
	end
	
	return false
end
local ping_stormvermin_action = {
	"BTBotInteractAction",
	condition = "can_ping_storm",
	name = "ping_stormvermin"
}
insert_bt_node(ping_stormvermin_action)
--[[
	This is literally a bugfix of Fatshark's code. Bots will no longer refuse to revive you if targeted by a stormvermin. By Grimalackt.
--]]
local function can_interact_with_ally(self_unit, target_ally_unit)
	local interactable_extension = ScriptUnit.extension(target_ally_unit, "interactable_system")
	local interactor_unit = interactable_extension.is_being_interacted_with(interactable_extension)
	local can_interact_with_ally = interactor_unit == nil or interactor_unit == self_unit

	return can_interact_with_ally
end

local function is_there_threat_to_aid(self_unit, proximite_enemies, force_aid)
	for _, enemy_unit in pairs(proximite_enemies) do
		if Unit.alive(enemy_unit) then
			local enemy_blackboard = Unit.get_data(enemy_unit, "blackboard")
			local enemy_breed = enemy_blackboard.breed

			if enemy_blackboard.target_unit == self_unit and (not force_aid or enemy_breed.is_bot_aid_threat) then
				return true
			end
		end
	end

	return false
end

BTConditions.can_revive = function (blackboard)
	if blackboard.target_ally_need_type == "knocked_down" then
		local ally_distance = blackboard.ally_distance

		if 2.25 < ally_distance then
			return false
		end

		local self_unit = blackboard.unit
		local target_ally_unit = blackboard.target_ally_unit
		local health = ScriptUnit.extension(target_ally_unit, "health_system"):current_health_percent()

		if 0.2 < health and is_there_threat_to_aid(self_unit, blackboard.proximite_enemies, blackboard.force_aid) and not get(me.SETTINGS.FIX_REVIVE.save) then
			return false
		end

		local destination_reached = blackboard.navigation_extension:destination_reached()
		local can_interact_with_ally = can_interact_with_ally(self_unit, target_ally_unit)

		if can_interact_with_ally and (destination_reached or ally_distance < 1.75) then
			return true
		end
	end

	return 
end
 
--[[
	Allow manual control of bot use of healing items. By Walterr.
--]]
local keyidx_0 = Keyboard.button_index("numpad 0")
local keyidx_1 = Keyboard.button_index("numpad 1")
local keyidx_2 = Keyboard.button_index("numpad 2")
local keyidx_3 = Keyboard.button_index("numpad 3")
 
local get_selected_player = function ()
	local selected_player_id = (((Keyboard.button(keyidx_0) > 0.5) and 0) or ((Keyboard.button(keyidx_1) > 0.5) and 1) or
		((Keyboard.button(keyidx_2) > 0.5) and 2) or ((Keyboard.button(keyidx_3) > 0.5) and 3))
	if not selected_player_id then
		return nil
	end
 
	local players = Managers.player:players()
	local i = 1
	for _, player in pairs(players) do
		if player.remote or player.bot_player then
			if i == selected_player_id then
				return player
			end
			i = i + 1
		elseif 0 == selected_player_id then
			return player
		end
	end
	EchoConsole("Error: selected player not found: " .. tostring(selected_player_id))
	return nil
end
 
local can_perform_heal = function (unit, heal_self)
		local inventory_ext = ScriptUnit.extension(unit, "inventory_system")
		local health_slot_data = inventory_ext:get_slot_data("slot_healthkit")
		if health_slot_data then
			local item_template = inventory_ext:get_item_template(health_slot_data)
			return (heal_self and item_template.can_heal_self) or ((not heal_self) and item_template.can_heal_other)
		end
		return false
end
 
local bot_heal_info = { player_unit = nil, healing_self = false }
 
Mods.hook.set(mod_name, "BTConditions.bot_should_heal", function (func, blackboard)
	local heal_option = get(me.SETTINGS.MANUAL_HEAL.save)
	
	if MANUAL_HEAL_MANDATORY ~= heal_option then
		local result = func(blackboard)
		if MANUAL_HEAL_DISABLED == heal_option or (result and not bot_heal_info.player_unit) then
			return result
		end
	end
	
	return bot_heal_info.healing_self and (bot_heal_info.player_unit == blackboard.unit)
end)
 
Mods.hook.set(mod_name, "PlayerBotBase._select_ally_by_utility", function(func, self, unit, blackboard, breed, t)
	local heal_option = get(me.SETTINGS.MANUAL_HEAL.save)
	if MANUAL_HEAL_DISABLED == heal_option then
		return func(self, unit, blackboard, breed, t)
	end
 
	local selected_player = get_selected_player()
	local selected_unit = selected_player and selected_player.player_unit
	if (selected_unit == unit) and selected_player.bot_player and can_perform_heal(unit, true) then
		bot_heal_info.player_unit = unit
		bot_heal_info.healing_self = true
		return nil, math.huge, nil
 
	elseif selected_unit and ((bot_heal_info.player_unit == nil and selected_unit ~= unit) or bot_heal_info.player_unit == unit) and
			can_perform_heal(unit, false) and not (selected_player.bot_player and can_perform_heal(selected_unit, true)) then
		bot_heal_info.player_unit = unit
		bot_heal_info.healing_self = false
		return selected_unit, Vector3.distance(POSITION_LOOKUP[unit], POSITION_LOOKUP[selected_unit]), "in_need_of_heal"
 
	elseif bot_heal_info.player_unit == unit then
		bot_heal_info.player_unit = nil
		bot_heal_info.healing_self = false
	end
 
	local ally_unit, real_dist, in_need_type = func(self, unit, blackboard, breed, t)
	if ally_unit and ("in_need_of_heal" == in_need_type) and (MANUAL_HEAL_MANDATORY == heal_option or bot_heal_info.player_unit) then
		-- stop the bot from auto-healing someone.
		in_need_type = nil
 
		local ally_player = Managers.player:unit_owner(ally_unit)
		if ally_player.bot_player then
			ally_unit = nil
			real_dist = math.huge
 
			local human_players = Managers.player:human_players()
			for _, human_player in pairs(human_players) do
				local human_player_unit = human_player.player_unit
 
				if AiUtils.unit_alive(human_player_unit) then
					local allowed_follow_path, _ = self:_ally_path_allowed(human_player_unit, t)
					if allowed_follow_path then
						local dist = Vector3.distance(POSITION_LOOKUP[unit], POSITION_LOOKUP[human_player_unit])
						if dist < real_dist then
							ally_unit = human_player_unit
							real_dist = dist
						end
					end
				end
			end
		end
	end
	
	return ally_unit, real_dist, in_need_type
end)

--[[
	Stops bots from exchanging tomes for medical supplies, unless they are wounded enough to use them on the spot, or the item is pinged. By Grimalackt.
--]]
Mods.hook.set(mod_name, "AIBotGroupSystem._update_health_pickups", function(func, self, dt, t)
	func(self, dt, t)
	if not get(me.SETTINGS.KEEP_TOMES.save) then
		return
	end
	local need_next_bot = nil
	local need_next_bot_pickup = nil
	local valid_bots = 0
	for unit, _ in pairs(self._bot_ai_data) do
		local blackboard = Unit.get_data(unit, "blackboard")
		local health_extension = ScriptUnit.extension(unit, "health_system")
		local inventory_ext = ScriptUnit.extension(unit, "inventory_system")
		local health_slot_data = inventory_ext.get_slot_data(inventory_ext, "slot_healthkit")
 		local status_extension = ScriptUnit.extension(unit, "status_system")
		local current_health = 1
		local healthy = true		
			
		-- Check if bot already has a tome
		if health_slot_data ~= nil then
			local item = inventory_ext.get_item_template(inventory_ext, health_slot_data)
			if item.name == "wpn_side_objective_tome_01" and blackboard.allowed_to_take_health_pickup and not (blackboard.health_pickup == pinged_heal) then
				if not status_extension.is_knocked_down(status_extension) then
					current_health = health_extension.current_health_percent(health_extension)
				end
				local pickup_extension = ScriptUnit.extension(blackboard.health_pickup, "pickup_system")
				local pickup_settings = pickup_extension and pickup_extension:get_pickup_settings()		
				if (current_health <= 0.5 or status_extension.wounded) and (pickup_settings.item_name == "potion_healing_draught_01") then
					healthy = false
				elseif (current_health <= 0.2 or status_extension.wounded) and (pickup_settings.item_name == "healthkit_first_aid_kit_01") then
					healthy = false
				end
				if healthy then
					blackboard.allowed_to_take_health_pickup = false
					need_next_bot = unit
					need_next_bot_pickup = blackboard.health_pickup
					valid_bots = valid_bots + 1
				end
			end
		end
	end
	if need_next_bot and valid_bots < 3 then
		for unit, _ in pairs(self._bot_ai_data) do
			if unit ~= need_next_bot then
				local blackboard = Unit.get_data(unit, "blackboard")
				local health_extension = ScriptUnit.extension(unit, "health_system")
				local inventory_ext = ScriptUnit.extension(unit, "inventory_system")
				local health_slot_data = inventory_ext.get_slot_data(inventory_ext, "slot_healthkit")
 				local status_extension = ScriptUnit.extension(unit, "status_system")
				local current_health = 1
				local healthy = true		
					
				-- Check if bot already has a tome
				if health_slot_data == nil and Vector3.distance(POSITION_LOOKUP[unit], POSITION_LOOKUP[need_next_bot_pickup]) < 15 then
					blackboard.allowed_to_take_health_pickup = true
					blackboard.health_pickup = need_next_bot_pickup
					blackboard.health_dist = Vector3.distance(POSITION_LOOKUP[unit], POSITION_LOOKUP[need_next_bot_pickup])
					blackboard.health_pickup_valid_until = math.huge
					return
				end
				if health_slot_data ~= nil then
					local item = inventory_ext.get_item_template(inventory_ext, health_slot_data)
					if item.name == "wpn_side_objective_tome_01" and Vector3.distance(POSITION_LOOKUP[unit], POSITION_LOOKUP[need_next_bot_pickup]) < 15 then
						if not status_extension.is_knocked_down(status_extension) then
							current_health = health_extension.current_health_percent(health_extension)
						end
				
						local pickup_extension = ScriptUnit.extension(need_next_bot_pickup, "pickup_system")
						local pickup_settings = pickup_extension and pickup_extension:get_pickup_settings()			

						if (current_health <= 0.5 or status_extension.wounded) and (pickup_settings.item_name == "potion_healing_draught_01") then
							healthy = false
						elseif (current_health <= 0.2 or status_extension.wounded) and (pickup_settings.item_name == "healthkit_first_aid_kit_01") then
							healthy = false
						end
						if not healthy then
							blackboard.allowed_to_take_health_pickup = true
							blackboard.health_pickup = need_next_bot_pickup
							blackboard.health_dist = Vector3.distance(POSITION_LOOKUP[unit], POSITION_LOOKUP[need_next_bot_pickup])
							blackboard.health_pickup_valid_until = math.huge
							return
						end
					end
				end
			end
		end
	end
end)
 
-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()