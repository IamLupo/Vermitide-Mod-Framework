--[[
	Type in the chat: /killonebot
--]]

local args = {...}

if #args == 0 then
	for i, player in pairs(Managers.player:bots()) do 
		StatusUtils.set_dead_network(player.player_unit, true)
		
		EchoConsole("Killed one bots")
		
		return true
	end
	
	return true
else
	return false
end
