--[[
	Type in the chat: /killbots
--]]

local args = {...}

if #args == 0 then
	for i, player in pairs(Managers.player:bots()) do 
		StatusUtils.set_dead_network(player.player_unit, true)
	end
	
	EchoConsole("Killed all bots")
	
	return true
else
	return false
end
