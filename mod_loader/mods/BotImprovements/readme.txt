Fixes and improvements to bot behavior.
Features:
	Prioritize Saltzpyre bot over Sienna when choosing which bots will be spawned. By Walterr.
	Will make the bots stick closer to players, especially during swarms. By Walterr.
	Allow bots to trade potions to human players who currently have none. By Walterr.
	Allow bots to trade bombs to human players who currently have none. Patch 1.5+ only. By Grimalackt.
	Allow bots to trade healing draughts to human players or other bots who are low health/wounded and have no healing item. By Grimalackt.
	Improve bot melee behaviour. Glaive equipped Kerillian especially. By Grimalackt.
	Drastically improves bots' aiming speed and will to fire. By Grimalackt.
	Allow bots to loot pinged items. By Walterr, IamLupo and Grimalackt.
	Allow bots to ping stormvermins attacking them. By Grimalackt.
	Allow manual control of bot use of healing items. By Walterr.
See options in Mod Settings -> Bot Improvements