Adds keyboard shortcuts to teleport around.
Keyboard shortcuts (default):
	Ctrl + Page Up/Down - teleport up/down
	Ctrl + Shift + Mouse Left - teleport to crosshair
Go to Mod Settings -> Movement -> Teleportation to change shortcuts.