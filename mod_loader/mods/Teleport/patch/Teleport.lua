local mod_name = "Teleport"

local oi = OptionsInjector

Teleport = {
	SETTINGS = {	
		SHOW = {
			["save"] = "cb_movement_teleport",
			["widget_type"] = "checkbox",
			["text"] = "Teleportation",
			["default"] = false,
			["hide_options"] = {
				{
					false,
					mode = "hide",
					options = {
						"cb_movement_hotkey_teleport_up",
						"cb_movement_hotkey_teleport_up_modifiers",
						"cb_movement_hotkey_teleport_down",
						"cb_movement_hotkey_teleport_down_modifiers",
						"cb_movement_hotkey_teleport_aim",
						"cb_movement_hotkey_teleport_aim_modifiers",
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_movement_hotkey_teleport_up",
						"cb_movement_hotkey_teleport_up_modifiers",
						"cb_movement_hotkey_teleport_down",
						"cb_movement_hotkey_teleport_down_modifiers",
						"cb_movement_hotkey_teleport_aim",
						"cb_movement_hotkey_teleport_aim_modifiers",
					}
				},
			},
		},
		HK_TELE_UP = {
			["save"] = "cb_movement_hotkey_teleport_up",
			["widget_type"] = "keybind",
			["text"] = "Teleport Up",
			["default"] = {
				"page up",
				oi.key_modifiers.CTRL,
			},
			["exec"] = {"Teleport", "action/teleport_up"},
		},
		HK_TELE_DOWN = {
			["save"] = "cb_movement_hotkey_teleport_down",
			["widget_type"] = "keybind",
			["text"] = "Teleport Down",
			["default"] = {
				"page down",
				oi.key_modifiers.CTRL,
			},
			["exec"] = {"Teleport", "action/teleport_down"},
		},
		HK_TELE_AIM = {
			["save"] = "cb_movement_hotkey_teleport_aim",
			["widget_type"] = "keybind",
			["text"] = "Teleport Aimed",
			["default"] = {
				"mouse_left",
				oi.key_modifiers.CTRL_SHIFT,
			},
			["exec"] = {"Teleport", "action/teleport"},
		},
	},
}

Teleport.create_options = function()
	Mods.option_menu:add_item("movement", Teleport.SETTINGS.SHOW, true)
	Mods.option_menu:add_item("movement", Teleport.SETTINGS.HK_TELE_UP)
	Mods.option_menu:add_item("movement", Teleport.SETTINGS.HK_TELE_DOWN)
	Mods.option_menu:add_item("movement", Teleport.SETTINGS.HK_TELE_AIM)
end

Teleport.teleport = function(direction) 
	local player_unit = Managers.player:player_from_peer_id(Network.peer_id()).player_unit
	local pos = Unit.local_position(player_unit, 0) + direction
	local rot = Unit.local_rotation(player_unit, 0)
	local locomotion_extension = ScriptUnit.extension(player_unit, "locomotion_system")
	
	locomotion_extension.teleport_to(locomotion_extension, pos, rot)
end

Teleport.create_options()