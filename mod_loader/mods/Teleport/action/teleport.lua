local player = Managers.player:local_player(1)

local player_pos = Unit.local_position(player.player_unit, 0)

local camera_position = Managers.state.camera:camera_position(player.viewport_name)
local camera_rotation = Managers.state.camera:camera_rotation(player.viewport_name)
local camera_direction = Quaternion.forward(camera_rotation)

local world = Managers.world:world("level_world")
local physics_world = World.get_data(world, "physics_world")

local result = PhysicsWorld.immediate_raycast(
	physics_world,
	camera_position,
	camera_direction,
	500,
	"all",
	"collision_filter",
	"filter_ray_horde_spawn")

if result then
	local num_hits = #result

	for i = 1, num_hits, 1 do
		local hit = result[i]
		local hit_actor = hit[4]
		local hit_unit = Actor.unit(hit_actor)
		local attack_hit_self = hit_unit == player.player_unit

		if not attack_hit_self then
			Teleport.teleport(hit[1] - player_pos)
			
			break;
		end
	end
end