local me = Mods.ChatDeveloper

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

pcall(function()
	set(me.SETTINGS.ACTIVE.save, not get(me.SETTINGS.ACTIVE.save))
	save()
	
	if get(me.SETTINGS.ACTIVE) then
		EchoConsole("Chat Developer On")
	else
		EchoConsole("Chat Developer Off")
	end
end)