local mod_name = "ChatDeveloper"
--[[
	Chat Developer
		- Your name will have a different color in the chat.
--]]

local oi = OptionsInjector

Mods.ChatDeveloper = {
	SETTINGS = {
		ACTIVE = {
			["save"] = "cb_chat_developer",
			["widget_type"] = "stepper",
			["text"] = "Chat Developer",
			["tooltip"] =  "Chat Developer\n" ..
				"Toggle chat developer on / off.\n\n" ..
				"Makes your name yellow in the chat.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
			["hide_options"] = {
				{
					false,
					mode = "hide",
					options = {
						"cb_chat_developer_hotkey_toggle",
						"cb_chat_developer_hotkey_toggle_modifiers",
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_chat_developer_hotkey_toggle",
						"cb_chat_developer_hotkey_toggle_modifiers",
					}
				},
			},
		},
		HK_TOGGLE = {
			["save"] = "cb_chat_developer_hotkey_toggle",
			["widget_type"] = "keybind",
			["text"] = "Toggle",
			["default"] = {
				"d",
				oi.key_modifiers.CTRL_SHIFT,
			},
			["exec"] = {"ChatDeveloper", "action/chat_developer"},
		},
	},
}
local me = Mods.ChatDeveloper

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Options ######################################################################################################
-- ####################################################################################################################
Mods.ChatDeveloper.create_options = function()
	Mods.option_menu:add_group("system", "System")
	
	Mods.option_menu:add_item("system", me.SETTINGS.ACTIVE, true)
end

-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "SteamHelper.is_dev", function(func)
	if get(me.SETTINGS.ACTIVE.save) then
		return true
	else
		return func()
	end
end)

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()