Makes your name golden in chat.
To enable go to Mod Settings -> System -> Chat Developer or use Ctrl+Shift+D to toggle.
Off by default.