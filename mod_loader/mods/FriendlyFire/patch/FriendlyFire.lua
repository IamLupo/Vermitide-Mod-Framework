local mod_name = "FriendlyFire"

Mods.FriendlyFire = {
	SETTINGS = {
		FRIENDLY_FIRE = {
			["save"] = "cb_hud_friendly_fire",
			["widget_type"] = "stepper",
			["text"] = "Alternative Friendly Fire UI",
			["tooltip"] = "Alternative Friendly Fire UI\n" ..
				"Instead of a red screen flash and directional indicator when hit by friendly " ..
				"fire, show a red rectangle beneath the HUD area of the player who hit you.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
	},
}

local me = Mods.FriendlyFire

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

local state = {
	STARTING = 1,
	ONGOING = 2
}

-- ####################################################################################################################
-- ##### Option #######################################################################################################
-- ####################################################################################################################
Mods.FriendlyFire.create_options = function()
	Mods.option_menu:add_group("hud", "HUD")
	
	Mods.option_menu:add_item("hud", me.SETTINGS.FRIENDLY_FIRE, true)
end

-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "PlayerDamageExtension.add_damage",
function(func, self, attacker_unit, damage_amount, hit_zone_name, damage_type,
	damage_direction, damage_source_name, hit_ragdoll_actor, damaging_unit)
 
	if get(me.SETTINGS.FRIENDLY_FIRE.save) and DamageUtils.is_player_unit(attacker_unit) and attacker_unit ~= self.unit then
		-- this type of damage is ignored by the damage indicator (see ignored_damage_types in
		-- damage_indicator_gui.lua, and also in hit_reactions.lua).
		damage_type = "knockdown_bleed"
	end
	
	func(self, attacker_unit, damage_amount, hit_zone_name, damage_type,
		damage_direction, damage_source_name, hit_ragdoll_actor, damaging_unit)
end)

Mods.hook.set(mod_name, "UnitFramesHandler.update", function(func, self, dt, t, my_player)
	local player_unit = self.my_player.player_unit
	
	if player_unit then
		-- Check recent damages for FF.
		if get(me.SETTINGS.FRIENDLY_FIRE.save) then
			local damage_extension = ScriptUnit.extension(player_unit, "damage_system")
			local strided_array, array_length = damage_extension:recent_damages()
			
			if 0 < array_length then
				for i = 1, array_length/DamageDataIndex.STRIDE, 1 do
					local index = (i - 1) * DamageDataIndex.STRIDE
					local attacker = strided_array[index + DamageDataIndex.ATTACKER]
					local damage_type = strided_array[index + DamageDataIndex.DAMAGE_TYPE]
 
					-- If this damage is FF, find the HUD area of the attacking player and tell it to
					-- display the FF indicator.
					if damage_type == "knockdown_bleed" and DamageUtils.is_player_unit(attacker) and attacker ~= player_unit then
						for _, unit_frame in ipairs(self._unit_frames) do
							local team_member = unit_frame.player_data.player
							
							if team_member and team_member.player_unit == attacker then
								unit_frame.data._hudmod_ff_state = state.STARTING
							end
						end
					end
				end
			end
		end
	end
	
	return func(self, dt, t, my_player)
end)
 
Mods.hook.set(mod_name, "UnitFrameUI.draw", function(func, self, dt)
	local data = self.data
	
	if self._is_visible then
		local ui_renderer = self.ui_renderer
		local input_service = self.input_manager:get_service("ingame_menu")
		
		UIRenderer.begin_pass(ui_renderer, self.ui_scenegraph, input_service, dt, nil, self.render_settings)
		
		if data._hudmod_ff_state ~= nil then
			local widget = self._hudmod_ff_widget
			if not widget then
				-- This is the first FF from this player, create the indicator widget for his HUD area.
				local rect = UIWidgets.create_simple_rect("pivot", Colors.get_table("firebrick"))
				rect.style.rect.size = { 308, 132 }
				rect.style.rect.offset = { -50, -65 }
				widget = UIWidget.init(rect)
				self._hudmod_ff_widget = widget
			end
 
			if data._hudmod_ff_state == state.STARTING then
				-- New damage, restart the animation.
				UIWidget.animate(widget, UIAnimation.init(UIAnimation.function_by_time, widget.style.rect.color, 1, 255, 0, 1, math.easeInCubic))
				data._hudmod_ff_state = state.ONGOING
			end
 
			if UIWidget.has_animation(widget) then
				UIRenderer.draw_widget(ui_renderer, widget)
				self._dirty = true
			else
				-- Animation is finished, reset the FF state.
				data._hudmod_ff_state = nil
			end
		end
		
		UIRenderer.end_pass(ui_renderer)
	end
 
	return func(self, dt)
end)

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()