Combines Deathwish game mode with Stormvermin Mutation. Requires both mods to run.
Go to Mod Settings -> Mutators -> Game Mode and pick Deathwish + Stormvermin from the dropdown menu.
This game mode works correctly only on Cataclysm difficulty.