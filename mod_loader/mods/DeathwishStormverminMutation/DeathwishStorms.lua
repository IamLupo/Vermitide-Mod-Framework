local mutator_name = "Deathwish + Stormvermin"
local mutator_group = "GAME_MODE"
local difficulty_name = "||Deathwish + Stormvermin Mutation||"

DeathwishStorms = {}

DeathwishStorms.name = mutator_name
DeathwishStorms.group = mutator_group
DeathwishStorms.difficulty_name = difficulty_name
DeathwishStorms.saved = {}

DeathwishStorms.enable = function(self)
	Mods.hook.enable(true, Deathwish.name)
	Mods.hook.enable(true, Storms.name)
	Mods.mutators.replace_data(Deathwish)
	Mods.mutators.replace_data(Storms)
	Mods.mutators:set_servername(self.difficulty_name)
end

DeathwishStorms.disable = function(self)
	Mods.hook.enable(false, Storms.name)
	Mods.hook.enable(false, Deathwish.name)
	Mods.mutators.restore_data(Deathwish)
	Mods.mutators.restore_data(Storms)
	Mods.mutators:restore_servername()
end

DeathwishStorms.check_conditions = function()
	return Managers.player.is_server and Managers.state.difficulty:get_difficulty_rank() == 5
end

--Adding mutator to mutator manager
Mods.mutators:add_mutator(mutator_group, DeathwishStorms)