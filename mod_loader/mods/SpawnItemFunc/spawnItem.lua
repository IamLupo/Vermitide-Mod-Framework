
Mods.spawnItem = function(pickup_name)
	local local_player_unit = Managers.player:player_from_peer_id(Network.peer_id()).player_unit

	Managers.state.network.network_transmit:send_rpc_server(
		'rpc_spawn_pickup_with_physics',
		NetworkLookup.pickup_names[pickup_name],
		Unit.local_position(local_player_unit, 0),
		Unit.local_rotation(local_player_unit, 0),
		NetworkLookup.pickup_spawn_types['dropped']
	)
end
