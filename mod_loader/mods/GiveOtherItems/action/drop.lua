local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

local player_unit = Managers.player:player_from_peer_id(Network.peer_id()).player_unit
local inventory_extension = ScriptUnit.extension(player_unit, "inventory_system")
local slot_name = inventory_extension.get_wielded_slot_name(inventory_extension)

if slot_name == "slot_healthkit" or slot_name == "slot_potion"  or slot_name == "slot_grenade" then
	local item_template = inventory_extension.get_wielded_slot_item_template(inventory_extension)
	local item_name = ""
	
	if item_template.pickup_data and item_template.pickup_data.pickup_name then
		item_name = item_template.pickup_data.pickup_name
	elseif item_template.is_grimoire then
		item_name = "grimoire"
	end
	
	-- Remove item
	inventory_extension.destroy_slot(inventory_extension, slot_name)
	
	-- Switch to melee weapon
	inventory_extension.wield(inventory_extension, "slot_melee")
	
	-- Spawn item
	Mods.spawnItem(item_name)
	
	-- Feedback
	if not get(ModSettings.SETTINGS.HK_FEEDBACK.save) then
		EchoConsole("Dropped: " .. item_name)
	end
end