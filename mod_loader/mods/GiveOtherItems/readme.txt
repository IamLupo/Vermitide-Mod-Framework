Allows all items except for the grim to be given to other players.
The grim is missing an attribute which crashes the game for players who are not using the mod.
Also lets you drop items to the ground.
Dropping items requires SpawnItemFunc mod to function.
Go to Mod Settings -> Items -> Give Other Items to specify which items can be given.
Off by default.
Keyboard shortcuts (default):
	Alt + right mouse button - drop item
Go to Mod Settings -> Items -> Drop Item to change shortcut.