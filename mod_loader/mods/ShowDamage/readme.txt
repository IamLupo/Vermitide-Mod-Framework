Adds the ability to display damage dealt by players in chat and in game world.
Go to Mod Settings -> Show Player Damage to tweak settings to your liking.