local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

local mode = Application.user_setting(ShowPlayerDamage.SETTINGS.MODE.save)

if mode == 1 then
	set(ShowPlayerDamage.SETTINGS.MODE.save, 2)
	EchoConsole("Show Player Damage: Default")
elseif mode == 2 then
	set(ShowPlayerDamage.SETTINGS.MODE.save, 3)
	EchoConsole("Show Player Damage: Kills")
elseif mode == 3 then
	set(ShowPlayerDamage.SETTINGS.MODE.save, 4)
	EchoConsole("Show Player Damage: Simple Kills")
else
	set(ShowPlayerDamage.SETTINGS.MODE.save, 1)
	EchoConsole("Show Player Damage: Off")
end

save()