local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

set(ShowPlayerDamage.SETTINGS.FLTNRS.save, not get(ShowPlayerDamage.SETTINGS.FLTNRS.save))
save()
	
if get(ShowPlayerDamage.SETTINGS.FLTNRS.save) then
	EchoConsole("Player Damage: Floating Numbers On")
else
	EchoConsole("Player Damage: Floating Numbers Off")
end