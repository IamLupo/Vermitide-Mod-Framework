local mod_name = "EnemySpawnRates"

--[[
	Enemie Spawn:
		This mod allows you to control the extra percentage rates of enemies spawning.
]]--

local oi = OptionsInjector

Mods.EnemySpawnRates = {
	SETTINGS = {
		CLAN_RAT = {
			["save"] = "cb_enemie_spawn_skaven_clan_rat",
			["widget_type"] = "slider",
			["text"] = "Clan rat",
			["tooltip"] = "Spawn rate\n\n" .. 
						"Change the extra spawn rate for the Clan rat in procentages.",
			["range"] = {0, 100},
			["default"] = 0,
		},
		STORM = {
			["save"] = "cb_enemie_spawn_skaven_storm_vermin_commander",
			["widget_type"] = "slider",
			["text"] = "Storm Vermine",
			["tooltip"] = "Spawn rate\n\n" .. 
						"Change the extra spawn rate for the Storm Vermine in procentages.",
			["range"] = {0, 100},
			["default"] = 0,
		},
		GUNNER = {
			["save"] = "cb_enemie_spawn_skaven_ratling_gunner",
			["widget_type"] = "slider",
			["text"] = "Ratling gunner",
			["tooltip"] = "Spawn rate\n\n" .. 
						"Change the extra spawn rate for the Ratling gunner in procentages.",
			["range"] = {0, 100},
			["default"] = 0,
		},
		ASSASSIN = {
			["save"] = "cb_enemie_spawn_skaven_gutter_runner",
			["widget_type"] = "slider",
			["text"] = "Gutter Runner",
			["tooltip"] = "Spawn rate\n\n" .. 
						"Change the extra spawn rate for the Gutter Runner in procentages.",
			["range"] = {0, 100},
			["default"] = 0,
		},
		POISON = {
			["save"] = "cb_enemie_spawn_skaven_poison_wind_globadier",
			["widget_type"] = "slider",
			["text"] = "Globadier",
			["tooltip"] = "Spawn rate\n\n" .. 
						"Change the extra spawn rate for the Globadier in procentages.",
			["range"] = {0, 100},
			["default"] = 0,
		},
		PACK = {
			["save"] = "cb_enemie_spawn_skaven_pack_master",
			["widget_type"] = "slider",
			["text"] = "Pack Master",
			["tooltip"] = "Spawn rate\n\n" .. 
						"Change the extra spawn rate for the Pack Master in procentages.",
			["range"] = {0, 100},
			["default"] = 0,
		},
		OGRE = {
			["save"] = "cb_enemie_spawn_skaven_rat_ogre",
			["widget_type"] = "slider",
			["text"] = "Ogre",
			["tooltip"] = "Spawn rate\n\n" .. 
						"Change the extra spawn rate for the Ogre in procentages.",
			["range"] = {0, 100},
			["default"] = 0,
		},
		LOOT = {
			["save"] = "cb_enemie_spawn_skaven_loot_rat",
			["widget_type"] = "slider",
			["text"] = "Loot rat",
			["tooltip"] = "Spawn rate\n\n" .. 
						"Change the extra spawn rate for the Loot rat in procentages.",
			["range"] = {0, 100},
			["default"] = 0,
		},
		CRITTER_RAT = {
			["save"] = "cb_enemie_spawn_critter_rat",
			["widget_type"] = "slider",
			["text"] = "Critter rat",
			["tooltip"] = "Spawn rate\n\n" .. 
						"Change the extra spawn rate for the Critter rat in procentages.",
			["range"] = {0, 100},
			["default"] = 0,
		},
		CRITTER_PIG = {
			["save"] = "cb_enemie_spawn_critter_pig",
			["widget_type"] = "slider",
			["text"] = "Critter pig",
			["tooltip"] = "Spawn rate\n\n" .. 
						"Change the extra spawn rate for the Critter pig in procentages.",
			["range"] = {0, 100},
			["default"] = 0,
		},
	},
	
	-- Order the procentages must be checked
	ORDER = {
		{
			["setting"] = "CLAN_RAT",
			["breed"] = "skaven_clan_rat",
		},
		{
			["setting"] = "STORM",
			["breed"] = "skaven_storm_vermin_commander",
		},
		{
			["setting"] = "GUNNER",
			["breed"] = "skaven_ratling_gunner",
		},
		{
			["setting"] = "ASSASSIN",
			["breed"] = "skaven_gutter_runner",
		},
		{
			["setting"] = "POISON",
			["breed"] = "skaven_poison_wind_globadier",
		},
		{
			["setting"] = "PACK",
			["breed"] = "skaven_pack_master",
		},
		{
			["setting"] = "OGRE",
			["breed"] = "skaven_rat_ogre",
		},
		{
			["setting"] = "LOOT",
			["breed"] = "skaven_loot_rat",
		},
		{
			["setting"] = "CRITTER_RAT",
			["breed"] = "critter_rat",
		},
		{
			["setting"] = "CRITTER_PIG",
			["breed"] = "critter_pig",
		},
	}
}
local me = Mods.EnemySpawnRates

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Options ######################################################################################################
-- ####################################################################################################################
Mods.EnemySpawnRates.create_options = function()
	Mods.option_menu:add_group("enemie_spawn", "Enemy Extra Spawn Rates")
	
	Mods.option_menu:add_item("enemie_spawn", me.SETTINGS.CLAN_RAT, true)
	Mods.option_menu:add_item("enemie_spawn", me.SETTINGS.STORM, true)
	Mods.option_menu:add_item("enemie_spawn", me.SETTINGS.GUNNER, true)
	Mods.option_menu:add_item("enemie_spawn", me.SETTINGS.ASSASSIN, true)
	Mods.option_menu:add_item("enemie_spawn", me.SETTINGS.POISON, true)
	Mods.option_menu:add_item("enemie_spawn", me.SETTINGS.PACK, true)
	Mods.option_menu:add_item("enemie_spawn", me.SETTINGS.OGRE, true)
	Mods.option_menu:add_item("enemie_spawn", me.SETTINGS.LOOT, true)
	Mods.option_menu:add_item("enemie_spawn", me.SETTINGS.CRITTER_RAT, true)
	Mods.option_menu:add_item("enemie_spawn", me.SETTINGS.CRITTER_PIG, true)
end

-- ####################################################################################################################
-- ##### Functions ####################################################################################################
-- ####################################################################################################################
Mods.EnemySpawnRates.switch_breed = function(unit, breed, ai_extension)
	local blackboard = ai_extension._blackboard
	local pos = Unit.local_position(unit, 0)
	local rot = Unit.local_rotation(unit, 0)
	
	-- Remove enemie
	Managers.state.conflict:destroy_unit(unit, blackboard, "switch_breed")
	
	-- Disable hook
	Mods.hook.enable(false, mod_name, "ConflictDirector.spawn_unit")
	
	-- Spawn enemies
	local new_unit = Managers.state.conflict:spawn_unit(breed, pos, rot, "switch_breed", nil)
	
	-- Enable hook
	Mods.hook.enable(true, mod_name, "ConflictDirector.spawn_unit")
	
	return new_unit
end

-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "ConflictDirector.spawn_unit", function(func, self, breed, ...)
	local unit = func(self, breed, ...)
	local ai_extension = ScriptUnit.extension(unit, "ai_system")
	
	-- Check settings to change the enemy
	for _, obj in ipairs(me.ORDER) do
		local setting = me.SETTINGS[obj.setting]
		
		if setting and get(setting.save) >= math.random(100) then
			return me.switch_breed(unit, Breeds[obj.breed], ai_extension)
		end
	end
	
	return unit
end)

Mods.hook.set(mod_name, "ConflictDirector.update", function(func, self, dt, t)
	pcall(func, self, dt, t)
end)

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()