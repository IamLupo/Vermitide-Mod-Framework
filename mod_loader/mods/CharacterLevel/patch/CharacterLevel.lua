local mod_name = "CharacterLevel"
--[[
	Add Defend while in chat:
		When you are typing in the chat the charter will automaticly
		block every attack.
--]]

local oi = OptionsInjector

Mods.CharacterLevel = {
	SETTINGS = {
		SHOW = {
			["save"] = "cb_character_level_show",
			["widget_type"] = "checkbox",
			["text"] = "Character Level",
			["default"] = false,
			["hide_options"] = {
				{
					false,
					mode = "hide",
					options = {
						"cb_character_level_change",
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_character_level_change",
					}
				},
			},
		},
		CHANGE = {
			["save"] = "cb_character_level_change",
			["widget_type"] = "slider",
			["text"] = "Change Level",
			["tooltip"] = "Change Level\n\n" .. 
						"Change the level of your character.",
			["range"] = {1, 2000},
			["default"] = 1,
		},
	},
}
local me = Mods.CharacterLevel

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Options ######################################################################################################
-- ####################################################################################################################
Mods.CharacterLevel.create_options = function()
	Mods.option_menu:add_group("cheats", "Cheats")
	
	Mods.option_menu:add_item("cheats", me.SETTINGS.SHOW, true)
	Mods.option_menu:add_item("cheats", me.SETTINGS.CHANGE)
end

-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, ...)
	safe_pcall(function()
		if get(me.SETTINGS.SHOW.save) then
			local experience = ScriptBackendProfileAttribute.get("experience")
			local level = get(me.SETTINGS.CHANGE.save)
			
			if level ~= ExperienceSettings.get_level(experience) then
				local new_experience = experience
				
				while level > ExperienceSettings.get_level(new_experience) do
					new_experience = new_experience + 500
				end
				
				while level < ExperienceSettings.get_level(new_experience) do
					new_experience = new_experience - 500
				end
				
				ScriptBackendProfileAttribute.set("experience", new_experience)
				EchoConsole("Updated Character level to " .. tostring(level))
			end
		end
	end)
	
	func(...)
end)

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()
