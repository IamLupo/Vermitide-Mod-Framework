local mod_name = "Superjump"

local oi = OptionsInjector

Superjump = {
	SETTINGS = {	
		ACTIVE = {
			["save"] = "cb_superjump_active",
			["widget_type"] = "stepper",
			["text"] = "Super Jump",
			["tooltip"] =  "Toggles extremely powerful unlimited jumps",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
			["hide_options"] = {
				{
					false,
					mode = "show",
					options = {
						"cb_superjump_toggle",
						"cb_superjump_toggle_modifiers",
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_superjump_toggle",
						"cb_superjump_toggle_modifiers",
					}
				},
			},
		},	
		TOGGLE = {
			["save"] = "cb_superjump_toggle",
			["widget_type"] = "keybind",
			["text"] = "Toggle",
			["default"] = {
				"j",
				oi.key_modifiers.CTRL_ALT,
			},
			["exec"] = {"Superjump", "action/superjump"},
		},
	},
}

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

Superjump.create_options = function()
	Mods.option_menu:add_item("movement", Superjump.SETTINGS.ACTIVE, true)
	Mods.option_menu:add_item("movement", Superjump.SETTINGS.TOGGLE)
end

Superjump.update_state = function()
	local enabled = get(Superjump.SETTINGS.ACTIVE.save) or false
	if enabled ~= (script_data.player_mechanics_goodness_debug or false) then
		Mods.exec("Superjump", "action/superjump")
	end
end

set(Superjump.SETTINGS.ACTIVE.save, false)
save()

Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, ...)
	func(...)
	Superjump.update_state()
end)

Superjump.create_options()