Adds the ability to enable extremely powerful unlimited jumps. Also disables fall damage but map must be restarted for it to work.
Chat command: /superjump
Keyboard shortcut (default): Ctrl + Alt + J
Go to Mod Settings -> Movement -> Super Jump to toggle.
Off by default.