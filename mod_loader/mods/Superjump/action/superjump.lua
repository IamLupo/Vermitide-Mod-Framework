--[[
	Type in the chat: /superjump
--]]

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

script_data.player_mechanics_goodness_debug = not script_data.player_mechanics_goodness_debug

set(Superjump.SETTINGS.ACTIVE.save, script_data.player_mechanics_goodness_debug)
save()

if script_data.player_mechanics_goodness_debug then
	PlayerUnitMovementSettings.fall.heights.MAX_FALL_DAMAGE = 0
	EchoConsole("You feel much lighter")
else
	PlayerUnitMovementSettings.fall.heights.MAX_FALL_DAMAGE = 100
	EchoConsole("You feel grounded again")
end
