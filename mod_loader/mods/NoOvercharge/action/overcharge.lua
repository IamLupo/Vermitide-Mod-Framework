local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

pcall(function()
	-- Set
	set(NoOvercharge.SETTINGS.ACTIVE.save, not get(NoOvercharge.SETTINGS.ACTIVE.save))
	save()
	
	-- Feedback
	if get(NoOvercharge.SETTINGS.ACTIVE.save) then
		EchoConsole("Overcharge Off")
	else
		EchoConsole("Overcharge On")
	end
end)