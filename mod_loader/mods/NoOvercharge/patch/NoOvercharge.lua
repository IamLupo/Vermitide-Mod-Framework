local mod_name = "NoOvercharge"
--[[
	No Overcharge
		You won't generate heat for any weapon
	
	author: grasmann
--]]

local oi = OptionsInjector

NoOvercharge = {
	SETTINGS = {
		ACTIVE = {
			["save"] = "cb_no_overcharge",
			["widget_type"] = "stepper",
			["text"] = "No Overcharge",
			["tooltip"] = "No Overcharge\n" ..
				"Toggle overcharge on / off.\n\n" ..
				"You will gain no overcharge heat for any weapon.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
			["hide_options"] = {
				{
					false,
					mode = "show",
					options = {
						"cb_no_overcharge_hotkey_toggle",
						"cb_no_overcharge_toggle_modifiers",
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_no_overcharge_hotkey_toggle",
						"cb_no_overcharge_toggle_modifiers",
					}
				},
			},
		},
		HK_TOGGLE = {
			["save"] = "cb_no_overcharge_hotkey_toggle",
			["widget_type"] = "keybind",
			["text"] = "Toggle",
			["default"] = {
				"o",
				oi.key_modifiers.CTRL_SHIFT,
			},
			["exec"] = {"NoOvercharge", "action/overcharge"},
		},
	},
}
local me = NoOvercharge

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Option #######################################################################################################
-- ####################################################################################################################
NoOvercharge.create_options = function()
	Mods.option_menu:add_item("cheats", me.SETTINGS.ACTIVE, true)
	Mods.option_menu:add_item("cheats", me.SETTINGS.HK_TOGGLE, true)
end

-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "OverChargeExtension.add_charge", function(func, self, ...)
	func(self, ...)		
	
	if get(me.SETTINGS.ACTIVE.save) then		
		self.overcharge_value = 0	
	end
end)

set(me.SETTINGS.ACTIVE.save, false)
save()

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()