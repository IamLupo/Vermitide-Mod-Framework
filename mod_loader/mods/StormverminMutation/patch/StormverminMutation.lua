local mutator_name = "Stormvermin Mutation"
local mutator_group = "GAME_MODE"
local difficulty_name = "||Stormvermin Mutation||"

Storms = {}

Storms.name = mutator_name
Storms.group = mutator_group
Storms.difficulty_name = difficulty_name
Storms.saved = {}
Storms.data = {
	["Breeds.skaven_slave"] = "Breeds.skaven_clan_rat",
	["Breeds.skaven_clan_rat"] = "Breeds.skaven_storm_vermin_commander",
	["Breeds.skaven_gutter_runner"] = "Breeds.skaven_rat_ogre",
	["Breeds.skaven_pack_master"] = "Breeds.skaven_rat_ogre",
	["Breeds.skaven_poison_wind_globadier"] = "Breeds.skaven_rat_ogre",
	["Breeds.skaven_ratling_gunner"] = "Breeds.skaven_rat_ogre",
}

Storms.enable = function(self)
	Mods.hook.enable(true, mutator_name)
	Mods.mutators.replace_data(Storms)
	Mods.mutators:set_servername(self.difficulty_name)
end

Storms.disable = function(self)
	Mods.hook.enable(false, mutator_name)
	Mods.mutators.restore_data(Storms)
	Mods.mutators:restore_servername()
end

Storms.check_conditions = function()
	return Managers.player.is_server
end

--Hooks

--Adding bonus dice
Mods.hook.set(mutator_name, "GameModeManager.complete_level", function (func, ...)
	local mission_system = Managers.state.entity:system("mission_system")
	for i = 1,5 do
		mission_system.request_mission(mission_system, "bonus_dice_hidden_mission")
		mission_system.update_mission(mission_system, "bonus_dice_hidden_mission", true, nil, true)
	end

	func(...)
end)

--Changing difficulty name
Mods.hook.set(mutator_name, "IngamePlayerListUI.set_difficulty_name", function (func, self, name)		
	local content = self.headers.content
	content.game_difficulty = name .. " " .. mutator_name
	return 
end)

--Setting servername
Mods.hook.set(mutator_name, "MatchmakingStateHostGame.host_game", function (func, self, ...)
	func(self, ...)

	Mods.mutators:set_servername(difficulty_name)
end)

Mods.hook.enable(false, mutator_name)

Mods.mutators.save_data(Storms)

Mods.mutators:add_mutator(mutator_group, Storms)