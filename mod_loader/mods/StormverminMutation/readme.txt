Stormvermin Mutation custom game mode.
"This simple mod replaces all slave rats with clan rats, all original clan rats with stormvermins, and all specials with ogres. Best played on hard difficulty, this mod will throw army upon army after you, overwhelming weak heroes through sheer brute force."
By Grimalackt
Go to Mod Settings -> Mutators -> Game Mode and pick Stormvermin Mutation from the dropdown menu.
This game mode works correctly on all difficulties.