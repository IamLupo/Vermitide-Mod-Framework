Adds a keyboard shortcut and a chat command pause and unpause game.
Beware: opening the game while paused will soft-lock the game.
Keyboard shortcut (default): Ctrl + Shift + P
Go to Mod Settings -> Cheats -> Pause Game to change shortcut.
Chat command: /freezeall