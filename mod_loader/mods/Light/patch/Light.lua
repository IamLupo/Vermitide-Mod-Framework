local mod_name = "Light"

if not MOD_LIGHT_BACKUP then
	MOD_LIGHT_BACKUP = {}
end

Mods.Light = {
	SETTINGS = {
		DISCO = {
			["save"] = "cb_light_disco",
			["widget_type"] = "stepper",
			["text"] = "Disco Light",
			["tooltip"] =  "Disco Light\n" ..
				"Toggle Disco Light on / off.\n\n" ..
				"Having disco light on every light object.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
		},
	},
	
	refresh = 0,
}

local me = Mods.Light

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Option #######################################################################################################
-- ####################################################################################################################
Mods.Light.create_options = function()
	Mods.option_menu:add_group("light", "Light")
	
	Mods.option_menu:add_item("light", me.SETTINGS.DISCO, true)
end

-- ####################################################################################################################
-- ##### Functions ####################################################################################################
-- ####################################################################################################################
Mods.Light.backup = function(unit, i, light)
	if MOD_LIGHT_BACKUP[unit] == nil then
		MOD_LIGHT_BACKUP[unit] = {}
	end
	
	if MOD_LIGHT_BACKUP[unit][i] == nil then
		local color = Light.color(light)
		
		MOD_LIGHT_BACKUP[unit][i] = {
			intensity = Light.intensity(light),
			color =  {
				r = color.x,
				g = color.y,
				b = color.z,
			},
		}
	end
end

Mods.Light.restore = function()
	for unit, lights in pairs(MOD_LIGHT_BACKUP) do
		if Unit.alive(unit) then
			for i, settings in pairs(lights) do
				local light = Unit.light(unit, i - 1)
				
				if light then
					local color = Vector3(settings.color.r, settings.color.g, settings.color.b)
					
					Light.set_intensity(light, settings.intensity)
					Light.set_color(light, color)
				end
			end
		end
	end
	
	MOD_LIGHT_BACKUP = {}
end

Mods.Light.update = function()
	if get(me.SETTINGS.DISCO.save) then
		for _, world in pairs(Application.worlds()) do
			for _, unit in pairs(World.units(world)) do
				local num_lights = Unit.num_lights(unit)
				
				if num_lights > 0 then
					for i = 1, num_lights, 1 do
						local light = Unit.light(unit, i - 1)
						
						if light then
							-- Save old light settings
							Mods.Light.backup(unit, i, light)
						
							-- Make random disco color
							Light.set_intensity(light, 1)
							Light.set_color(light, Vector3(math.random(), math.random(), math.random()))
						end
					end
				end
			end
		end
	else
		--Restore old settings
		if MOD_LIGHT_BACKUP then
			Mods.Light.restore()
		end
	end
end

-- ####################################################################################################################
-- ##### Hook #########################################################################################################
-- ####################################################################################################################
Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, self, dt, t)
	func(self, dt, t)
	
	if me.refresh + 1 < t then
		Mods.Light.update()
		
		me.refresh = t
	end
end)

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################
me.create_options()