--[[
	Type in the chat: /disco
--]]

local args = {...}

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

if #args == 0 then
	set(Mods.Light.SETTINGS.DISCO.save, not get(Mods.Light.SETTINGS.DISCO.save))
	save()
	
	return true
else
	return false
end