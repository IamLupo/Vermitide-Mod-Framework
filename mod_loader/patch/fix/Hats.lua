local mod_name = "Hats"
--[[
	Patch World:
		I patch 1.4.0 the dev liked it to remove hats out the game.
		A lot of people could not start the game anymore. This let them
		start the game and patch there account again.
		
		Extra: in versions 1.5.0 and higher they have fix this issue
--]]

Mods.hook.set(mod_name, "World.spawn_unit", function(func, world, unit_name, pos, rot)
	if unit_name == "units/beings/player/way_watcher/headpiece/ww_hood_13" then
		unit_name = "units/beings/player/way_watcher/headpiece/ww_hood_01"
	end
	
	if unit_name == "units/beings/player/witch_hunter/headpiece/wh_hat_13" then
		unit_name = "units/beings/player/way_watcher/headpiece/ww_hood_01"
	end
	
	if unit_name == "units/beings/player/empire_soldier/headpiece/es_helmet_07" then
		unit_name = "units/beings/player/way_watcher/headpiece/ww_hood_01"
	end
	
	if unit_name == "units/beings/player/dwarf_ranger/headpiece/dr_helmet_12" then
		unit_name = "units/beings/player/way_watcher/headpiece/ww_hood_01"
	end
	
	-- Call orginal function
	return func(world, unit_name, pos, rot)
end)