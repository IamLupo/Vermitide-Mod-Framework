local mod_name = "Grey_Seer"
--[[
	Game Version: Unknown
	
	Patch Grey Seer:
		When spawning the grey seer it has some events that can crash the game.
]]--

Mods.hook.set(mod_name, "AISystem.update_brains", function (func, ...)
	pcall(func, ...)
end)
