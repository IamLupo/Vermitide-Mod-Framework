local mod_name = "Breed"
--[[
	Patch Breed:
		skaven_stormfiend will crash your game when you try to spawn.
		Only breed allowed in the inn is the ogre.
--]]

local check_breed = function(self)
	if Managers.state.game_mode._game_mode_key == "inn" then
		while self._debug_breed ~= "skaven_rat_ogre" and self._debug_breed ~= "skaven_grey_seer" do
			self._debug_breed = next(Breeds, self._debug_breed)
		end
	else
		if self._debug_breed == "skaven_stormfiend" then
			self._debug_breed = next(Breeds, self._debug_breed)
		end
	end
end

Mods.hook.set(mod_name, "ConflictDirector.debug_spawn_switch_breed", function(func, self, t)
	-- Call orginal function
	func(self, t)
	
	-- My patch
	check_breed(self)
	
	return 
end)

Mods.hook.set(mod_name, "ConflictDirector.debug_spawn_breed", function(func, self, t)
	-- My patch
	check_breed(self)
	
	-- Call orginal function
	func(self, t)
	
	return 
end)

--[[
	Patch spawn_unit:
		When a ogre spawns in the inn its missing level_settings.
		I initialize it before it spawns
--]]

Mods.hook.set(mod_name, "ConflictDirector.spawn_unit", function(func, self, ...)
	if self.level_settings == nil then
		self.level_settings = {}
	end
	
	return func(self, ...) 
end)

Mods.hook.set(mod_name, "LocomotionTemplates.AILocomotionExtensionC.update", function(func, ...)
	if Managers.state.game_mode._level_key ~= "inn_level" then
		func(...)
	end
end)