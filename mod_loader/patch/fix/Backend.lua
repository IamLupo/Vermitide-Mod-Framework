local mod_name = "Backend"
--[[
	Game Version: All
	
	Detail:
		Case:
			There is a backend array with a list of stat of your profile. This list contains the "Managers.backend._stats"
			variable that contains all the variables that are commited to the server. This list has a buggy case that all
			variables in this list are in it 3x times or more. When someone makes progress like beating a level, collected
			a tome or collected a lorebook page these variables will be updated and commited to the server.

			But if you read "ScriptBackend.set_stats" you see it only updates the first record it finds and not updated all
			records because of the "break" code. If you read the code "ScriptBackend._refresh_stats" you see it reads the
			last record it found in this list.

		Conclusion:
			Because variables can accure multiple times and the first record is updated and the last record is been read.
			This way you can not make any progress in your account.
			
		http://steamcommunity.com/app/235540/discussions/8/133256758580242127/
]]--

Mods.hook.set(mod_name, "ScriptBackend.set_stats", function(func, self, nice_stats)
	self._refresh_stats(self)

	local new_stats = table.clone(nice_stats)

	for stat_name, stat_value in pairs(nice_stats) do
		for backend_stat_id, backend_stat_data in pairs(self._stats) do
			if backend_stat_data.key == stat_name then
				new_stats[stat_name] = nil

				if backend_stat_data.data ~= stat_value then
					BackendStats.set_stat(backend_stat_id, stat_name, stat_value)
				end

				--break
			end
		end
	end

	for stat_name, stat_value in pairs(new_stats) do
		ScriptApplication.send_to_crashify("ScriptBackend", "Tried to set unregistered stat %s, value: %s", tostring(stat_name), tostring(stat_value))
	end

	self.commit(self)

	self._dirty_stats = true

	return 
end)