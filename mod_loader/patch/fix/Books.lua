local mod_name = "Books"
--[[
	Patch Books:
		The game doesnt filter if there are more then 7 "grims + tomes + loot dices".
		This cause the game crash when there are to much.
--]]

Mods.hook.set(mod_name, "GameModeManager.complete_level", function(func, self)
	local total = 0
	local mission_system = Managers.state.entity:system("mission_system")
	local active_mission = mission_system.active_missions
	
	-- Calculate the total
	for name, obj in pairs(active_mission) do
		if name == "tome_bonus_mission" or name == "grimoire_hidden_mission" or name == "bonus_dice_hidden_mission" then
			total = total + obj.current_amount 
		end
	end
	
	-- Remove if there are to much total
	if active_mission.bonus_dice_hidden_mission then
		for i = 1, active_mission.bonus_dice_hidden_mission.current_amount do
			if total > 7 then
				mission_system.request_mission(mission_system, "bonus_dice_hidden_mission", nil, Network.peer_id())
				mission_system.update_mission(mission_system, "bonus_dice_hidden_mission", false, nil, Network.peer_id(), nil, true)
				
				total = total - 1
			end
		end
	end
	
	if active_mission.tome_bonus_mission then
		for i = 1, active_mission.tome_bonus_mission.current_amount do
			if total > 7 then
				mission_system.request_mission(mission_system, "tome_bonus_mission", nil, Network.peer_id())
				mission_system.update_mission(mission_system, "tome_bonus_mission", false, nil, Network.peer_id(), nil, true)
				
				total = total - 1
			end
		end
	end
	
	if active_mission.grimoire_hidden_mission then
		for i = 1, active_mission.grimoire_hidden_mission.current_amount do
			if total > 7 then
				mission_system.request_mission(mission_system, "grimoire_hidden_mission", nil, Network.peer_id())
				mission_system.update_mission(mission_system, "grimoire_hidden_mission", false, nil, Network.peer_id(), nil, true)
				
				total = total - 1
			end
		end
	end
	
	-- Call orginal function
	func(self)

	return
end)
