local mod_name = "fix_13_02_2017_0001"

--[[
	Game Version: 1.6 betatest
	
	Error:
		<<Lua Error>> scripts/entity_system/systems/mission/mission_templates.lua:444: table index is nil<</Lua Error>>
		<<Lua Stack>>   [1] scripts/entity_system/systems/mission/mission_templates.lua:444: in function add_unique_id_for_peer
		  [2] scripts/entity_system/systems/mission/mission_templates.lua:472: in function update
		  [3] scripts/entity_system/systems/mission/mission_system.lua:236: in function update_mission
		<</Lua Stack>>
		<<Lua Locals>>
		[1] self = table: 97CD0820; peer_id = nil; unique_id = nil
		[2] data = table: 97CD0820; positive = true; unique_id = nil; peer_id = nil; dt = nil; network_time = 924.98825468309224
		[3] self = table: 97D76AA0; mission_name = "lorebook_page_hidden_mission"; positive = true; unique_id = nil; requesting_peer_id = nil; dt = nil; sync = true; data = table: 97CD0820; network_time = 924.98825468309224; mission_template_name = "collect_unique_uncompletable"; template = table: 2D2C92E0
		<</Lua Locals>>
	
	Detail:
		When you pick up a lorenook page this error will accour. This happends because you collected all
		lorebook pages for a level. But because the game doesnt remove the remaining lorebook page in the
		level it crash your game if you pick up to much.
		Because this is still a beta version i do a pcall to at least not crash the game.
]]--

Mods.hook.set(mod_name, "MissionSystem.update_mission", function(func, ...)
	local status, value = pcall(func, ...)
	
	if not status then
		EchoConsole(value)
	end
end)