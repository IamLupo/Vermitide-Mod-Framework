local mod_name = "Fireflame"
--[[
	Game Version: Unknown
	
	Patch Fireflame staff:
		When fire the fireflame staff to a team mate, the game crashed.
		It looks for MaterialEffect but without checking it even exist.
		I added the check before executing the function.
--]]

Mods.hook.set(mod_name, "EffectHelper.play_surface_material_effects", function(func, effect_name, ...)
	if MaterialEffectMappings[effect_name] then
		-- Call orginal function
		func(effect_name, ...)
	end
	
	return
end)

Mods.hook.set(mod_name, "EffectHelper.play_skinned_surface_material_effects", function(func, effect_name, ...)
	if MaterialEffectMappings[effect_name] then
		-- Call orginal function
		func(effect_name, ...)
	end
	
	return
end)