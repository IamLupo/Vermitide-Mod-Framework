local mod_name = "ModSettings"

local oi = OptionsInjector

ModSettings = {
	SETTINGS = {
		HK_FEEDBACK = {
			["save"] = "cb_mod_settings_hotkey_feedback",
			["widget_type"] = "stepper",
			["text"] = "Hotkey Feedback",
			["tooltip"] = "Hotkey Feedback\n" ..
				"When you press a hotkey it will normally give feedback in the system chat.\n\n" ..
				"When enabled no feedback will be given in the system chat.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true}
			},
			["default"] = 1, -- Default first option is enabled. In this case On
		},
		HK_RELOAD = {
			["save"] = "cb_mod_settings_hotkey_reload",
			["widget_type"] = "keybind",
			["text"] = "Reload Mod",
			["default"] = {
				"r",
				oi.key_modifiers.CTRL_SHIFT,
			},
			["exec"] = {"action", "reload"},
		},
	},
}
local me = ModSettings

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- ####################################################################################################################
-- ##### Options ######################################################################################################
-- ####################################################################################################################
ModSettings.create_options = function()
	Mods.option_menu:add_item("mod_settings", me.SETTINGS.HK_FEEDBACK, true)
	Mods.option_menu:add_item("mod_settings", me.SETTINGS.HK_RELOAD, true)
	
	--Fixes
	--widget_mode.style.tooltip_text.cursor_offset = {-10, 250} -- fix tooltip position
end

me.create_options()