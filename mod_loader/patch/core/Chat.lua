local mod_name = "Chat"
--[[
	Patch Chat:
		This allows me to filter out commands out the chat box and
		let the Mods.update.chat handle it.
--]]

Mods.hook.set(mod_name, "ChatManager.send_chat_message", function(func, self, channel_id, message)
	if Mods.chat.on_message(message) then
		fassert(self.has_channel(self, channel_id), "Haven't registered channel: %s", tostring(channel_id))

		local localization_param = ""
		local is_system_message = Mods.chat.system
		local pop_chat = true
		local my_peer_id = self.my_peer_id
		local is_dev = SteamHelper.is_dev(my_peer_id)

		if self.is_server then
			local members = self.channel_members(self, channel_id)

			for _, member in pairs(members) do
				if member ~= my_peer_id then
					RPC.rpc_chat_message(member, channel_id, my_peer_id, message, localization_param, is_system_message, pop_chat, is_dev)
				end
			end
		else
			local host_peer_id = self.host_peer_id

			if host_peer_id then
				RPC.rpc_chat_message(host_peer_id, channel_id, my_peer_id, message, localization_param, is_system_message, pop_chat, is_dev)
			end
		end

		self._add_message_to_list(self, channel_id, my_peer_id, message, is_system_message, pop_chat, is_dev)
	end
	
	return 
end)

Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, ...)
	func(...)
	
	-- Check Animation is active
	if Mods.chat.animation then
		Mods.chat.animation(...)
	end
	
	return
end)