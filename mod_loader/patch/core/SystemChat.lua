local mod_name = "SystemChat"
--[[
	author: grasmann
	
	System Chat
		- Display information and stuff
		- EXAMPLES in MatchmakingManager.update hook
--]]

local oi = OptionsInjector

SystemChat = {
	
	active = nil,
	DISABLE_POSITIVE_REINFORCEMENTS = nil,
	REDIRECT_ECHOCONSOLE = nil,
	DEFAULT_FONT_SIZE = 22,
	
	SETTINGS = {
		ACTIVE = {
			["save"] = "cb_system_chat_active",
			["widget_type"] = "stepper",
			["text"] = "Active",
			["tooltip"] = "System Chat Active\n" ..
				"Toggle system chat on / off.\n\n" ..
				"If switched off output will be redirected to the normal chat.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default second option is enabled. In this case On
			["hide_options"] = {
				{
					false,
					mode = "hide",
					options = {
						"cb_system_chat_fit",
						"cb_system_chat_position",
						"cb_system_chat_always_visible",
						"cb_system_chat_font_size",
						"cb_system_chat_advanced",
						"cb_system_chat_positive_reinforcements",
						"cb_system_chat_redirect_echo",
						"cb_system_chat_always_scrolling",
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_system_chat_fit",
						"cb_system_chat_position",
						"cb_system_chat_always_visible",
						"cb_system_chat_font_size",
						"cb_system_chat_advanced",
						"cb_system_chat_positive_reinforcements",
						"cb_system_chat_redirect_echo",
						"cb_system_chat_always_scrolling",
					}
				},
			},
		},
		POSITION = {
			["save"] = "cb_system_chat_active",
			["widget_type"] = "stepper",
			["text"] = "Position",
			["tooltip"] = "",
			["value_type"] = "number",
			["options"] = {
				{text = "Above Chat", value = 1},
				{text = "Top Left", value = 2},
				{text = "Top Right", value = 3},
				{text = "Bottom Right", value = 4}
			},
			["default"] = 1, -- Default first option is enabled. In this case Above Chat
		},
		FIT = {
			["save"] = "cb_system_chat_fit",
			["widget_type"] = "dropdown",
			["text"] = "Fit",
			["tooltip"] = "Fit\n" ..
				"Choose the height of the system chat.\n\n" ..
				"-- TEXT --\nThe height of the window is calculated from the text.\n\n" ..
				"-- SCREEN --\nThe window will extend to the edge of the screen.\n\n" ..
				"-- PERCENTAGE --\nChoose a percentage of the screen height that should be used.",
			["value_type"] = "number",
			["options"] = {
				{text = "Text", value = 1},
				{text = "Screen", value = 2},
				{text = "Percentage", value = 3},
			},
			["default"] = 1, -- Default first option is enabled. In this case Text
			["hide_options"] = {
				{
					1,
					mode = "hide",
					options = {
						"cb_system_chat_size"
					},
				},
				{
					2,
					mode = "hide",
					options = {
						"cb_system_chat_size"
					},
				},
				{
					3,
					mode = "show",
					options = {
						"cb_system_chat_size"
					},
				},
			},
		},
		SIZE = {
			["save"] = "cb_system_chat_size",
			["widget_type"] = "slider",
			["text"] = "Percentage Height",
			["tooltip"] = "Percentage Height\n" ..
				"Choose a percentage of the screen the window shouldn't exceed.",
			["range"] = {40, 100},
			["default"] = 65,
		},
		FONT_SIZE = {
			["save"] = "cb_system_chat_font_size",
			["widget_type"] = "slider",
			["text"] = "Font Size",
			["tooltip"] = "Font Size\n" ..
				"Change the font size of the output window.",
			["range"] = {10, 48},
			["default"] = 22,
		},
		ALWAYS_VISIBLE = {
			["save"] = "cb_system_chat_always_visible",
			["widget_type"] = "stepper",
			["text"] = "Always visible",
			["tooltip"] = "Always Visible\n" ..
				"Toggle on / off if system chat should always be visible.\n\n" ..
				"By default the system chat will fade out after a certain while.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
		},
		ADVANCED = {
			["save"] = "cb_system_chat_advanced",
			["widget_type"] = "checkbox",
			["text"] = "Advanced",
			["default"] = false,
			["hide_options"] = {
				{
					false,
					mode = "hide",
					options = {
						"cb_system_chat_transparency",
						"cb_system_chat_transparency_window",
						"cb_system_chat_fade_delay",
						"cb_system_chat_fade_speed",
						"cb_system_chat_remove",
						
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_system_chat_transparency",
						"cb_system_chat_transparency_window",
						"cb_system_chat_fade_delay",
						"cb_system_chat_fade_speed",
						"cb_system_chat_remove",
					}
				},
			},
		},
		TRANSPARENCY = {
			["save"] = "cb_system_chat_transparency",
			["widget_type"] = "slider",
			["text"] = "Opacity",
			["tooltip"] = "Opacity\n" ..
				"Set the opacity of the system chat.",
			["range"] = {20, 80},
			["default"] = 80,
		},
		TRANSPARENCY_WINDOW = {
			["save"] = "cb_system_chat_transparency_window",
			["widget_type"] = "slider",
			["text"] = "Window Opacity",
			["tooltip"] = "Window Opacity\n" ..
				"Choose the opacity of the system chat window.\n" ..
				"This leaves the text alone.",
			["range"] = {0, 100},
			["default"] = 100,
		},
		FADE_DELAY = {
			["save"] = "cb_system_chat_fade_delay",
			["widget_type"] = "slider",
			["text"] = "Fade Delay",
			["tooltip"] = "Fade Delay\n" ..
				"Time it takes before auto-hide of the system chat.",
			["range"] = {0, 5},
			["default"] = 0,
		},
		FADE_SPEED = {
			["save"] = "cb_system_chat_fade_speed",
			["widget_type"] = "slider",
			["text"] = "Fade Duration",
			["tooltip"] = "Fade Duration\n" ..
				"Time it takes to fade in / out the system chat.",
			["range"] = {0, 5},
			["default"] = 1,
		},
		DISABLE_POSITIVE_REINFORCEMENTS = {
			["save"] = "cb_system_chat_positive_reinforcements",
			["widget_type"] = "stepper",
			["text"] = "Redirect Standard Output",
			["tooltip"] = "Redirect Standard Output\n" ..
				"Toggle on / off if system chat should display all output from the mod.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
		},
		REMOVE = {
			["save"] = "cb_system_chat_remove",
			["widget_type"] = "checkbox",
			["text"] = "Auto-Remove Lines",
			["default"] = false,
			["hide_options"] = {
				{
					false,
					mode = "hide",
					options = {
						"cb_system_chat_remove_lines",
						"cb_system_chat_remove_timer",
					}
				},
				{
					true,
					mode = "show",
					options = {
						"cb_system_chat_remove_lines",
						"cb_system_chat_remove_timer",
					}
				},
			},
		},
		REMOVE_LINES = {
			["save"] = "cb_system_chat_remove_lines",
			["widget_type"] = "stepper",
			["text"] = "Active",
			["tooltip"] = "Auto-Remove Lines\n" ..
				"Toggle on / off if system chat should remove lines after a certain while.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 1, -- Default first option is enabled. In this case Off
		},
		REMOVE_TIMER = {
			["save"] = "cb_system_chat_remove_timer",
			["widget_type"] = "slider",
			["text"] = "Time",
			["tooltip"] = "Auto-Remove Time\n" ..
				"Time between auto-removing lines.",
			["range"] = {1, 10},
			["default"] = 5,
		},
		REDIRECT_ECHOCONSOLE = {
			["save"] = "cb_system_chat_redirect_echo",
			["widget_type"] = "stepper",
			["text"] = "Disable Positive Reinforcements",
			["tooltip"] = "Disable Positive Reinforcements\n" ..
				"Toggle positive reinforcements on / off.\n\n" ..
				"The game will show some messages for certain events.\n" ..
				"For example if a special rat is killed.\n" ..
				"These messages can be removed with this option.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
		ALWAYS_SCROLLING = {
			["save"] = "cb_system_chat_always_scrolling",
			["widget_type"] = "stepper",
			["text"] = "Always Scroll",
			["tooltip"] = "Always Scroll\n" ..
				"Toggle on / off if scrolling should always work.\n\n" ..
				"If switched off you can only scroll through the system chat if the normal chat is visible.\n" ..
				"But not if you're typing something.",
			["value_type"] = "boolean",
			["options"] = {
				{text = "Off", value = false},
				{text = "On", value = true},
			},
			["default"] = 2, -- Default second option is enabled. In this case On
		},
	},
	
	text = {
		max_lines = 100,
		["lines"] = {},
		position = 0,
		scroll_timer = 0,
		scroll_delay = 0.02,
		scroll_block_time = 2,
		scroll_block_timer = 0,
	},
	
	t = 0,
	
	window = {
		fade_timer = 0,
		remove_timer = 0,
		alpha = 1,
		size = {550, 150},
		position = {5, 200, 1},
	},
	
	chat = {
		visible = false,
		has_focus = false,
		size = {365, 230},
		text = "",
	},
	
	sense = {
		max_lines = 5,
		text = "",
		items = {
			{"/reload",		"/reload > Reload the mod"},
			{"/help",		"/help > Opens a help page in steam overlay"},
			{"/clear",		"/clear > Clear the system chat text"},
			{"/playmusic",	"/playmusic > Play de funky music"},
			
			-- Cheats
			{"/itemlist",	"/itemlist > Create a list of items to copy ids from"},
			
			--Unshame
			{"/keyblock",	"/keyblock > toggles all keyboard shortcuts on or off"},
			
		},
		f_items = {},
		size = {365, 230},
		position = {365, 230},
		selection = 0,
	},
	
}
local me = SystemChat

local get = Application.user_setting
local set = Application.set_user_setting
local save = Application.save_user_settings

-- Round function
function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

-- ####################################################################################################################
-- ##### Init #########################################################################################################
-- ####################################################################################################################
SystemChat.init = function()

	-- Options
	me.create_options()

	-- Init hooks
	Mods.hook.enable(get(me.SETTINGS.REDIRECT_ECHOCONSOLE.save), mod_name, "EchoConsole")
	Mods.hook.enable(get(me.SETTINGS.DISABLE_POSITIVE_REINFORCEMENTS.save), mod_name, "PositiveReinforcementUI.add_event")

	-- Init input system
	me.input.init()

	-- Stuff
	me.draw_christmas_tree()
	--run_examples()
	
end

-- ####################################################################################################################
-- ##### Intellisense #################################################################################################
-- ####################################################################################################################
SystemChat.sense.register = function(name, description)
	table.insert(me.sense.items, {name, description})
end

--[[
	Update sense
--]]
SystemChat.sense.update = function()
	if me.chat.has_focus then
		me.sense.text = me.chat.text
		me.sense.filter()
		-- Draw window
		me.sense.draw_window()
	end
end
--[[
	Filter command list for current text
--]]
SystemChat.sense.filter = function()
	if me.sense.text then 
		me.sense.f_items = {}
		
		if type(me.sense.text) == "string" and me.sense.text ~= "" then
			for _, item in pairs(me.sense.items) do
				-- pcall prevents charter "[" to crash the game
				pcall(function()
					local found = string.find(item[1], me.sense.text)
					if found then
						me.sense.f_items[#me.sense.f_items+1] = item
					end
				end)
			end
		end
		
		-- Catch empty list
		if me.sense.f_items == {} then
			me.sense.f_items = me.sense.items
		end
	else
		me.sense.f_items = me.sense.items
	end
end
--[[
	Draw the window frame
--]]
SystemChat.sense.draw_window = function()
	if #me.sense.f_items > 0 then
		local scale = UIResolutionScale()
		local font, font_, font_size = me.fonts(get(me.SETTINGS.FONT_SIZE.save) or me.DEFAULT_FONT_SIZE)
		local alpha = (255 * (get(me.SETTINGS.TRANSPARENCY_WINDOW.save)/100)) * (get(me.SETTINGS.TRANSPARENCY.save)/100)
		local color = Color(alpha * me.window.alpha, 10, 10, 10)
		
		local items_n = #me.sense.f_items
		if items_n > me.sense.max_lines then items_n = me.sense.max_lines end
		
		local size = {550, 10}
		size[1] = size[1] * scale
		size[2] = size[2] * scale
		size[2] = size[2] + (items_n * font_size)
		me.sense.size = size
		
		local pos = {5, 200, 1}
		pos[1] = pos[1] * scale
		pos[2] = pos[2] * scale
		pos[3] = pos[3] * scale
		me.sense.position = pos
		
		--me.draw_screen_rect(pos[1], pos[2] - size[2], pos[3], size[1], size[2], color)
		Mods.gui.rect(pos[1], pos[2] - size[2], pos[3], size[1], size[2], color)
		
		me.sense.draw_lines()
	end
end
--[[
	Draw the item lines
--]]
SystemChat.sense.draw_lines = function()
	local font, font_, font_size = me.fonts(get(me.SETTINGS.FONT_SIZE.save) or me.DEFAULT_FONT_SIZE)
	local alpha = (255 * (get(me.SETTINGS.TRANSPARENCY.save)/100))
	local color = Color(alpha * me.window.alpha, 255, 255, 255)
	local alpha_select = (255 * (get(me.SETTINGS.TRANSPARENCY_WINDOW.save)/100)) * (get(me.SETTINGS.TRANSPARENCY.save)/100)
	local color_select = Color(alpha_select * me.window.alpha, 10, 10, 10)
	local count = 0
	local y = me.sense.position[2] - font_size - 2
	local x = 8
	local z = 1
	
	for _, item in pairs(me.sense.f_items) do
		count = count + 1
		
		if count == me.sense.selection then
			local size = {me.sense.size[1] - 5, font_size}
			local pos = table.clone(me.sense.position)
			pos[1] = pos[1] + 2
			pos[2] = pos[2] - 5 - (count * font_size)
			--me.draw_screen_rect(pos[1], pos[2], pos[3], size[1], size[2], color_select)
			Mods.gui.rect(pos[1], pos[2], pos[3], size[1], size[2], color_select)
		end
		
		-- Draw text
		--me.draw_screen_text(x, y, z, item[2], font_size, color, font)
		Mods.gui.text(item[2], x, y, z, font_size, color, font)
		
		y = y - font_size
		if count >= me.sense.max_lines then break end
	end
	
	if count > 0 then
		me.sense.selection = 1
	else
		me.sense.selection = 0
	end
	
end

-- ####################################################################################################################
-- ##### Options ######################################################################################################
-- ####################################################################################################################
SystemChat.create_options = function()
	--Mods.option_menu:add_group("system_chat", "System Chat")
	
	Mods.option_menu:add_item("system_chat", me.SETTINGS.ACTIVE, true)
	--Mods.option_menu:add_item("system_chat", me.SETTINGS.POSITION)
	Mods.option_menu:add_item("system_chat", me.SETTINGS.FONT_SIZE)
	Mods.option_menu:add_item("system_chat", me.SETTINGS.ALWAYS_VISIBLE)
	Mods.option_menu:add_item("system_chat", me.SETTINGS.ALWAYS_SCROLLING)
	Mods.option_menu:add_item("system_chat", me.SETTINGS.REDIRECT_ECHOCONSOLE)
	Mods.option_menu:add_item("system_chat", me.SETTINGS.DISABLE_POSITIVE_REINFORCEMENTS)
	Mods.option_menu:add_item("system_chat", me.SETTINGS.FIT)
	Mods.option_menu:add_item("system_chat", me.SETTINGS.SIZE)
	Mods.option_menu:add_item("system_chat", me.SETTINGS.ADVANCED)
	Mods.option_menu:add_item("system_chat", me.SETTINGS.TRANSPARENCY)
	Mods.option_menu:add_item("system_chat", me.SETTINGS.TRANSPARENCY_WINDOW)
	Mods.option_menu:add_item("system_chat", me.SETTINGS.FADE_DELAY)
	Mods.option_menu:add_item("system_chat", me.SETTINGS.FADE_SPEED)
	Mods.option_menu:add_item("system_chat", me.SETTINGS.REMOVE)
	Mods.option_menu:add_item("system_chat", me.SETTINGS.REMOVE_LINES)
	Mods.option_menu:add_item("system_chat", me.SETTINGS.REMOVE_TIMER)
	
	--[[
	-- Custom check - !need to be checked for future uses!
	local value = get(me.SETTINGS.FONT_SIZE.save)
	if type(value) ~= "number" or value < min_ then
		local resx, resy = Application.resolution()
		value = resy / 50
		
		if value < min_ or value > max_ then value = default end
		set(me.SETTINGS.FONT_SIZE.save, value)
		save()
	end
	]]--
end

-- ####################################################################################################################
-- ##### Standard Output ##############################################################################################
-- ####################################################################################################################
--[[
	EchoConsole for standard output
--]]
Mods.hook.set(mod_name, "EchoConsole", function(func, message)
	local _, err = pcall(SystemChat.add_line, message)
	
	if err then
		func(message)
	end
end)
--[[
	Check and change positive reinforcements
--]]
SystemChat.set_redirect_echo_console = function()
	local redirect_echo_console = get(me.SETTINGS.REDIRECT_ECHOCONSOLE.save)
	if type(redirect_echo_console) == "boolean" then
		if me.REDIRECT_ECHOCONSOLE ~= redirect_echo_console then
			Mods.hook.enable(redirect_echo_console, mod_name, "EchoConsole")
			me.REDIRECT_ECHOCONSOLE = redirect_echo_console
		end
	end
end

-- ####################################################################################################################
-- ##### Positive Reinforcements ######################################################################################
-- ####################################################################################################################
--[[
	Hook to cancel positive reinforcements
--]]
Mods.hook.set(mod_name, "PositiveReinforcementUI.add_event", 
function(func, self, hash, local_player, color_from, event_type, ...)
	return
end)
--[[
	Check and change positive reinforcements
--]]
SystemChat.set_positive_reinforcements = function()
	local disable_positive_reinforcements = get(me.SETTINGS.DISABLE_POSITIVE_REINFORCEMENTS.save)
	if type(disable_positive_reinforcements) == "boolean" then
		if me.DISABLE_POSITIVE_REINFORCEMENTS ~= disable_positive_reinforcements then
			Mods.hook.enable(disable_positive_reinforcements, mod_name, "PositiveReinforcementUI.add_event")
			me.DISABLE_POSITIVE_REINFORCEMENTS = disable_positive_reinforcements
		end
	end
end

-- ####################################################################################################################
-- ##### Lines ########################################################################################################
-- ####################################################################################################################
SystemChat.clear_lines = function()
	me.text.lines = {}
	me.active = nil
end
--[[
	Add multiple lines
--]]
SystemChat.add_lines = function(lines_array)
	if type(lines_array) == "table" then
		for _, line in pairs(lines_array) do
			me.add_line(line)
		end
	end
end
--[[
	Add line
--]]
SystemChat.add_line = function(line)
	if get(me.SETTINGS.ACTIVE.save) then
		if #me.text.lines > me.text.max_lines then
			table.remove(me.text.lines, #me.text.lines)
		end
		table.insert(me.text.lines, 1, line)
		me.activate_window()
		me.scroll_down()
	else
		if type(line) == "string" then
			EchoConsole(line)
		end
	end
end
--[[
	Get font data
--]]
SystemChat.fonts = function(size)
	-- Return font_group, font_path, font_size
	if size == nil then size = 20 end
	if size > 32 then
		return "hell_shark_arial", "materials/fonts/gw_body_64", size
	else
		return "hell_shark", "materials/fonts/gw_body_32", size
	end
end
--[[
	Draw lines
--]]
SystemChat.draw_lines = function()
	local scale = UIResolutionScale()
	local font, font_, font_size = me.fonts(get(me.SETTINGS.FONT_SIZE.save) or me.DEFAULT_FONT_SIZE)
	local alpha = (255 * (get(me.SETTINGS.TRANSPARENCY.save)/100))
	local color_default = Color(alpha * me.window.alpha, 255, 255, 255)
	local pos = table.clone(me.window.position)
	local y = pos[2] + 5
	local z = pos[3] + 1
	local position = me.text.position
	local line_n = 1
	for _, line in pairs(me.text.lines) do
		if line_n >= position then
			local x = pos[1] + 5
			local text = ""
			if y + font_size*0.5 - pos[2] > me.window.size[2] then return end
			if type(line) == "table" then
				if type(line[2]) == "string" then
					local alpha = (line[1][1] * (get(me.SETTINGS.TRANSPARENCY.save)/100))
					local color = Color(alpha * me.window.alpha, line[1][2], line[1][3], line[1][4])
					text = line[2]
					--me.draw_screen_text(x, y, z, text, font_size, color, font)
					Mods.gui.text(text, x, y, z, font_size, color, font)
				else
					for _, seg in pairs(line) do
						local alpha = (seg[1][1] * (get(me.SETTINGS.TRANSPARENCY.save)/100))
						local color = Color(alpha * me.window.alpha, seg[1][2], seg[1][3], seg[1][4])
						text = seg[2]
						--me.draw_screen_text(x, y, z, text, font_size, color, font)
						Mods.gui.text(text, x, y, z, font_size, color, font)
						-- local text_extent_min, text_extent_max = Gui.text_extents(me.gui, text, font_, font_size)
						-- local text_width = text_extent_max[1] - text_extent_min[1]
						local text_width = Mods.gui.text_width(text, font_, font_size)
						x = x + text_width
					end
				end			
			elseif type(line) == "string" then
				text = line
				--me.draw_screen_text(x, y, z, text, font_size, color_default, font)
				Mods.gui.text(text, x, y, z, font_size, color_default, font)
			end
			-- local text_extent_min, text_extent_max = Gui.text_extents(me.gui, text, font_, font_size)
			-- local text_height = text_extent_max[2] - text_extent_min[2]
			y = y + font_size
		end
		line_n = line_n + 1
		--if y - pos[2] > me.window.size[2] then return end
	end
end
--[[
	Draw text
--]]
-- SystemChat.draw_screen_text = function(x, y, z, text, size, color, font)
	-- local font_type = font or "hell_shark"
	-- local font_by_resolution = UIFontByResolution({
		-- dynamic_font = true,
		-- font_type = font_type,
		-- font_size = size
	-- })
	-- local font, result_size, material = unpack(font_by_resolution)
	-- Gui.text(me.gui, text, font, size, material, Vector3(x, y, z), color or Color(255, 255, 255, 255))
-- end
--[[
	Draw rect to screen
--]]
-- SystemChat.draw_screen_rect = function(x, y, z, w, h, color)
	-- Gui.rect(me.gui, Vector3(x, y, z or 1), Vector2(w, h), color or Color(255, 255, 255, 255))
-- end
--[[
	Automatically remove lines after a certain time
--]]
SystemChat.auto_remove_line = function()
	if get(me.SETTINGS.REMOVE_LINES.save) then
		if #me.text.lines > 0 then
			local time_ = get(me.SETTINGS.REMOVE_TIMER.save) or 3
			if me.t - me.window.remove_timer > time_ then
				table.remove(me.text.lines, #me.text.lines)
				me.window.remove_timer = me.t
			end
		else
			me.window.remove_timer = me.t
		end
	end
end

-- ####################################################################################################################
-- ##### Scroll bar ###################################################################################################
-- ####################################################################################################################
--[[
	Draw the scrollbar
--]]
SystemChat.draw_scrollbar = function()
	if me.window.alpha > 0 then
		-- Bar
		local width = 2
		local alpha = (100 * (get(me.SETTINGS.TRANSPARENCY_WINDOW.save)/100))
		local position = {me.window.position[1] + me.window.size[1] - width - 1, me.window.position[2] + 1, 1}
		local size = {width, me.window.size[2] - 2}
		local color = Color(alpha * me.window.alpha, 127, 127, 127)
		
		local text_height = me.complete_lines_height()
		local window_height = me.window.size[2]
		local multiplier = window_height / text_height
		--EchoConsole(string.format("multiplier : '%f'", multiplier))
		
		local scroll_position = me.text.position - 1
		local lines_n = #me.text.lines
		local scroll_pos = window_height * (scroll_position / lines_n)
		
		if multiplier < 1 then
			--me.draw_screen_rect(position[1] + size[1], position[2], position[3], size[1], size[2], color)
			Mods.gui.rect(position[1] + size[1], position[2], position[3], size[1], size[2], color)
			local size = {width, size[2] * multiplier}
			local alpha = (200 * (get(me.SETTINGS.TRANSPARENCY_WINDOW.save)/100))
			local color = Color(alpha * me.window.alpha, 255, 255, 255)
			--me.draw_screen_rect(position[1] + size[1], position[2] + scroll_pos, position[3], size[1], size[2], color)
			Mods.gui.rect(position[1] + size[1], position[2] + scroll_pos, position[3], size[1], size[2], color)
		end
	end
end
--[[
	Calculate combined height of all lines
--]]
SystemChat.complete_lines_height = function()
	local font, font_, font_size = me.fonts(get(me.SETTINGS.FONT_SIZE.save) or me.DEFAULT_FONT_SIZE)
	local height = #me.text.lines * font_size
	return height
end
--[[
	Count of visible lines
--]]
SystemChat.visible_lines = function()
	local font, font_, font_size = me.fonts(get(me.SETTINGS.FONT_SIZE.save) or me.DEFAULT_FONT_SIZE)
	local lines_n = (me.window.size[2] - 10) / font_size
	if #me.text.lines < lines_n then lines_n = #me.text.lines end
	return lines_n
end
--[[
	Count of max visible lines
--]]
SystemChat.visible_lines_max = function()
	local font, font_, font_size = me.fonts(get(me.SETTINGS.FONT_SIZE.save) or me.DEFAULT_FONT_SIZE)
	return (me.window.size[2] - 10) / font_size
end
--[[
	Scroll controls
--]]
SystemChat.scroll = function()
	local lines_n = #me.text.lines
	
	if not SystemChat.chat.has_focus and (SystemChat.chat.visible or get(me.SETTINGS.ALWAYS_SCROLLING.save)) then
		-- Delay timer to slow down input
		if me.t - me.text.scroll_timer > me.text.scroll_delay then
			-- Check input
			if me.input.pressed("scroll_up") or me.input.pressed("scroll_up2") then
				me.text.position = me.text.position + 1
				me.text.scroll_block_timer = me.t
				me.activate_window()
			elseif me.input.pressed("scroll_down") or me.input.pressed("scroll_down2") then
				me.text.position = me.text.position - 1
				me.text.scroll_block_timer = me.t
				me.activate_window()
			end
			me.text.scroll_timer = me.t
		end
	end
	
	-- Check if position is bigger 0
	if me.text.position < 1 then
		me.text.position = 1
	end
	
	-- Check if max line is exceeded
	local max_line = lines_n - me.visible_lines_max()
	if me.text.position > max_line then
		me.text.position = max_line
	end
	
	-- Check if max visible lines is bigger lines
	if me.visible_lines_max() > lines_n then
		me.text.position = 1
	end
	
end
--[[
	Try to scroll down to new message
--]]
SystemChat.scroll_down = function()
	if me.t - me.text.scroll_block_timer > me.text.scroll_block_time then
		me.text.position = 1
	end
end

-- ####################################################################################################################
-- ##### Window #######################################################################################################
-- ####################################################################################################################
--[[
	Activate window
--]]
SystemChat.activate_window = function()
	if me.state == "invisible" then
		me.state = "fading_in"
		me.window.fade_timer = me.t
	elseif me.state == "visible" or me.state == "fading_out"  then
		me.window.fade_timer = me.t
	end
end
--[[
	Fade window
--]]
SystemChat.fade_window = function()
	local fade_speed = get(me.SETTINGS.FADE_SPEED.save)/2
	if not get(me.SETTINGS.ALWAYS_VISIBLE.save) then
		if me.state == "fading_in" then
			if me.t - me.window.fade_timer > fade_speed then
				me.window.fade_timer = me.t
				me.state = "visible"
				me.window.alpha = 1
			else
				local value = (me.t - me.window.fade_timer) / fade_speed
				me.window.alpha = value --math.lerp(me.alpha, 255, value)
			end
		elseif me.state == "visible" then
			if me.t - me.window.fade_timer > get(me.SETTINGS.FADE_DELAY.save) then
				me.window.fade_timer = me.t
				me.state = "fading_out"
			end
		elseif me.state == "fading_out" then
			if me.t - me.window.fade_timer > fade_speed then
				me.window.fade_timer = me.t
				me.state = "invisible"
				me.window.alpha = 0
			else
				local value = (me.t - me.window.fade_timer) / fade_speed
				me.window.alpha = 1-value --math.lerp(me.alpha, 0, value)
			end
		elseif me.state == "invisible" then
		elseif me.state == nil then
			me.window.fade_timer = me.t
			me.state = "visible"
		end
	else
		me.window.alpha = 1
	end
end
--[[
	Draw window
--]]
SystemChat.draw_window = function()
	if me.window.alpha > 0 and #me.text.lines > 0 then
		local resx, resy = Application.resolution()
		local scale = UIResolutionScale()
		local alpha = (255 * (get(me.SETTINGS.TRANSPARENCY_WINDOW.save)/100)) * (get(me.SETTINGS.TRANSPARENCY.save)/100)
		local color = Color(alpha * me.window.alpha, 10, 10, 10)
		local font, font_, font_size = me.fonts(get(me.SETTINGS.FONT_SIZE.save) or me.DEFAULT_FONT_SIZE)
		-- Chat size with scaling applied
		local chat_size = me.chat.size[2] * scale
		-- Size with scaling applied
		local size = {550, 150}
		size[1] = size[1] * scale
		size[2] = size[2] * scale
		
		-- Position with scaling applied
		local position = {5, 200, 1}
		position[1] = position[1] * scale
		position[2] = position[2] * scale
		position[3] = position[3] * scale

		-- Add chat size to position when chat open
		if me.chat.visible then
			position[2] = position[2] + chat_size
		end
		
		me.window.position = position

		if get(me.SETTINGS.FIT.save) and get(me.SETTINGS.FIT.save) == 2 then
			local height = resy - position[2] - 5
			size = {size[1], height}
		elseif get(me.SETTINGS.FIT.save) and get(me.SETTINGS.FIT.save) == 1 then
			local height = resy - position[2] - 5
			local lines_n = #me.text.lines
			local lines_height = (lines_n * font_size) + 5
			if lines_height < height then height = lines_height end
			size = {size[1], height}
		else
			local height = resy - position[2] - 5
			local multiplier = get(me.SETTINGS.SIZE.save) or 100
			local height_percentage = resy * (multiplier / 100) - position[2]
			if height_percentage < height then height = height_percentage end
			size = {size[1], height}
		end
		
		me.window.size = size
		
		if me.window.size[2] > 0 then
			-- Draw window rect
			--me.draw_screen_rect(position[1], position[2], position[3], me.window.size[1], me.window.size[2], color)
			Mods.gui.rect(position[1], position[2], position[3], me.window.size[1], me.window.size[2], color)
			
			me.draw_lines()
			me.draw_scrollbar()
		end
	end
	me.scroll()
end
--[[
	Activated
--]]
SystemChat.check_status = function()
	local value = get(me.SETTINGS.ACTIVE.save)
	
	if value ~= me.active then
		me.active = value
		if me.active then
			me.on_enter()
		else
			me.on_exit()
		end
	end
end
--[[
	Activated
--]]
SystemChat.on_enter = function()
	local white = Colors.get_table("white")
	local lime = Colors.get_table("lime")
	me.add_line({{white, "System Chat"}, {lime, "enabled"}, {white, "."}})
	me.text.position = 1
end
--[[
	Activated
--]]
SystemChat.on_exit = function()
	EchoConsole("System Chat disabled.")
end

-- ####################################################################################################################
-- ##### Hooks ########################################################################################################
-- ####################################################################################################################
--[[
	Hook MatchmakingManager.update to process functionality
--]]
Mods.hook.set(mod_name, "MatchmakingManager.update", function(func, self, dt, t)
	me.t = t
	
	-- Call orginal function
	func(self, dt, t)
	
	-- Check status
	me.check_status()
	
	-- Hook positive reinforcements
	me.set_positive_reinforcements()
	
	-- Hook echo console
	me.set_redirect_echo_console()
	
	-- Auto-remove lines
	me.auto_remove_line()
	
	-- Process
	local ingame_ui = Managers.matchmaking.ingame_ui
	if get(me.SETTINGS.ACTIVE.save) 
	and (not ingame_ui.menu_active or me.chat.has_focus) then
		me.fade_window()
		me.draw_window()
	end
end)
--[[
	Hook ChatGui.update to process functionality
--]]
Mods.hook.set(mod_name, "ChatGui.update", function(func, self, ...)
	func(self, ...)
	
	-- Values
	me.chat.visible = not self.chat_closed or self.menu_active
	me.chat.has_focus = self.chat_focused
	me.chat.text = self.chat_message
	
	-- Input
	if self.chat_focused then
		if me.sense.selection > 0 then
			if me.input.pressed("select_sense") then
				local item = me.sense.f_items[me.sense.selection]
				if item ~= nil then
					local value = item[1]
					if value ~= nil then
						self.chat_message = value
						self.chat_index = string.len(value) + 1
					end
				end
			end
		end
	end
	
	if get(me.SETTINGS.ACTIVE.save) then
		-- Sense
		me.sense.update()
		if not self.chat_closed then
			me.activate_window()
		end
	end
	
end)
--[[
	Reinitialize input after loading screen
--]]
Mods.hook.set(mod_name, "StateInGameRunning.event_game_started", function(func, ...)
	func(...)
	me.input.init()
end)
--[[
	Reinitialize input after loading screen
--]]
Mods.hook.set(mod_name, "StateInGameRunning.event_game_actually_starts", function(func, ...)
	func(...)
	me.input.init()
end)
--[[
	Unblock input
--]]
Mods.hook.set(mod_name, "ChatGui.block_input", function(func, ...)
	func(...)
	SystemChat.input.unblock()
end)
--[[
	Block input
--]]
Mods.hook.set(mod_name, "ChatGui.unblock_input", function(func, ...)
	func(...)
	SystemChat.input.unblock()
end)
--Mods.hook.enable(false, mod_name, "unblock_input")

-- ####################################################################################################################
-- ##### EXAMPLES #####################################################################################################
-- ####################################################################################################################
-- Mods.hook.set(mod_name, "SystemChat.on_enter", function(func)
local run_examples = function()
	local white = Colors.get_table("white")
	local lime = Colors.get_table("lime")
	
	-- String
	for i=1, 4 do
		me.add_line("System Chat enabled.")
	end
	
	-- Array of chunks
	me.add_line({{white, "System Chat"}, {lime, "enabled"}, {white, "."}})
	
	-- String with color in as a table
	local lines_ = {}
	for i=1, 8 do
		lines_[#lines_+1] = {lime, string.format("System %i Chat enabled.", i)}
	end
	me.add_lines(lines_)

end

-- ####################################################################################################################
-- ##### Input System #################################################################################################
-- ####################################################################################################################
--[[
	Keymap
--]]
SystemChatKeyMap = {
	win32 = {
		["scroll_up"] = {"keyboard", "up", "held"},
		["scroll_down"] = {"keyboard", "down", "held"},
		--["scroll_up2"] = {"keyboard", "page up", "held"},
		--["scroll_down2"] = {"keyboard", "page down", "held"},
		["select_sense"] = {"keyboard", "tab", "pressed"},
	},
}
-- Fix for gamepads
SystemChatKeyMap.xb1 = SystemChatKeyMap.win32
--[[
	Init input system
--]]
SystemChat.input = {
	
	init = function()
		Managers.input:create_input_service(mod_name, "SystemChatKeyMap")
		Managers.input:map_device_to_service(mod_name, "keyboard")
		Managers.input:map_device_to_service(mod_name, "mouse")
		Managers.input:map_device_to_service(mod_name, "gamepad")
	end,
	
	pressed = function(key)
		local input_service = Managers.input:get_service(mod_name)
		
		if input_service then
			return input_service.get(input_service, key)
		else
			return false
		end
	end,
	
	block = function()
		local input_service = Managers.input:get_service(mod_name)
		
		if input_service then
			for name, obj in pairs(Managers.input.input_devices) do
				obj.blocked_access[mod_name] = true
			end
			input_service:set_blocked(true)
		end
	end,
	
	unblock = function()
		local input_service = Managers.input:get_service(mod_name)
		
		if input_service then
			for name, obj in pairs(Managers.input.input_devices) do
				obj.blocked_access[mod_name] = nil
			end
			input_service:set_blocked(nil)
		end
	end,
}

-- ####################################################################################################################
-- ##### Special ######################################################################################################
-- ####################################################################################################################
--[[
	Compose line table from string
--]]
SystemChat.draw_christmas_tree = function()
	if os.date("%m") == "12" then
		local white = Colors.get_table("white")
		local lime = Colors.get_table("lime")
		local brown = Colors.get_table("brown")
		local yellow = Colors.get_table("yellow")
		local blue = Colors.get_table("blue")
		local red = Colors.get_table("red")
		
		me.add_line({{yellow, "                        *"}})
		me.add_line({{lime, "                        ^"}})
		me.add_line({{lime, "                      <>"}})
		me.add_line({{lime, "                    <"}, {blue, "O"}, {lime, ">"}})
		me.add_line({{lime, "                  < "}, {red, "O"}, {lime, ">>"}})
		me.add_line({{lime, "                <<< "}, {yellow, "O"}, {lime, ">"}})
		me.add_line({{lime, "              < "}, {blue, "O"}, {lime, "<>>>"}})
		me.add_line({{lime, "            <<< "}, {red, "O"}, {lime, ">>>"}})
		me.add_line({{lime, "          <<<<>> "}, {blue, "O"}, {lime, ">"}})
		me.add_line({{lime, "        <  "}, {blue, "O"}, {lime, "<< "}, {yellow, "O"}, {lime, ">>>>"}})
		me.add_line({{lime, "      <<<<<>> "}, {red, "O"}, {lime, ">>"}})
		me.add_line({{lime, "    <<<<< "}, {blue, "O"}, {lime, ">>> "}, {yellow, "O"}, {lime, ">"}})
		me.add_line({{lime, "  <<"}, {yellow, "O"}, {lime, "<<<>>>> "}, {red, "O"}, {lime, ">"}})
		me.add_line({{lime, "<"}, {red, "O"}, {lime, "<<<<< "}, {red, "O"}, {lime, ">>>>>"}})
		me.add_line({{brown, "                      |  |"}})
		me.add_line({{white, "           Merry Christmas"}})
		me.add_line({{white, "                    and"}})
		me.add_line({{white, "          a Happy new year"}})
		me.add_line({{white, ""}})
	end
end
--[[
	Compose line table from string
--]]
SystemChat.compose_line = function(str)
	--EchoConsole(str)
	--for i=1, string.len(str) do
	-- Check for upcoming color statement
	local pos1, end1 = string.find(str, "%[color")
	while pos1 ~= nil do
	--if pos1 ~= nil then
		--EchoConsole("Color pattern found")
		local pos2, end2 = string.find(str, "%[/color%]", end1)
		if pos2 ~= nil then
			--EchoConsole("Color pattern end found")
			local statement = string.sub(str, pos1, pos2+end2)
			local color = string.match(statement, "%[color%s-=%s-[%.*]%]")
			EchoConsole("statement = " .. statement)
			if color ~= nil then
				EchoConsole("color = " .. color)
			else
				--EchoConsole("color = nil")
			end
			
			-- local cpos, cend = string.find(statement, "=.*]")
			-- if cpos ~= nil then
				-- local color = string.sub(statement, cpos+1, cend-1)
				-- local tpos, tend = string.find(statement, "].*[")
				-- if tpos ~= nil then
					-- local text = string.sub(statement, cpos+1, cend-1)
					-- EchoConsole(string.format("color=%s; text=%s", color, text))
				-- end
			-- end
			
			--i = i + (i - end2)
			pos1, end1 = string.find(str, "%[color", end2)
		end
	end
	--end
end

-- ####################################################################################################################
-- ##### Start ########################################################################################################
-- ####################################################################################################################

me.init()