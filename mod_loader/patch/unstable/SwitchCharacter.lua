local mod_name = "Characters"
--[[
	Characters
		- Allows you to switch any charter you wish
--]]

local profile_synchronizer = Managers.state.network.profile_synchronizer

if not SwitchCharacter then
	SwitchCharacter = {}
	SwitchCharacter.is_updating = false
	SwitchCharacter.selected = 1
	SwitchCharacter.profile_index = 1
	SwitchCharacter.sp_backup = table.clone(SPProfiles)
else
	SwitchCharacter.profile_index = 1
end

--[[
	Looks for a empty profile spot and return the Unit
--]]
local find_empty_unit = function(self)
	-- Find empty profile spot
	local profile_index = 1
	for j = 1, #SPProfiles, 1 do
		if profile_synchronizer:owner(j) == nil then
			profile_index = j
			EchoConsole("Profile " .. tostring(j) .. " avaidable")
		end
	end
	
	-- get unit of profile_index
	local profile_unit = nil
	for unit, obj in pairs(self.units) do
		if obj == profile_index then
			return unit
		end
	end
	
	return nil
end

--[[
	Restore SPProfiles
--]]
local spprofiles_restore = function()
	SPProfiles = table.clone(SwitchCharacter.sp_backup)
end

local spprofiles_patch = function(index)
	SwitchCharacter.profile_index = index
	
	for i, profile in ipairs(SPProfiles) do
		SPProfiles[i] = table.clone(SwitchCharacter.sp_backup[index])
	end
end

--[[
	Patch FindProfileIndex:
		Because we patched the SPProfiles this function try to convert a name to
		a index. Because we changed all SPProfiles the filter can't find the name.
		This will result to a nil value.
--]]
Mods.hook.set(mod_name, "FindProfileIndex", function(func, profile_name)
	local value = func(profile_name)
	
	if value == nil then
		return SwitchCharacter.profile_index
	else
		return value
	end
end)

--- Make all charters selectable
Mods.hook.set(mod_name, "ProfileSynchronizer.owner", function (func, ...)
	if SwitchCharacter.is_updating then
		return nil
	end
	
	return func(...)
end)

Mods.hook.set(mod_name, "ProfileWorldView.update", function (func, ...)
	SwitchCharacter.is_updating = true
	
	func(...)
	
	SwitchCharacter.is_updating = false
end)

Mods.hook.set(mod_name, "ProfileWorldView.select_unit", function (func, self, unit)
	SwitchCharacter.selected = self.units[unit]
	
	local empty_unit = find_empty_unit(self)
	
	func(self, empty_unit)
end)

-- Patch Spawn/Despawn
Mods.hook.set(mod_name, "BulldozerPlayer.despawn", function(func, ...)	
	func(...)
	
	spprofiles_patch(SwitchCharacter.selected)
end)

Mods.hook.set(mod_name, "BulldozerPlayer.spawn", function(func, ...)
	return func(...)
end)

-- Patch Profile View
Mods.hook.set(mod_name, "ProfileView.on_enter", function(func, ...)
	spprofiles_restore()
	
	func(...)
end)

Mods.hook.set(mod_name, "ProfileView.on_exit", function(func, ...)
	func(...)
	
	spprofiles_patch(SwitchCharacter.profile_index)
end)


Mods.hook.set(mod_name, "MatchmakingStateJoinGame.rpc_matchmaking_request_profile_reply",
function (func, self, sender, client_cookie, host_cookie, profile, ...)
	spprofiles_restore()
	
	profile = FindProfileIndex(self.selected_hero_name)
	
	func(self, sender, client_cookie, host_cookie, profile, ...)
	
	spprofiles_patch(SwitchCharacter.profile_index)
end)