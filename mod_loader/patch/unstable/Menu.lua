script_data.always_load_all_views = true

IngameView.update_menu_options = function (self)
	self.pause_menu_full_access = true
	local full_access_layout = {
		{
			fade = false,
			transition = "exit_menu",
			display_name = "return_to_game_button_name"
		},
		{
			fade = true,
			transition = "profile_view",
			display_name = "profile_menu_button_name"
		},
		{
			fade = true,
			transition = "inventory_view",
			display_name = "inventory_menu_button_name"
		},
		{
			fade = true,
			transition = "forge_view",
			display_name = "interact_open_forge"
		},
		{
			fade = false,
			transition = "friends_view",
			display_name = "friends_menu_button_name"
		},
		{
			fade = true,
			transition = "lobby_browser_view",
			display_name = "lobby_browser_button_name "
		},
		{
			fade = true,
			transition = "options_menu",
			display_name = "options_menu_button_name"
		},
		{
			fade = false,
			transition = "leave_group",
			display_name = "leave_game_menu_button_name"
		},
		{
			fade = false,
			transition = "quit_game",
			display_name = "quit_menu_button_name"
		}
	}

	self.setup_button_layout(self, full_access_layout)
	
	return
end