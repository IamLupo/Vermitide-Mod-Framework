'use strict';

const 
	fs = require('fs'),
	path = require('path'),
	gulp = require('gulp'),
	zip = require('gulp-zip'),
	jsonfile = require('jsonfile'),
	file = require('gulp-file');

var modPaths = './mod_loader/mods';

//Returns a list of existing folders
function getFolders(dir) {
	return fs.readdirSync(dir)
		.filter(function(fileName) {
		return fs.statSync(path.join(dir, fileName)).isDirectory();
		});
}

//Reads config.json file in folder and zips mod .mod file
function packMod(folder, k, all, forModdb){

	let configFile = path.join(modPaths, folder,'config.json');

	jsonfile.readFile(
		configFile,
		(err, obj) => {
			zipUp(err, obj, folder, forModdb);
		}
	);
}

//Does the actual zipping
function zipUp(err, config, folder, forModdb){

	if(err){
		console.error('Can\'t parse config.json file for mod ' + folder);
		return;
	}

	if(!config.mod_name){
		console.error('No mod_name in config.json file for mod ' + folder);
		return;
	}

	let fileName = config.mod_name + '-' + (config.version || '1.0.0'),

		//Change log file
		changelogPath = path.join(modPaths, folder, '/change.log'),
		changelogText = fs.existsSync(changelogPath) ? fs.readFileSync(changelogPath) : null,

		//Readme file
		readmePath = path.join(modPaths, folder, '/readme.txt'),
		readmeText = fs.existsSync(readmePath) ? fs.readFileSync(readmePath) : null,
		readmeAuthor = config.author || null,
		readmeContent = 
			config.mod_name + ' v' + (config.version || '1.0.0') + '\n' +	//Mod name
			(readmeAuthor ? 'Author: ' + readmeAuthor + '\n' : '') + 		//Author
			readmeText + '\n' +												//Readme text
			(changelogText ? 'Changelog:\n' + changelogText + '\n' : ''),	//Change log text
		readme = file('readme.txt', readmeContent);

	console.log('Building ' + config.mod_name + ' from folder ' + folder);

	//Building .mod file
	if(!forModdb){
		let src = [
			path.join(modPaths, folder, '/**/*'),
			'!' + readmePath,
			'!' + changelogPath
		];
		gulp.src(src)
			.pipe(readme)
			.pipe(zip(fileName + '.mod'))
			.pipe(gulp.dest('mods'));
	}

	//Building .zip file with existing .mod file
	else{
		let src = [
			path.join('./mods/', fileName + '.mod'),
			'!' + readmePath,
			'!' + changelogPath
		];
		gulp.src(src)
			.pipe(readme)
			.pipe(zip(fileName + '.zip'))
			.pipe(gulp.dest('moddb'));
	}

}

//Copies files to dist/Vermitide-Mod-Framework-version/ folder
function copyFiles(err, config){

	if(err){
		console.error('Can\'t parse package.json');
		return;
	}

	gulp.src([
			'!./mod_loader/mods/**/*',
			'!./mod_loader/loadfile/dev/**/*',
			'!./mod_loader/patch/dev/**/*',
			'!./mod_loader/DamageFilter.lua',
			'!./mod_loader/Ban_list.lua',

			'./doc/**/*',
			'./mod_loader/**/*',
			'./mods/*.mod',

			'./mod',
			'./dinput8.dll',
			'./vermintideconsole.dll',
			'./readme.txt',
			'./change.log'
		], {base: './'})
		.pipe(gulp.dest('./dist/Vermitide-Mod-Framework-' + config.version));
}


/* Gulp tasks */

//Builds all mods
gulp.task('build', () => {
	let args = process.argv;
	let i = args.indexOf("-f");	
	if(~i){
		let folders = args[i + 1] && args[i + 1].split('.');
		if(folders && folders.length){
			folders.map(packMod);
		}
		else{
			console.log("No folders specified");
		}
	}
	else{
 		let folders = getFolders(modPaths);
 		folders.map(packMod);
 	}
})

//Builds all mods and then zip them up for moddb
gulp.task('moddb', () => {
	let folders = getFolders(modPaths);
	folders.map((folder, k, all) => {
		packMod(folder, k, all, true)
	});
})


//Watches changes and builds all mods
gulp.task('watch', () => {
	gulp.watch(path.join(modPaths, '/**/*'), ['build']);
})

//Reads package.json file and copies files to dist/Vermitide-Mod-Framework-version/ folder
gulp.task('dist', () =>{
	let packagePath = './package.json';
	jsonfile.readFile(
		packagePath,
		copyFiles
	);
})

//Builds all mods then starts watching changes
gulp.task('autobuild', ['build', 'watch']);
